#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "state_publisher");
	ros::NodeHandle n;
	ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
	tf::TransformBroadcaster broadcaster;
	ros::Rate loop_rate(30);
	const double degree = M_PI/180;
	// robot state
	double inc= 0.005;

	double angle= 0;

	double arm1 = 0;
	double arm2 = 0;
	double arm3 = 0;
	double arm4 = 0;
	double arm5 = 0;
	double arm6 = 0;
	double hand_knuckle = 0;
	double finger12 = 0;
	double finger13 = 0;
	double finger21 = 0;
	double finger22 = 0;
	double finger23 = 0;
	double thumb2 = 0;
	double thumb3 = 0;

	// message declarations
	geometry_msgs::TransformStamped odom_trans;
	sensor_msgs::JointState joint_state;
	odom_trans.header.frame_id = "odom";
	odom_trans.child_frame_id = "base_link";

	while (ros::ok()) 
	{
		arm1 += inc;
		arm2 += inc;
		arm3 += inc;
		arm4 += inc;
		arm5 += inc;
		arm6 += inc;

		//update joint_state
		joint_state.header.stamp = ros::Time::now();
		joint_state.name.resize(14);
		joint_state.position.resize(14);
		joint_state.name[0] ="arm_1_joint";
		joint_state.position[0] = arm1;
		joint_state.name[1] ="arm_2_joint";
		joint_state.position[1] = arm2;
		joint_state.name[2] ="arm_3_joint";
		joint_state.position[2] = arm3;
		joint_state.name[3] ="arm_4_joint";
		joint_state.position[3] = arm4;
		joint_state.name[4] ="arm_5_joint";
		joint_state.position[4] = arm5;
		joint_state.name[5] ="arm_6_joint";
		joint_state.position[5] = arm6;
		joint_state.name[6] ="hand_knuckle_joint";
		joint_state.position[6] = hand_knuckle;
		joint_state.name[7] ="hand_finger_12_joint";
		joint_state.position[7] = finger12;
		joint_state.name[8] ="hand_finger_13_joint";
		joint_state.position[8] = finger13;
		joint_state.name[9] ="hand_finger_21_joint";
		joint_state.position[9] = finger21;
		joint_state.name[10] ="hand_finger_22_joint";
		joint_state.position[10] = finger22;
		joint_state.name[11] ="hand_finger_23_joint";
		joint_state.position[11] = finger23;
		joint_state.name[12] ="hand_thumb_2_joint";
		joint_state.position[12] = thumb2;
		joint_state.name[13] ="hand_thumb_3_joint";
		joint_state.position[13] = thumb3;

		// update transform
		// (moving in a circle with radius 1)
		odom_trans.header.stamp = ros::Time::now();
		odom_trans.transform.translation.x = cos(angle);
		odom_trans.transform.translation.y = sin(angle);
		odom_trans.transform.translation.z = 0.0;
		odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(angle);
		//send the joint state and transform
		joint_pub.publish(joint_state);
		broadcaster.sendTransform(odom_trans);

		angle += degree/4;

		// This will adjust as needed per iteration
		loop_rate.sleep();
	}
	return 0;
}


