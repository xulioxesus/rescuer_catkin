#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <cstdlib>
#include <signal.h>
#include <termios.h>
#include <stdio.h>

#define KEYCODE_R 0x43
#define KEYCODE_L 0x44
#define KEYCODE_U 0x41
#define KEYCODE_D 0x42
#define KEYCODE_Q 0x71
#define MAX_EFFORT 1
#define STEP 0.02

class rescuer_teleop_key
{
	public:

		double linearSpeed, angularSpeed, linearPercent, angularPercent;

		ros::Publisher publisher;
		ros::NodeHandle n;
		  
		rescuer_teleop_key();

};


//=============================================================================
// Constructor
//=============================================================================

rescuer_teleop_key::rescuer_teleop_key():
  linearSpeed(MAX_EFFORT),
  angularSpeed(MAX_EFFORT),
  linearPercent(STEP),
  angularPercent(STEP)
{
  publisher = n.advertise<geometry_msgs::Twist>("cmd_vel",1000);
}

int kfd = 0;
struct termios cooked, raw;

//=============================================================================
// Termina con el programa si se recibe la señal de interrupción CTRL+C 
//=============================================================================

void quit(int sig)
{
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}


//=============================================================================
//  Programa principal
//=============================================================================

int main(int argc, char** argv)
{
	ros::init(argc, argv, "rescuer_teleop_key");
	rescuer_teleop_key teleoperator;
	geometry_msgs::Twist msg;
	ros::Rate loop_rate(50);

	//---------------------------------------------------------------------
	// Console configuration
	//---------------------------------------------------------------------
	signal(SIGINT,quit);

	teleoperator.linearPercent = 0;
	teleoperator.angularPercent = 0;

	char c;
	bool dirty=false;


	// get the console in raw mode
	tcgetattr(kfd, &cooked);
	memcpy(&raw, &cooked, sizeof(struct termios));
	raw.c_lflag &=~ (ICANON | ECHO);

	// Setting a new line, then end of file
	raw.c_cc[VEOL] = 1;
	raw.c_cc[VEOF] = 2;
	tcsetattr(kfd, TCSANOW, &raw);

	puts("Reading from keyboard");
	puts("---------------------------");
	puts("Use arrow keys to move the rover rescuer.");

	//---------------------------------------------------------------------
	// Main Loop
	//---------------------------------------------------------------------

	while(ros::ok())
	{
		// get the next event from the keyboard
		if(read(kfd, &c, 1) < 0)
		{
			perror("read():");
			exit(-1);
		}

		ROS_INFO("value: 0x%02X\n", c);
	  
		switch(c)
		{
		  
			case 's':
				ROS_DEBUG("STOP");
				teleoperator.angularPercent =  0.0;
				teleoperator.linearPercent  =  0.0;
				dirty = true;
			break;

			case KEYCODE_L:
				ROS_DEBUG("LEFT");
				teleoperator.angularPercent = teleoperator.angularPercent - STEP;
				teleoperator.linearPercent = 0;
				dirty = true;
			break;

			case KEYCODE_R:
				ROS_DEBUG("RIGHT");
				teleoperator.angularPercent = teleoperator.angularPercent + STEP;
				teleoperator.linearPercent= 0;
				dirty = true;
			break;

			case KEYCODE_U:
				ROS_DEBUG("UP");
				teleoperator.linearPercent  = teleoperator.linearPercent + STEP;
				teleoperator.angularPercent= 0;
				dirty = true;
			break;

			case KEYCODE_D:
				ROS_DEBUG("DOWN");
				teleoperator.linearPercent  = teleoperator.linearPercent - STEP;
				teleoperator.angularPercent = 0;
				dirty = true;
			break;
		}
			
		
		if(dirty == true)
		{

			if(teleoperator.linearPercent > MAX_EFFORT)
				teleoperator.linearPercent= MAX_EFFORT;
			else if (teleoperator.linearPercent< -MAX_EFFORT)
				teleoperator.linearPercent= -MAX_EFFORT;

			if(teleoperator.angularPercent> MAX_EFFORT)
				teleoperator.angularPercent= MAX_EFFORT;
			else if (teleoperator.angularPercent< -MAX_EFFORT)
				teleoperator.angularPercent= -MAX_EFFORT;

		  	msg.linear.x = teleoperator.linearSpeed * teleoperator.linearPercent;
		  	msg.angular.z =  teleoperator.angularSpeed * teleoperator.angularPercent;

		  	dirty = false;

			teleoperator.publisher.publish(msg);
		}
		
		ROS_INFO("End Loop | linearSpeed: %f linearPercent: %f ",teleoperator.linearSpeed,teleoperator.linearPercent);
		ROS_INFO("End Loop | angularSpeed: %f angularPercent: %f\n",teleoperator.angularSpeed,teleoperator.angularPercent);

		ros::spinOnce();
		loop_rate.sleep();

	   }

	  return 0;
}
