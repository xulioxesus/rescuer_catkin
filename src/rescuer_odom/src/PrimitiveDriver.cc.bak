/** \file PrimitiveDriver.cpp
 * \author Robotnik Automation S.L.L.
 * \version 1.0
 * \date    
 * 
 * \brief PrimitiveDriver Component, based in OpenJAUS
 *  Uses dx60co8_kvaser driver     
 *	
 * Primitive Driver (ID 33)
 *
 * Function:
 * The Primitive Driver component performs basic driving and all platform related mobility
 * functions including operation of common platform devices such as the engine and lights.
 *
 * Description:
 * This component does not imply any particular platform type such as tracked or wheeled, but
 * describes mobility in six degrees of freedom using a percent of available effort in each
 * direction. Additionally, no power plant (gasoline, diesel, or battery) is implied and the
 * component functions strictly in an open loop manner, i.e., a velocity is not commanded since
 * that requires a speed sensor. Note that the specific actuator commands are not defined by
 * JAUS.
 *
 * (C) 2007 Robotnik Automation, SLL  
 *  All rights reserved.
*/
 
#include <sys/types.h>
#include <fcntl.h>
#include <pthread.h>			// Multi-threading functions (standard to unix)
#include <stdlib.h>				
#include <unistd.h>				// Unix standard functions
#include <string.h>
#include <time.h>		 
#include <math.h>
#include <time.h>
#include "PrimitiveDriver.h"	

/*! \fn void *AuxPDControlThread(void *threadParam)
 	* @param threadParam as void *, parameters of thread
	* Function executing in the thread
*/
void *AuxPDControlThread(void *threadParam){
	PrimitiveDriver *PrimitiveDriverThread = (PrimitiveDriver *)threadParam;
	PrimitiveDriverThread->ControlThread();
	return NULL;
}


/*! \fn PrimitiveDriver::PrimitiveDriver(void)
 	* Constructor by default
*/
PrimitiveDriver::PrimitiveDriver(void):Component(){
	state = SHUTDOWN_STATE;
	motor_left = NULL;
	motor_right = NULL;
	motor_max_speed = 0.0;
	motor_max_turnspeed = 0.0;
	inet_channel = NET_ID;		//ID by default
	pthread_mutex_init(& mutex_pd, NULL);//Initialization of the mutex
	ResetOdometry();
	bReady = true;
};


/*! \fn PrimitiveDriver::PrimitiveDriver(void)
 	* Constructor by default
*/
PrimitiveDriver::PrimitiveDriver(const char *can_port):Component(){
	state = SHUTDOWN_STATE;
	motor_left = NULL;
	motor_right = NULL;
	motor_max_speed = 0.0;
	motor_max_turnspeed = 0.0;
	pthread_mutex_init(& mutex_pd, NULL);//Initialization of the mutex
	ResetOdometry();
	bReady = true;
	
	if(!strcmp(can_port, "/dev/can0"))
		inet_channel = 0;
	else if(!strcmp(can_port, "/dev/can1"))
		inet_channel = 1;
	else
	{
		inet_channel = NET_ID;
		cError("PrimitiveDriver::PrimitiveDriver: Invalid net channel %s",can_port);
	}
};


/*! \fn PrimitiveDriver::4PrimitiveDriver(void)
 	* Destructor by default
*/
PrimitiveDriver::~PrimitiveDriver(void){
	delete motor_left;
	delete motor_right;
}


/*! \fn int PrimitiveDriver::GetState()
   	* function to get the state of the platform
    * @return PrimitiveDriver's state
*/ 
int PrimitiveDriver::GetState(){
	 return state;
}


/*!	\fn double PrimitiveDriver::GetUpdateRate()
	* function to get current update rate of the thread
	* @return pthread_hz
*/
double PrimitiveDriver::GetUpdateRate(){
	return pthread_hz;
}


/*! \fn int PrimitiveDriver::Move(double propulsiveLinearEffortXPercent, double propulsiveRotationalEffortZPercent)
   	* function to send velocity references to the motors of the platform
	* @param propulsiveLinearEffortXPercent a double
	* @param propulsiveRotationalEffortZPercent a double
	* @see Stop()
   	* @return OK if references can be sent
	* @return ERROR if references can't be sent 
*/
int PrimitiveDriver::Move(double propulsiveLinearEffortXPercent, double propulsiveRotationalEffortZPercent){
	double v_robot_mps, w_robot_rads;	// Linear and angular robot speed references
	double v_left_mps, v_right_mps;		// Linear speed ref of right and left tracks in m/s
	double v_left_rpm, v_right_rpm; 	// Linear speed ref of right and left tracks in rpm
	
	if(state==READY_STATE)
	{
		// Scale X, Z percent to m/s and rads/s
		v_robot_mps = (propulsiveLinearEffortXPercent / 100.0) * motor_max_speed;	// [m/s]
		//w_robot_rads = propulsiveRotationalEffortZPercent / 100.0 * ROVER_MAX_ROTATIONAL_VELOCITY_GPS * ROVER_PI/180.0; // [rad/s]
		w_robot_rads = (propulsiveRotationalEffortZPercent / 100.0) * motor_max_turnspeed * ROVER_PI/180.0; // [rad/s]
		//cDebug(DEBUG_LEVEL_PD, "PrimitiveDriver: Move: propulsiveLinearEffortXPercent= %f propulsiveRotationalEffortZPercent = %f", v_robot_mps, w_robot_rads);
	
		//Jacobian (Z+ points to the ground)
		v_left_mps = v_robot_mps + 0.5 * ROVER_D_TRACKS_M * w_robot_rads;
		v_right_mps = v_robot_mps - 0.5 * ROVER_D_TRACKS_M * w_robot_rads;
					
		//Conversion mm/s to rpm
		v_right_rpm = v_right_mps * ROVER_MPS2RPM;
		v_left_rpm = v_left_mps * ROVER_MPS2RPM;
				
		//Send Velocity Refs
		motor_left->SetMotorSpeed(v_left_rpm);
		motor_right->SetMotorSpeed(v_right_rpm);
		return OK;
	}
	
	return ERROR;
}


/*! \fn int PrimitiveDriver::Stop()
    * function to stop inmediately the movement of the platform
    * @return OK if stops successfully
	* @return ERROR if can't stop 
*/
int PrimitiveDriver::Stop (){
	
	//motor_left->SetMotorSpeed(0.0);
	//motor_right->SetMotorSpeed(0.0);
	if(dx60co8_SendQuickStopMsg(CAN_fd, LEFT_ID)==CAN_ERROR || dx60co8_SendQuickStopMsg(CAN_fd, RIGHT_ID)==CAN_ERROR)
	{
		cError("PrimitiveDriver::Stop: Error sending quickstop messages to the drivers");
		return ERROR;
	}
	
	return OK;
}


/*! \fn int PrimitiveDriver::UpdateOdometry()
	* Read current velocities from amplifiers 
	* and update global location solving kinematics 
	* Update odometry with last measurement
	* @return OK 
*/
int PrimitiveDriver::UpdateOdometry(){
	double fVelocityLeftRpm=0.0;
	double fVelocityRightRpm=0.0;
	double v_left_mps, v_right_mps;
	double fSamplePeriod = 1.0/PRIMITIVE_DRIVER_THREAD_DESIRED_HZ; // Default sample period
	
	fVelocityLeftRpm =  motor_left->GetMotorSpeed();		// left track motor velocity [rpm]
	fVelocityRightRpm =  motor_right->GetMotorSpeed();		// right track motor velocity [rpm]
	
	
	//cDebug(DEBUG_LEVEL_PD, "PrimitiveDriver: UpdateOdometry: v_left_rpm = %f v_right_rpm = %f", fVelocityLeftRpm, fVelocityRightRpm );
	
	// For accurate location estimation sample period must be measured
	// Timing can be done in 3 ways:
	//  	->Period hard rt (as in kernel 2.6.18)
	// 		-Period obtained by timestamping of CAN msg (dx60co8_ReadVelocityMsgWithTS())
	// 		-Period obtained by OS call	
 
	// Convert velocities from rpm to m/s
	v_left_mps = fVelocityLeftRpm * ROVER_RPM2MPS;
	v_right_mps = fVelocityRightRpm * ROVER_RPM2MPS;
	//cDebug(DEBUG_LEVEL_PD, "v_left_mps : %f  v_right_mps : %f ", v_left_mps, v_right_mps);
	
	// CRITICAL SECTION START
	// use of global mutex g_pmutex
	// Inverse Jacobian (Z+ points to the ground)
	
	pthread_mutex_lock( &mutex_pd );
	
		g_odom.linearSpeedMps = (v_right_mps + v_left_mps) / 2.0;			   			// m/s		
		g_odom.angularSpeedRads = (v_left_mps - v_right_mps) / (ROVER_D_TRACKS_M); 	// radians / s	
		// Integrate to obtain locations in global frame
		g_odom.yawRad += g_odom.angularSpeedRads * fSamplePeriod;					// radians
		// radnorm
		while (g_odom.yawRad>= (ROVER_PI)) g_odom.yawRad -= 2.0 * ROVER_PI;	
		while (g_odom.yawRad <= (-ROVER_PI)) g_odom.yawRad += 2.0 * ROVER_PI;
			
		g_odom.xM += g_odom.linearSpeedMps * cos(g_odom.yawRad) * fSamplePeriod;	// metres
		g_odom.yM += g_odom.linearSpeedMps * sin(g_odom.yawRad) * fSamplePeriod;	// metres	
		g_odom.zM = 0;  // ??
	
	pthread_mutex_unlock( &mutex_pd );


	return OK;
}


/*! \fn int PrimitiveDriver::GetDigitalInputs(byte *LeftInputs, byte *RightInputs)
	* Get the value of the digital inputs from the drive
	* @param LeftInputs as a byte * with the returned values of the digital inputs from left drive
	* @param RightInputs as a byte * with the returned values of the digital inputs from right drive
	* @return OK 
	* @return ERROR
*/	
int PrimitiveDriver::GetDigitalInputs(byte *LeftInputs, byte *RightInputs){
//	*LeftInputs = motor_left->GetDigitalInputs();
//	*RightInputs = motor_right->GetDigitalInputs();
	return OK;
}


/*! \fn int PrimitiveDriver::SetDigitalOutputs(byte LeftOutputs, byte RightOutputs)
	* set the value of the digital outputs from the drive
	* @param LeftOutputs as a byte with the values of digital outputs to left drive
	* @param RightOutputs as a byte with the values of digital outputs to right drive
	* @return OK 
	* @return ERROR
*/
int PrimitiveDriver::SetDigitalOutputs(byte LeftOutputs, byte RightOutputs){
	motor_left->SetDigitalOutputs(true, false, false, false, false, true, false, false);
	motor_right->SetDigitalOutputs(true, false, false, false, false, true, false, false);
	//motor_right->SetDigitalOutputs(RightOutputs);
	return OK;
}


/*! \fn double PrimitiveDriver::GetAngularSpeed()
	* @return Platforms Angular Speed
*/
double PrimitiveDriver::GetAngularSpeed(){
	return g_odom.angularSpeedRads;
}


/*! \fn double PrimitiveDriver::GetLinearSpeed()
	* @return Platforms Linear Speed
*/
double PrimitiveDriver::GetLinearSpeed(){
	return g_odom.linearSpeedMps;
}


/*! \fn int PrimitiveDriver::StartUp()
	* This function allows the abstracted component functionality contained in this file to be started from an external source.
	* It must be called first before the component state machine and mailman interaction will begin
	* Each call to "StartUp" should be followed by one call to the "ShutDown" function
	* @return OK
	* @return ERROR
*/
int PrimitiveDriver::StartUp(){
	pthread_attr_t attr;	// Thread attributed for the component threads spawned in this function
	int dRet;	
		
	if(state == SHUTDOWN_STATE)	// Execute the startup routines only if the component is not running
	{
		
		dRet = dx60co8_Connect(&CAN_fd, inet_channel,(int) BAUD_1M);	// Open bus	
		if (dRet!=OK) 
		{			
			cError("PrimitiveDriver: StartUp:  Error opening CAN port\n");
			SwitchToState(FAILURE_STATE);
			error= ERROR_CAN_CONNECTION;
			return ERROR; 
		}
		else
		{
			motor_left = new MotorDrive(CAN_fd, LEFT_ID, VELOCITY_MODE, false, false);
			//motor_right = new MotorDrive(CAN_fd, RIGHT_ID, VELOCITY_MODE, false, false);	
			motor_right = new MotorDrive(CAN_fd, RIGHT_ID, VELOCITY_MODE, true, true);	
			SwitchToState(INITIALIZE_STATE);//TEST
		}
			
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	
		run = true;
	
		// Robotnik Linux RT thread parameters
		par.prio = THREAD_PRIOR_PD;				// Priority level 0[min]-80[max]
		par.clock=CLOCK_REALTIME; 				// 0-CLOCK_MONOTONIC 1-CLOCK_REALTIME				
		
		if(pthread_create(&pthread_id, &attr, AuxPDControlThread, this) != 0)
		{
			cError("PrimitiveDriver: Startup: Could not create ControlThread\n");
			ShutDown();
			pthread_attr_destroy(&attr);
			return ERROR;
		}
		pthread_attr_destroy(&attr);	
	}
	else
	{
		cError("PrimitiveDriver: Attempted startup while not shutdown\n");
		return ERROR;
	}
	
	return OK;
}


/*! \fn int PrimitiveDriver::ShutDown()
	* This function allows the abstracted component functionality contained in this file to be stoped from an external source.
	* If the component is in the running state, this function will terminate all threads running 
	* @return OK
	* @return ERROR
*/
int PrimitiveDriver::ShutDown(){
	struct timespec timeout, next;
	
	if(state != SHUTDOWN_STATE)	// Execute the shutdown routines only if the component is running
	{
		cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: ShutDown: Shuting Down the component\n");
		SwitchToState(SHUTDOWN_STATE);
		
		clock_gettime(par.clock, &timeout);
		timeout.tv_sec += PRIMITIVE_DRIVER_THREAD_TIMEOUT_SEC;
		
		while(pthread_running)
		{	
			clock_gettime(par.clock, &next);
			if((next.tv_sec > timeout.tv_sec))
			{
				pthread_cancel(pthread_id);
				pthread_running = false;
				cError("PrimitiveDriver: ShutDown: Thread Shutdown Improperly\n");
				break;
			}else{
				next.tv_nsec+=1000000;
				clock_nanosleep(par.clock, TIMER_ABSTIME, &next, NULL);
			}
		}

	}else{
		cError("PrimitiveDriver: ShutDown: component is already in shutdown state\n");
		return ERROR;
	}

	return OK;
}


/*! \fn void PrimitiveDriver::InitState()
	* \brief Function called during initialization state 
	* Start both motors. If OK, go to STANBY State, else go to FAILURE State
*/
void PrimitiveDriver::InitState(){
	
	if(motor_right->Run()!= OK || motor_left->Run()!= OK){
		error= ERROR_INITILIZATION_FAULT;
		SwitchToState(FAILURE_STATE);
	}else
		SwitchToState(STANDBY_STATE);
	
}


/*! \fn void PrimitiveDriver::StartUpState()
	* \brief Function called during startup state 	
	* Creates both motor objects with its CAN IDs 
	* Configures the JAUS Services -> Goes to FAILURE if ERROR
	* Creates JAUS Service Connections used for the communication
	* Creates JAUS Messages 
	* Goes to INITIALIZE STATE
*/
void PrimitiveDriver::StartUpState(){
	
	cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::StartUpState: Starting Primitive Driver...\n");
		
		
	
	SwitchToState(INITIALIZE_STATE);	
}


/*! \fn void PrimitiveDriver::StandbyState()
	* \brief Function called during standby state.
	* The component stays in this state until the communication status of each drive becomes OPERATIONAL and
	* internal status will be in OPERATION_ENABLED.
	* If communication fails, go to Emergency State.
*/
void PrimitiveDriver::StandbyState(){
	bool Drive1 = false, Drive2 = false;
	static int iCycles=0,iCycles2=0;
	static int iPeriod = 2*PRIMITIVE_DRIVER_THREAD_DESIRED_HZ;
	
		
	//Control the communication status of both drives
	
	if((motor_left->communication_status == FAULT) || (motor_right->communication_status == FAULT))
	{//If the communication with one driver is failing, go to EmergencyState
		SwitchToState(EMERGENCY_STATE);
		error = ERROR_COMM_STATUS_FAULT;
	}else
	{
		//If communication status is not in OPERATIONAL state, reconfigure CAN messages for each drive
		// MOTOR LEFT
		if((motor_left->communication_status == OPERATIONAL))
		{
			//If drive status is in FAULT state, go to EmergencyState
			if(motor_left->status == FAULT)
			{
				SwitchToState(EMERGENCY_STATE);
				error=ERROR_MOTOR_STATUS_FAULT;
			}else
			{
				// 
				if(motor_left->status==OPERATION_ENABLED)
					Drive1=true;
				else
				{	
					if(iCycles==iPeriod) //Every "iPeriod" cycles tries to force switch on the drive
					{ 
						iCycles=0;
						switch(motor_left->status)
						{
							case SWITCH_ON_DISABLED:
								cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::StandbyState:Sending ShutDown to motor left");
								motor_left->ShutDown();
							break;
							case READY_TO_SWITCH_ON:
								cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::StandbyState:Sending SwitchOn to motor left");
								motor_left->SwitchOn();
								motor_left->EnableOperation();
							break;
						}
					}else
						iCycles++;
				}
			}				
		}
		
		// MOTOR RIGHT
		if((motor_right->communication_status == OPERATIONAL))
		{
			//If drive status is in FAULT state, go to EmergencyState
			if(motor_right->status == FAULT)
			{
				SwitchToState(EMERGENCY_STATE);
				error=ERROR_MOTOR_STATUS_FAULT;
			}else
			{ 
				if(motor_right->status == OPERATION_ENABLED)
				{
					Drive2=true;
				}
				else
				{	
					if(iCycles2==iPeriod) //Every "iPeriod" cycles tries to force switch on the drive
					{ 
						iCycles2=0;
						switch(motor_right->status)
						{
							case SWITCH_ON_DISABLED:
								cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::StandbyState:Sending ShutDown to motor right");
								motor_right->ShutDown();
							break;
							case READY_TO_SWITCH_ON:
								cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::StandbyState:Sending SwitchOn to motor right");
								motor_right->SwitchOn();
								motor_right->EnableOperation();
							break;
						}
					}else
						iCycles2++;
				}
			}			
		}
		
		//If Communication status is OPERATIONAL and status is in OPERATION_ENABLED for both motor drives ->
		// go to Ready state
		if(Drive1 && Drive2 && bReady){
			SwitchToState(READY_STATE);
		}
		
	}
}


/*! \fn void PrimitiveDriver::ReadyState()
	* \brief Function called during ready state 
	* Checks communication status-> Goes to EMERGENCY state if it is in FAULT 
	* Checks motors status -> Goes to STANDBY if it is not in OPERATION_ENABLED
	* If some component has the control:
	*	- Creates (or checks the status) a Service connection (status) with this component  
	*	- Creates (or checks the status) a Service connection (SetWrenchEffort) with this component
	*	- If both Services are OK, then move the motors.
*/
void PrimitiveDriver::ReadyState(){	
	//Control the communication status of both drives
	//If any drive communication is failing, go to EmergencyState
	if((motor_left->communication_status == FAULT) || (motor_right->communication_status == FAULT)){
		SwitchToState(EMERGENCY_STATE);
		error=ERROR_COMM_STATUS_FAULT;
		return;
	}
	//Check Drivers' status
	//If the status is not in OPERATION_ENABLED, go to STANBY
	if((motor_right->status != OPERATION_ENABLED) || motor_left->status!= OPERATION_ENABLED){
		SwitchToState(STANDBY_STATE);
		return;
	}
		
	if(!bReady)
	{
		SwitchToState(STANDBY_STATE);
		return;
	}	
		
	/*if(JausData.Component->controller.active) // PD Needs an active controller  
	{
		SwitchOn(BLUE_LIGHT);
		// SERVICE CONNECTION CONTROLLER STATUS
		if(!controllerStatusSc->isActive) 		//no service connection to know the status of the controller
		{	
			Stop();					
		} 
		else //exists status service connection 
		{ 	
			if(!setWrenchEffortSc->isActive)//no service connection to receive velocity references
			{ 				
				Stop();		
			}
			else //service connection for setwrencheffort is OK
			{ 
				if(JausData.Component->controller.state == READY_STATE ) //If the controller is in READY_STATE
				{
					//cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver:ReadyState: efforts linear:%f rotational:%f \n", 
					//	setWrenchEffortMsg->propulsiveLinearEffortXPercent, 
					//	setWrenchEffortMsg->propulsiveRotationalEffortZPercent);
					
					Move( setWrenchEffortMsg->propulsiveLinearEffortXPercent, setWrenchEffortMsg->propulsiveRotationalEffortZPercent); //Send the last ref received										
				}else	//controller in a wrong state
				{				
					Stop();						
				}														
			}
		}											
	}
	else //NO CONTROLLER
	{ 		
		Stop();
		SwitchOff(BLUE_LIGHT);		
	}*/

	UpdateOdometry();
}


/*! \fn void PrimitiveDriver::EmergencyState()
	* \brief Function called during emergency state 	
	* Depending of the error code, an handler will be executed for solving the problem
*/
void PrimitiveDriver::EmergencyState(){
	static int iLightFrec = 0;
	static bool bLight=false;
	static int iCycles=0;
	static int iRecoveryFrec = 2*PRIMITIVE_DRIVER_THREAD_DESIRED_HZ;
	static int iMaxAttempts = 5;
	static int iAttempts = 0;
	// ALARM LIGHTING
	if(iLightFrec==PRIMITIVE_DRIVER_THREAD_DESIRED_HZ)
	{
		if(bLight)
			SwitchOff(GREEN_LIGHT);
		else
			SwitchOn(GREEN_LIGHT);
		bLight = !bLight;
		iLightFrec=0;
	}else
		iLightFrec++;
	
	//HANDLING ERRORS
	switch(error)
	{
		case ERROR_COMM_STATUS_FAULT:
			//Trying to recover the communication
			if(iCycles==iRecoveryFrec)
			{
				if(motor_left->communication_status==FAULT)
					motor_left->ResetCANCommunication();
				if(motor_right->communication_status==FAULT)
					motor_right->ResetCANCommunication();
				iCycles=0;
				iAttempts++;
			}else
			{
				iCycles++;
			}
			if(motor_left->communication_status!=FAULT && motor_right->communication_status!=FAULT)
			{
				error= ERROR_NONE;
				iAttempts = 0;
			}
		break;
		
		case ERROR_MOTOR_STATUS_FAULT:
			if(iCycles==iRecoveryFrec)
			{
				//Trying to recover the state
				if(motor_left->status==FAULT)
					motor_left->InitializeDrive();
				if(motor_right->status==FAULT)
					motor_right->InitializeDrive();
				iCycles=0;
				iAttempts++;
			}else
			{
				iCycles++;
			}
			if(motor_left->status!=FAULT && motor_right->status!=FAULT)
			{
				error= ERROR_NONE;
				iAttempts=0;
			}
		break;
		
		case ERROR_NONE:
			//When the error is solved, goes to STANDBY state
			SwitchToState(STANDBY_STATE);
		break;
	}
	
	if(iAttempts>=iMaxAttempts)	//If the error can't be solved, goes to FAILURE state
	{
		SwitchToState(FAILURE_STATE);
		error = ERROR_CAN_FATAL;
	}
}

	
/*! \fn void PrimitiveDriver::FailureState
	* \brief Function called during failure state 	
*/
void PrimitiveDriver::FailureState(){
	static int iLightFrec = 0;
	static bool bLight=false;
	static int iCycles=0;
	static int iRecoveryFrec = 10*PRIMITIVE_DRIVER_THREAD_DESIRED_HZ;
	int dRet;
	struct timespec next;
		
	// ALARM LIGHTING
	if(iLightFrec==PRIMITIVE_DRIVER_THREAD_DESIRED_HZ)
	{
		if(bLight)
		{
			SwitchOff(BLUE_LIGHT);
			SwitchOn(GREEN_LIGHT);
		}else
		{
			SwitchOn(BLUE_LIGHT);
			SwitchOff(GREEN_LIGHT);
		}
		bLight = !bLight;
		iLightFrec=0;
	}else
		iLightFrec++;
	
	if(iCycles==iRecoveryFrec)
	{
		iCycles=0;
		switch(error)
		{
			case ERROR_INITILIZATION_FAULT:
				cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver:FailureState: Attempting of recovering ERROR INITIALIZATION...");
				error=ERROR_NONE;
				SwitchToState(INITIALIZE_STATE);
			break;
			
			case ERROR_CAN_FATAL:
				cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver:FailureState: Attempting of recovering ERROR CAN FATAL...");
				
				//Stop Drivers
				if(motor_left!=NULL){
					motor_left->Stop();
				}
				if(motor_right!=NULL){
					motor_right->Stop();	
				}
				clock_gettime(par.clock, &next);
				next.tv_nsec = 100000000;
				clock_nanosleep(par.clock, TIMER_ABSTIME, &next, NULL);
				//Close CAN 
				if(dx60co8_CloseCan(CAN_fd)!=OK){
					cError("PrimitiveDriver: FailureState: Error closing CAN connection\n");
				}
				//Open CAN
				dRet = dx60co8_Connect(&CAN_fd, NET_ID,(int) BAUD_1M);	// Open bus	
				if (dRet!=OK) {			
					cError("PrimitiveDriver: FailureState:  Error opening CAN port\n");
				}else
				{
					error=ERROR_NONE;
					SwitchToState(INITIALIZE_STATE);
				}
			break;
				
			case ERROR_CAN_CONNECTION:
				cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver:FailureState: Attempting of recovering ERROR CAN CONNECTION...");
				dRet = dx60co8_Connect(&CAN_fd, inet_channel,(int) BAUD_1M);	// Open bus	
				if (dRet!=OK) {			
					cError("PrimitiveDriver: FailureState:  Error opening CAN port\n");
				}else
				{
					SwitchToState(STARTUP_STATE);
					error=ERROR_NONE;
				}
			break;		
				
			case ERROR_NONE:
				
			break;
			
		}
	}else
		iCycles++;
}

	
/*! \fn void PrimitiveDriver::ShutdownState()
	* function called during shutdown state 
	* Reject the current component control
	* Close and Destroy all established Service Connections 
	* Destroy all reserved Jaus messages 
	* Close Jaus Communication Interface
	* Close CAN communication
*/
void PrimitiveDriver::ShutDownState(){
	struct timespec next;
	cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::ShutDownState");
		
	//Stop Drivers
	if(motor_left!=NULL){
		motor_left->Stop();
	}
	if(motor_right!=NULL){
		motor_right->Stop();	
	}
	clock_gettime(par.clock, &next);
	next.tv_nsec = 100000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &next, NULL);
	if(motor_left!=NULL){
		delete motor_left;
	}
	if(motor_right!=NULL){
		delete motor_right;
	}
	if(dx60co8_CloseCan(CAN_fd)!=OK){
		cError("PrimitiveDriver: ShutDownState: Error closing CAN connection\n");
	}

}
	

/*! \fn void PrimitiveDriver::AllState()
	* \brief Function called during all states 	
*/
void PrimitiveDriver::AllState(){
	int iControlTemp = PRIMITIVE_DRIVER_THREAD_DESIRED_HZ;
	static int iCycles=0;
	
	if(error != ERROR_CAN_CONNECTION){
		ReadCANMessages();
		//cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::AllState: %d msgs read",ReadCANMessages());
		
		//If communication status is not in FAULT at least in one motor drive, PrimitiveDriver will
		// continue synchronizing the communication
		if((motor_left->communication_status!=FAULT) || (motor_right->communication_status!=FAULT))
		{
			if(SynchronizeDrives()==ERROR)
			{
				cError("PrimitiveDriver: AllState: Error syncronizing the motor drives\n");
			}				
		}
	}
	
	//// Control for COOLING_SYSTEM
	if(iCycles==iControlTemp)
	{
		iCycles=0;
		if(motor_left->GetTemperature()> MAX_DRIVER_TEMP || motor_right->GetTemperature() > MAX_DRIVER_TEMP)
		{
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::AllState:: High Temp in Drivers-> Switching On Cooling system");
			SwitchOn(COOLING_SYSTEM);
			cooling = true;
		}
		else if(cooling==true && motor_left->GetTemperature() < MIN_DRIVER_TEMP && motor_right->GetTemperature() < MIN_DRIVER_TEMP)
		{
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::AllState:: Normal temperature -> Switching Off Cooling system");
			SwitchOff(COOLING_SYSTEM);
			cooling = false;
		}		
	}else
		iCycles++;
}


/*! \fn void * PrimitiveDriver::ControlThread(void *threadData)
	* All core component functionality is contained in this thread.
	* All of the PrimitiveDriver component state machine code can be found here.
*/
void PrimitiveDriver::ControlThread(){
	struct sched_param schedp;
	int policy = par.prio? SCHED_FIFO : SCHED_OTHER;
	struct timespec now, next, interval;
	long diff;
	unsigned long sampling_period_us;

	pthread_running = true;

	//StartUpState(); TEST
	
	// Robotnik thread priority and timing management
	memset(&schedp, 0, sizeof(schedp));
	schedp.sched_priority = par.prio;
	sched_setscheduler(0, policy, &schedp);
	clock_gettime(par.clock, &now);
	next = now;
	next.tv_sec++;  // start in next second? 
	sampling_period_us = (int)(1.0 / PRIMITIVE_DRIVER_THREAD_DESIRED_HZ * 1000000.0);		
	interval.tv_sec = sampling_period_us / USEC_PER_SEC;  // interval parameter in uS
	interval.tv_nsec = (sampling_period_us % USEC_PER_SEC)*1000;
	

	while(run) // Execute state machine code while not in the SHUTDOWN state
	{
					
		clock_nanosleep(par.clock, TIMER_ABSTIME, &next, NULL);
		clock_gettime(par.clock, &now);
		next.tv_sec += interval.tv_sec;
		next.tv_nsec += interval.tv_nsec;
		tsnorm(&next);			
		diff = calcdiff(next, now);
			
		// Thread frequency update
		pthread_hz = 1.0/(diff / 1000000.0); // Compute the update rate of this thread
		
		switch(state) // Switch component behavior based on which state the machine is in
		{
			case STARTUP_STATE://TEST
				StartUpState();
				break;
			
			case INITIALIZE_STATE:
				InitState();
				break;
				
			case READY_STATE:
				ReadyState();
				break;
			
			case STANDBY_STATE:				
			    StandbyState();
				break;
			
			case EMERGENCY_STATE:
				EmergencyState();
				break;
				
			case FAILURE_STATE:
				FailureState();
				break;		
						
			case SHUTDOWN_STATE:
				run = false;			
				break;		

			default:
				//cError("PrimitiveDriver: Default state\n");
				//pdSwitchTo(FAILURE_STATE); // The default case JAUS_is undefined, therefore go into Failure State
				break;
		}	
		
		AllState();	
	}	
	
	ShutDownState();
	pthread_running = false;
}


/**
	* \fn void PrimitiveDriver::SwitchToState(int new_state)
	* function that switches the state of the component into the desired state
	* @param new_state as an integer, the new state of the component
*/
void PrimitiveDriver::SwitchToState(int new_state){
	state = new_state;
	printf("PD: state: %d l: %d r: %d\n",state,motor_left->status,motor_right->status);

	
	switch (new_state) 
	{
		case STARTUP_STATE:
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to STARTUP STATE\n");
			break;
		case INITIALIZE_STATE:
			SwitchOff(GREEN_LIGHT);		//Switch off green light connected to the digital output 2
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to INITIALIZE STATE\n");
			break;
		case READY_STATE:
			SwitchOn(GREEN_LIGHT);		//Switch on green light connected to the digital output 2
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to READY STATE\n");
			break;
		case STANDBY_STATE:
			SwitchOff(GREEN_LIGHT);		//Switch off green light connected to the digital output 2
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to STANDBY STATE\n");
			break;
		case SHUTDOWN_STATE:
			SwitchOff(GREEN_LIGHT);
			SwitchOff(COOLING_SYSTEM);
			SwitchOff(BLUE_LIGHT);
			motor_right->SetDigitalOutputs(0);
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to SHUTDOWN STATE\n");
			break;
		case FAILURE_STATE:
			SwitchOff(GREEN_LIGHT);
			SwitchOff(BLUE_LIGHT);
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to FAILURE STATE\n");
			break;
		case EMERGENCY_STATE:
			SwitchOff(GREEN_LIGHT);
			SwitchOff(BLUE_LIGHT);
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to EMERGENCY STATE\n");
			break;
		default:
			cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver: switching to  UNDEFINED_STATE\n");
			break;		
    }
}


/**
	* \fn void PrimitiveDriver::SwitchOff(int digital_output)
	* function that switches off the selected digital output
	* @param digital_output as an integer, number of digital output
*/
void PrimitiveDriver::SwitchOff(int digital_output){
	Digital *outputs;
	//Digital Outputs are connected in the right driver
	if(digital_output> 0 && digital_output <= NUM_DIGITAL_OUTPUTS){
		outputs = motor_right->GetDigitalOutputs();
		outputs[digital_output-1].state = false;
		motor_right->SetDigitalOutputs(outputs[0].state,outputs[1].state,outputs[2].state,outputs[3].state,outputs[4].state,
		outputs[5].state,outputs[6].state,outputs[7].state);
	}else
		cError("PrimitiveDriver::SwitchOn: Error, digital output out of range");
}


/**
	* \fn void PrimitiveDriver::SwitchOn(int digital_output)
	* function that switches on the selected digital output
	* @param digital_output as an integer, number of digital output
*/
void PrimitiveDriver::SwitchOn(int digital_output){
	Digital *outputs;
	//Digital Outputs are connected in the right driver
	if(digital_output> 0 && digital_output <= NUM_DIGITAL_OUTPUTS){
		outputs = motor_right->GetDigitalOutputs();
		outputs[digital_output-1].state = true;
		motor_right->SetDigitalOutputs(outputs[0].state,outputs[1].state,outputs[2].state,outputs[3].state,outputs[4].state,
		outputs[5].state,outputs[6].state,outputs[7].state);
	}else
		cError("PrimitiveDriver::SwitchOn: Error, digital output out of range");
	
}


/*!	\fn int PrimitiveDriver::SynchronizeDrives()
	* @brief function that send SYNC signal to the drives
	* @return OK
	* @return ERROR
*/
int PrimitiveDriver::SynchronizeDrives(){
	static CMSG msg;
	int len = 1;
	dx60co8_CreateSYNCMsg(&msg);
	
	return dx60co8_SendMsg(CAN_fd, &msg, &len);
}


/*!	\fn double PrimitiveDriver::CalculateBatteryLevel(double analog_input)
	* Calculate the % of platform's battery.
	* @param analog_input as a double, value obtained from some analog_input
	* @return the value of the battery in Volts
*/
double PrimitiveDriver::CalculateBatteryLevel(double analog_input){	
	return (double)(analog_input*(double)BATTERY_RESISTIVE_DIVISOR);//*(100/BATTERY_VOLTAGE); -> Battery in %
}


/*!	\fn double PrimitiveDriver::GetBattery()
	* Returns current battery level
	* @return the value of the battery in %
*/
double PrimitiveDriver::GetBattery(){
	double analog_input[NUM_ANALOG_INPUTS];
	
	if(BATTERY_NODE==LEFT_ID){
		motor_left->GetAnalogInputs(&analog_input[0], &analog_input[1], &analog_input[2]);
		return CalculateBatteryLevel(analog_input[BATTERY_INPUT-1]);
		
	}else{
		motor_right->GetAnalogInputs(&analog_input[0], &analog_input[1], &analog_input[2]);
		return CalculateBatteryLevel(analog_input[BATTERY_INPUT-1]);
	}

}


/*! \fn int PrimitiveDriver::ReadCANMessages()
    * Read all CAN messages in the buffer and pass its to each MotorDrive
	* @return Number of read CAN Messages
*/
int PrimitiveDriver::ReadCANMessages(){
	CMSG cmsg_read;
	int len=1, cont = 0;
	
	while(dx60co8_ReadMsg(CAN_fd, &cmsg_read, &len)==OK){
		motor_left->ProcessCANMessage(cmsg_read);
		motor_right->ProcessCANMessage(cmsg_read);
		len = 1;
		cont++;
	}
	
	return cont;
}


/*!	\fn double PrimitiveDriver::GetRotationalEffortZPercent()
	* @return current rotational effort Z percent
*/
double PrimitiveDriver::GetRotationalEffortZPercent(){
	return 0.0;
}


/*! \fn double PrimitiveDriver::GetOdometry(OdomType *Od)
	* @return Returns the current Rescuer's odometry value
*/
void PrimitiveDriver::GetOdometry (OdomType *Od){
	pthread_mutex_lock( &mutex_pd );//MUTEX PROTECTION
	
	Od->xAbsM = g_odom.xAbsM;
	Od->yAbsM = g_odom.yAbsM;
	Od->xM = g_odom.xM;
	Od->yM = g_odom.yM;
	Od->zM = g_odom.zM;
	Od->yawRad = g_odom.yawRad;
	Od->angularSpeedRads = g_odom.angularSpeedRads;
	Od->linearSpeedMps = g_odom.linearSpeedMps;
	
	pthread_mutex_unlock( &mutex_pd );
}

	
/*! \fn void PrimitiveDriver::ResetOdometry()
	* Resets odometry's values of the robot
*/
void PrimitiveDriver::ResetOdometry(){
	pthread_mutex_lock( &mutex_pd );//MUTEX PROTECTION
	
	cDebug(DEBUG_LEVEL_PD,"PrimitiveDriver::ResetOdometry: ");
	printf("PrimitiveDriver::ResetOdometry:\n ");
	g_odom.xAbsM = 0;
	g_odom.yAbsM = 0;
	g_odom.xM = 0;
	g_odom.yM = 1.0;
	g_odom.zM = 0;
	g_odom.yawRad = 0;
	g_odom.angularSpeedRads = 0;
	g_odom.linearSpeedMps = 0;
	
	pthread_mutex_unlock( &mutex_pd );
}


/*! \fn int PrimitiveDriver::Start()
	* Sets the motor on operational state
*/
int PrimitiveDriver::Start(){
	
	return OK;
}


/*!	\fn void PrimitiveDriver::SetMaxSpeed(double MaxLinearSpeed, double MaxRotationalSpeed)
	* Set the maximun linear and rotational speed
*/
void PrimitiveDriver::SetMaxSpeed(double MaxLinearSpeed, double MaxRotationalSpeed){
	motor_max_speed = MaxLinearSpeed;
	motor_max_turnspeed = MaxRotationalSpeed;
}
