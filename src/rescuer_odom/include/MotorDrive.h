/*! \class MotorDrive
 *	\author Robotnik Automation S.L.L
 *	\version 1.0
 *  \brief Class for motor drive 
 *   
 *  This class contains all the data relative to the control drives used in the RESCUER platform
 *
 */
 
//#include <vector.h>
#include "dx60co8_esd.h"
//#include "dx60co8c.h"

#include "defines.h"

#ifndef __MOTOR_DRIVE_H
	#define __MOTOR_DRIVE_H
	
	#define ROVER_MAX_PHI_RATE_EFFORT_PER_SEC		60.0 
	#define ROVER_CURVATURE_PER_EFFORT				0.0016f 
	#define ROVER_MAX_PHI_EFFORT					100.0
	
	// global constants 
	#define ROVER_PI 								3.14159265358979323846
	#define ROVER_MAX_LINEAR_VELOCITY_MPS			0.75 	// [m/s]
	#define ROVER_MAX_ROTATIONAL_VELOCITY_GPS		110		// [grad/s]
	#define ROVER_D_TRACKS_M						0.750	// distance between track centers [m]
	#define ROVER_D_SPROCKET_M						0.150	// traction sprocket diameter
	#define ROVER_D_TRACK_HEIGHT_M					0.030	// height of track (could vary depending
															// on terrain and type of track).	
	#define ROVER_D_TRACTION_M 						0.180	//ROVER_D_SPROCKET_M + ROVER_D_TRACK_HEIGHT_M 	// Diameter for kinematic equations
	#define ROVER_GEARBOX 							1.0 / 10.0						// Mounted Gearbox 
	#define ROVER_RPM2MPS 							0.000942478 //((ROVER_GEARBOX * ROVER_PI * ROVER_D_TRACTION_M) / 60.0) // Conversion factor m/s to rpm
	#define ROVER_MPS2RPM 							1061.032953946 //(60.0 / (ROVER_GEARBOX * ROVER_PI * ROVER_D_TRACTION_M)) // Conversion factor rpm to mm/s
	
	#define MOTOR_DRIVE_THREAD_DESIRED_HZ			15
	#define MOTOR_DRIVE_THREAD_TIMEOUT_SEC			2
	#define MOTOR_DRIVE_NMT_TIMEOUT_SEC				2
	
	#define NUM_CAN_PERIODIC_MSGS					2
	#define CAN_PERIODIC_MSGS_HZ					2	//Frequency of CAN periodic messages
	
	#define MAX_DRIVER_TEMP							40.0
	#define MIN_DRIVER_TEMP							38.0

	/**
		* Function executing in the thread
	*/
	void *AuxMDControlThread(void *threadParam);
	
	class MotorDrive {
		friend void *AuxMDControlThread(void *);
	  // Attributes
	public:
		/** 
			* a public variable.
			* Contains the status of the drive (SWITCH_ON_DISABLE,READY_TO_SWITCH_ON,OPERATION_DISABLED, OPERATION_ENABLED,FAULT).
		*/
		int status;
	
		/** 
			* a public variable.
			* Contains the status of the communication with the drive (UNKNOWN, PRE_OPERATIONAL, OPERATIONAL, STOPPED, FAULT).
		*/
		int communication_status;
	
		/**
			* a public variable.
			* Enable/Disable read digital inputs from the drive
		*/
		bool read_digital_inputs_enable;
	
		/**
			* a public variable.
			* Enable/Disable read digital outputs of the drive
		*/
		bool read_digital_outputs_enable;
		
		/** 
			* a public variable.
			* Contains the values of the digital inputs
		*/
		Digital digital_inputs[NUM_DIGITAL_INPUTS];
		
		/** 
			* a public variable.
			* Contains the values of the digital inputs (byte format)
		*/
		byte b_digital_inputs;
		
		/** 
			* a public variable.
			* Contains the values of the digital outputs
		*/
		Digital digital_outputs[NUM_DIGITAL_OUTPUTS];
		
		/** 
			* a public variable.
			* Contains the values of the digital outputs (byte format)
		*/
		byte b_digital_outputs;
		
		/** 
			* a public variable.
			* Contains the values of the digital outputs (byte format)
		*/
		byte b_aux_digital_outputs;
	private:	
		/** 
			* a private variable.
			* Contains the status of the drive (SWITCH_ON_DISABLE,READY_TO_SWITCH_ON,OPERATION_DISABLED, OPERATION_ENABLED,FAULT) in Hexadecimal format.
			* For communication.
		*/
		byte b_status;
	
		/** 
			* a private variable.
			* Contains the id of the control thread
		*/
		pthread_t pthread_id;
		
		/** 
			* a private variable.
			* 
		*/
		bool pthread_running;
	
		/** 
			* a private variable.
			* Contains the real frequency of the thread
		*/
		double pthread_hz;
		/** 
			* a private variable.
			* Contains the id of the CAN node
		*/
		int CAN_node_id;
		
		/** 
			* a private variable.
			* Contains the CAN file descriptor
		*/
		int CAN_fd;
	
		/** 
			* a private variable.
			* Refers to how the drives internal control loops are configured.
			* Position Mode | Velocity Mode | Torque(current) Mode | Homing Mode | PVT Mode
			* Implemented for Position and Velocity Mode
		*/
		int mode_of_operation;
	
		/** 
			* a private variable.
			* Contains the current velocity (in rpm) of the drive
		*/
		double motor_speed;
	
		/** 
			* a private variable.
			* Contains the temperature of the drive
		*/
		double temperature;
	
		/** 
			* a private variable.
			* Contains the values of the analog inputs
		*/
		double analog_inputs[NUM_ANALOG_INPUTS];	
	
		/** 
			* a private variable.
			* Controls the execution of the Component's thread
		*/
		bool run;
		
		/** 
		   * a private struct.
		   * Contains the control thread parameters 
		*/
		struct thread_param par;
		
		/** 
		   * a private variable.
		   * Time of the last NodeGuard reply from the drive 
		*/
		struct timespec node_guard_reply;
			
		/**
			* a private variable.
			* Array of CAN messages that have to be sent periodically
		*/
		CMSG CAN_periodic_msgs[NUM_CAN_PERIODIC_MSGS];
	
		/**
			*
		*/
		bool calibrated;
		
		// Operations
	public:
		
		/** 
			* public constructor
		*/
		MotorDrive(int can_fd, int can_node_id, int mode_of_operation, bool read_digital_inputs, bool read_digital_outputs);
	
		/** 
			* function to get the status of the drive
			* @return the status of the motor drive
		*/
		int GetStatus ();
	
		/**
			* function to get the status of the drive
			* @return status of the drive
		*/ 
		byte GetBStatus();
	
		/** 
			* function to set the speed of the motor drive
			* @param speed as a double, target speed
			* @return OK
			* @return ERROR
		*/
		int SetMotorSpeed (double speed);
	
		/** 
			* function to set motor's position 
			* @param position as a int, target position
			* @return OK
			* @return ERROR
		*/
		int SetMotorPosition(int position);
	
		/** 
			* function to get the motor speed 
			* @return motor speed
			* @return ERROR
		*/
		double GetMotorSpeed ();
		
		/** 
			* function to get motor's position 
			* @return Motor position
		*/
		int GetMotorPosition();
		
		/** 
			* function to get the temperature of the drive
			* @return the temperature of drive
			* @return ERROR
		*/
		double GetTemperature ();
		
		/** 
			* function to get the values of drive's analog inputs
			* @return drive's analog inputs
			* @return NULL if ERROR
		*/
		void GetAnalogInputs(double *input1, double *input2, double *input3);
		
		/** 
			* function to get the values of drive's digital inputs
			* @return drive's digital inputs
			* @return ERROR
		*/
		Digital *GetDigitalInputs ();
		
		/** 
			* function to set drive's digital outputs
			* @param Outputs as a byte, value of the digital outputs
			* @return OK
			* @return ERROR
		*/
		int SetDigitalOutputs (byte Outputs);
		
		/** 
			* function to set drive's digital outputs
			* @param Out1..Out8 as a bool, values for each output
			* @return OK
			* @return ERROR
		*/
		int SetDigitalOutputs (bool out1, bool out2, bool out3, bool out4, bool out5, bool out6, bool out7, bool out8);
		
		/** 
			* function to get the values of drive's digital outputs
			* @return drive's digital outputs
			* @return NULL if error
		*/
		Digital *GetDigitalOutputs();
		
		/**  
			* Start the control thread of the MotorDrive
			* @return OK
			* @return ERROR
		*/
		int Run();
		
		/** 
			* Stops the control thread of the MotorDrive
			* @return OK
			* @return ERROR
		*/
		int Stop();
		
		/** 
			* Sends the start signal to the drive
			* @return OK
			* @return ERROR
		*/
		int StartCANCommunication();
		
		/** 
			* Reset and initialize the status of the drive
			* @return OK
			* @return ERROR
		*/
		int InitializeDrive();
		
		/** 
			* Process the received Node Guard Reply from the drive
		*/
		void ProcessNodeGuardReply(CMSG msg);
		
		/** 
			* Process received CAN TPDO1 messages (Default COB-ID= 0x180 + node id).
			* This type of message contains the following data from Motor Drive: 
			* Status Word (first 4 bytes) & Status Word 1 (last 4 bytes)
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO1Msg(CMSG msg);
		
		/** 
			* Process received CAN TPDO4 messages (Default COB-ID= --- + node id).
			* This type of message contains the following data from Motor Drive: 
			* Status Word (first 2 bytes) & Velocity Actual Value (last 4 bytes)
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO4Msg(CMSG msg);
		
		/** 
			* Process received CAN TPDO3 messages.
			* This type of message contains the following data from Motor Drive: 
			* Status Word (first 4 bytes) & Position Actual Value (last 4 bytes)
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO3Msg(CMSG msg);
		
		/** 
			* Process received CAN TPDO22 messages (Default COB-ID= 0x280 + node id).
			* This type of message contains the following data from Motor Drive: 
			* Actual Velocity Value (First 4 bytes)
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO22Msg(CMSG msg);
		
		/** 
			* Process received CAN TPDO25 messages (Default COB-ID= 0x680 + node id).
			* This type of message contains the following data from Motor Drive: 
			* Programmable Digital Inputs & Dedicated Digital Inputs
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO25Msg(CMSG msg);
		
		/** 
			* Process received CAN TPDO26 messages (Default COB-ID= 0x480 + node id).
			* This type of message contains the following data from Motor Drive: 
			* Analog Input 1 & Input 2 & Input 3 | Input 4
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessTPDO26Msg(CMSG msg);
		
		/** 
			* Process received CAN messages
			* @param msg as a canmsg_t, the message to process
		*/
		void ProcessCANMessage(CMSG msg);
	
		/** 
			* Get current thread update rate 
		*/
		double GetUpdateRate();
		
		/** 
			* Get communication status
		*/
		char *GetCommunicationStatusString();
		
		/** 
			* function to get the status of the drive
			* @return the status of the motor drive
		*/
		char *GetStatusString();
		
		/**
			* Function to calibrate the movement of the motor
			* @return OK			
			* @return ERROR	
		*/
		int CalibrateDrive();
		
		/**
			* return if the drive is calibrated
		*/
		bool IsCalibrated();
		
		/**
			* Function to Reset CANOpen Communication state machine
			* @return OK			
			* @return ERROR	
		*/
		int ResetCANCommunication();
		
		/**
			* Function to reset the node (same as power cycle)
			* @return OK			
			* @return ERROR	
		*/
		int ResetCANNode();
		
		/**
			* Function to start the communication, setting the communication state machine to operational
			* @return OK			
			* @return ERROR	
		*/
		int StartRemoteNode();
		
		/**
			* Function to disable the voltage of the drive
			* @return OK			
			* @return ERROR	
		*/
		int DisableVoltage();
		
		/**
			* Function to shutdown the drive
			* @return OK			
			* @return ERROR	
		*/
		int ShutDown();
		
		/**
			* Function to switch on the drive
			* @return OK			
			* @return ERROR	
		*/
		int SwitchOn();
		
		/**
			* Function for enabling operations of the drive
			* @return OK			
			* @return ERROR	
		*/
		int EnableOperation();
		
		
	private:
		/**
			* All core component functionality is contained in this thread.
			* All of the Motor Drive component state machine code can be found here.
			* @param *threadData, priority of thread and type of clock
		*/
		void ControlThread(void);
		
	
		/**
			* Sends one NodeGuard message
			* @return OK
			* @return ERROR
		*/
		int SendNodeGuardMsg();
		
		
		/** 
			* Controls the status of the communication with the drive
		*/
		void ControlCommunicationStatus();
		
		/** 
			* Change the value of the status
		*/
		void ChangeStatus(int new_status);
		
		/** 
			* Change the value of the communication status
		*/
		void ChangeCommunicationStatus(int new_status);
		
		/**
			* Process the status_word value received from the drive and changes the status of the object
			* @param status_word as a byte, with the current drive internal status 
		*/
		void ProcessStatusWord(byte status_word);
		
		/**
			* Process the velocity value received from the drive and converts it to rpm 
			* @param velocity_ref as an integer, current velocity of the motor
		*/
		void ProcessVelocityValue(int velocity_ref);
		
		/**
			* Process the value of the digital inputs received from the drive
			* @param input as a byte, value of all the digital inputs
		*/
		void ProcessDigitalInput(byte input);
		
		/**
			* Process the value of the digital Outputs received from the drive
			* @param output as a byte, value of all the digital outputs
		*/
		void ProcessDigitalOutput(byte output);
		
		/**
			* Process the value of the Temperature of the drive
			* @param temp as unsigned short, measured value
		*/
		void ProcessTemperature(unsigned short temp);
		
		/**
			* Initialize and configure the periodic messages to send to the drive.
		*/
		void ConfigureCANPeriodicMessages();
	
		/**
			* Send some CAN Periodic Messages to the drive. It will respond with the corresponding messages. 
			* @return OK
			* @return FALSE
		*/
		int SendCANPeriodicMessages();
		
		/**
			* Initialize and configure PDO messages to send to the drive.
			* @return OK
			* @return FALSE
		*/
		int ConfigureCANPDOMessages();
		
		
	};

#endif //__MOTOR_DRIVE_H
