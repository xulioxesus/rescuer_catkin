
#ifndef RESCUER_ARM_HARDWARE_INTERFACE_JOINT_TRAJECTORY_ACTION_CONTROLLER_SIMULATOR_H
#define RESCUER_ARM_HARDWARE_INTERFACE_JOINT_TRAJECTORY_ACTION_CONTROLLER_SIMULATOR_H

#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

// Standard
#include <sstream>
#include <string>
#include <vector>
#include <XmlRpcValue.h>

// Messages
#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <sensor_msgs/JointState.h>
#include <brics_actuator/JointVelocities.h>

// ROS
#include <ros/ros.h>
#include <ros/package.h> // for getting package file path
#include <pluginlib/class_list_macros.h>
#include <actionlib/server/simple_action_server.h>

struct Segment
{
    double start_time;
    double duration;
    std::vector<double> positions;
    std::vector<double> velocities;
};

class JointTrajectoryActionControllerSimulator
{
private:
    ros::NodeHandle node_;
    int trajectory_update_rate_;/*!< Update rate used onto processTrajectory method*/
    int state_update_rate_;/*!< Update rate used onto updateState method*/
    std::vector<Segment> trajectory_;

    bool is_action;
    double goal_time_constraint_;
    double stopped_velocity_tolerance_;
    double min_velocity_;
    double max_velocity_;
    std::vector<double> goal_constraints_;
    std::vector<double> trajectory_constraints_;

    ros::Subscriber command_sub_;
    ros::Subscriber joint_state_sub_;
    ros::Publisher state_pub_;
    ros::Publisher trajectory_pub_;

    typedef actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> FJTAS;
    boost::scoped_ptr<FJTAS> action_server_;/*!<The scoped_ptr class template stores a pointer to a dynamically allocated object. It is not copyable.*/

    boost::thread* feedback_thread_;
    boost::mutex terminate_mutex_;

    bool terminate_;/*!<The action server must be terminated.*/
    int num_joints_;
    std::vector<std::string> joint_names_;

    control_msgs::FollowJointTrajectoryFeedback feedback_msg_;
    sensor_msgs::JointState joint_states_;

    void setTrajectoryUpdateRate(int value);
    int getTrajectoryUpdateRate();
    
    void setStateUpdateRate(int value);
    int getStateUpdateRate();

    void setIsAction(bool value);
    bool isAction();

    bool checkSegmentVelocityElements(const trajectory_msgs::JointTrajectoryPoint point,const int position);

    bool checkPointWithoutVelocity(const trajectory_msgs::JointTrajectoryPoint point);

    bool checkSegmentPositionElements(const trajectory_msgs::JointTrajectoryPoint point,const int position);
    void generateErrorNotLocatedJoint(const size_t index);
    void generateErrorFirstPointNoPositions();
    void generateErrorUnexpectedVelocities(const size_t size, const int position);
    void generateErrorUnexpectedPositions(const size_t size, const int position);
    void generateErrorNoVelocities();
    void generateErrorGoalConstraints(const size_t index);
    void generateErrorTrajectoryConstraints(const int segment_index, const size_t index);
    void generateErrorMaxVelocityExceeded(const int segment_index, const int joint_index, const double desired_velocity);
    void generateSuccesfulTrajectorySkipped();
    void generateSuccesfulTrajectoryPreemted();
    void generateSuccesfulTrajectory();

    trajectory_msgs::JointTrajectoryPoint getTrajectoryPoint(const std::vector<double> velocities, const std::vector<double> positions, const ros::Duration duration);
    trajectory_msgs::JointTrajectoryPoint getTrajectoryPointZero();
    void publishTrajectoryMessage(const trajectory_msgs::JointTrajectory trajectorySimulated);

public:
    JointTrajectoryActionControllerSimulator();
    virtual ~JointTrajectoryActionControllerSimulator();

    bool initialize(std::string name);

    void start();
    void stop();

    void processCommand(const trajectory_msgs::JointTrajectoryConstPtr& msg);
    void processFollowTrajectory(const control_msgs::FollowJointTrajectoryGoalConstPtr& goal);
    void initState();
    void updateState();
    void processTrajectory(const trajectory_msgs::JointTrajectory& traj);
    bool checkControllerJoints(const trajectory_msgs::JointTrajectory& traj_msg, std::vector<int> &lookup);
    bool validateInitialPositions(const trajectory_msgs::JointTrajectory& traj_msg);
    void fillSegmentsDuration(const trajectory_msgs::JointTrajectory& traj_msg, std::vector<double> &durations, double &trajectory_duration);
    bool createSegments(const trajectory_msgs::JointTrajectory& traj_msg, const ros::Time time, const std::vector<double> durations , const std::vector<int> lookup, std::vector<Segment> &trajectory);
    bool checkOnGoal(const std::vector<Segment> trajectory, const std::vector<int> lookup);
    bool checkGoalConstraints();
    bool validateTrajectoryConstraints(int segment_index);
    void getJointStates(const sensor_msgs::JointStateConstPtr& joint_state);
};

#endif  // RESCUER_ARM_HARDWARE_INTERFACE_JOINT_TRAJECTORY_ACTION_CONTROLLER_SIMULATOR_H
