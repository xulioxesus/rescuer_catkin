#include <base_controller.hpp>

using namespace std;


PrimitiveDriver base_controller::driver = *(new PrimitiveDriver(DEFAULT_RESCUER_PORT));
double base_controller::x = 0.0;
double base_controller::y = 0.0;
double base_controller::theta = 0.0;
double base_controller::vx = 0.0;
double base_controller::vth = 0.0;
ros::Time base_controller::last_time;
geometry_msgs::TransformStamped base_controller::odom_trans;
nav_msgs::Odometry base_controller::odom;
//=============================================================================
// Constructor and Destructor =================================================
//=============================================================================

base_controller::base_controller()
{
}

base_controller::~base_controller()
{
}

//=============================================================================
// Setters and getters ========================================================
//=============================================================================

void base_controller::SetX(double pos)
{
	base_controller::x = pos;
}

double base_controller::GetX()
{
	return base_controller::x;
}

void base_controller::SetY(double pos)
{
	base_controller::y = pos;
}

double base_controller::GetY()
{
	return base_controller::y;
}


void base_controller::SetTheta(double pos)
{
	base_controller::theta = pos;
}

double base_controller::GetTheta()
{
	return base_controller::theta;
}


void base_controller::SetXVelocity(double vel)
{
	base_controller::vx = vel;
}

double base_controller::GetXVelocity()
{
	return base_controller::vx;
}


void base_controller::SetThetaVelocity(double vel)
{
	base_controller::vth = vel;
}

double base_controller::GetThetaVelocity()
{
	return base_controller::vth;
}

//=============================================================================
// Callback to /cmd_vel =======================================================
//=============================================================================

void base_controller::cmd_velCallback(const geometry_msgs::Twist &twist_original)
{
	geometry_msgs::Twist twist = twist_original;
	double velocity_x = twist_original.linear.x;
	double velocity_th = twist_original.angular.z;

	ROS_INFO("request: linear=%f, angular=%f", velocity_x, velocity_th);

	base_controller::SetXVelocity(velocity_x);
	base_controller::SetThetaVelocity(velocity_th);

        float propulsiveLinearEffortXPercent=0.0, propulsiveRotationalEffortZPercent=0.0;

        //Converts velocity values into proportional percentage
        //propulsiveLinearEffortXPercent = velocity_x*100;
        //propulsiveRotationalEffortZPercent = velocity_th*100;
        propulsiveLinearEffortXPercent = velocity_x*LINEAR_SPEED_TO_EFFORT_FACTOR*100;
        propulsiveRotationalEffortZPercent = velocity_th*ANGULAR_SPEED_TO_EFFORT_FACTOR*100;

        /*if(propulsiveLinearEffortXPercent > 100.0)
                propulsiveLinearEffortXPercent = 100.0;
        else if(propulsiveLinearEffortXPercent < -100.0)
                propulsiveLinearEffortXPercent = -100.0;

        if(propulsiveRotationalEffortZPercent > 100.0)
                propulsiveRotationalEffortZPercent = 100.0;
        else if(propulsiveRotationalEffortZPercent < -100.0)
                propulsiveRotationalEffortZPercent = -100.0;
	*/


        int result = driver.Move(propulsiveLinearEffortXPercent,propulsiveRotationalEffortZPercent);

        ROS_INFO("sending back response: [%ld]", (long int) result);

        //return true;
}

//=============================================================================
// Calculate the odometry and publishing onto /odom topic =====================
//=============================================================================

void base_controller::CalculateOdometry()
{

	ros::Time current_time = ros::Time::now();
	double dt = (current_time - base_controller::last_time).toSec();;

	double vx = base_controller::GetXVelocity();
	double vy = 0;
	//double vth = base_controller::GetThetaVelocity();
	double vth = - base_controller::GetThetaVelocity();
	double th = base_controller::GetTheta();

	double dx = (vx * cos(th) - vy * sin(th)) * dt;
	double dy = (vx * sin(th) + vy * cos(th)) * dt;
	double dth = vth * dt;


	base_controller::SetX(base_controller::GetX()+dx);
	base_controller::SetY(base_controller::GetY()+dy);
	base_controller::SetTheta(base_controller::GetTheta()+dth);

	geometry_msgs::Quaternion odom_quat;
	odom_quat = tf::createQuaternionMsgFromRollPitchYaw(0,0,base_controller::GetTheta());

	//base_controller::odom_trans.header.frame_id = "odom";
	base_controller::odom_trans.header.frame_id = "odom_combined";
	//base_controller::odom_trans.child_frame_id = "base_link";
	base_controller::odom_trans.child_frame_id = "base_footprint";
	base_controller::odom_trans.header.stamp = current_time;
	base_controller::odom_trans.transform.translation.x = base_controller::GetX();
	base_controller::odom_trans.transform.translation.y = 0.0;
	base_controller::odom_trans.transform.translation.z = 0.0;
	base_controller::odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(base_controller::GetTheta());

	base_controller::odom.header.stamp = current_time;
	base_controller::odom.header.frame_id = "odom";
	base_controller::odom.child_frame_id = "base_link";

	// position
	base_controller::odom.pose.pose.position.x = base_controller::GetX();
	base_controller::odom.pose.pose.position.y = base_controller::GetY();
	base_controller::odom.pose.pose.position.z = 0.0;
	base_controller::odom.pose.pose.orientation = odom_quat;
	// velocity
	base_controller::odom.twist.twist.linear.x = vx;
	base_controller::odom.twist.twist.linear.y = vy;
	base_controller::odom.twist.twist.linear.z = 0.0;
	base_controller::odom.twist.twist.angular.x = 0.0;
	base_controller::odom.twist.twist.angular.y = 0.0;
	base_controller::odom.twist.twist.angular.z = vth;

	last_time = current_time;

}


void base_controller::StartUp()
{
        driver.SetMaxSpeed(MOTOR_DEF_MAX_SPEED,MOTOR_DEF_MAX_TURNSPEED);

        if(driver.StartUp() == ERROR)
        {
                exit(-1);
        }
        else
        {
                ROS_INFO("Starting rover rescuer motors...");
        }
}

void base_controller::Sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "base_controller");
	ros::NodeHandle node;
	base_controller::StartUp();
	ros::Subscriber cmd_vel_subscriber = node.subscribe("cmd_vel", 50, base_controller::cmd_velCallback);
	ros::Publisher odom_pub = node.advertise<nav_msgs::Odometry>("odom", 50);
	tf::TransformBroadcaster broadcaster;
	ros::Rate loop_rate(50);

	while(ros::ok())
	{

		ros::spinOnce();

		base_controller::CalculateOdometry();

		broadcaster.sendTransform(base_controller::odom_trans);
		odom_pub.publish(base_controller::odom);

		loop_rate.sleep();
	}
}

