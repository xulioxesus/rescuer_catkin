#include "SingleRequest.hpp"
#include <stdio.h>

using namespace std;

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Cliente");

  if (argc != 3)
  {
	 ROS_INFO("usage: Cliente X Y");
	 return 1;
  }

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<rescuer_odom::Velocity>("Server");

  rescuer_odom::Velocity srv;
  srv.request.linear = atof(argv[1]);
  srv.request.angular =  atof(argv[2]);

  if (client.call(srv))
{
	ROS_INFO("Moving Rescuer");
}
else
{
	ROS_ERROR("Failed to call service Server");
	return 1;
}

  return 0;
}
