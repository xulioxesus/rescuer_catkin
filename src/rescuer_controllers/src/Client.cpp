#include <ros/ros.h>
#include "Client.hpp"
#include <signal.h>
#include <termios.h>
#include <stdio.h>

#define KEYCODE_R 0x43
#define KEYCODE_L 0x44
#define KEYCODE_U 0x41
#define KEYCODE_D 0x42
#define KEYCODE_Q 0x71

class TeleopRescuer
{
public:

  double linearSpeed, angularSpeed, linearPercent, angularPercent;

  ros::ServiceClient client;
  ros::NodeHandle n;
  
  TeleopRescuer();
  int keyLoop();

};

TeleopRescuer::TeleopRescuer():
  linearSpeed(1),
  angularSpeed(1),
  linearPercent(0.1),
  angularPercent(0.1)
{
  client = n.serviceClient<rescuer_odom::Velocity>("Server");
}

int kfd = 0;
struct termios cooked, raw;

void quit(int sig)
{
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleoperator");

  TeleopRescuer teleoperator;

  signal(SIGINT,quit);

  teleoperator.linearPercent = 0.1;
  teleoperator.angularPercent = 0.1;
  teleoperator.keyLoop();
  
  return(0);
}


int TeleopRescuer::keyLoop()
{
  char c;
  bool dirty=false;


  // get the console in raw mode
  tcgetattr(kfd, &cooked);
  memcpy(&raw, &cooked, sizeof(struct termios));
  raw.c_lflag &=~ (ICANON | ECHO);
  // Setting a new line, then end of file
  raw.c_cc[VEOL] = 1;
  raw.c_cc[VEOF] = 2;
  tcsetattr(kfd, TCSANOW, &raw);

  puts("Reading from keyboard");
  puts("---------------------------");
  puts("Use arrow keys to move the rescuer.");

  rescuer_odom::Velocity srv;

  for(;;)
  {
	// get the next event from the keyboard
	if(read(kfd, &c, 1) < 0)
	{
	  perror("read():");
	  exit(-1);
	}

	ROS_DEBUG("value: 0x%02X\n", c);
  
	switch(c)
	{
	  
	  case 's':
		ROS_DEBUG("STOP");
		angularPercent =  0.0;
		linearPercent  =  0.0;
		dirty = true;
		break;

	  case KEYCODE_L:
		ROS_DEBUG("LEFT");
		angularPercent = angularPercent - 0.1;
		linearPercent = 0;
		dirty = true;
		break;
	  case KEYCODE_R:
		ROS_DEBUG("RIGHT");
		angularPercent = angularPercent + 0.1;
		linearPercent= 0;
		dirty = true;
		break;
	  case KEYCODE_U:
		ROS_DEBUG("UP");
		linearPercent  = linearPercent + 0.1;
		angularPercent= 0;
		dirty = true;
		break;
	  case KEYCODE_D:
		ROS_DEBUG("DOWN");
		linearPercent  = linearPercent - 0.1;
		angularPercent = 0;
		dirty = true;
		break;
	}
	
	if(dirty ==true)
	{
		printf("linearSpeed: %f linearPercent: %f ",linearSpeed,linearPercent);
		printf("angularSpeed: %f angularPercent: %f\n",angularSpeed,angularPercent);

		if(linearPercent > 1.0)
			linearPercent= 1.0;
		else if (linearPercent< -1.0)
			linearPercent= -1.0;

		if(angularPercent> 1.0)
			angularPercent= 1.0;
		else if (angularPercent< -1.0)
			angularPercent= -1.0;

	  srv.request.linear = linearSpeed*linearPercent;
	  srv.request.angular =  angularSpeed*angularPercent;
	  dirty=false;

	if (client.call(srv))
	{
		ROS_INFO("Moving Rescuer");
	}
	else
	{
		ROS_ERROR("Failed to call service Server");
		return 1;
	}
	}
  }

  return 0;
}
