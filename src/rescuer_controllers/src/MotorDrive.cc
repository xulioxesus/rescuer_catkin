/** \file MotorDrive.cpp
 * \author Robotnik Automation S.L.L.
 * \version 1.0
 * \date    
 * 
 * \brief Class for Motor Drives, used by the Rover component
 *  Uses dpcante driver                                               
 * (C) 2007 Robotnik Automation, SLL  
 *  All rights reserved.
*/


#include "MotorDrive.h"
#include "logLib.h"
#include <string.h>
#include <unistd.h>
#include <math.h>


/*! \fn void *AuxMDControlThread(void *threadParam)
 	* @param threadParam as void *, parameters of thread
	* Function executing in the thread
*/
void *AuxMDControlThread(void *threadParam){
	MotorDrive *MotorDriveThread = (MotorDrive *)threadParam;
	MotorDriveThread->ControlThread();
	return NULL;
}


/*! \fn MotorDrive::MotorDrive(int can_fd, int can_net_id, int can_node_id, int mode_of_operation, bool read_digital_inputs, bool read_digital_outputs)
 	* @param can_fd as an integer, CAN file descriptor
	* @param can_node_id as an integer, CAN node id	
	* @param mode_of_operation as int, establishes the mode of operation of the driver
	* @param enable_read_digital_inputs as bool, if true digital inputs will be read 
	* Constructor by default
*/
MotorDrive::MotorDrive(int can_fd, int can_node_id, int mode_of_operation, bool read_digital_inputs, bool read_digital_outputs){
	int i;
	CAN_fd = can_fd;
	CAN_node_id = can_node_id;
	this->mode_of_operation= mode_of_operation; 
	motor_speed = 0.0;
	status = UNKNOWN;
	communication_status = UNKNOWN;
	run = false;
	calibrated = false;
	b_aux_digital_outputs=0;
	temperature = 0.0;
	read_digital_inputs_enable = read_digital_inputs;
	read_digital_outputs_enable = read_digital_outputs;
	
	for(i=0; i<NUM_DIGITAL_INPUTS;i++){
		digital_inputs[i].state = false;
		digital_inputs[i].reserved = false;		
	}
	for(i=0; i<NUM_DIGITAL_OUTPUTS;i++){
		digital_outputs[i].state = false;
		digital_outputs[i].reserved = false;		
	}
	for(i=0; i<NUM_ANALOG_INPUTS;i++){
		analog_inputs[i]= -1.0;		
	}
	
	ConfigureCANPeriodicMessages();  
}


/*! \fn int MotorDrive::GetStatus()
   	* function to get the status of the drive
    * @return status of the drive
	* @return ERROR
*/ 
int MotorDrive::GetStatus(){
	return status;
}


/*! \fn byte MotorDrive::GetBStatus()
   	* function to get the status of the drive
    * @return status of the drive
	* @return ERROR
*/ 
byte MotorDrive::GetBStatus(){
	return b_status;
}

  
/*! \fn int MotorDrive::SetMotorSpeed(double speed)
   	* function to set the speed of the motor drive
	* @param speed as a double, target speed in rpm
    * @return OK
	* @return ERROR
*/
int MotorDrive::SetMotorSpeed(double speed){
	
	int velocity_aux = 0;
		
	if(communication_status==OPERATIONAL){
		//Change sign to move properly
		velocity_aux = (int)speed; // reference in RPM;
		cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::SetMotorSpeed (node %d)->%lf ->%d",CAN_node_id, speed,velocity_aux);				
		//if (dx60co8_SendPDOVelocityRefMsg(CAN_fd, CAN_node_id, velocity_aux)!=OK) 	// 
		if (dx60co8_SendVelocityRefMsg(CAN_fd, CAN_node_id, velocity_aux)!=OK) 	
			return ERROR;		
		}
	return OK;
}


/** 
	* function to set motor's position 
	* @param position as a int, target position
	* @return OK
	* @return ERROR
*/
int MotorDrive::SetMotorPosition(int position){
	/*NOT USED*/
	return OK;
}


/** 
	* \fn double MotorDrive::GetMotorSpeed()
    * function to get the motor speed 
	* @return motor speed
	* @return ERROR
*/
double MotorDrive::GetMotorSpeed(){
	return motor_speed;
}


/** 
	* function to get motor's position 
	* @return Motor position
*/
int MotorDrive::GetMotorPosition(){
	/*NOT USED*/
	return 0;
}


/** 
	* \fn double MotorDrive::GetTemperature()
	* function to get the temperature of the drive
	* @return the temperature of drive
	* @return ERROR 
*/
double MotorDrive::GetTemperature(){
	return temperature;
}


/** 
	* \fn Vector MotorDrive::GetAnalogInputs()
	* function to get the values of drive's analog inputs
	* @return drive's analog inputs
	* @return NULL if ERROR
*/
void MotorDrive::GetAnalogInputs(double *input1, double *input2, double *input3){
	if(input1!=NULL)
		*input1=analog_inputs[0];
	if(input2!=NULL)
		*input2=analog_inputs[1];
	if(input3!=NULL)
		*input3=analog_inputs[2];
}


/** 
	* \fn Digital *MotorDrive::GetDigitalInputs()
	* function to get the values of drive's digital inputs
	* @return drive's digital inputs
	* @return NULL if error
*/
Digital *MotorDrive::GetDigitalInputs(){
	return digital_inputs;
}

/** 
	* \fn Digital *MotorDrive::GetDigitalOutputs()
	* function to get the values of drive's digital outputs
	* @return drive's digital outputs
	* @return NULL if error
*/
Digital *MotorDrive::GetDigitalOutputs(){
	return digital_outputs;
}


/** 
	* \fn int MotorDrive::SetDigitalOutputs(byte Outputs)
	* function to set drive's digital outputs
	* @param Outputs as a byte, value of the digital outputs
	* @return OK
	* @return ERROR
*/
int MotorDrive::SetDigitalOutputs(byte Outputs){
	return dx60co8_SendDigitalOutputs(CAN_fd, CAN_node_id, Outputs);
}


/*!	\fn int MotorDrive::SetDigitalOutputs (bool out1, bool out2, bool out3, bool out4, bool out5, bool out6, bool out7, bool out8)  
	* function to set drive's digital outputs
	* @param Out1..Out8 as a bool, values for each output
	* @return OK
	* @return ERROR
*/
int MotorDrive::SetDigitalOutputs (bool out1, bool out2, bool out3, bool out4, bool out5, bool out6, bool out7, bool out8){
	byte aux = 0;
	
	digital_outputs[0].state=out1;
	digital_outputs[1].state=out2;
	digital_outputs[2].state=out3;
	digital_outputs[3].state=out4;
	digital_outputs[4].state=out5;
	digital_outputs[5].state=out6;
	digital_outputs[6].state=out7;
	digital_outputs[7].state=out8;
	
	if(out1)
		aux=1;
	if(out2)
		aux+=2;
	if(out3)
		aux+=4;
	if(out4)
		aux+=8;
	if(out5)
		aux+=16;
	if(out6)
		aux+=32;
	if(out7)
		aux+=64;
	if(out8)
		aux+=128;
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::SetDigitalOutputs: Sending digital outputs(%x) to node %d",aux,CAN_node_id);
	

	
	return dx60co8_SendDigitalOutputs(CAN_fd, CAN_node_id, aux);
}



/**  
	* \fn int MotorDrive::Run()
	* Start the control thread of the MotorDrive
	* @return OK
	* @return ERROR
*/
int MotorDrive::Run(){
	pthread_attr_t attr;	// Thread attributed for the component threads spawned in this function
	//struct thread_param par;
	
	if(!run){
		
		if(StartCANCommunication()== OK){
			pthread_attr_init(&attr);
			pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		
			run = true;
		
			par.prio = THREAD_PRIOR_MOTORDRIVE;				// Priority level 0[min]-80[max]
			par.clock=CLOCK_REALTIME; 	// 0-CLOCK_MONOTONIC 1-CLOCK_REALTIME				
			
			if(pthread_create(&pthread_id, &attr, AuxMDControlThread, this) != 0)
			{
				cError("MotorDrive: Node %d: Could not create ControlThread\n", CAN_node_id);
				pthread_attr_destroy(&attr);
				return ERROR;
			}
			pthread_attr_destroy(&attr);	
			
			return OK;
		}else{
			cError("MotorDrive::Run: node %d: Error configuring CAN messages\n",CAN_node_id);
			return ERROR;
		}
	}else{
		cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::Run: node %d: Drive is already running\n",CAN_node_id);
		return OK;
	}
		
}


/** 
	\fn int MotorDrive::Stop()
	* Stops the control thread of the MotorDrive.
	* Puts motor drive on Disabled State
	* @return OK
	* @return ERROR
*/
int MotorDrive::Stop(){
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Stop: Stopping MotorDrive %d\n", CAN_node_id);

	if(dx60co8_SendDisableVoltageMsg(CAN_fd, CAN_node_id)!=OK){
		cError("MotorDrive: Stop: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
	
	run = false;
	
	return OK;
}


/** 
	* \fn int MotorDrive::StartCANCommunication()
	* \brief Configures and sends the start signal to the Motor Drive 
	* \return OK if config OK.
	* \return ERROR if the configuration process fails.
*/
int MotorDrive::StartCANCommunication(){
	struct timespec wait;	
	
	// RESET COMMUNICATION
	if(ResetCANCommunication()!=OK)
		return ERROR;
	
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=50000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);	
	
	//RESET NODE
	if(ResetCANNode()!=OK)
		return ERROR;
	
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=50000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);	
	
	
	if(InitializeDrive()!=OK)
	{
		cError("MotorDrive: StartCANCommunication: Node %d: Error Initializing the drive\n", CAN_node_id);
		return ERROR;
	}
	
	//CAN PDO Messages configured by driver software application 
	//if(ConfigureCANPDOMessages()!=OK){
	//	cError("MotorDrive: StartCANCommunication: Node %d: Error in CAN PDO configuration\n", CAN_node_id);
	//	return ERROR;
	//}
	
	if(StartRemoteNode()!=OK)
		return ERROR;
	
	return OK;
}


/*!	\fn int MotorDrive::InitializeDrive() 
	* Reset and initialize the status of the drive
	* @return OK
	* @return ERROR
*/
int MotorDrive::InitializeDrive(){
	struct timespec wait;
	
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::InitializeDrive: Node %d Intializing...\n",CAN_node_id);
	
	if(DisableVoltage()!=OK){
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
/*	if(dx60co8_SendResetMsg(CAN_fd, CAN_node_id)!=OK){
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);*/
	
	if(ShutDown()!=OK)
	{
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
	if(SwitchOn()!=OK)
	{
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
		
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
	if(EnableOperation()!=OK)
	{
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
		
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
	
	//Digital Outputs to 0
	if(dx60co8_SendDigitalOutputs(CAN_fd, CAN_node_id,0x00))
	{
		cError("MotorDrive: InitializeDrive: Node %d: Error\n",CAN_node_id);
		return ERROR;
	}
	
	return OK;
}



/*! \fn void * MotorDrive::ControlThread(void)
	* All core component functionality is contained in this thread.
	* All of the MotorDrive component state machine code can be found here.
*/
void MotorDrive::ControlThread(void){
	struct sched_param schedp;
	int policy = par.prio? SCHED_FIFO : SCHED_OTHER;
	struct timespec now, next, interval;
	long diff;
	unsigned long sampling_period_us;
	int iCycle=0;
	
	
	pthread_running = true;
	
	// Robotnik thread priority and timing management
	memset(&schedp, 0, sizeof(schedp));
	schedp.sched_priority = par.prio;
	sched_setscheduler(0, policy, &schedp);
	clock_gettime(par.clock, &now);
	next = now;
	next.tv_sec++;  // start in next second? 
	sampling_period_us = (int)(1.0 / MOTOR_DRIVE_THREAD_DESIRED_HZ * 1000000.0);		
	interval.tv_sec = sampling_period_us / USEC_PER_SEC;  // interval parameter in uS
	interval.tv_nsec = (sampling_period_us % USEC_PER_SEC)*1000;
	node_guard_reply = now;
	calibrated = false;
	
	while(run) // Execute state machine code while not in the SHUTDOWN state
	{
		clock_nanosleep(par.clock, TIMER_ABSTIME, &next, NULL);
		clock_gettime(par.clock, &now);
		next.tv_sec += interval.tv_sec;
		next.tv_nsec += interval.tv_nsec;
		tsnorm(&next);			
		diff = calcdiff(next, now);
			
		// Thread frequency update
		pthread_hz = 1.0/(diff / 1000000.0); // Compute the update rate of this thread

		// Actions performed by thread	
		ControlCommunicationStatus();	//Control de estado de la comunicacion CAN
		
		// Switch from PREOPERATIONAL to OPERATIONAL
		if (communication_status != OPERATIONAL )
		{
			if (iCycle>=30)
			{
				iCycle = 0;
				StartRemoteNode();
				/*dx60co8_CreateNMTMsg(&msg, CAN_node_id, START_NODE); 	//Start node Comunication 	
				if(dx60co8_SendMsg(CAN_fd, &msg, &len)!=OK)
				{ 		  	// Change from Preoperational -> Operational
					cError("MotorDrive: StartCANCommunication: Node %d: Error\n", CAN_node_id);					
				}*/
			}
			iCycle++;
		}
			
		SendNodeGuardMsg();				//Envio del mensaje de control de comunicacion CAN
		
		// Desde aqui o desde PD
		SendCANPeriodicMessages();		//Envio de los mensajes SDO periodicos, como la lectura de las salidas digitales y la temperatura de los driver
				
	}	
	
	usleep(50000);	// Sleep for 50 milliseconds and then exit

	pthread_running = false;
	
}


/*!	\fn int MotorDrive::SendNodeGuardMsg() 
	* Sends one NodeGuard message.Used for monitoring its communication state.
	* @return OK
	* @return ERROR
*/
int MotorDrive::SendNodeGuardMsg(){
	CMSG msg;
	int len = 1;
	
	dx60co8_CreateNodeGuardMsg(&msg, CAN_node_id);
	
	return dx60co8_SendRTRMsg(CAN_fd, &msg, &len);
}


/*!	\fn void MotorDrive::ProcessNodeGuardReply(CMSG msg)
	* Process the received Node Guard Reply from the drive
*/
void MotorDrive::ProcessNodeGuardReply(CMSG msg){
	
	clock_gettime(par.clock, &node_guard_reply);
	if((msg.data[0]== 0x4) || (msg.data[0] == 0x84)){ // Communication in STOPPED state
		ChangeCommunicationStatus(STOPPED);		
		return;
	}
	if((msg.data[0]== 0x5) || (msg.data[0] == 0x85)){ // Communication in OPERATIONAL state
		ChangeCommunicationStatus(OPERATIONAL);
		return;
	}
	if((msg.data[0]== 0x7F) || (msg.data[0] == 0xFF)){ // Communication in PRE_OPERATIONAL state
		ChangeCommunicationStatus(PRE_OPERATIONAL);
		return;
	}
	ChangeCommunicationStatus(UNKNOWN);
}


/*!	\fn void MotorDrive::ControlCommunicationStatus()
	* Controls the status of the communication with the drive
	* Calculate the difference between current time and the last NodeGuard message received from drive. If this difference is
	* greater than MOTOR_DRIVE_NMT_TIMEOUT_SEC, the communication with the device has failed.
*/
void MotorDrive::ControlCommunicationStatus(){
	struct timespec time;
	double time_diff = 0.0;

	clock_gettime(par.clock, &time);
	time_diff = time.tv_sec - node_guard_reply.tv_sec;
	if(time_diff > MOTOR_DRIVE_NMT_TIMEOUT_SEC){
		ChangeCommunicationStatus(FAULT);
	}
}


/*!	\fn void MotorDrive::ProcessCANMessage(CMSG msg)
	* Process received CAN messages from Drivers
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessCANMessage(CMSG msg){
	int NMT_id = 0x700 + CAN_node_id;		// COB-id for NMT message response from drive
	int TPDO1_id = 0x180 + CAN_node_id;		// StatusWord-StatusWord_1
	//int TPDO4_id = --- + CAN_node_id;	
	int TPDO22_id = 0x280 + CAN_node_id;	// Velocity Actual Value
	int TPDO25_id = 0x680 + CAN_node_id;	// Digital Inputs 
	int TPDO26_id = 0x480 + CAN_node_id;	// Analog Inputs
	int SDO_Response_id = 0x580 + CAN_node_id;
	unsigned short aux = 0;
		
	if(msg.id == NMT_id){ //node Guard message received		
		ProcessNodeGuardReply(msg);
		return;
		}
	
	// PDO (Process Data Object) Messages
	if(msg.id == TPDO1_id){ //Status Word  & StatusWord_1
		ProcessTPDO1Msg(msg);
		return;
		}	

	if(msg.id == TPDO22_id){ // Velocity Actual Value
		ProcessTPDO22Msg(msg);
		return;
		}	
		
	if(msg.id == TPDO25_id){ // Digital Inputs
		ProcessTPDO25Msg(msg);
		return;
		}
	
	if(msg.id == TPDO26_id){// Analog input1 & input2 & input3 & input4
		ProcessTPDO26Msg(msg);	
		
		return;
		}
		
	// SDO (Service Data Objects) Messages
	if(msg.id == SDO_Response_id){
		//printf("SDO RESPONSE (node %d): %x %x \n",CAN_node_id, msg.data[1],msg.data[2]);
		if(msg.data[1] == 0xA1 && msg.data[2] == 0x20){//Digital Outputs (COB-ID = 0x20A1)
			//Process Digital Outputs
			ProcessDigitalOutput(msg.data[4]);
			return;
			}
		if(msg.data[1] == 0xC2 && msg.data[2] == 0x20){//Power Stage Temperature (COB-ID = 0x20C2)
			//Process Temperature
			aux = msg.data[5] << 8;	//BYTE 6 
			aux |= msg.data[4];		//BYTE 5
			ProcessTemperature(aux);
			cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::ProcessCANMessage Node %d Temperautre = %d", CAN_node_id, aux);
			return;
			}
	}
	
}


/*!	\fn	 void MotorDrive::ProcessTPDO1Msg(CMSG msg)
	* Process received CAN TPDO1 messages (Default COB-ID= 0x180 + node id).
	* This type of message contains the following data from Motor Drive: 
	* Status Word (first 4 bytes)
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO1Msg(CMSG msg){
	
	byte aux_status = 0, aux_status2;
	
	aux_status2 = msg.data[3] << 8;
	aux_status2 |= msg.data[2];
	//Extract Status word
	aux_status = msg.data[1] << 8;
	aux_status |= msg.data[0];
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::ProcessTPDO1Msg: Node %d: Status received=%x\t%x",CAN_node_id, aux_status,aux_status2);
	ProcessStatusWord(aux_status);			
}


/*!	\fn	 void MotorDrive::ProcessTPDO4Msg(CMSG msg)
	* Process received CAN TPDO4 messages (Default COB-ID= 0x280 + node id).
	* This type of message contains the following data from Motor Drive: 
	* Status Word (first 2 bytes) & Velocity Actual Value (last 4 bytes)
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO4Msg(CMSG msg){
	byte aux_status = 0;
	int aux_velocity = 0;
	//Extract Status word
	aux_status = msg.data[1] << 8;
	aux_status |= msg.data[0];
	//Extract velocity value
	aux_velocity = (int) msg.data[7] * 16777216 + (int) msg.data[6] * 65536 + (int) msg.data[5] * 256 + (int) msg.data[4];		
	ProcessStatusWord(aux_status);
	ProcessVelocityValue(aux_velocity);
	
}


/*!	\fn	 void MotorDrive::ProcessTPDO3Msg(CMSG msg)
	* Process received CAN TPDO4 messages
	* This type of message contains the following data from Motor Drive: 
	* Status Word (first 2 bytes) & Position Actual Value (last 4 bytes)
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO3Msg(CMSG msg){
	byte aux_status = 0;
	int aux_velocity = 0;
	//Extract Status word
	aux_status = msg.data[1] << 8;
	aux_status |= msg.data[0];
	//Extract velocity value
	aux_velocity = (int) msg.data[7] * 16777216 + (int) msg.data[6] * 65536 + (int) msg.data[5] * 256 + (int) msg.data[4];		
	ProcessStatusWord(aux_status);
	//ProcessVelocityValue(aux_velocity);
	
}


/*!	\fn	 void MotorDrive::ProcessTPDO22Msg(CMSG msg)
	* Process received CAN TPDO22 messages (Default COB-ID= 0x280 + node id).
	* This type of message contains the following data from Motor Drive: 
	* Velocity Actual Value (first 4 bytes) 
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO22Msg(CMSG msg){
		
	int32_t aux_velocity = 0;
	//Extract actual velocity value
	aux_velocity = 0;
	aux_velocity = (int32_t) msg.data[3] * 16777216 + (int32_t) msg.data[2] * 65536 + (int32_t) msg.data[1] * 256 + (int32_t) msg.data[0];		
	ProcessVelocityValue( aux_velocity );	
	//cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::ProcessTPDO22Msg  id= %d velocity = %d\n", CAN_node_id, aux_velocity  );
}


/*!	\fn void MotorDrive::ProcessTPDO25Msg(CMSG msg) 
	* Process received CAN TPDO25 messages (Default COB-ID= 0x680 + node id).
	* This type of message contains the following data from Motor Drive: 
	* Programmable Digital Inputs & Dedicated Digital Inputs
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO25Msg(CMSG msg){
	//int i;
	//Sets the local values of the object drive with the read values
	ProcessDigitalInput(msg.data[0]);
	
	/*for(i=0;i<NUM_DIGITAL_INPUTS;i++){
		cDebug(DEBUG_LEVEL_MOTORDRIVE,"D/I[%d]=%d  ",i,digital_inputs[i].state);
	}
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"\n");*/
}


/*!	\fn void MotorDrive::ProcessTPDO26Msg(CMSG msg)
	* Process received CAN TPDO26 messages (Default COB-ID= 0x480 + node id).
	* This type of message contains the following data from Motor Drive: 
	* Analog Input 1 & Input 2 & Input 3 
	* These values are converted into Volts
	* @param msg as a CMSG, the message to process
*/
void MotorDrive::ProcessTPDO26Msg(CMSG msg){
	short aux_input = 0;
	//INPUT 1
	aux_input = msg.data[1] << 8;
	aux_input|= msg.data[0];
	analog_inputs[0]=(double)aux_input*20.0/pow(2,16); // Convert into volts
	//INPUT 2
	aux_input = 0;
	aux_input = msg.data[3] << 8;
	aux_input|= msg.data[2];
	analog_inputs[1]= ((double)aux_input*20.0/pow(2,16));// Convert into volts
	//INPUT 3
	aux_input = 0;
	aux_input = msg.data[5] << 8;
	aux_input|= msg.data[4];
	analog_inputs[2]=((double)aux_input*20.0/pow(2,16));// Convert into volts
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::ProcessTPO26Msg: node %d: Analog Inputs: %lf %lf %lf",CAN_node_id,analog_inputs[0],analog_inputs[1],analog_inputs[2]);
}


/*!	\fn void ChangeStatus(int new_status)
	* Change the value of the status
	* @param new_status as integer, new status value
*/
void MotorDrive::ChangeStatus(int new_status){
	if(new_status!=status){ //Only if it's different
		status = new_status;
		switch(new_status){
			case UNKNOWN:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive status changed to UNKNOWN\n",CAN_node_id);
			break;
			case SWITCH_ON_DISABLED:
				cDebug(4,"MotorDrive: Node %d: Drive status changed to SWITCH_ON_DISABLED\n",CAN_node_id);
			break;
			case READY_TO_SWITCH_ON:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive status changed to READY_TO_SWITCH_ON\n",CAN_node_id);
			break;
			case OPERATION_ENABLED:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive status changed to OPERATION_ENABLED\n",CAN_node_id);
			break;
			case OPERATION_DISABLED:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive status changed to OPERATION_DISABLED\n",CAN_node_id);
			break;
			case FAULT:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive status changed to FAULT\n",CAN_node_id);
			break;
		}
	}
}


/*!	\fn void ChangeCommunicationStatus(int new_status)
	* Change the value of the communication status
	* @param new_status as integer, new communication status value
*/
void MotorDrive::ChangeCommunicationStatus(int new_status){
	if(new_status!=communication_status){ //Only if it's different
		communication_status = new_status;
		switch(new_status){
			case UNKNOWN:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive Communication status changed to UNKNOWN\n",CAN_node_id);
			break;
			case STOPPED:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive Communication status changed to STOPPED\n",CAN_node_id);
			break;
			case PRE_OPERATIONAL:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive Communication status changed to PRE_OPERATIONAL\n",CAN_node_id);
			break;
			case OPERATIONAL:
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: Drive Communication status changed to OPERATIONAL\n",CAN_node_id);
			break;
			case FAULT:
				//cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: ControlCommunicationStatus: node %d NMT Timeout!!!\n", CAN_node_id);
				cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: NMT Timeout!!! \n\tDrive Communication status changed to FAULT\n",CAN_node_id);
			break;
		}
	}
}


/*!	\fn void MotorDrive::ProcessStatusWord(byte status_word)
	* Process the status_word value received from the drive and changes the status of the object
	* Data Description:
	* Bit	Description
	*  0	Ready To Switch On state indicator
	*  1	Switched On state indicator
	*  2	Operation Enabled state indicator
	*  3	Fault state indicator
	*  4	Voltage Enabled state indicator
	*  5	Quick Stop state indicator
	*  6	Switch On Disabled state indicator
	*  7	Warning indicator
	*  8	Delay state
	*  ..	Not used for this application
	* @param status_word as a byte, with the current drive internal status 
*/
void MotorDrive::ProcessStatusWord(byte status_word){
	byte aux_ready_switchon = 0x01;
	byte aux_switchon = 0x03;
	byte aux_op_enabled = 0x07;	
	byte aux_switchon_disable = 0x40;
	byte aux_fault = 0x08;
	
//	byte aux_quick_stop = 0x07;

	b_status = status_word;//Saves the value
	
	if((status_word & aux_fault)==aux_fault) {						// FAULT state?
		ChangeStatus(FAULT);	
		return;
	}
		
	if((status_word & aux_switchon_disable)==aux_switchon_disable) { // SWITCH_ON_DISABLED state?
		ChangeStatus(SWITCH_ON_DISABLED);	
		return;
	}
	
	if((status_word & aux_op_enabled)==aux_op_enabled) {			// OPERATION_ENABLED state?
		ChangeStatus(OPERATION_ENABLED);	
		return;
	}
	
	if((status_word & aux_switchon)==aux_switchon) {				// SWITCHED ON
		ChangeStatus(SWITCHED_ON);	
		return;
	}
		
	if((status_word & aux_ready_switchon)==aux_ready_switchon) {	// READY_TO_SWITCH_ON state?
		ChangeStatus(READY_TO_SWITCH_ON);	
		return;
	}
		
	
	ChangeStatus(UNKNOWN);											//UNKNOWN state
}


/*!	\fn void MotorDrive::ProcessVelocityValue(int velocity_ref)
	* Process the velocity value received from the drive and changes converts it to rpm 
	* @param velocity_ref as an integer, current velocity of the motor
*/
void MotorDrive::ProcessVelocityValue(int velocity_ref){
	motor_speed = (double)velocity_ref * REF2RPM;// Convert to rpm
	//cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: ProcessVelocityValue: node %d: ref = %d motor_speed = %lf\n",CAN_node_id, velocity_ref, motor_speed);
}


/*!	\fn void MotorDrive::ProcessDigitalInput(byte input)
	* Process the value of the digital inputs received from the drive
	* @param input as a byte, value of all the digital inputs
*/
void MotorDrive::ProcessDigitalInput(byte input){
	int i;
	
	b_digital_inputs = input; //Byte format
	
	for(i=0; i<NUM_DIGITAL_INPUTS; i++){
		if((input& 1) == 1){
				digital_inputs[i].state=true;			
		}else
			digital_inputs[i].state=false;
		input= input>> 1;
	}	
	
}


/*!	\fn void MotorDrive::ProcessDigitalOutput(byte output)
	* Process the value of the digital Outputs received from the Stardrive
	* @param output as a byte, value of all the digital outputs
*/
void MotorDrive::ProcessDigitalOutput(byte output){
	//int i;
	
	b_digital_outputs = output; //Byte format
	
	/*for(i=0; i<NUM_DIGITAL_OUTPUTS; i++){
		if((output& 1) == 1){
			digital_outputs[i].state=true;			
		}else
			digital_outputs[i].state=false;
		output= output>> 1;
	}	*/
	
	/*for(i=0;i<NUM_DIGITAL_OUTPUTS;i++){
		cDebug(DEBUG_LEVEL_MOTORDRIVE,"D/O[%d]=%d  ",i,digital_outputs[i].state);
	}
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"\n");*/
}


/*!	\fn void MotorDrive::ProcessTemperature(unsigned short temp)
	* Process the value of the Temperature of the drive
	* @param temp as unsigned short, measured value
*/
void MotorDrive::ProcessTemperature(unsigned short temp){
	temperature = (double) temp*0.01; //Units from driver are 0.01 ºC
	//cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: Node %d: temp = %d (%lf ºC)\n", CAN_node_id, temp, temperature);
}


/*!	\fn void MotorDrive::ConfigureCANPeriodicMessages()
	* Initialize and configure the periodic messages (SDO) to send to the drive:
	* 1- ReadTemperature 
	* 2- ReadDigitalOutput
*/
void MotorDrive::ConfigureCANPeriodicMessages(){
	dx60co8_CreateReadPowerStageTemp(&CAN_periodic_msgs[0],CAN_node_id);
	dx60co8_CreateReadDigitalOutputs(&CAN_periodic_msgs[1],CAN_node_id);
	//dx60co8c_CreateReadDigitalOutputs(&CAN_periodic_msgs[1],CAN_node_id,1);

	//dx60co8_CreateReadDigitalInputsMsg(&CAN_periodic_msgs[2],CAN_node_id);
	//dx60co8c_CreateReadAnalogInputs(&CAN_periodic_msgs[3], CAN_node_id, 1);
	
}


/*!	\fn int MotorDrive::SendCANPeriodicMessages()
	* Send some CAN Periodic Messages to the drive. It will respond with the corresponding messages. 
	* @return OK
	* @return ERROR
*/
int MotorDrive::SendCANPeriodicMessages(){
	int len = NUM_CAN_PERIODIC_MSGS;
	static int cycle = 0;
	static int frecuency = (int)MOTOR_DRIVE_THREAD_DESIRED_HZ/CAN_PERIODIC_MSGS_HZ; //Frequency to sending periodic messages
	
	if(communication_status == OPERATIONAL){
		cycle++;
		
		if(cycle == frecuency){	//Se envian siguiendo la frecuencia establecida
			cycle = 0;
		//	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive::SendCANPeriodicMessages: Sending Periodical request to node %d",CAN_node_id);
		
			if(dx60co8_SendMsg(CAN_fd, CAN_periodic_msgs, &len)==CAN_ERROR){ //Fallo en la comunicacion
				cError("MotorDrive: SendCANPeriodicMessages: Node %d: Error sending messages\n", CAN_node_id);
				return ERROR;
			}else{
				if(len!=NUM_CAN_PERIODIC_MSGS){//Si no se envian todos los mensajes que se pretende -> ERROR
					cError("MotorDrive: SendCANPeriodicMessages: Node %d: Error, %d of %d messages sent successfully",len, NUM_CAN_PERIODIC_MSGS);
					return ERROR;
				}
			}
			
			return OK;
		}
	}else
		return ERROR;
	
	return OK;
}


/*!	\fn int MotorDrive::ConfigureCANPDOMessages()
	* Initialize and configure PDO messages to send to the drive.
	* RPDO Messages
	* 
	* TPDO Messages
	*
	*
	* @return OK
	* @return ERROR
*/
int MotorDrive::ConfigureCANPDOMessages(){	
	struct timespec wait;	
	//int len = 1;
	//CMSG msg;
	
	cDebug(DEBUG_LEVEL_MOTORDRIVE,"MotorDrive: ConfigureCANPDOMessages: Configuring messages...");
	
	//Common PDO Messages
	if(dx60co8_ConfigureRPDO1(CAN_fd, CAN_node_id, ASYNCHRONOUS_TRANS)!=CAN_OK){
		cError("MotorDrive::ConfigureCANPDOMessages 1: Node %d: Error in configuration for node motor drive\n",CAN_node_id);
		return ERROR;
	}
	
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);


	/*dx60co8_CreateNMTMsg(&msg, CAN_node_id, PREOPERATIONAL_NODE); //Start node Comunication
	
	if(dx60co8__SendMsg(CAN_fd, &msg, &len)!=OK){
		cError("MotorDrive: InitializeDrive: Node %d: Error\n", CAN_node_id);
		return ERROR;
	}	*/
	
	if(dx60co8_ConfigureRPDO22(CAN_fd, CAN_node_id, ASYNCHRONOUS_TRANS)!=CAN_OK){
		cError("MotorDrive::ConfigureCANPDOMessages 3: node %d: Error in configuration for node motor\n",CAN_node_id);
		return ERROR;
	}
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
	//TPDOs
	if(dx60co8_ConfigureTPDO4(CAN_fd, CAN_node_id, 0x07)!=CAN_OK){
		cError("MotorDrive::ConfigureCANPDOMessages 4: Node %d: Error in configuration for node motor TRACK\n",CAN_node_id);
		return ERROR;
	}
	clock_gettime(par.clock, &wait);
	wait.tv_nsec+=5000000;
	clock_nanosleep(par.clock, TIMER_ABSTIME, &wait, NULL);
	
	if(read_digital_inputs_enable){
		if(dx60co8_ConfigureTPDO25(CAN_fd, CAN_node_id, 2)!=CAN_OK){
			cError("MotorDrive::ConfigureCANPDOMessages 2: node %d: Error in configuration for node motor drive\n",CAN_node_id);
			return ERROR;
		}
	}
	return OK;
}


/*!	\fn  double MotorDrive::GetUpdateRate()
	* Get current thread update rate 
*/
double MotorDrive::GetUpdateRate(){
	return pthread_hz;
}


/*!	\fn	 char *MotorDrive::GetCommunicationStatusString()
	* Get communication status
*/
char *MotorDrive::GetCommunicationStatusString(){
	switch(communication_status){
		case PRE_OPERATIONAL:
			return "PRE_OPERATIONAL";
		break;
		case OPERATIONAL:
			return "OPERATIONAL";
		break;
		case STOPPED:
			return "STOPPED";
		break;
		default:
			return "";
		break;
	}
}


/*!	\fn char *MotorDrive::GetStatusString()
	* function to get the status of the drive
	* @return the status of the motor drive
*/
char *MotorDrive::GetStatusString(){
	switch(status){
		case UNKNOWN:
			return "UNKNOWN";
		break;
		case SWITCH_ON_DISABLED:
			return "SWITCH_ON_DISABLED";
		break;
		case READY_TO_SWITCH_ON:
			return "READY_TO_SWITCH_ON";
		break;
		case OPERATION_ENABLED:
			return "OPERATION_ENABLED";
		break;
		case OPERATION_DISABLED:
			return "OPERATION_DISABLED";
		break;
		case FAULT:
			return "FAULT";
		break;
		default:
			return "";
		break;
		
	}
}


/*!	\fn int MotorDrive::CalibrateDrive()
	* Function to calibrate the movement of the motor
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::CalibrateDrive(){
	//NOT USED
	return OK;
}

/*! \fn bool MotorDrive::IsCalibrated()
	* @return if the drive is calibrated
*/
bool MotorDrive::IsCalibrated(){
	return calibrated;
}


/*!	\fn int MotorDrive::ResetCANCommunication()
	* Function to Reset CANOpen Communication state machine
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::ResetCANCommunication(){
	CMSG msg;
	int len = 1;
		
	// RESET COMMUNICATION
	dx60co8_CreateNMTMsg(&msg, CAN_node_id, RESET_COMMUNICATION); // Reset Communication
	if(dx60co8_SendMsg(CAN_fd, &msg, &len)!=OK)
	{ 		// Force state change of the communication state machine		
		cError("MotorDrive: Error ResetCommunication \n");
		return ERROR;
	}
	return OK;
}


/*!	\fn int MotorDrive::ResetCANNode()
	* Function to reset the node (same as power cycle)
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::ResetCANNode(){
	CMSG msg;
	int len = 1;
	//RESET NODE
	dx60co8_CreateNMTMsg(&msg, CAN_node_id, RESET_NODE); // Reset Node
	if(dx60co8_SendMsg(CAN_fd, &msg, &len)!=OK)
	{ 		// Force state change of the communication state machine		
		cError("MotorDrive: Error ResetNode \n");
		return ERROR;
	}
	return OK;
}


/*!	\fn int MotorDrive::StartRemoteNode()
	* Function to start the communication, setting the communication state machine to operational
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::StartRemoteNode(){
	CMSG msg;
	int len = 1;
	dx60co8_CreateNMTMsg(&msg, CAN_node_id, START_NODE); //Start node Comunication
	
	if(dx60co8_SendMsg(CAN_fd, &msg, &len)!=OK){
		cError("MotorDrive: StartRemoteNode: Node %d: Error Starting the comunication\n", CAN_node_id);
		return ERROR;
	}
	return OK;
}


/*!	\fn int MotorDrive::DisableVoltage()
	* Function to disable the voltage of the drive
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::DisableVoltage(){
	return dx60co8_SendDisableVoltageMsg(CAN_fd, CAN_node_id);
}

/*! \fn int MotorDrive::ShutDown()
	* Function to shutdown the drive
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::ShutDown(){
	return dx60co8_SendShutdownMsg(CAN_fd, CAN_node_id);
}

/*!	\fn int MotorDrive::SwitchOn()
	* Function to switch on the drive
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::SwitchOn(){
	return dx60co8_SendSwitchOnMsg(CAN_fd, CAN_node_id);
}

/*!	\fn int MotorDrive::EnableOperation()
	* Function for enabling operations of the drive
	* @return OK			
	* @return ERROR	
*/
int MotorDrive::EnableOperation(){
	return dx60co8_SendEnableOpMsg(CAN_fd, CAN_node_id);
}






