#!/bin/bash
# file: rescuer-startup.sh
 
echo "Launch roscore and load rescuer description"
screen -dmS rescuer-roscore rescuer-bringup-load-description.sh
sleep 15
echo "Bringup base"
screen -dmS rescuer-base rescuer-bringup-base.sh
echo "Bringup powecube"
screen -dmS rescuer-powercube rescuer-bringup-powercube.sh
echo "Bringup follow joint trajectory controller"
screen -dmS rescuer-arm-joint rescuer-bringup-arm-joint-trajectory.sh
echo "Bringup kinect"
screen -dmS rescuer-kinect rescuer-bringup-kinect.sh
echo "Bringup joints state publisher"
screen -dmS rescuer-joints-state-publisher rescuer-joints-state-publisher.sh
echo "Bringup robot state publisher"
screen -dmS rescuer-robot-state-publisher rescuer-robot-state-publisher.sh
echo "done. view with screen -ls"
