/*
 * Author: Julio Jesús León Pérez 
 */

#include "ArmToZero.hpp"

static const double ACCEPTABLE_BOUND = 0.02; // amount two positions can vary without being considered different positions.
static const double MAX_VELOCITY= 0.1; // amount two positions can vary without being considered different positions.

/**
*	Constructor:
*	- initialize the number of joints
*	- initialize the joint names
*/
ArmToZero::ArmToZero()
{
    joint_state_readed_ = false;
    num_joints_ = 6;
    joint_names_.push_back("arm_1_joint");
    joint_names_.push_back("arm_2_joint");
    joint_names_.push_back("arm_3_joint");
    joint_names_.push_back("arm_4_joint");
    joint_names_.push_back("arm_5_joint");
    joint_names_.push_back("arm_6_joint");
}

/**
*	Destructor:
*	Empty by default.
*/
ArmToZero::~ArmToZero()
{
}

/**
*	Getters and Setters:
*/

void ArmToZero::setUpdateRate(int value)
{
	update_rate_ = value;
}

int ArmToZero::getUpdateRate()
{
	return update_rate_;
}

void ArmToZero::setJointStateReaded(bool value)
{
	joint_state_readed_ = value;
}

bool ArmToZero::isJointStateReaded()
{
	return joint_state_readed_;
}


/**
*	Initialize.
*/
bool ArmToZero::initialize()
{

    setUpdateRate(100);

    return true;
}

/**
*	Start
*/

void ArmToZero::start()
{
    ArmToZero::initialize();

    joint_state_sub_ = node_.subscribe("/joint_states", 70, &ArmToZero::getJointStates, this);

    velocity_pub_ = node_.advertise<brics_actuator::JointVelocities>("/powercube_chain/command_vel", 70);
}

/**
*	Stops the controller
*/

void ArmToZero::stop()
{
    velocity_pub_.shutdown();
    joint_state_sub_.shutdown();
}

/**
* Check if the arm is in zero position
*/

bool ArmToZero::isOnGoal()
{

    for( std::size_t i = 0; i < num_joints_; ++i)
    {
        if(std::abs(joint_states_.position[i]) > ACCEPTABLE_BOUND)
        {
            return false;
        }
    }

    return true;
}

/**
* Check if the arm joint i is in zero position
*/

bool ArmToZero::isJointOnGoal(int joint)
{
	if(std::abs(joint_states_.position[joint]) > ACCEPTABLE_BOUND)
        {
        	return false;
        }
	else
	{
		return true;
	}
}

/**
* Check if the arm joint i is in zero position
*/

bool ArmToZero::isJointPositiveValue(int joint)
{
	if(joint_states_.position[joint] > ACCEPTABLE_BOUND)
	{
        	return true;
        }
	else
	{
		return false;
	}
}

/**
* Check if the arm joint i is in zero position
*/

bool ArmToZero::isJointNegativeValue(int joint)
{
	if(joint_states_.position[joint] < -ACCEPTABLE_BOUND)
	{
        	return true;
        }
	else
	{
		return false;
	}
}



/**
*	Get Joint States
*/

void ArmToZero::getJointStates(const sensor_msgs::JointStateConstPtr &joint_state)
{
    setJointStateReaded(true);
    joint_states_ = *joint_state;
    return;
}

/**
*	Generates velocity command to arm.
*/

brics_actuator::JointVelocities ArmToZero::getVelocitiesMessage()
{

	brics_actuator::JointVelocities jointVelocitiesMsg;

	jointVelocitiesMsg.poisonStamp.originator = "";
	jointVelocitiesMsg.poisonStamp.description = "";
	jointVelocitiesMsg.poisonStamp.qos = 0.9;

	jointVelocitiesMsg.velocities.resize(num_joints_);
	
	for(size_t i=0; i < num_joints_; i++)
	{
		//jointVelocitiesMsg.velocities[i].timeStamp.secs = 0;	
		//jointVelocitiesMsg.velocities[i].timeStamp.nsecs = 0;	
		jointVelocitiesMsg.velocities[i].joint_uri = joint_names_[i];	
		jointVelocitiesMsg.velocities[i].unit = "rad";	
		if(!isJointOnGoal(i))
		{
			if(isJointPositiveValue(i))
			{
				jointVelocitiesMsg.velocities[i].value = -MAX_VELOCITY;	
			}
			else if(isJointNegativeValue(i))
			{
				jointVelocitiesMsg.velocities[i].value = MAX_VELOCITY;	
			}
			else
			{
				jointVelocitiesMsg.velocities[i].value = 0.0;	
			}
		}
		else
		{	
			jointVelocitiesMsg.velocities[i].value = 0.0;	
		}
	}

	return jointVelocitiesMsg;
}

/**
*	Generates velocity command to arm.
*/

brics_actuator::JointVelocities ArmToZero::getVelocitiesMessageZero()
{

	brics_actuator::JointVelocities jointVelocitiesMsg;

	jointVelocitiesMsg.poisonStamp.originator = "";
	jointVelocitiesMsg.poisonStamp.description = "";
	jointVelocitiesMsg.poisonStamp.qos = 0.9;

	jointVelocitiesMsg.velocities.resize(num_joints_);
	
	for(size_t i=0; i < num_joints_; i++)
	{
		//jointVelocitiesMsg.velocities[i].timeStamp.secs = 0;	
		//jointVelocitiesMsg.velocities[i].timeStamp.nsecs = 0;	
		jointVelocitiesMsg.velocities[i].joint_uri = joint_names_[i];	
		jointVelocitiesMsg.velocities[i].unit = "rad";	
		jointVelocitiesMsg.velocities[i].value = 0.0;	
	}

	return jointVelocitiesMsg;
}
/**
*	Publish velocity command to arm.
*/

void ArmToZero::publishVelocitiesMessage(const brics_actuator::JointVelocities velocities)
{
	std::string error_msg;
	error_msg = "Sending brics_actuator/JointVelocities Message";
	ROS_INFO("%s", error_msg.c_str());

        velocity_pub_.publish(velocities);
}

//=============================================================================
// MAIN                                                                       =
//=============================================================================

int main(int argc, char **argv)
{

	ros::init(argc,argv,"rescuer_arm_to_zero");

	ArmToZero arm2zero;    
	arm2zero.start();

	ros::Rate rate(arm2zero.getUpdateRate());

	ROS_INFO("Wait for /joint_states reading...");
	while(!arm2zero.isJointStateReaded() && ros::ok())
	{
		ros::spinOnce();
		rate.sleep();
	}
	ROS_INFO("Readed.");

	ROS_INFO("Moving arm to zero...");
	while(!arm2zero.isOnGoal() && ros::ok())
	{
		arm2zero.publishVelocitiesMessage(arm2zero.getVelocitiesMessage());
		ros::spinOnce();
		rate.sleep();
	}

	arm2zero.publishVelocitiesMessage(arm2zero.getVelocitiesMessageZero());
	ROS_INFO("Moved.");

	return 0;
}
