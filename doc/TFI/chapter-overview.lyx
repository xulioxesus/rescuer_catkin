#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Introducción
\begin_inset CommandInset label
LatexCommand label
name "chap:Introducción"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/roverPortonAbierto.jpg
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
RESCUER
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:rescuerPortonAbierto"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
RESCUER es un modelo de robot fabricado por la empresa Robotnik.
 La Universitat Jaume I cuenta con un modelo de este tipo.
 Al inicio del presente proyecto, RESCUER se encuentra prácticamente en
 desuso en el laboratorio TI2022DL.
\end_layout

\begin_layout Standard
RESCUER está formado por una base móvil y un brazo articulado (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:rescuerPortonAbierto"

\end_inset

).
 La base está traccionada por cadenas, lo que habilita al robot para moverse
 por terrenos irregulares e incluso subir y bajar escaleras.
 El robot no tiene montado ningún manipulador en el momento de comenzar
 el proyecto, por lo que no puede realizar tareas de agarre.
 Tampoco tiene ningún sensor visual ni de rango.
\end_layout

\begin_layout Standard
Tras conversaciones con la empresa Robotnik, se orienta el presente trabajo
 a hacer operativo el robot bajo la plataforma MoveIt!, conjunto de herramientas
 que funcionan con Robotic Operating System (en adelante, ROS
\begin_inset CommandInset nomenclature
LatexCommand nomenclature
symbol "ROS"
description "Robot Operating System"

\end_inset

).
 Mientras que ROS es un metasistema operativo para robots que proporciona
 funcionalidad de bajo nivel como construcción del sistema, paso de mensajes,
 controladores de dispositivos, etc.; MoveIt! proporciona herramientas relacionad
as con cinemática, planificación de movimiento y rutas, detección de colisiones,
 percepción 3D e interacción con el robot, entre otras.
\end_layout

\begin_layout Standard
En la 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:IntroduccionMoveIt"

\end_inset

 se puede ver el nodo move_group, que forma parte de la arquitectura fundamental
 de MoveIt!.
 El punto de partida de RESCUER es que no existe ninguno de los componentes
 que necesita la arquitectura de MoveIt!.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/introduccionMoveIt.jpg
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
El nodo move_group de MoveIt!
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:IntroduccionMoveIt"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/introduccionMoveItRobotControllers.jpg
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Robot Controllers
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:IntroduccionMoveItRobotControllers"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Los objetivos fundamentales del presente trabajo son los siguientes:
\end_layout

\begin_layout Itemize

\series bold
Controlar la base móvil desde ROS
\end_layout

\begin_layout Itemize

\series bold
Controlar el brazo robótico desde ROS
\end_layout

\begin_layout Standard
Estos dos objetivos se corresponden con Robot Controllers (marcado en rojo)
 de la 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:IntroduccionMoveItRobotControllers"

\end_inset

.
\end_layout

\begin_layout Standard
En los capítulos que siguen a esta introducción se verá la planificación
 del trabajo (
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Planificación"

\end_inset

), se hará una revisión de la arquitectura software y hardware existente
 de RESCUER (
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Revisión-hardware"

\end_inset

 y 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Revisión-software"

\end_inset

).
 En el siguiente capítulo (
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Arquitectura"

\end_inset

), se explica la arquitectura basada en ROS que se va a utilizar, así como
 algunos cambios en el hardware del robot para preservar el trabajo anterior.
 En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Controladores"

\end_inset

 se ven los controladores implementados para habilitar los movimientos del
 robot comentados con anterioridad.
 En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Teleoperacion"

\end_inset

 se muestra como teleoperar la base.
 En el capítulo siguiente, 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Pruebas-básicas"

\end_inset

, se prueban los controladores y se observa el movimiento real del robot.
 Finalmente y relacionado con los controladores, 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Problemas-hardware"

\end_inset

, se solucionan algunos problemas hardware encontrados a la hora de implementar
 los controladores.
 
\end_layout

\begin_layout Standard

\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status open

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
