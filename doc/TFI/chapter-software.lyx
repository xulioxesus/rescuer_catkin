#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Revisión del software
\begin_inset CommandInset label
LatexCommand label
name "chap:Revisión-software"

\end_inset


\end_layout

\begin_layout Standard
En este capítulo se estudia:
\end_layout

\begin_layout Itemize
La documentación existente sobre RESCUER.
\end_layout

\begin_layout Itemize
El software instalado en el robot mediante:
\end_layout

\begin_deeper
\begin_layout Itemize
exploración de disco duro del PC de control en busca de documentación, código
 ejecutable y código fuente.
\end_layout

\begin_layout Itemize
estudio del proceso de arranque del PC de control, en lo que se refiere
 a software relacionado con el control del robot.
\end_layout

\end_deeper
\begin_layout Section
Arquitectura JAUS
\end_layout

\begin_layout Standard
Según 
\begin_inset CommandInset citation
LatexCommand cite
key "Rob03"

\end_inset

, el sistema de control de la plataforma RESCUER está basado en la arquitectura
 JAUS
\begin_inset CommandInset nomenclature
LatexCommand nomenclature
symbol "JAUS"
description "Joint Architecture for Unmanned Systems"

\end_inset

 y, más concretamente, en la versión de código abierto OpenJAUS.
\end_layout

\begin_layout Standard
JAUS está diseñada para el control e interoperabilidad entre vehículos no
 tripulados, proporcionando las siguientes ventajas:
\end_layout

\begin_layout Itemize
Reduce el tiempo de desarrollo e integración del programa.
\end_layout

\begin_layout Itemize
Facilita la expansión del sistema, con nuevas capacidades.
\end_layout

\begin_layout Itemize
Fomenta la interoperabilidad.
\end_layout

\begin_layout Itemize
Proporciona un estándar con un conjunto de mensajes minuciosamente probados
 que permite la inserción de nuevos mensajes creados por el usuario.
\end_layout

\begin_layout Standard
La arquitectura de RESCUER que se debería encontrar en el robot es la que
 se puede observar en la 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:topologiaJAUSRescuer"

\end_inset

.
 Una serie de nodos agrupados en 
\begin_inset Quotes eld
\end_inset

Plataforma Móvil
\begin_inset Quotes erd
\end_inset

 que se encargan de los controladores del robot y otros nodos agrupados
 como 
\begin_inset Quotes eld
\end_inset

PC de Control
\begin_inset Quotes erd
\end_inset

 que se ocupan de la interacción mediante interfaz gráfica con el robot.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/topologiaJAUSenRescuer.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Topología JAUS en RESCUER
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:topologiaJAUSRescuer"

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Section
Aplicación de control
\end_layout

\begin_layout Standard
Según la documentación de RESCUER, en el directorio /opt/RESCUER se encuentran
 las librerías y código fuente necesario para la construcción de las aplicación
 de control o programas de nueva creación.
\end_layout

\begin_layout Standard
Se observa que esta descripción es irreal.
 No se encuentra instalado el software en la forma que indica la documentación
 original de 
\emph on
RESCUER.
\end_layout

\begin_layout Section
Conexión a RESCUER
\end_layout

\begin_layout Standard
Se procede a estudiar el software instalado en el robot.
 La forma de acceder al PC de control puede realizarse de dos formas:
\end_layout

\begin_layout Itemize
Directa.
\end_layout

\begin_layout Itemize
Remota.
\end_layout

\begin_layout Subsection
Conexión de forma directa
\end_layout

\begin_layout Standard
Para conectarse de forma directa hace falta conectar una pantalla, teclado
 y ratón al robot.
 Una nota pegada al robot contiene las credenciales de acceso.
\end_layout

\begin_layout Standard
La nota dice:
\end_layout

\begin_layout Itemize
En septiembre de 2010, Dan Puiu realiza la instalación de Ubuntu 8.10 LTS.
\end_layout

\begin_layout Itemize
Los datos de acceso:
\end_layout

\begin_deeper
\begin_layout Itemize
rescuer/rescuer007.
\end_layout

\begin_layout Itemize
sudo: tux007
\end_layout

\end_deeper
\begin_layout Standard
La documentación estudiada es de 2007 que, comparándola con la nota informativa
 en el robot, hace suponer que el sistema ha sufrido modificaciones desde
 que fué entregado a la universidad.
\end_layout

\begin_layout Subsection
Conexión de forma remota
\end_layout

\begin_layout Standard
El robot posee un router inalámbrico que crea una red con SSID 
\begin_inset Quotes eld
\end_inset

RESCUER
\begin_inset Quotes erd
\end_inset

.
 No tiene instalado el servicio DHCP, por lo que ha de configurarse la conexión
 de forma manual.
\end_layout

\begin_layout Paragraph
Configuración inalámbrica para equipo remoto.
\end_layout

\begin_layout Itemize
IP: 192.168.1.200
\end_layout

\begin_layout Itemize
Máscara: 255.255.255.0
\end_layout

\begin_layout Itemize
Gateway: 192.168.1.1
\end_layout

\begin_layout Paragraph
Acceso por ssh.
\end_layout

\begin_layout Itemize
ssh -l rescuer 192.168.1.100
\end_layout

\begin_layout Itemize
password: tux007
\end_layout

\begin_layout Section
Configuración del sistema
\end_layout

\begin_layout Standard
Una vez que se accede al robot, se procede a investigar qué programas y
 controladores se ponen en marcha cuando se arranca el sistema.
 Así mismo, se procede a investigar qué programas disponibles en la instalación
 sirven para interactuar con el hardware del robot.
\end_layout

\begin_layout Standard
Tras realizar esa investigación, se detallan a continuación los ficheros
 y programas relevantes.
\end_layout

\begin_layout Subsection
.bashrc
\end_layout

\begin_layout Standard
Como puede observarse en el contenido del fichero .bashrc (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:bashrc"

\end_inset

), que se ejecuta al inicio de cualquier terminal que iniciemos, se cargan
 las variables de entorno adecuadas para la compilación y enlazado del driver
 md_stln que se utiliza para controlar el brazo robótico.
\end_layout

\begin_layout Standard
En el terminal /dev/tty3 se pone en marcha player con la configuración contenida
 en rescuer.cfg (ver
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:rescuercfg"

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small},breaklines=true,language=bash"
inline false
status open

\begin_layout Plain Layout

#PLAYER 2.1
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/rescuer/opt/es
dcan-pci331-linux-2.6.x-x86-3.8.6/lib32:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/Desktop/player
/RESCUER_PLAYER/Build:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/rescuer/opt/es
dcan-pci331-linux-2.6.x-x86-3.8.6/lib32:/home/rescuer/Desktop/player/RESCUER_PLAYER/
Build:/home/rescuer/rescuer/opt/m5api/Device:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/Desktop/player
/RESCUER_PLAYER/Build:/home/rescuer/Desktop/player/RESCUER_PLAYER/lib:$LD_LIBRAR
Y_PATH
\end_layout

\begin_layout Plain Layout

export LD_LIBRARY_PATH=/home/rescuer/rescuer/Enviar/md_stln/lib/:/home/rescuer/p
layer-2.1/lib:/home/rescuer/Desktop/player/RESCUER_PLAYER/Build:/home/rescuer/Des
ktop/player/RESCUER_PLAYER/lib:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

export PKG_CONFIG_PATH=/home/rescuer/Desktop/player/player-2.1.3/libplayercore:/ho
me/rescuer/Desktop/player/player-2.1.3/libplayerxdr:/home/rescuer/Desktop/player/p
layer-2.1.3/libplayersd:/home/rescuer/Desktop/player/player-2.1.3/client_libs/libpla
yerc++:/home/rescuer/Desktop/player/player-2.1.3/client_libs/libplayerc:/home/resc
uer/Desktop/player/player-2.1.3/libplayertcp:$PKG_CONFIG_PATH
\end_layout

\begin_layout Plain Layout

echo "ROBOTNIK RESCUER"
\end_layout

\begin_layout Plain Layout

Terminal=`tty`
\end_layout

\begin_layout Plain Layout

case $Terminal in
\end_layout

\begin_layout Plain Layout

"/dev/tty3") sleep 4;
\end_layout

\begin_layout Plain Layout

cd /home/rescuer/player-2.1;
\end_layout

\begin_layout Plain Layout

./bin/player rescuer.cfg;;
\end_layout

\begin_layout Plain Layout

esac
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
.bashrc
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "img:bashrc"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
El contenido de la variable de entorno PKG_CONFIG_PATH queda como sigue:
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/libplayercore
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/libplayerxdr
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/libplayersd
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/client_libs/libplayerc++
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/client_libs/libplayerc
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/player-2.1.3/libplayertcp
\end_layout

\begin_layout Standard
Por otro lado, el contenido de la variable de entorno LD_LIBRARY_PATH queda
 del siguiente modo:
\end_layout

\begin_layout Itemize
/home/rescuer/rescuer/Enviar/md_stln/lib/
\end_layout

\begin_layout Itemize
/home/rescuer/player-2.1/lib
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/RESCUER_PLAYER/Build
\end_layout

\begin_layout Itemize
/home/rescuer/Desktop/player/RESCUER_PLAYER/lib
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

# Instantiate the example driver, which supports the position interface
\end_layout

\begin_layout Plain Layout

driver
\end_layout

\begin_layout Plain Layout

(
\end_layout

\begin_layout Plain Layout

	name "rescuer"
\end_layout

\begin_layout Plain Layout

	plugin "/home/rescuer/Desktop/player/RESCUER_PLAYER/bin/librescuer"
\end_layout

\begin_layout Plain Layout

	provides ["odometry:::position2d:0" "power:0"]
\end_layout

\begin_layout Plain Layout

	log_file "/home/rescuer/player-2.1/"
\end_layout

\begin_layout Plain Layout

)
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
rescuer.cfg
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "fig:rescuercfg"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
El driver del brazo tiene dependencias con librerías que se encuentran dispersas
 por el sistema de ficheros, de forma un tanto desorganizada.
\end_layout

\begin_layout Subsection
Scripts en /etc/init.d
\end_layout

\begin_layout Standard
Otros scripts relacionados con RESCUER que se ejecutan en el arranque del
 robot son:
\end_layout

\begin_layout Itemize
launch_player.sh
\end_layout

\begin_deeper
\begin_layout Itemize
Lanza el software player con la configuración del robot.
\end_layout

\end_deeper
\begin_layout Itemize
launch_playerjoy.sh
\end_layout

\begin_deeper
\begin_layout Itemize
lanza el software necesario para controlar el robot con un joystick.
\end_layout

\end_deeper
\begin_layout Itemize
load_esdcan.sh
\end_layout

\begin_deeper
\begin_layout Itemize
crea los nodos de comunicaciones para interactuar con el bus CAN
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=bash"
inline false
status open

\begin_layout Plain Layout

#!/bin/bash
\end_layout

\begin_layout Plain Layout

/home/rescuer/player-2.1/bin/player /home/rescuer/player-2.1/rescuer.cfg &
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
launch_player.sh
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

#!/bin/bash
\end_layout

\begin_layout Plain Layout

sleep 3
\end_layout

\begin_layout Plain Layout

/home/rescuer/player-2.1/bin/playerjoy -dev /dev/input/js0 &
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
launch_playerjoy.sh
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

#!/bin/sh
\end_layout

\begin_layout Plain Layout

mknod --mode=a+rw /dev/can0 c 50 0
\end_layout

\begin_layout Plain Layout

mknod --mode=a+rw /dev/can1 c 50 1
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
load_esdcan.sh
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Las acciones que se realizan en launch_playerjoy.sh y en .bashrc están duplicadas,
 pudiendo comentar parcialmente el fichero .bashrc.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small},breaklines=true,language=bash"
inline false
status open

\begin_layout Plain Layout

#PLAYER 2.1
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/rescuer/opt/es
dcan-pci331-linux-2.6.x-x86-3.8.6/lib32:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/Desktop/player
/RESCUER_PLAYER/Build:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/rescuer/opt/es
dcan-pci331-linux-2.6.x-x86-3.8.6/lib32:/home/rescuer/Desktop/player/RESCUER_PLAYER/
Build:/home/rescuer/rescuer/opt/m5api/Device:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

#export LD_LIBRARY_PATH=/home/rescuer/player-2.1/lib:/home/rescuer/Desktop/player
/RESCUER_PLAYER/Build:/home/rescuer/Desktop/player/RESCUER_PLAYER/lib:$LD_LIBRAR
Y_PATH
\end_layout

\begin_layout Plain Layout

export LD_LIBRARY_PATH=/home/rescuer/rescuer/Enviar/md_stln/lib/:/home/rescuer/p
layer-2.1/lib:/home/rescuer/Desktop/player/RESCUER_PLAYER/Build:/home/rescuer/Des
ktop/player/RESCUER_PLAYER/lib:$LD_LIBRARY_PATH
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

export PKG_CONFIG_PATH=/home/rescuer/Desktop/player/player-2.1.3/libplayercore:/ho
me/rescuer/Desktop/player/player-2.1.3/libplayerxdr:/home/rescuer/Desktop/player/p
layer-2.1.3/libplayersd:/home/rescuer/Desktop/player/player-2.1.3/client_libs/libpla
yerc++:/home/rescuer/Desktop/player/player-2.1.3/client_libs/libplayerc:/home/resc
uer/Desktop/player/player-2.1.3/libplayertcp:$PKG_CONFIG_PATH
\end_layout

\begin_layout Plain Layout

#echo "ROBOTNIK RESCUER"
\end_layout

\begin_layout Plain Layout

#Terminal=`tty`
\end_layout

\begin_layout Plain Layout

#case $Terminal in
\end_layout

\begin_layout Plain Layout

#"/dev/tty3") sleep 4;
\end_layout

\begin_layout Plain Layout

#cd /home/rescuer/player-2.1;
\end_layout

\begin_layout Plain Layout

#./bin/player rescuer.cfg;;
\end_layout

\begin_layout Plain Layout

#esac
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
.bashrc modificado
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En el fichero /etc/profile se ven las mismas líneas en load_esdcan.sh, con
 lo que pueden comentarse.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

#sudo mknod --mode=a+rw /dev/can0 c 50 0
\end_layout

\begin_layout Plain Layout

#sudo mknod --mode=a+rw /dev/can1 c 50 1
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
/etc/profile
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Software para mover el brazo
\end_layout

\begin_layout Standard
Para intentar poner en marcha el brazo robótico hay que ejecutar el servidor
 RemoteLog.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

rescuer@rescuer:~/standalone2/standalone2/RemoteLog/bin$ ./RemoteLog 15560
 ./log log.txt overwrite
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para poner en marcha el brazo:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

rescuer@rescuer:~/md_stln/bin$ ./md_stln
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
El resultado de este último comando se puede ver en la 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:md_stdln"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily},breaklines=true,language=sh"
inline false
status open

\begin_layout Plain Layout

SocketServer::Connect(): Server listening at port 15560, ip 127.0.0.1
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:39: 127.0.0.1: Main:Starting Up
\end_layout

\begin_layout Plain Layout

RemoteLog::ServerLoop: New connection
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::Setting UP
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::Start: the component
 is initialized
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::Start: launching the
 thread
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::ControlThread: Starting
 the thread 1
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::ControlThread: Starting
 the thread 2
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator:: switching to INIT
 state
\end_layout

\begin_layout Plain Layout

RemoteLog::ServerLoop: New connection
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:39: 127.0.0.1: PrimitiveManipulator::Start: Created ControlThre
ad
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: PM: InitModules: Successfull: PCube_openDevice
  0
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: PM: InitModules: Module 1 not found(-207)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: PM: InitModules: Error: PCube_openDevice
 -207
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: PM: InitModules: Switching to Emergency
 All Modules Halted 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: PrimitiveManipulator:: switching to EMERGENCY
 state
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: PrimitiveManipulator::Start: Nodes Not Initializ
ed
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: Main: Error running StartUp
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: main: Shutting Down the system
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: Main:ShuttingDown
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: RobotModular::Stop: Thread not running
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: RobotModular::ShutDown: Impossible because
 of it's not initialized
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: PrimitiveManipulator::Stop: Stopping the
 thread
\end_layout

\begin_layout Plain Layout

RemoteLog::ServerLoop: New connection
\end_layout

\begin_layout Plain Layout

++ 10-30-2013 19:58:40: 127.0.0.1: BarretHand::Stop: Thread not running
\end_layout

\begin_layout Plain Layout

** 10-30-2013 19:58:40: 127.0.0.1: BarretHand::ShutDown: Impossible because
 of it's not initialized
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: Connection closed
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: ending loop ...
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: Connection closed
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: ending loop ...
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: Connection closed
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: ending loop ...
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: Connection closed
\end_layout

\begin_layout Plain Layout

RemoteLog::SocketTask: ending loop ...
\end_layout

\begin_layout Plain Layout

RemoteLog::ServerLoop: New connection
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Resultado de ejecutar md_stdln
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "fig:md_stdln"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Tras intentar poner en marcha el brazo, se observa que la inicialización
 de éste falla, como también falla la inicialización de la mano de Barret,
 ya que ésta no se encuentra montada en el robot.
\end_layout

\begin_layout Section
Probar los motores del brazo individualmente
\end_layout

\begin_layout Standard
Según el fabricante del robot, para probar los módulos del brazo individualmente
 se requiere la instalación de Windows.
 Este sistema operativo no se encuentra disponible en el PC de control en
 este punto de la investigación.
\end_layout

\begin_layout Section
Conclusiones
\end_layout

\begin_layout Itemize
Aunque el software parece haber funcionado con anterioridad, en este punto
 del estudio no se puede poner en marcha.
 Se sospecha que el hardware no está funcionando correctamente.
\end_layout

\begin_layout Itemize
La tecnología JAUS se descarta porque el objetivo es utilizar ROS.
\end_layout

\begin_layout Itemize
Sin el uso de Windows resulta imposible comprobar el funcionamiento de los
 motores del brazo.
\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
