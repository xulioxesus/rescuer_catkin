#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% Linkfläche für Querverweise vergrößern und automatisch benenne
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasngerman{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{Kap.\negthinspace}
 \renewcommand{\subsectionautorefname}{Kap.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{Kap.\negthinspace}
 \renewcommand{\figureautorefname}{Abb.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% für den Fall, dass jemand die Bezeichnung "Gleichung" haben will
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% Setzt den Link für Sprünge zu Gleitabbildungen
% auf den Anfang des Gelitobjekts und nicht aufs Ende
\usepackage[figure]{hypcap}

% Die Seiten des Inhaltsverzeichnisses werden römisch numeriert,
% ein PDF-Lesezeichen für das Inhaltsverzeichnis wird hinzugefügt
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

%Vergrößert den Teil der Seite, in dem Gleitobjekte
% unten angeordnet werden dürfen
\renewcommand{\bottomfraction}{0.5}

% Vermeidet, dass Gleitobjekte vor ihrem Abschnitt gedruckt werden
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Rescuer URDF
\begin_inset CommandInset label
LatexCommand label
name "chap:PowercubeProgrammingGuide"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

<?xml version="1.0" ?>
\end_layout

\begin_layout Plain Layout

<robot name="rescuer" xmlns:controller="http://playerstage.sourceforge.net/gazebo/
xmlschema/#controller" xmlns:interface="http://playerstage.sourceforge.net/gazebo/
xmlschema/#interface" xmlns:sensor="http://playerstage.sourceforge.net/gazebo/xmls
chema/#sensor" xmlns:xacro="http://ros.org/wiki/xacro">
\end_layout

\begin_layout Plain Layout

  <link name="base"/>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_0_joint" type="fixed">
\end_layout

\begin_layout Plain Layout

    <pose rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

    <parent link="base"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_0_link"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_0_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="1.0"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.01" ixy="0" ixz="0" iyy="0.01" iyz="0" izz="0.01"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <cylinder length="0.18" radius="0.045"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/LightGrey" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Red"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0.005"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <box size="0.01 0.01 0.01"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_1_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="0 0 3.1415926" xyz="0 0 -0.240"/>
\end_layout

\begin_layout Plain Layout

    <!--origin xyz="0 0 -0.240" rpy="0 0 1.5708"/-->
\end_layout

\begin_layout Plain Layout

    <parent link="arm_0_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_1_link"/>
\end_layout

\begin_layout Plain Layout

    <axis xyz="0 0 1"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="10"/>
\end_layout

\begin_layout Plain Layout

    <limit effort="370" lower="-6.2831853" upper="6.2831853" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="50" soft_lower_limit="-6.27318
53" soft_upper_limit="6.2731853"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_1_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="0.29364"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.01" ixy="0" ixz="0" iyy="0.01" iyz="0" izz="0.01"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular1.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/Blue" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Blue"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular1.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_2_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="1.5708 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

    <parent link="arm_1_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_2_link"/>
\end_layout

\begin_layout Plain Layout

    <axis xyz="0 0 1"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="10"/>
\end_layout

\begin_layout Plain Layout

    <limit effort="370" lower="-2.0943951" upper="2.0943951" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="50" soft_lower_limit="-2.0843"
 soft_upper_limit="2.0843"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_2_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="1.68311"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.03" ixy="0" ixz="0" iyy="0.03" iyz="0" izz="0.03"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular2.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/LightGrey" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Red"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular2.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_3_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="-1.5708 0 0" xyz="0 -0.395 0"/>
\end_layout

\begin_layout Plain Layout

    <parent link="arm_2_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_3_link"/>
\end_layout

\begin_layout Plain Layout

    <axis xyz="0 1 0"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="5"/>
\end_layout

\begin_layout Plain Layout

    <limit effort="176" lower="-6.2831853" upper="6.2831853" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="25" soft_lower_limit="-6.27318
53" soft_upper_limit="6.2731853"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_3_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="-1.5708 0 0" xyz="0 -0.395 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="2.1"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.03" ixy="0" ixz="0" iyy="0.03" iyz="0" izz="0.03"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular3.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/Blue" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Blue"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular3.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_4_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="1.5708 0 0" xyz="0 0 -0.2715"/>
\end_layout

\begin_layout Plain Layout

    <parent link="arm_3_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_4_link"/>
\end_layout

\begin_layout Plain Layout

    <axis xyz="0 1 0"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="5"/>
\end_layout

\begin_layout Plain Layout

    <limit effort="176" lower="-2.0943951" upper="2.0943951" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="25" soft_lower_limit="-2.08439
51" soft_upper_limit="2.0843951"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_4_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="1.68311"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.03" ixy="0" ixz="0" iyy="0.03" iyz="0" izz="0.03"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular4.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/LightGrey" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Red"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular4.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_5_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="1.5708 -1.5708 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

    <parent link="arm_4_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_5_link"/>
\end_layout

\begin_layout Plain Layout

    <!--axis xyz="1 0 0" /-->
\end_layout

\begin_layout Plain Layout

    <axis xyz="1 0 0"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="10.0" friction="10.0"/>
\end_layout

\begin_layout Plain Layout

    <!--limit effort="41.6" velocity="0.4363" lower="-1.57079" upper="1.57079"
 /-->
\end_layout

\begin_layout Plain Layout

    <!--limit effort="20" velocity="2" /-->
\end_layout

\begin_layout Plain Layout

    <!--safety_controller k_position="20" k_velocity="25" soft_lower_limit="${-1.
57079 + 0.01}" soft_upper_limit="${1.57079 - 0.01}" /-->
\end_layout

\begin_layout Plain Layout

    <limit effort="176" lower="-6.2831853" upper="6.2831853" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="25" soft_lower_limit="-6.27318
53" soft_upper_limit="6.2731853"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_5_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="0.807"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.03" ixy="0" ixz="0" iyy="0.03" iyz="0" izz="0.03"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular5.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/Blue" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Blue"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular5.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

  <joint name="arm_6_joint" type="revolute">
\end_layout

\begin_layout Plain Layout

    <origin rpy="0 0 0" xyz="0 0 0.1847"/>
\end_layout

\begin_layout Plain Layout

    <parent link="arm_5_link"/>
\end_layout

\begin_layout Plain Layout

    <child link="arm_6_link"/>
\end_layout

\begin_layout Plain Layout

    <axis xyz="0 0 1"/>
\end_layout

\begin_layout Plain Layout

    <calibration rising="0.1"/>
\end_layout

\begin_layout Plain Layout

    <dynamics damping="5"/>
\end_layout

\begin_layout Plain Layout

    <limit effort="20.1" lower="-2.0943951" upper="2.0943951" velocity="2.0"/>
\end_layout

\begin_layout Plain Layout

    <safety_controller k_position="20" k_velocity="25" soft_lower_limit="-2.08439
51" soft_upper_limit="2.0843951"/>
\end_layout

\begin_layout Plain Layout

  </joint>
\end_layout

\begin_layout Plain Layout

  <link name="arm_6_link">
\end_layout

\begin_layout Plain Layout

    <inertial>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <mass value="0.819"/>
\end_layout

\begin_layout Plain Layout

      <inertia ixx="0.01" ixy="0" ixz="0" iyy="0.01" iyz="0" izz="0.01"/>
\end_layout

\begin_layout Plain Layout

    </inertial>
\end_layout

\begin_layout Plain Layout

    <visual>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular6.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

      <!-- material name="Schunk/LightGrey" /-->
\end_layout

\begin_layout Plain Layout

      <material name="Red"/>
\end_layout

\begin_layout Plain Layout

    </visual>
\end_layout

\begin_layout Plain Layout

    <collision>
\end_layout

\begin_layout Plain Layout

      <origin rpy="0 0 0" xyz="0 0 0"/>
\end_layout

\begin_layout Plain Layout

      <geometry>
\end_layout

\begin_layout Plain Layout

        <mesh filename="package://rescuer_description/meshes/modular/modular6.stl
"/>
\end_layout

\begin_layout Plain Layout

      </geometry>
\end_layout

\begin_layout Plain Layout

    </collision>
\end_layout

\begin_layout Plain Layout

  </link>
\end_layout

\begin_layout Plain Layout

</robot>
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "biblio/Plasma"
options "biblio/alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
