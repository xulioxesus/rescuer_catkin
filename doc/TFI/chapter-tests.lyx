#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Pruebas básicas
\begin_inset CommandInset label
LatexCommand label
name "chap:Pruebas-básicas"

\end_inset


\end_layout

\begin_layout Section
Lanzar controlador de la base móvil
\end_layout

\begin_layout Standard
Vía 
\emph on
rosrun
\emph default
:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosrun rescuer_controllers base_controller
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
O vía 
\emph on
roslaunch
\emph default
:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_controllers base_controller.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Lanzar controlador del brazo
\end_layout

\begin_layout Standard
Cargar la configuración del controlador ubicada en:
\end_layout

\begin_layout Itemize

\emph on
src/rescuer_controllers/config/powercube_chain.yaml
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosparam load powercube_chain.yaml
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Cargar la descripción del brazo robótico ubicada en
\emph on
:
\end_layout

\begin_layout Itemize

\emph on
src/rescuer_description/urdf/rescuer.urdf
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosparam set /powercube_chain/robot_description --textfile rescuer.urdf
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Arrancar el nodo de control de schunk_powercube_chain:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosrun schunk_powercube_chain schunk_powercube_chain
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Invocar al servicio que inicia el controlador del brazo:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosservice call /powercube_chain/init
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
El resultado de ejecutar el servicio:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosservice call /powercube_chain/init[ INFO] [1446915637.387410000]: Parameter
 force_use_movevel set, using moveVel
\end_layout

\begin_layout Plain Layout

[ INFO] [1446915787.810503420]: Initializing powercubes...
\end_layout

\begin_layout Plain Layout

 D  O  F  :6
\end_layout

\begin_layout Plain Layout

========================================================
\end_layout

\begin_layout Plain Layout

PowerCubeCtrl:Init: Trying to initialize with the following parameters:
 
\end_layout

\begin_layout Plain Layout

DOF: 6
\end_layout

\begin_layout Plain Layout

CanModule: ESD
\end_layout

\begin_layout Plain Layout

CanDevice: 1/dev/can1
\end_layout

\begin_layout Plain Layout

CanBaudrate: 500
\end_layout

\begin_layout Plain Layout

ModulIDs: 1 2 3 4 5 6 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

maxVel: 2 2 2 2 2 2 
\end_layout

\begin_layout Plain Layout

maxAcc: 0.5 0.5 0.5 0.5 0.5 0.5 
\end_layout

\begin_layout Plain Layout

upperLimits: 6.28319 2.0944 6.28319 2.0944 6.28319 2.0944 
\end_layout

\begin_layout Plain Layout

lowerLimits: -6.28319 -2.0944 -6.28319 -2.0944 -6.28319 -2.0944 
\end_layout

\begin_layout Plain Layout

offsets: 0.1 0.1 0.1 0.1 0.1 0.1 
\end_layout

\begin_layout Plain Layout

========================================================
\end_layout

\begin_layout Plain Layout

initstring = ESD:1/dev/can1,500
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Tras ejecutar 
\emph on
rosparam list:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

/powercube_chain/OperationMode
\end_layout

\begin_layout Plain Layout

/powercube_chain/can_baudrate 
\end_layout

\begin_layout Plain Layout

/powercube_chain/can_device 
\end_layout

\begin_layout Plain Layout

/powercube_chain/can_module 
\end_layout

\begin_layout Plain Layout

/powercube_chain/force_use_movevel 
\end_layout

\begin_layout Plain Layout

/powercube_chain/frequency 
\end_layout

\begin_layout Plain Layout

/powercube_chain/horizon /powercube_chain/joint_names 
\end_layout

\begin_layout Plain Layout

/powercube_chain/max_accelerations 
\end_layout

\begin_layout Plain Layout

/powercube_chain/max_error 
\end_layout

\begin_layout Plain Layout

/powercube_chain/min_publish_duration 
\end_layout

\begin_layout Plain Layout

/powercube_chain/modul_ids 
\end_layout

\begin_layout Plain Layout

/powercube_chain/robot_description 
\end_layout

\begin_layout Plain Layout

/rosdistro /roslaunch/uris/host_xulioxesus_desktop__41569 
\end_layout

\begin_layout Plain Layout

/roslaunch/uris/host_xulioxesus_desktop__45686 /rosversion /run_id 
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Lanzar nodo de teleoperación con el teclado
\end_layout

\begin_layout Standard
Vía 
\emph on
rosrun
\emph default
:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rosrun rescuer_teleop rescuer_teleop_key
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
O vía 
\emph on
roslaunch
\emph default
:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_teleop keyboard_teleop.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Una vez lanzado el nodo de teleoperación y con el controlador de la base
 funcionando, se puede mover la base móvil con las flechas del teclado y
 pararla con la tecla s.
\end_layout

\begin_layout Standard

\series bold
\noun on
¡El objetivo de mover la base se cumple!
\end_layout

\begin_layout Section
Mandar mensajes al controlador del brazo
\end_layout

\begin_layout Standard
Publicando un mensaje en 
\emph on
/powercube_chain/command_vel
\emph default
 de tipo 
\emph on
brics_actuator/JointVelocities.
\end_layout

\begin_layout Standard

\series bold
\noun on
¡El objetivo de mover el brazo se cumple!
\end_layout

\begin_layout Standard
En el directorio 
\emph on
src/rescuer_controllers/tests
\emph default
 se crean varios ficheros para mandar mensajes a los motores del brazo.
\end_layout

\begin_layout Standard
Los ficheros con el patrón arm(
\emph on
Número
\emph default
)(
\emph on
pos
\emph default
|
\emph on
neg
\emph default
).msg envían un mensaje al controlador del brazo para mover el motor que
 coincida con 
\emph on
Número
\emph default
 utilizando una velocidad angular positiva o negativa de valor absoluto
 0'2 rad/s.
 Se envían velocidades a los seis motores, pero solamente el motor a probar
 recibe una velocidad distinta a cero.
\end_layout

\begin_layout Standard
Se muestra como ejemplo el contenido de arm2pos.msg (
\begin_inset CommandInset ref
LatexCommand ref
reference "img:TestsArm2posMsg"

\end_inset

):
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

rostopic pub -r 70 /powercube_chain/command_vel brics_actuator/JointVelocities
 '{poisonStamp:  {originator: '', description: '',  qos: 0.9}, velocities:[
 {timeStamp:{ secs: 0, nsecs: 0}, joint_uri: 'arm_1_joint', unit: 'rad',
 value: 0.0},{timeStamp:{ secs: 0, nsecs: 0}, joint_uri: 'arm_2_joint', unit:
 'rad', value: 0.2},{timeStamp:{ secs: 0, nsecs: 0}, joint_uri: 'arm_3_joint',
 unit: 'rad', value: 0.0},{timeStamp:{ secs: 0, nsecs: 0}, joint_uri: 'arm_4_join
t', unit: 'rad', value: 0.0},{timeStamp:{ secs: 0, nsecs: 0}, joint_uri:
 'arm_5_joint', unit: 'rad', value: 0.0},{timeStamp:{ secs: 0, nsecs: 0},
 joint_uri: 'arm_6_joint', unit: 'rad', value: 0.0}, ]}'
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
arm2pos.msg
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:TestsArm2posMsg"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Mover individualmente un motor
\end_layout

\begin_layout Standard
Mediante los ficheros de 
\emph on
tests:
\end_layout

\begin_layout Itemize
arm1neg.msg
\end_layout

\begin_layout Itemize
arm1pos.msg
\end_layout

\begin_layout Itemize
arm2neg.msg
\end_layout

\begin_layout Itemize
arm2pos.msg
\end_layout

\begin_layout Itemize
arm3neg.msg
\end_layout

\begin_layout Itemize
arm3pos.msg
\end_layout

\begin_layout Itemize
arm4neg.msg
\end_layout

\begin_layout Itemize
arm4pos.msg
\end_layout

\begin_layout Itemize
arm5neg.msg
\end_layout

\begin_layout Itemize
arm5pos.msg
\end_layout

\begin_layout Itemize
arm6neg.msg
\end_layout

\begin_layout Itemize
arm6pos.msg
\end_layout

\begin_layout Subsection*
Mover los motores conjuntamente
\end_layout

\begin_layout Itemize
encoger.msg
\end_layout

\begin_deeper
\begin_layout Itemize
Todos los motores reciben una velocidad de 0'2 rad/s
\end_layout

\end_deeper
\begin_layout Itemize
estirar.msg
\end_layout

\begin_deeper
\begin_layout Itemize
Todos los motores reciben una velocidad de -0'2 rad/s
\end_layout

\end_deeper
\begin_layout Subsection*
Parar todos los motores
\end_layout

\begin_layout Itemize
parar.msg 
\end_layout

\begin_deeper
\begin_layout Itemize
Todos los motores reciben una velocidad de 0 rad/s
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
