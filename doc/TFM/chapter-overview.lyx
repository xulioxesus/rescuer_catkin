#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Introducción
\begin_inset CommandInset label
LatexCommand label
name "chap:Introducción"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/brazoPosicionHome3.jpg
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
RESCUER
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:rescuerPortonAbierto"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\emph on
RESCUER
\emph default
 es un modelo de robot fabricado por la empresa 
\emph on
Robotnik
\emph default
.
 La Universitat Jaume I cuenta con un modelo de este tipo.
 
\end_layout

\begin_layout Standard

\emph on
RESCUER
\emph default
 está formado por una base móvil y un brazo articulado (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:rescuerPortonAbierto"

\end_inset

).
 La base está traccionada por cadenas, lo que habilita al robot para moverse
 por terrenos irregulares e incluso subir y bajar escaleras.
 El robot no tiene montado ningún manipulador en el momento de comenzar
 el proyecto, por lo que no puede realizar tareas de agarre.
 Tampoco tiene ningún sensor visual ni de rango.
\end_layout

\begin_layout Standard
Al inicio del presente proyecto, 
\emph on
RESCUER
\emph default
 está funcionando utilizando Robotic Operating System (en adelante, ROS
\begin_inset CommandInset nomenclature
LatexCommand nomenclature
symbol "ROS"
description "Robot Operating System"

\end_inset

).
 Se puede controlar la base móvil para desplazar el robot y se pueden realizar
 movimientos de lo motores que forman el brazo.
\end_layout

\begin_layout Standard
Tal y como se refleja en 
\begin_inset Quotes eld
\end_inset

Adaptación del robot 
\emph on
RESCUER
\emph default
 a 
\emph on
ROS
\emph default

\begin_inset Quotes erd
\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "Leo01"

\end_inset

, interesa hacer operativo el robot bajo la plataforma MoveIt!, conjunto
 de herramientas que funcionan con 
\emph on
ROS
\emph default
, que permite realizar tareas de planificación automática.
 También interesa construir una versión simulada del robot para poder realizar
 estudios sin necesidad de tener físicamente el robot.
 Por último, ya que se dispone de una base móvil, también resulta de interés
 integrar el robot con la pila de navegación de 
\emph on
ROS
\emph default
, lo que permitiría realizar la planificación de movimientos de la base.
\end_layout

\begin_layout Standard
Los objetivos fundamentales del presente trabajo son los siguientes:
\end_layout

\begin_layout Itemize

\series bold
Realizar tareas de planificación automática para mover el brazo robótico
 y la mano (no instalada).
\end_layout

\begin_layout Itemize

\series bold
Integrar el robot con la pila de navegación de 
\emph on
ROS
\emph default
, para realizar tareas de planificación de movimiento de la base de forma
 autónoma.
\end_layout

\begin_layout Itemize

\series bold
Crear una versión simulada del robot e integrarlo con la planificación automátic
a y la navegación autónoma.
\end_layout

\begin_layout Standard
El trabajo realizado se refleja en la presente memoria que se compone de
 los capítulos siguientes:
\end_layout

\begin_layout Standard
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Introducción"

\end_inset

, esta introducción.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Planificación"

\end_inset

, se ve la planificación y la metodología de trabajo.
\end_layout

\begin_layout Standard
En el siguiente capítulo (
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Arquitectura"

\end_inset

), se explica la arquitectura, basada en 
\emph on
ROS
\emph default
 y el simulador, que se utiliza.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:rescuer_description"

\end_inset

, se construye la descripción del robot, necesaria para la práctica totalidad
 de componentes del sistema.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Controladores"

\end_inset

, se ven los controladores implementados y configurados para trabajar con
 el hardware del robot.
\end_layout

\begin_layout Standard
En el capítulo siguiente, 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Bringup"

\end_inset

, se crea una interfaz, en forma de paquete catkin, para trabajar de manera
 más sencilla con los controladores y la descripción del robot.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Planificación.-MoveIt!."

\end_inset

, se crea la configuración de 
\emph on
MoveIt!
\emph default
.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Simulación.-Gazebo."

\end_inset

, se crea la configuración adecuada para el simulador.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Navegación"

\end_inset

, se explican las tareas realizadas que tienen que ver con la navegación.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Problemas"

\end_inset

, se detallan algunos problemas encontrados durante la realización del proyecto.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Guia"

\end_inset

, se crea una guía rápida para la utiliza
\emph on
ción de RESCUER
\emph default
.
\end_layout

\begin_layout Standard
En el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Conclusiones"

\end_inset

, aparecen las conclusiones extraídas tras la ejecución del proyecto.
\end_layout

\begin_layout Standard
Finalmente, en el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:TrabajoFuturo"

\end_inset

, se listan las propuestas de trabajo futuro que se han ido viendo a lo
 largo del proyecto.
\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status open

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
