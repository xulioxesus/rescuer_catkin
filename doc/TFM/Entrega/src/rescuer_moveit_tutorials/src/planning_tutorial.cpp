/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta */

#include <ros/ros.h>


#include <eigen_conversions/eigen_msg.h>
#include <visualization_msgs/Marker.h>
#include <pluginlib/class_loader.h>

//#include <geometry_msgs/Pose.h>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit_msgs/DisplayTrajectory.h>
//#include <moveit/robot_model_loader/robot_model_loader.h>
//#include <moveit/planning_scene/planning_scene.h>
//#include <moveit/kinematic_constraints/utils.h>
//#include <eigen_conversions/eigen_msg.h>
//#include <moveit_msgs/PlanningScene.h>
//#include <moveit_msgs/AttachedCollisionObject.h>
//#include <moveit_msgs/GetStateValidity.h>
//#include <moveit_msgs/DisplayRobotState.h>
//#include <moveit/robot_state/robot_state.h>
//#include <moveit/robot_state/conversions.h>
//#include <moveit/move_group_interface/move_group.h>
//#include <tf/transform_listener.h>


ros::Publisher display_publisher;
ros::Subscriber marker_subscriber; 
ros::WallDuration sleep_time(15.0);
planning_interface::PlannerManagerPtr planner_instance;
planning_scene::PlanningScenePtr scene;

bool planned = false;

void trajectoryToMarker(const visualization_msgs::Marker inputMarker)
{
	planned = true;
	marker_subscriber.shutdown();

	planning_interface::MotionPlanRequest req;
	planning_interface::MotionPlanResponse res;
	geometry_msgs::PoseStamped pose;
	pose.header.frame_id = "arm_6_link";
	pose.pose.position.x = inputMarker.pose.position.x - 0.4;
	pose.pose.position.y = inputMarker.pose.position.y;
	pose.pose.position.z = inputMarker.pose.position.z;
	pose.pose.orientation.w = 1.0;

	std::vector<double> tolerance_pose(3, 0.01);
	std::vector<double> tolerance_angle(3, 0.01);

	req.group_name = "arm";
	req.allowed_planning_time = 60;
	req.workspace_parameters.header.frame_id = "base_footprint";
        req.workspace_parameters.max_corner.x = 3;
        req.workspace_parameters.max_corner.y = 3;
        req.workspace_parameters.max_corner.z = 3;
        req.workspace_parameters.min_corner.x = -1;
        req.workspace_parameters.min_corner.y = -1;
        req.workspace_parameters.min_corner.z = 0;
	moveit_msgs::Constraints pose_goal = kinematic_constraints::constructGoalConstraints("arm_6_link", pose, tolerance_pose, tolerance_angle);
	req.goal_constraints.push_back(pose_goal);

	planning_interface::PlanningContextPtr context = planner_instance->getPlanningContext(scene, req, res.error_code_);
	context->solve(res);

	if(res.error_code_.val != res.error_code_.SUCCESS)
	{
	  ROS_ERROR("Could not compute plan successfully");
	  return;
	}


	moveit_msgs::DisplayTrajectory display_trajectory;

	/* Visualize the trajectory */
	ROS_INFO("Visualizing the trajectory");
	moveit_msgs::MotionPlanResponse response;
	res.getMessage(response);

	display_trajectory.trajectory_start = response.trajectory_start;
	display_trajectory.trajectory.push_back(response.trajectory);
	display_publisher.publish(display_trajectory);

	sleep_time.sleep();
	return;
}

int main(int argc, char **argv)
{
	ros::init (argc, argv, "move_group_tutorial");
	ros::NodeHandle node_handle;

	ros::Rate rate(100);

	robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
	robot_model::RobotModelPtr robot_model = robot_model_loader.getModel();

	scene.reset(new planning_scene::PlanningScene(robot_model));

	boost::scoped_ptr<pluginlib::ClassLoader<planning_interface::PlannerManager> > planner_plugin_loader;
	std::string planner_plugin_name;

	if (!node_handle.getParam("planning_plugin", planner_plugin_name))
		ROS_FATAL_STREAM("Could not find planner plugin name");
	try
	{
		planner_plugin_loader.reset(new pluginlib::ClassLoader<planning_interface::PlannerManager>("moveit_core", "planning_interface::PlannerManager"));
	}
	catch(pluginlib::PluginlibException& ex)
	{
		ROS_FATAL_STREAM("Exception while creating planning plugin loader " << ex.what());
	}

	try
	{
		planner_instance.reset(planner_plugin_loader->createUnmanagedInstance(planner_plugin_name));
		if (!planner_instance->initialize(robot_model, node_handle.getNamespace()))
			ROS_FATAL_STREAM("Could not initialize planner instance");
		ROS_INFO_STREAM("Using planning interface '" << planner_instance->getDescription() << "'");
	}
	catch(pluginlib::PluginlibException& ex)
	{
		const std::vector<std::string> &classes = planner_plugin_loader->getDeclaredClasses();
		std::stringstream ss;
		for (std::size_t i = 0 ; i < classes.size() ; ++i)
			ss << classes[i] << " ";
		ROS_ERROR_STREAM("Exception while loading planner '" << planner_plugin_name << "': " << ex.what() << std::endl
				<< "Available plugins: " << ss.str());
	}

	/* Sleep a little to allow time to startup rviz, etc. */
	sleep_time.sleep();


	display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
	marker_subscriber = node_handle.subscribe ("/detect_cylinder/odom_combined/cluster_marker", 1, trajectoryToMarker);


	while(ros::ok() && !planned)
	{

		ros::spinOnce();
		rate.sleep();
	}
}

