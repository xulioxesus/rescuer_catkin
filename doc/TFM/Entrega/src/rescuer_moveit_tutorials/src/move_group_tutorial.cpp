/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta */

#include <ros/ros.h>


#include <eigen_conversions/eigen_msg.h>
#include <visualization_msgs/Marker.h>
#include <pluginlib/class_loader.h>

//#include <geometry_msgs/Pose.h>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <geometry_msgs/TransformStamped.h>

#include <math.h>

#include <tf/transform_broadcaster.h>

//#include <moveit_msgs/DisplayRobotState.h>
//#include <moveit_msgs/DisplayTrajectory.h>

//#include <moveit_msgs/AttachedCollisionObject.h>
//#include <moveit_msgs/CollisionObject.h>
//#include <moveit/robot_model_loader/robot_model_loader.h>
//#include <moveit/planning_scene/planning_scene.h>
//#include <moveit/kinematic_constraints/utils.h>
//#include <eigen_conversions/eigen_msg.h>
//#include <moveit_msgs/PlanningScene.h>
//#include <moveit_msgs/AttachedCollisionObject.h>
//#include <moveit_msgs/GetStateValidity.h>
//#include <moveit_msgs/DisplayRobotState.h>
//#include <moveit/robot_state/robot_state.h>
//#include <moveit/robot_state/conversions.h>
//#include <moveit/move_group_interface/move_group.h>
//#include <tf/transform_listener.h>


ros::Publisher display_publisher;
ros::Subscriber marker_subscriber; 
geometry_msgs::Pose target_pose1;

bool planned = false;

void trajectoryToMarker(const visualization_msgs::Marker inputMarker)
{
	planned = true;
	marker_subscriber.shutdown();
	//target_pose1.header.frame_id = "arm_6_link";
	//target_pose1.orientation.w = 1.0;
	target_pose1.position.x = inputMarker.pose.position.x;
	target_pose1.position.y = inputMarker.pose.position.y;
	target_pose1.position.z = inputMarker.pose.position.z;
	//target_pose1.position.x = 1.31286461353;
	//target_pose1.position.y = 0.54036896348;
	//target_pose1.position.z = 0.517397403717;

	return;
}

int main(int argc, char **argv)
{
	ros::init (argc, argv, "move_group_tutorial");
	ros::AsyncSpinner spinner(1);
  	spinner.start();
	ros::NodeHandle node_handle;

	marker_subscriber = node_handle.subscribe ("/detect_cylinder/base_footprint/cluster_marker", 1, trajectoryToMarker);
	display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);

	
	while(!planned)
	{
	    ros::WallDuration sleep_t(0.5);
	    sleep_t.sleep();
	}

	moveit::planning_interface::MoveGroup group("arm");
	//sleep(10.0);
	moveit::planning_interface::PlanningSceneInterface planning_scene_interface;  

	// (Optional) Create a publisher for visualizing plans in Rviz.
	moveit_msgs::DisplayTrajectory display_trajectory;


	//group.setEndEffectorLink("hand_base_link");
	//group.setPlannerId("PRMstarkConfigDefault");
	ROS_INFO("End effector link: %s", group.getEndEffectorLink().c_str());    
	group.setPlannerId("RRTConnectkConfigDefault");
	//group.setPlannerId("KPIECEkConfigDefault");
	group.setPlanningTime(20);
	group.setNumPlanningAttempts(5);
	group.setGoalPositionTolerance(0.4);
	group.setGoalOrientationTolerance(0.3);
	//group.setGoalJointTolerance(0.2);
	//group.setGoalTolerance(0.2);
	//group.setPoseReferenceFrame("odom_combined");
	geometry_msgs::PoseStamped current = group.getCurrentPose();

	current.pose.position.x = target_pose1.position.x;
	current.pose.position.y = target_pose1.position.y;
	current.pose.position.z = target_pose1.position.z;

	tf::Quaternion q = tf::createQuaternionFromRPY(M_PI/2,-M_PI/2,0);
	current.pose.orientation.x = q.getX();
	current.pose.orientation.y = q.getY();
	current.pose.orientation.z = q.getZ();
	current.pose.orientation.w = q.getW();
	//current.pose.position.x = 1.1;
	//current.pose.position.y = 0;
	//current.pose.position.z = 0.23;
	//current.pose.position.x = 0.8;
	//current.pose.position.y = 0;
	//current.pose.position.z += 0;
	group.setWorkspace(-3,-3,-3,3,3,3);
	//group.setApproximateJointValueTarget(target_pose1);
	//group.setApproximateJointValueTarget(current);
	group.setPoseTarget(current);

	moveit::planning_interface::MoveGroup::Plan my_plan;
	bool success = group.plan(my_plan);

	if(success)
	{
		ROS_INFO("Visualizing plan 1 (again)");    
		display_trajectory.trajectory_start = my_plan.start_state_;
		display_trajectory.trajectory.push_back(my_plan.trajectory_);
		display_publisher.publish(display_trajectory);
		group.execute(my_plan);
	}
	return 0;
}

