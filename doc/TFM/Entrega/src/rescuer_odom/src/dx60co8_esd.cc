   /** \file dx60co8_esd.c
 * \author Robotnik Automation S.L.L.
 * \author robotnik
 * \version 1.0
 * \date    2007-2008
 *
 * \brief Header for Servo Motor Driver DX60CO8 serie
 * Compatible with ESD/PCI331 can boards.  
 * (C) 2008 Robotnik Automation, SLL
 * 
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <sys/ioctl.h>
#include <math.h>
#include <termios.h>
#include <errno.h>

#include "dx60co8_esd.h"
#include "defines.h"


/*! \fn int dx60co8_Connect(int *handle, int NetChannel, int baudrate)
*   \brief Connect to the CAN bus NetChannel
*	\param handle an integer, is the can file descriptor for the CAN device
*   \return CAN_ERROR
*	\return CAN_OK
*/
int dx60co8_Connect(int *handle, int NetChannel, int baudrate)
{
	long dRet;
	int i;
	int32_t       txbufsize     = TXBUFSIZE;
	int32_t       rxbufsize     = RXBUFSIZE;
	int32_t       txtout        = TXTOUT;
	int32_t       rxtout        = RXTOUT;
	
	dRet = canOpen(NetChannel,	// NET number
			CAN_OPEN_MODE,		// opening mode
			txbufsize,			// Tamaño de la cola de salida
			rxbufsize,			// Tamaño de la cola de entrada
			txtout,				// Timeout para escribir
			rxtout,				// Timeout para leer
			handle);
	
	if( dRet != NTCAN_SUCCESS){
		printf("dx60co8_Connect: Failed opening CAN connection\n");
		return CAN_ERROR;
	}

	dRet = canSetBaudrate(*handle, baudrate); 
	if( dRet != NTCAN_SUCCESS){
		printf("dx60co8_Connect: Failed setting baudrate\n");
		return CAN_ERROR;
	}

	for(i=0; i<2048; i++){
		dRet = canIdAdd(*handle, i); 

		if( dRet != NTCAN_SUCCESS){
			printf("dx60co8_Connect: Failed in canIdAd: \n");
			return CAN_ERROR;
		}
	}
	
	
	return CAN_OK;
}


/*! \fn int dx60co8_CloseCan(int fd)
*   \brief Connect to the CAN bus NetChannel
*	\param handle an integer, is the can file descriptorCAN device
*   \return CAN_ERROR
*	\return CAN_OK
*/
int dx60co8_CloseCan(int fd){
	long dRet;
	dRet = canClose(fd);
	if( dRet != NTCAN_SUCCESS){
		printf("dx60co8_CloseCan: Fallo al cerrar la conexión CAN\n");
		return CAN_ERROR;		
	}
	
	return CAN_OK;
}


/*! \fn int dx60co8_SendMsg(int handle, CMSG *message, int32_t *length)
*   \brief Sends CAN messages
*	\param handle as int, CAN file descriptor
*	\param message as CMSG *, message to send
*	\param length as int32_t, number of messages to send and returns the real number of sent messages
*   \return CAN_ERROR if fails any trasmission 
*	\return CAN_OK
*/
int dx60co8_SendMsg(int handle, CMSG *message, int32_t *length){	
	long dRet;

	dRet = canSend(handle, message, length);
	if( dRet != NTCAN_SUCCESS){
		printf("dx60co8_SendMsg: Failed sending the message/s\n");
		return CAN_ERROR;
	}
		
	
	/*for(i=0; i<*len; i++){
		dRet = canWrite(handle, message[i].id, message[i].data, message[i].len, canMSG_STD);		
		if(dRet!=canOK){
			printf("dx60co8_SendMsg: canWrite");
			*len = i;
			return CAN_ERROR;
		}
	}	*/
	
	return CAN_OK;	
}


/*! \fn int dx60co8_SendRTRMsg(HANDLE handle, CMSG *cmsg, int32_t *length)
*   \brief Sends CAN messages with RTR enable-> is a remote request
*   \return ERROR if fails any trasmission 
*	\return OK
*/
int dx60co8_SendRTRMsg(int handle, CMSG *cmsg, int32_t *length){
	int ret, i;
	cmsg->len |= NTCAN_RTR;			//Flag RTR 
	ret = canSend(handle, cmsg, length);	
	if (ret != NTCAN_SUCCESS) {
		//printf("dx60co8_SendRTRMsg: Error sending the message\n");
		return CAN_ERROR;
	}else {
		for(i=0;i<*length;i++){
			//printf("dx60co8_SendRTRMsg: transmitted %d -> id=%x,len=%d (%x %x %x %x %x %x %x %x)\n", ret,cmsg[i].id, cmsg[i].len, cmsg[i].data[0], cmsg[i].data[1],cmsg[i].data[2],cmsg[i].data[3],cmsg[i].data[4],cmsg[i].data[5],cmsg[i].data[6],cmsg[i].data[7]);  
		}
	}
	
	return CAN_OK;	
}


/*! \fn int dx60co8_ReadMsg(int handle, CMSG *cmsg, int32_t *length)
*   \brief Reads message from CAN bus
*   \return ERROR
*	\return OK
*/
int dx60co8_ReadMsg(int handle, CMSG *cmsg, int32_t *length){
	long dRet;
	int k;
	
	dRet = canRead(handle, cmsg, length,NULL);
	if(dRet != NTCAN_SUCCESS /*&& dRet!=NTCAN_RX_TIMEOUT*/){
		if(dRet== NTCAN_RX_TIMEOUT)
		{
			//printf("dx60co8_ReadMsg Failed reading message/s \n");
			return CAN_ERROR;
		}else
		{
			//printf("dx60co8_ReadMsg Failed reading message/s \n");
			return CAN_ERROR;
		}
	}
	
	for(k = 0; k < *length; k++) {
		//printf("dx60co8_ReadMsg(%d): ID =%x length=%d (%02x %02x %02x %02x %02x %02x %02x %02x)",k,cmsg[k].id, cmsg[k].len, cmsg[k].data[0],cmsg[k].data[1],cmsg[k].data[2],cmsg[k].data[3],cmsg[k].data[4],cmsg[k].data[5],cmsg[k].data[6],cmsg[k].data[7]);
	}
	return CAN_OK;
}


/*! \fn void dx60co8_CreateNMTsg(CMSG* msg, byte id, int mode)
*   \brief Creates a CANopen NMT Message
*	\param msg byref, id node_id
*/
void dx60co8_CreateNMTMsg(CMSG* msg, byte node_id, int mode)
{
	msg->id = 0x00;
	msg->len = 0x02; // Tamaño del mensaje (8 bytes)
	
	switch(mode){
		case START_NODE:
			msg->data[0]=0x01; //mode
		break;
		case STOP_NODE:
			msg->data[0]=0x02; 
		break;
		case PREOPERATIONAL_NODE:
			msg->data[0]=0x80; 
		break;
		case RESET_NODE:
			msg->data[0]=0x81;
		break;
		case RESET_COMMUNICATION:
			msg->data[0]=0x82; 
		break;
	}
	msg->data[1]= node_id; // node
	msg->data[2]=0;
	msg->data[3]=0;
	msg->data[4]=0;
	msg->data[5]=0;
	msg->data[6]=0;
	msg->data[7]=0;
}


/*! \fn void dx60co8_CreateSYNCMsg(CMSG* msg)
*   \brief Creates a CANopen SYNC Message
*	\param msg by ref
*/
void dx60co8_CreateSYNCMsg(CMSG* msg)
{
	msg->id = 0x80;
	msg->len = 0x00; 
}


/*! \fn void dx60co8_CreateNodeGuardMsg(CMSG* msg, byte id, int mode)
*   \brief Creates a CANopen NodeGuard Message
*	\param msg by ref, id node_id
*/
void dx60co8_CreateNodeGuardMsg(CMSG* msg, byte node_id)
{
	msg->id = 0x700 + node_id;
	msg->len = 0x00; 
}


/*! \fn void dx60co8_CreatePDOTargetVelociyMsg(CMSG* msg, byte id, int mode)
*   \brief Creates a CANopen PDO msg to set target velocity of the driver
*	\param msg byref, id node_id
*/
void dx60co8_CreatePDOTargetVelociyMsg(CMSG* msg, byte node_id, double rpm)
{
	msg->id = 0x500 + node_id;
	msg->len = 4; 
}


/*! \fn int dx60co8_ConfigureRPDO1(int handle, byte node_id, byte trans_type)
*   \brief Configure RPDO1 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureRPDO1(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP RPDO1 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
    //msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    //msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x00;					//Object Index(LSB)
    msg[0].data[2] = 0x14;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (RPDO1 + node_id)&0xFF;//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = RPDO1_MSB;				//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = Assynchronous (the received data is applied to its mapped objects inmediately)
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
    //msg[1].cob= 0;
    msg[1].data[0] = 0x22;					//Command -> 22h
    msg[1].data[1] = 0x00;					//Object Index(LSB)
    msg[1].data[2] = 0x14;					//Object Index(MSB)
    msg[1].data[3] = 0x02;					//Sub-Index
    msg[1].data[4] = trans_type;			//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;					//Byte 6
    msg[1].data[6] = 0x00;					//Byte 7
    msg[1].data[7] = 0x00;					//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO1 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO1 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}

	
	return CAN_OK;
	
}


/*! \fn int dx60co8_ConfigureRPDO21(int handle, byte node_id, byte trans_type)
*   \brief Configure RPDO1 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureRPDO21(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP RPDO21 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
    //msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    //msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x14;					//Object Index(LSB)
    msg[0].data[2] = 0x14;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (RPDO21 + node_id)&0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = RPDO21_MSB;			//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = Assynchronous (the received data is applied to its mapped objects inmediately)
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
   // msg[1].cob= 0;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x14;		//Object Index(LSB)
    msg[1].data[2] = 0x14;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO21 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}

	length = 1;
	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO21 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
	
}



/*! \fn int dx60co8_ConfigureRPDO22(int handle, byte node_id, byte trans_type)
*   \brief Configure RPDO1 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureRPDO22(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP RPDO22 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
  //  msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    //msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x15;					//Object Index(LSB)
    msg[0].data[2] = 0x14;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (RPDO22 + node_id)&0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = RPDO22_MSB;			//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = Assynchronous (the received data is applied to its mapped objects inmediately)
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
    //msg[1].cob= 0;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x15;		//Object Index(LSB)
    msg[1].data[2] = 0x14;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO21 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureRPDO21 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
	
}


/*! \fn int dx60co8_ConfigureTPDO3(int handle, byte node_id, byte trans_type)
*   \brief Configure TPDO3 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureTPDO3(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP TPDO3 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
    //msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    //msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x02;					//Object Index(LSB)
    msg[0].data[2] = 0x18;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (TPDO3 + node_id)&0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = TPDO3_MSB;				//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = 
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
    //msg[1].cob= 0;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x02;		//Object Index(LSB)
    msg[1].data[2] = 0x18;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO3 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO3 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
	
}


/*! \fn int dx60co8_ConfigureTPDO4(int handle, byte node_id, byte trans_type)
*   \brief Configure TPDO4 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureTPDO4(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP TPDO4 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
    //msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    //msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x03;					//Object Index(LSB)
    msg[0].data[2] = 0x18;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (TPDO4 + node_id) & 0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = TPDO4_MSB;				//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = Assynchronous (the received data is applied to its mapped objects inmediately)
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
    //msg[1].cob= 0;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x03;		//Object Index(LSB)
    msg[1].data[2] = 0x18;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO4 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO4 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
		
	return CAN_OK;
}


/*! \fn int dx60co8_ConfigureTPDO26(int handle, byte node_id, byte trans_type)
*   \brief Configure TPDO26 Msg
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureTPDO26(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP TPDO26 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs
   // msg[0].flags = 0;  
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
   // msg[0].cob= 0;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x19;					//Object Index(LSB)
    msg[0].data[2] = 0x18;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (TPDO26+ node_id)&0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = TPDO26_MSB;			//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = 
    //msg[1].flags = 0;  
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
   	//msg[1].cob= 0;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x19;		//Object Index(LSB)
    msg[1].data[2] = 0x18;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO26 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	length = 1;

	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO26 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}



/*! \fn int dx60co8_ConfigureTPDO25(int handle, byte node_id, byte trans_type)
*   \brief Configure TPDO25 Msg -> Prog. Digital Inprintf
*	\param handle as int, file descriptor for CAN communication
*	\param node_id as byte, CAN-id of the device
*   \return ERROR
*	\return OK
*/
int dx60co8_ConfigureTPDO25(int handle, byte node_id, byte trans_type){
	CMSG msg[2];
	int length;
	 /// MAP TPDO26 ///
	// sub-index 1 -> COB-ID = Configurable, Enable, Allow RTRs 
    msg[0].len = 8;//strlen(tx[i].data);  
    msg[0].id= 0x600 + node_id;
    msg[0].data[0] = 0x22;					//Command -> 22h
    msg[0].data[1] = 0x18;					//Object Index(LSB)
    msg[0].data[2] = 0x18;					//Object Index(MSB)
    msg[0].data[3] = 0x01;					//Sub-Index
    msg[0].data[4] = (TPDO25+ node_id)&0xFF;	//Byte 5 LSB	(COB-ID)
    msg[0].data[5] = TPDO25_MSB;			//Byte 6		(COB-ID)
    msg[0].data[6] = 0x00;					//Byte 7
    msg[0].data[7] = 0x00;					//Byte 8 (MSB)
	// Sub-Index 2-> Transmission type = 
    msg[1].len = 8;//strlen(tx[i].data);  
    msg[1].id= 0x600 + node_id;
    msg[1].data[0] = 0x22;		//Command -> 22h
    msg[1].data[1] = 0x18;		//Object Index(LSB)
    msg[1].data[2] = 0x18;		//Object Index(MSB)
    msg[1].data[3] = 0x02;		//Sub-Index
    msg[1].data[4] = trans_type;//Byte 5 LSB (Type)
    msg[1].data[5] = 0x00;		//Byte 6
    msg[1].data[6] = 0x00;		//Byte 7
    msg[1].data[7] = 0x00;		//Byte 8 (MSB)
	
	length = 1;
	if(dx60co8_SendMsg(handle, &msg[0], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO25 1: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	length = 1;

	if(dx60co8_SendMsg(handle, &msg[1], &length)!=CAN_OK){
		printf("dx60co8_ConfigureTPDO25 3: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn void dx60co8_CreateDisableVoltageMsg(CMSG* msg, byte id)
*   \brief Creates a CAN (DS301 DSP402) disable voltage message (see CANOpen object dictionary reference)
*	\param msg byref, id node_id
*/
void dx60co8_CreateDisableVoltageMsg(CMSG* msg, byte id){
	// DISABLE VOLTAGE
	msg->id = 0x600 + id;
	msg->len = 0x08; // Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22; //Command -> 22h
	msg->data[1]=0x40; //Object Index(LSB)
	msg->data[2]=0x60; //Object Index(MSB)
	msg->data[3]=0x00; //Sub-Index
	msg->data[4]=0x0D; //Byte 5
	msg->data[5]=0x00; //Byte 6
	msg->data[6]=0x00; //Byte 7
	msg->data[7]=0x00; //Byte 8
}


/*! \fn void dx60co8_CreateResetMsg(CMSG* msg, byte id)
*   \brief Creates a CAN (DS301 DSP402) reset message (see CANOpen object dictionary reference)
*	\param msg byref, id node_id
*/
void dx60co8_CreateResetMsg(CMSG* msg, byte id){
	// RESET FAULT	
  	// COB-ID = 600h + Node-ID 
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22;  //Command -> 22h
	msg->data[1]=0x40;  //Object Index(LSB)
	msg->data[2]=0x60;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x88;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn void dx60co8_CreateShutdownMsg(CMSG* msg, byte id)
*   \brief Creates a CAN (DS301 DSP402) shutdown message (see CANOpen object dictionary reference)
*	\param msg byref, id node_id
*/
void dx60co8_CreateShutdownMsg(CMSG* msg, byte id){
	// SHUTDOWN 	
  	// COB-ID = 600h + Node-ID 
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22;  //Command -> 22h
	msg->data[1]=0x40;  //Object Index(LSB)
	msg->data[2]=0x60;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x06;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn void dx60co8_CreateSwitchOnEnableMsg(CMSG* msg, byte id)
*   \brief Creates a CAN (DS301 DSP402) switch on and enable message (see CANOpen object dictionary reference)
*	\param msg byref, id node_id
*/
void dx60co8_CreateSwitchOnEnableMsg(CMSG* msg, byte id){
	// SWICTH ON + ENABLE OPERATION
	msg->id = 0x600 + id;
	msg->len = 0x08; // Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22; //Command -> 22h
	msg->data[1]=0x40; //Object Index(LSB)
	msg->data[2]=0x60; //Object Index(MSB)
	msg->data[3]=0x00; //Sub-Index
	msg->data[4]=0x07; //Byte 5
	msg->data[5]=0x00; //Byte 6
	msg->data[6]=0x00; //Byte 7
	msg->data[7]=0x00; //Byte 8
}


/*! \fn int dx60co8_SendDisableVoltageMsg(int handle, int id)
*   \brief Create and send CreateDisableVoltage message with confirmation to Right and Left drivers
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendDisableVoltageMsg(int handle, int id){
	CMSG msg;
	int	length=1;
	printf("dx60co8_SendDisableVoltageMsg: Sending Disable Voltage msg to %d\n.",id);
	dx60co8_CreateDisableVoltageMsg(&msg, id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendDisableVoltageMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn int dx60co8_SendResetMsg(int handle, int id)
*   \brief Send Reset Message to Right and Left drivers
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendResetMsg(int handle, int id){
	CMSG msg;
	int length=1;
	
	printf("dx60co8_SendResetMsg: Sending Reset msg to %d.\n",id);
	dx60co8_CreateResetMsg(&msg, id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendResetMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn int dx60co8_SendShutdownMsg(int handle, int id)
*   \brief Create and send with confirmation Shutdown Message to Right and Left drivers
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendShutdownMsg(int handle, int id){
	CMSG msg;
	int length=1;
	
	dx60co8_CreateShutdownMsg(&msg, id);
	printf("dx60co8_SendShutdownMsg: Sending Shutdown msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_endShutdownMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn int dx60co8_SendSwitchOnMsg(int handle, int id)
*   \brief Create and send with confirmation SwitchOn Message to Right and Left drivers
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendSwitchOnMsg(int handle, int id){
	CMSG msg;
	int length=1;
	
	dx60co8_CreateSwitchOnEnableMsg(&msg, id);
	printf("dx60co8_SendSwitchOnMsg: Sending Switch On msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendSwitchOnMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn void dx60co8_CreateSetModeOfOperationMsg(CMSG *msg, byte id, int mode)
*   \brief Creates a CAN message (DS301 DSP402) t modify the mode of operation  (see CANOpen object dictionary reference)
*   \return CAN_OK
*	\return CAN_ERROR
*/
void dx60co8_CreateSetModeOfOperationMsg(CMSG *msg, byte id, int mode){
		// COB-ID = 600h + Node-ID 	
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Msg size (8 bytes)
	msg->data[0]=0x22;  //Command -> 40h read data
	msg->data[1]=0x60;  //Object Index(LSB)
	msg->data[2]=0x60;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=mode;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn int dx60co8_SendSetModeOfOperationMsg(int handle, int id, int mode)
*   \brief Creates and sends a SetModeOfOperation message to the driver
*   \return CAN_OK
*	\return CAN_ERROR
*/

int dx60co8_SendSetModeOfOperationMsg(int handle, int id, int mode){
	CMSG msg;
	int length=1;
	
	dx60co8_CreateSetModeOfOperationMsg(&msg, id, mode);
	printf("dx60co8_SendSetModeOfOperationMsg: Sending Set mode of operation msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendSetModeOfOperation: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn int dx60co8_SendNMTMsg(int handle, int node_id, int mode)
*   \brief Creates and sends a NMT message to the driver
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendNMTMsg(int handle, int node_id, int mode){
	CMSG msg;
	int length=1;
	
	dx60co8_CreateNMTMsg(&msg, node_id, mode);
	
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendNMTMsg: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn int dx60co8_SendVelocityRefMsg(int handle, int id, double rpm)
*   \brief Creates and sends a NMT message to the driver
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendVelocityRefMsg(int handle, int id, double rpm){
	CMSG msg;
	int len=1;
	int iRpm;
	byte b1, b2, b3, b4;	
	
	//if (rpm < 0){
	//	b1=b2=b3=b4=0x00;
	//}else{
		iRpm = (int32_t) (rpm * RPM2REF);
		b1 = iRpm & 0xFF;
		b2 = (iRpm>>8) & 0xFF;
		b3 = (iRpm>>16) & 0xFF;
		b4 = (iRpm>>24) & 0xFF;
	//}
	
	// MOVE MOTOR
	msg.id = 0x600 + id;
	msg.len = 0x08; // Tamaño del mensaje (8 bytes)
	msg.data[0]=0x22; //Command -> 22h
	msg.data[1]=0xff; //Object Index(LSB)
	msg.data[2]=0x60; //Object Index(MSB)
	msg.data[3]=0x00; //Sub-Index
	msg.data[4]=b1; //Byte 5 LSB
	msg.data[5]=b2; //Byte 6
	msg.data[6]=b3; //Byte 7
	msg.data[7]=b4; //Byte 8 MSB
	
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendVelocityRefMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn void dx60co8_CreateGetModeOfOperationMsg(CMSG* msg, byte id)
*   \brief Create a GetModeOfOperation message
*/
void dx60co8_CreateGetModeOfOperationMsg(CMSG* msg, byte id){
  	// COB-ID = 600h + Node-ID 	
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Msg size (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h read data
	msg->data[1]=0x61;  //Object Index(LSB)
	msg->data[2]=0x60;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn int dx60co8_SendGetModeOfOperation(int handle, int id)
*   \brief Creates and sends a request to get the mode of operation of the motor
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendGetModeOfOperation(int handle, int id){
	CMSG msg;
	int len=1;
		
	dx60co8_CreateGetModeOfOperationMsg(&msg, id);
	printf("dx60co8_SendGetModeOfOperation: Sending Get mode of operation msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendGetModeOfOperation: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn int dx60co8_SendPositionRefMsg(int handle, int id, int position)
*   \brief Creates and sends a message with the position reference of the motor
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendPositionRefMsg(int handle, int id, int position){
	CMSG msg;
	int len=1;
	byte b1, b2, b3, b4;	
	
	b1 = position & 0xFF;
	b2 = (position>>8) & 0xFF;
	b3 = (position>>16) & 0xFF;
	b4 = (position>>24) & 0xFF;
	
	// MOVE MOTOR
	msg.id = 0x600 + id;
	msg.len = 0x08; // Tamaño del mensaje (8 bytes)
	msg.data[0]=0x22; //Command -> 22h
	msg.data[1]=0x7A; //Object Index(LSB)
	msg.data[2]=0x60; //Object Index(MSB)
	msg.data[3]=0x00; //Sub-Index
	msg.data[4]=b1; //Byte 5 LSB
	msg.data[5]=b2; //Byte 6
	msg.data[6]=b3; //Byte 7
	msg.data[7]=b4; //Byte 8 MSB
	
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendPositionRefMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn dx60co8_CreateGetStatusWordMsg(CMSG* msg, byte id)
*   \brief Creates a message to get the status word from a driver
*/
void dx60co8_CreateGetStatusWordMsg(CMSG* msg, byte id){
  	// COB-ID = 600h + Node-ID 	
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Msg size (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h read data
	msg->data[1]=0x41;  //Object Index(LSB)
	msg->data[2]=0x60;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn int dx60co8_SendGetStatusWordMsg(int handle, int id)
*   \brief Creates and sends a message to get the status word from the driver
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendGetStatusWordMsg(int handle, int id){
	CMSG msg;
	int len=1;
		
	dx60co8_CreateGetStatusWordMsg(&msg, id);
	printf("dx60co8_SendGetStatusWordMsg: Sending Get Status Word msg to %d\n.",id);
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_CreateGetStatusWordMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn void dx60co8_CreateEnableOpMsg(CMSG* msg, byte id)
*   \brief Creates a CAN (DS301 DSP402) switch on and enable message (see CANOpen object dictionary reference)
*	\param msg byref, id node_id
*/
void dx60co8_CreateEnableOpMsg(CMSG* msg, byte id){
	// SWICTH ON + ENABLE OPERATION
	msg->id = 0x600 + id;
	msg->len = 0x08; // Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22; //Command -> 22h
	msg->data[1]=0x40; //Object Index(LSB)
	msg->data[2]=0x60; //Object Index(MSB)
	msg->data[3]=0x00; //Sub-Index
	msg->data[4]=0x0F; //Byte 5
	msg->data[5]=0x00; //Byte 6
	msg->data[6]=0x00; //Byte 7
	msg->data[7]=0x00; //Byte 8
}


/*! \fn int dx60co8_SendEnableOpMsg(int handle, int id)
*   \brief Creates and sends a EnableOperation message to a driver
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendEnableOpMsg(int handle, int id){
	CMSG msg;
	int	length=1;
	
	dx60co8_CreateEnableOpMsg(&msg, id);
	printf("dx60co8_SendEnableOpMsg: Sending Enable operation msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendEnableOpMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn void dx60co8_CreateQuickStopMsg(CMSG* msg, byte id)
*   \brief Creates a QuickStop Message
*/
void dx60co8_CreateQuickStopMsg(CMSG* msg, byte id){
	msg->id = 0x600 + id;
	msg->len = 0x08; // Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22; //Command -> 22h
	msg->data[1]=0x40; //Object Index(LSB)
	msg->data[2]=0x60; //Object Index(MSB)
	msg->data[3]=0x00; //Sub-Index
	msg->data[4]=0x02; //Byte 5
	msg->data[5]=0x00; //Byte 6
	msg->data[6]=0x00; //Byte 7
	msg->data[7]=0x00; //Byte 8
}


/*! \fn int dx60co8_SendQuickStopMsg(int handle, int id)
*   \brief Creates and sends a QuickStop message to the driver
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendQuickStopMsg(int handle, int id){
	CMSG msg;
	int	length=1;
	
	dx60co8_CreateQuickStopMsg(&msg, id);
	printf("dx60co8_SendQuickStopMsg: Sending Quick Stop msg to %d.\n",id);
	if(dx60co8_SendMsg(handle, &msg, &length)!=CAN_OK){
		printf("dx60co8_SendQuickStopMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn void dx60co8_CreateReadDigitalOutprintf(CMSG* msg, byte id)
*   \brief Creates a message to read the value of the digital outprintf from the driver
*/
void dx60co8_CreateReadDigitalOutprintf(CMSG* msg, byte id){
	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h
	msg->data[1]=0xA1;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn int dx60co8_SendReadDigitalOutprintfMsg(int handle, int id)
*   \brief Create and sends a message to read the values of the digital outprintf
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendReadDigitalOutprintfMsg(int handle, int id){
	CMSG msg;
	int len=1;
		
	dx60co8_CreateReadDigitalOutprintf(&msg, id);
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendReadDigitalOutprintfMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	return CAN_OK;
}


/*! \fn dx60co8_CreateReadPowerStageTemp(CMSG* msg, byte id)
*   \brief Creates a message to read the driver's temperature
*/
void dx60co8_CreateReadPowerStageTemp(CMSG* msg, byte id){	
  	// COB-ID = 600h + Node-ID 
  	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h
	msg->data[1]=0xC2;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn void dx60co8_CreateSetDigitalOutprintf(CMSG *msg, byte id, byte value)
*   \brief Create a message to modify the value of the digital outprintf
*/
void dx60co8_CreateSetDigitalOutprintf(CMSG *msg, byte id, byte value){
	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x22;  //Command -> 22h
	msg->data[1]=0xA1;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=value; //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}
 

/*! \fn int dx60co8_SendDigitalOutprintf(int handle, int id, byte output)
*   \brief Create and send values of digital outprintf
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*	\param output as byte, value for digital outprintf
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendDigitalOutprintf(int handle, int id, byte output){
	CMSG msg;
	int len=1;
		
	dx60co8_CreateSetDigitalOutprintf(&msg, id, output);
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendDigitalOutprintf: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn int dx60co8_SendPDOVelocityRefMsg(int handle, byte node_id, int velocity)
*   \brief Send a PDO message with velocity reference. 
*	\param handle as int, CAN file descriptor
*	\param node_id as int, CAN node to send the message
*	\param velocity as int, is the reference velocity that determines the set point of the velocity loop.
*	\return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendPDOVelocityRefMsg(int handle, byte node_id, int velocity){
	CMSG msg;
	int len=1;
	byte b1, b2, b3, b4;	
	
	b1 = velocity & 0xFF;
	b2 = (velocity>>8) & 0xFF;
	b3 = (velocity>>16) & 0xFF;
	b4 = (velocity>>24) & 0xFF;

	// MOVE MOTOR
	msg.id = RPDO22 + node_id;
	msg.len = 0x04; // Tamaño del mensaje (4 bytes)
	msg.data[0]=b1; 
	msg.data[1]=b2; 
	msg.data[2]=b3; 
	msg.data[3]=b4; 
	msg.data[4]=0x00; 
	msg.data[5]=0x00; 
	msg.data[6]=0x00; 
	msg.data[7]=0x00; 
	
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendPDOVelocityRefMsg: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}	
	
	return CAN_OK;
}


/*! \fn int dx60co8_SendPDOPositionRefMsg(int handle, byte node_id, int velocity)
*   \brief Send a PDO message with position reference. 
*	\param handle as int, CAN file descriptor
*	\param node_id as int, CAN node to send the message
*	\param position as int, is the reference position that determines the set point of the position loop.
*	\return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendPDOPositionRefMsg(int handle, byte node_id, int position){
	CMSG msg;
	int len=1;
	byte b1, b2, b3, b4;	

	b1 = position & 0xFF;
	b2 = (position>>8) & 0xFF;
	b3 = (position>>16) & 0xFF;
	b4 = (position>>24) & 0xFF;
	
	// MOVE MOTOR
	msg.id = RPDO21 + node_id;
	msg.len = 0x04; // Tamaño del mensaje (8 bytes)
	msg.data[0]=b1; //Command -> 22h
	msg.data[1]=b2; //Object Index(LSB)
	msg.data[2]=b3; //Object Index(MSB)
	msg.data[3]=b4; //Sub-Index
	msg.data[4]=0x00; //Byte 5 LSB
	msg.data[5]=0x00; //Byte 6
	msg.data[6]=0x00; //Byte 7
	msg.data[7]=0x00; //Byte 8 MSB
	
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendPDOPositionRefMsg: Error sending to node %d\n",node_id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
}


/*! \fn void dx60co8_CreateReadDigitalInprintfMsg(CMSG* msg, byte id)
*   \brief Creates a message to read the digital inprintf from the driver
*/
void dx60co8_CreateReadDigitalInprintfMsg(CMSG *msg, byte id){
	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h
	msg->data[1]=0xA0;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=0x00;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


/*! \fn int dx60co8_SendReadDigitalInprintfMsg(int handle, int id)
*   \brief Creates and sends values of digital outprintf
*	\param handle as int, file descriptor for CAN communication
*	\param id as integer, CAN-id of the device
*   \return CAN_OK
*	\return CAN_ERROR
*/
int dx60co8_SendReadDigitalInprintfMsg(int handle, int id){
	CMSG msg;
	int len=1;
		
	dx60co8_CreateReadDigitalInprintfMsg(&msg, id);
	if(dx60co8_SendMsg(handle, &msg, &len)!=CAN_OK){
		printf("dx60co8_SendReadDigitalInprintfMsg: Error sending to node %d\n",id);
		return CAN_ERROR;
	}
	
	return CAN_OK;
	
}


/*! \fn void dx60co8c_CreateReadAnalogInprintf()
*   \brief 
*/
void dx60co8_CreateReadAnalogInprintf(CMSG *msg, byte id, byte input){
	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tamaño del mensaje (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h
	msg->data[1]=0xA2;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=input;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}











//JORGE:FAKE

void dx60co8_CreateReadDigitalOutputs(CMSG *msg, byte id) {
	msg->id = 0x600 + id;
	msg->len = 0x08;	// Tama�o del mensaje (8 bytes)
	msg->data[0]=0x40;  //Command -> 40h
	msg->data[1]=0xA1;  //Object Index(LSB)
	msg->data[2]=0x20;  //Object Index(MSB)
	msg->data[3]=1;  //Sub-Index
	msg->data[4]=0x00;  //Byte 5
	msg->data[5]=0x00;  //Byte 6
	msg->data[6]=0x00;  //Byte 7
	msg->data[7]=0x00;  //Byte 8
}


int dx60co8_SendDigitalOutputs(int handle, int id, byte output) {
  return 0;
}
