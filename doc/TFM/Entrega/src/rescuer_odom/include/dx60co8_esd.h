/** \file dx60co8_esd.h
 * \author Robotnik Automation S.L.L.
 * \author robotnik
 * \version 1.0
 * \date    2007-2008
 *
 * \brief Header for Servo Motor Driver DX60CO8 serie
 * Compatible with ESD/PCI331 can boards.  
 * (C) 2008 Robotnik Automation, SLL
 * 
*/

#include <stdint.h>
#include <ntcan.h>

#ifndef __CAN_ESD_H
	#define __CAN_ESD_H

#define TXBUFSIZE 				200			/*size of the can transmision buffer*/
#define RXBUFSIZE 				200			/*size of the can reader buffer*/
#define TXTOUT 					5			/*timeout in ms for write accesses*/
#define RXTOUT 					5			/*timeout in ms for read accesses*/
#define CAN_OPEN_MODE 			0
#define BAUD_1M					0

//MODES OF OPERATION
#define POSITION_MODE			0x01
#define VELOCITY_MODE			0x03

#define CAN_ERROR				-1
#define CAN_OK					0

//TYPES OF NMT MESSAGES
#define START_NODE				1
#define STOP_NODE				2
#define PREOPERATIONAL_NODE		3
#define RESET_NODE				4
#define RESET_COMMUNICATION		5

//Possible Drive Status
#define UNKNOWN					0
#define SWITCH_ON_DISABLED		8
#define READY_TO_SWITCH_ON		9
#define	OPERATION_DISABLED		10
#define OPERATION_ENABLED		11
#define FAULT					12
#define SWITCHED_ON				13

//Possible Drive Communication Status 
#define PRE_OPERATIONAL			14
#define OPERATIONAL				15
#define STOPPED					16

#define LEFT_ID 				2			// CAN-ID for Left MotorDrive
#define RIGHT_ID 				3			// CAN-ID for Right MotorDrive
#define NET_ID 					0			// CanBus id of the drivers network

#define MAX_SPEED_RPM			1250			// Maximal motor speed 
#define RPM2REF					6.66666667 		// Rpm to reference
#define REF2RPM 				1.0 / RPM2REF 	// Read speed to rpm
#define PERCENT2REF				MAX_SPEED_RPM * RPM2REF / 100.0 // rpm 2 speed reference

//OTHERS
#define NUM_ANALOG_INPUTS		3	// Number of analog inputs
#define NUM_DIGITAL_INPUTS		7	// Number of digital inputs
#define NUM_DIGITAL_OUTPUTS		8	// Number of digital outputs
	
//PDOs
#define RPDO1					0x180
#define RPDO1_MSB				0x01

#define RPDO21					0x200
#define RPDO21_MSB				0x02

#define RPDO22					0x280
#define RPDO22_MSB				0x02					

#define TPDO3					0x300
#define TPDO3_MSB				0x03

#define TPDO4					0x380
#define TPDO4_MSB				0x03

#define TPDO26					0x400
#define TPDO26_MSB				0x04

#define TPDO25					0x480
#define TPDO25_MSB				0x04

//TRANSMISSION PDOs
#define ASYNCHRONOUS_TRANS		0xFE

typedef struct Digital{
	bool state;					
	bool reserved;
}Digital;

//typedef struct CMSG{
//        int32_t  id;            /* can-id                                     */
//        uint8_t  len;           /* length of message: 0-8                     */
//        uint8_t  msg_lost;      /* count of lost rx-messages                  */
//       uint8_t  reserved[2];   /* reserved                                   */
//        uint8_t  data[8];       /* 8 data-bytes                               */
//} CMSG;*/

typedef unsigned char byte;

int dx60co8_Connect(int *handle, int NetChannel, int baudrate);
int dx60co8_CloseCan(int fd);

int dx60co8_SendMsg(int handle, CMSG *cmsg, int32_t *len);
int dx60co8_ReadMsg(int handle, CMSG *cmsg, int32_t *len);
int dx60co8_SendMsg(int handle, CMSG *cmsg, int32_t *len);
int dx60co8_SendRTRMsg(int handle, CMSG *cmsg, int32_t *len);
int dx60co8_ReadMsg(int handle, CMSG *cmsg, int32_t *len);
//NMT
void dx60co8_CreateNMTMsg(CMSG *msg, byte node_id, int mode);
int dx60co8_SendNMTMsg(int handle, int node_id, int mode);
//SYNC
void dx60co8_CreateSYNCMsg(CMSG* msg);
//NodeGuard
void dx60co8_CreateNodeGuardMsg(CMSG* msg, byte node_id);
//PDOs
void dx60co8_CreatePDOTargetVelociyMsg(CMSG* msg, byte node_id, double rpm);	
int dx60co8_ConfigureRPDO1(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureRPDO21(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureRPDO22(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureTPDO3(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureTPDO4(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureTPDO26(int handle, byte node_id, byte trans_type);
int dx60co8_ConfigureTPDO25(int handle, byte node_id, byte trans_type);
int dx60co8_SendPDOVelocityRefMsg(int handle, byte node_id, int velocity);
int dx60co8_SendPDOPositionRefMsg(int handle, byte node_id, int position);

//SDOs
void dx60co8_CreateSetModeOfOperationMsg(CMSG *msg, byte id, int mode);
int dx60co8_SendSetModeOfOperationMsg(int handle, int id, int mode);
void dx60co8_CreateDisableVoltageMsg(CMSG* msg, byte id);
int dx60co8_SendDisableVoltageMsg(int handle, int id);
void dx60co8_CreateResetMsg(CMSG* msg, byte id);
int dx60co8_SendResetMsg(int handle, int id);
void dx60co8_CreateShutdownMsg(CMSG* msg, byte id);
int dx60co8_SendShutdownMsg(int handle, int id);
void dx60co8_CreateSwitchOnEnableMsg(CMSG* msg, byte id);
int dx60co8_SendSwitchOnMsg(int handle, int id);
void dx60co8_CreateGetModeOfOperationMsg(CMSG *msg, byte id);
int dx60co8_SendGetModeOfOperation(int handle, int id);
int dx60co8_SendPositionRefMsg(int handle, int id, int position);
void dx60co8_CreateGetStatusWordMsg(CMSG* msg, byte id);
int dx60co8_SendGetStatusWordMsg(int handle, int id);
void dx60co8_CreateEnableOpMsg(CMSG* msg, byte id);
int dx60co8_SendEnableOpMsg(int handle, int id);
void dx60co8_CreateQuickStopMsg(CMSG* msg, byte id);
int dx60co8_SendQuickStopMsg(int handle, int id);
void dx60co8_CreateReadDigitalOutputs(CMSG *msg, byte id);
int dx60co8_SendReadDigitalOutputsMsg(int handle, int id);
void dx60co8_CreateReadPowerStageTemp(CMSG* msg, byte id);
void dx60co8_CreateSetDigitalOutputs(CMSG *msg, byte id, byte value);
int dx60co8_SendDigitalOutputs(int handle, int id, byte output);
int dx60co8_SendVelocityRefMsg(int handle, int id, double rpm);
void dx60co8_CreateReadDigitalInputsMsg(CMSG *msg, byte id);
int dx60co8_SendReadDigitalInputsMsg(int handle, int id);
void dx60co8_CreateSendVelocityRefMsg(CMSG* msg, byte id);
void dx60co8c_CreateReadAnalogInputs(CMSG *msg, byte id, byte input);



#endif //__CAN_ESD_H
