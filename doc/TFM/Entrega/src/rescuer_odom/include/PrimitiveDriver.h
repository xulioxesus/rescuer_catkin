/** \file PrimitiveDriver.h
 * \author Robotnik Automation S.L.L.
 * \version 1.0
 * \date    
 * 
 * \brief PrimitiveDriver Component       
 *
 * Primitive Driver (ID 33)
 *
 * Function:
 * The Primitive Driver component performs basic driving and all platform related mobility
 * functions including operation of common platform devices such as the engine and lights.
 *
 * Description:
 * This component does not imply any particular platform type such as tracked or wheeled, but
 * describes mobility in six degrees of freedom using a percent of available effort in each
 * direction. Additionally, no power plant (gasoline, diesel, or battery) is implied and the
 * component functions strictly in an open loop manner, i.e., a velocity is not commanded since
 * that requires a speed sensor. Note that the specific actuator commands are not defined by
 * JAUS.
 *
 * (C) 2007 Robotnik Automation, SLL  
 * All rights reserved.
*/

#ifndef __PD_H
	#define __PD_H
 
#include "MotorDrive.h"
#include "Component.h"
#include "logLib.h"

// Internal robot states
#define STARTUP_STATE		1	
#define INITIALIZE_STATE	2
#define STANDBY_STATE		3
#define READY_STATE			4
#define EMERGENCY_STATE		5
#define FAILURE_STATE		6
#define SHUTDOWN_STATE		7


#define PRIMITIVE_DRIVER_LOAD_CONFIGURATION_ERROR 		-1
#define PRIMITIVE_DRIVER_NODE_MANAGER_OPEN_ERROR		-2
#define PRIMITIVE_DRIVER_STARTUP_BEFORE_SHUTDOWN_ERROR	-3
#define PRIMITIVE_DRIVER_THREAD_CREATE_ERROR			-4

#define PRIMITIVE_DRIVER_THREAD_DESIRED_HZ	15
#define PRIMITIVE_DRIVER_THREAD_TIMEOUT_SEC	6
#define BATTERY_NODE						RIGHT_ID		// Drive that measures the battery level
#define BATTERY_INPUT						1			// Analog Input associated with battery level
#define BATTERY_RESISTIVE_DIVISOR			3.23		// Constant used to calculate the battery
#define BATTERY_VOLTAGE						24			// Battery Voltage 

/////////////// Digital Outputs ////////////////////////
#define COOLING_SYSTEM						1
#define GREEN_LIGHT							2
#define BLUE_LIGHT							3



/**
	* Function executing in the thread
*/
void *AuxPDControlThread(void *threadParam);


/*! \class PrimitiveDriver
 *	\author Robotnik Automation S.L.L
 *	\version 1.0
 *  \brief  This class contains all the data relative to the control of the RESCUER platform
 *
 */
class PrimitiveDriver:public Component{
	friend void *AuxPDControlThread(void *threadParam);
  // Attributes
public:
	/** 
       * a private class.
       * Control data from the left motor drive.
    */
	MotorDrive *motor_left;
	/** 
       * a private class.
       * Control data from the right motor drive.
    */
	MotorDrive *motor_right;

private:
	/**
		* Rescuer's odometry value
	*/
	OdomType g_odom;

	/**
		* Net channel for CAN communication
	*/
	int inet_channel;	

	/** 
    	* a private variable.
    	* Allows the transition to JAUS_READY_STATE. Only the controller can modify it.
    */
	bool bReady;
	
	/** 
       * a private variable.
       * Contains the CAN file descriptor
    */
	int CAN_fd;
	
	/** 
       * a private struct.
       * Contains the control thread parameters 
    */
	struct thread_param par;
		
	/**
		* a private variable.
		* Contains the last error value.	
	*/
	int error;	
	
	/** 
       	* a private variable.
       	* Specifies the mode of control for this component. It can be manual or by the component
		* WaypointDriver
    */
	int controller;
	
	/**
		* Control the state of the cooling system
	*/
	bool cooling;
	
	/**
		* Mutux for controlling the changes and access to pd values
	*/
	pthread_mutex_t mutex_pd;
	
	
	/**
		* Max Linear speed
	*/
	double motor_max_speed;
	
	/**
		* Max rotational speed 
	*/
	double motor_max_turnspeed;
		
	// Operations
public:
	/**
		* Public constructor
	*/
	PrimitiveDriver(void);
	PrimitiveDriver(const char *can_port);
	~PrimitiveDriver();
	/**
    	* function to get the state of the platform
        * @return Component's state
    */
	int GetState ();
	
	/**
		* function to get current update rate of the thread
		* @return pthread_hz
	*/
	double GetUpdateRate();
	
	/**
       	* function to send velocity references to the motors of the platform
		* @param propulsiveLinearEffortXPercent a double
		* @param propulsiveRotationalEffortZPercent a double
		* @see Stop()
       	* @return OK if references can be sent
		* @return ERROR if references can't be sent 
    */
  	int Move(double propulsiveLinearEffortXPercent, double propulsiveRotationalEffortZPercent);
	
	/**
       	* function to stop inmediately the movement of the platform
       	* @return OK if stops successfully
		* @return ERROR if can't stop 
    */
  	int Stop();
		
	/**
		* @return Returns the current Rescuer's odometry value
	*/
  	void GetOdometry (OdomType *Od);
	
	/**
		* Get the value of the digital inputs from the drive
		* @param LeftInputs as a byte * with the returned values of the digital inputs from left drive
		* @param RightInputs as a byte * with the returned values of the digital inputs from right drive
		* @return OK 
		* @return ERROR
	*/	
  	int GetDigitalInputs (byte *LeftInputs, byte *RightInputs);
	
	/**
		* set the value of the digital outputs from the drive
		* @param LeftOutputs as a byte with the values of digital outputs to left drive
		* @param RightOutputs as a byte with the values of digital outputs to right drive
		* @return OK 
		* @return ERROR
	*/
  	int SetDigitalOutputs (byte LeftOutputs, byte RightOutputs);
	
	/**
		* Get the value of the digital outputs from the drive
		* @param LeftOutputs as a byte * with the returned values of the digital outputs from left drive
		* @param RightOutputs as a byte * with the returned values of the digital outputs from right drive
		* @return OK 
		* @return ERROR
	*/	
  	int GetDigitalOutputs (byte *LeftOutputs, byte *RightOutputs);

	/**
		* This function allows the abstracted component functionality contained in this file to be started from an external source.
		* It must be called first before the component state machine and mailman interaction will begin
		* Each call to "StartUp" should be followed by one call to the "ShutDown" function
		* @return OK
		* @return ERROR
	*/
	int StartUp();
	
	/**
		* This function allows the abstracted component functionality contained in this file to be stoped from an external source.
		* If the component is in the running state, this function will terminate all threads running 
		* This function will also close the connection to the mailman and check out the component from the mailman network
		* @return OK
		* @return ERROR
	*/
	int ShutDown();
	
	/**
		* @return Platform's Angular Speed
	*/
	double GetAngularSpeed();
	
	/**
		* @return Platform's Linear Speed
	*/
	double GetLinearSpeed();
	
	/**
		* All core component functionality is contained in this thread.
		* All of the Rover component state machine code can be found here.
		* @param *threadData, priority of thread and type of clock
	*/
	void ControlThread(void);
	
	/**
		* Returns current battery level
		* @return the value of the battery in Volts
	*/
	double GetBattery();
	
	/**
		* @return current rotational effort Z percent
	*/
	double GetRotationalEffortZPercent();
	
	/**
		* Resets odometry's values of the robot
	*/
	void ResetOdometry();
	
	/**
		* Sets the motor in operational state
	*/
	int Start();
	
	/**
		* Set the maximun linear and rotational speed
	*/
	void SetMaxSpeed(double MaxLinearSpeed, double MaxRotationalSpeed);
	
private:
	
	/**
		* function called during initialization state 	
	*/
	void InitState();

	/**
		* function called during startup state 	
	*/
	void StartUpState();

	/**
		* function called during ready state 	
	*/
	void ReadyState();

	/**
		* function called during standby state 	
	*/
	void StandbyState();

	/**
		* function called during emergency state 	
	*/
	void EmergencyState();
	
	/**
		* function called during failure state 	
	*/
	void FailureState();
	
	/**
		* function called during shutdown state 	
	*/
	void ShutDownState();
	
	/**
		* function called during all states 	
	*/
	void AllState();
	
	/**
		* function that switches the state of the component into the desired state
		* @param new_state as an integer, the new state of the component
	*/
	void SwitchToState(int new_state);
	
	/**
		* function that switches off the selected digital output
		* @param digital_output as an integer, number of digital output
	*/
	void SwitchOff(int digital_output);
	
	/**
		* function that switches on the selected digital output
		* @param digital_output as an integer, number of digital output
	*/
	void SwitchOn(int digital_output);
	
	/**
		* Calculate the % of platform's battery.
		* @param analog_input as a double, value obtained from some analog_input
		* @return the value of the battery in %
	*/
	double CalculateBatteryLevel(double analog_input);

	/**
		* function that send SYNC signal to the drives
		* @return OK
		* @return ERROR
	*/
	int SynchronizeDrives();
	
	/** 
       	* Read all CAN messages in the buffer
		* @return OK
		* @return ERROR
    */
	int ReadCANMessages();
	
	/**
		* Read current velocities from amplifiers 
		* and update global location solving kinematics 
		* Update odometry with last measurement
		* @return OK 
	*/
  	int UpdateOdometry ();

};

#endif
