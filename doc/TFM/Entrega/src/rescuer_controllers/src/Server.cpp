#include "Server.hpp"

PrimitiveDriver Server::driver = *(new PrimitiveDriver(DEFAULT_RESCUER_PORT));

Server::Server()
{	
	driver.SetMaxSpeed(MOTOR_DEF_MAX_SPEED,MOTOR_DEF_MAX_TURNSPEED);

	if(driver.StartUp() == ERROR)
	{
		exit(-1);
	}
	else
	{
		ROS_INFO("rescuer motors ready");
	}
}

bool Server::Move(rescuer_odom::Velocity::Request  &req, rescuer_odom::Velocity::Response &res)
{
	ROS_INFO("request: linear=%f, angular=%f", req.linear, req.angular);

        float propulsiveLinearEffortXPercent=0.0, propulsiveRotationalEffortZPercent=0.0;

        //Converts velocity values into proportional percentage
        propulsiveLinearEffortXPercent = req.linear*100;
        propulsiveRotationalEffortZPercent = req.angular*100;

        if(propulsiveLinearEffortXPercent > 100.0)
                propulsiveLinearEffortXPercent = 100.0;
        else if(propulsiveLinearEffortXPercent < -100.0)
                propulsiveLinearEffortXPercent = -100.0;

        if(propulsiveRotationalEffortZPercent > 100.0)
                propulsiveRotationalEffortZPercent = 100.0;
        else if(propulsiveRotationalEffortZPercent < -100.0)
                propulsiveRotationalEffortZPercent = -100.0;


	int result = driver.Move(propulsiveLinearEffortXPercent,propulsiveRotationalEffortZPercent);

	ROS_INFO("sending back response: [%ld]", (long int) result);

	return true;
}

 
void Server::Sleep(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

Server::~Server()
{
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rescuer_odom");
  ros::NodeHandle n;

  Server servidor = *(new Server());

  ros::ServiceServer service = n.advertiseService("Server", Server::Move);
  ROS_INFO("Ready to move rescuer.");
  ros::spin();

  return 0;
}
