/*
 * Copyright (c) 2008, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor tnhe names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Author: Stuart Glaser
 * Modified by: Julio Jesús León Pérez 
 */

#include "joint_trajectory_action_controller_simulator.hpp"

//PLUGINLIB_DECLARE_CLASS(JointTrajectoryActionController, controller::JointTrajectoryActionController, controller::MultiJointController)

static const double ACCEPTABLE_BOUND = 0.05; // amount two positions can vary without being considered different positions.

/**
*	Constructor:
*	- the server must not be terminated
*	- initialize the number of joints
*	- initialize the joint names
*/
JointTrajectoryActionControllerSimulator::JointTrajectoryActionControllerSimulator()
{
    terminate_ = false;
    num_joints_ = 6;
    joint_names_.push_back("arm_1_joint");
    joint_names_.push_back("arm_2_joint");
    joint_names_.push_back("arm_3_joint");
    joint_names_.push_back("arm_4_joint");
    joint_names_.push_back("arm_5_joint");
    joint_names_.push_back("arm_6_joint");

    setIsAction(false);
}

/**
*	Destructor:
*	Empty by default.
*/
JointTrajectoryActionControllerSimulator::~JointTrajectoryActionControllerSimulator()
{
}

/**
*	Getters and Setters:
*/
void JointTrajectoryActionControllerSimulator::setTrajectoryUpdateRate(int value)
{
	trajectory_update_rate_ = value;
}

int JointTrajectoryActionControllerSimulator::getTrajectoryUpdateRate()
{
	return trajectory_update_rate_;
}

void JointTrajectoryActionControllerSimulator::setStateUpdateRate(int value)
{
	state_update_rate_ = value;
}

int JointTrajectoryActionControllerSimulator::getStateUpdateRate()
{
	return state_update_rate_;
}

void JointTrajectoryActionControllerSimulator::setIsAction(bool value)
{
	is_action = value;
}

bool JointTrajectoryActionControllerSimulator::isAction()
{
	return is_action;
}

/**
*	Initialize:
*	- Update rate to processTrajectory set to 1000
*	- Update rate to updateState set to 50 
*	- Fill node parameters
*	- Resize feedback_msg_
*/
bool JointTrajectoryActionControllerSimulator::initialize(std::string prefix)
{
    setTrajectoryUpdateRate(70);
    setStateUpdateRate(50);

    node_.param<double>(prefix + "goal_time", goal_time_constraint_, 0.0);
    node_.param<double>(prefix + "stopped_velocity_tolerance", stopped_velocity_tolerance_, 0.01);
    node_.param<double>("joint_trajectory_action_node/min_velocity", min_velocity_, 0.1);
    node_.param<double>("joint_trajectory_action_node/max_velocity", max_velocity_, 0.5);

    goal_constraints_.resize(num_joints_);
    trajectory_constraints_.resize(num_joints_);

    for (size_t i = 0; i < num_joints_; ++i)
    {
        node_.param<double>(prefix + joint_names_[i] + "/goal", goal_constraints_[i], -1.0);
        node_.param<double>(prefix + joint_names_[i] + "/trajectory", trajectory_constraints_[i], -1.0);
    }

    // Setup/resize feedback message
    feedback_msg_.joint_names = joint_names_;
    feedback_msg_.desired.positions.resize(num_joints_);
    feedback_msg_.desired.velocities.resize(num_joints_);
    feedback_msg_.desired.accelerations.resize(num_joints_);
    feedback_msg_.actual.positions.resize(num_joints_);
    feedback_msg_.actual.velocities.resize(num_joints_);
    feedback_msg_.actual.accelerations.resize(num_joints_);
    feedback_msg_.error.positions.resize(num_joints_);
    feedback_msg_.error.velocities.resize(num_joints_);
    feedback_msg_.error.accelerations.resize(num_joints_);


    return true;
}

/**
*	- ros::Subscriber command_sub_ is subscribed to command topic. Requests are processed by JointTrajectoryActionControllerSimulator::processCommand.
*	- ros::Subscriber joint_state_sub_ is subscribed to joint_states topic. Requests are processed by JointTrajectoryActionControllerSimulator::getJointStates.
*
*	- ros::Publisher state_pub__ publishes onto state topic. Topic published is control_msgs::FollowJointTrajectoryFeedback type.
*
*	- action_server_ pointer is reset.
*		+ See: typedef actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> FJTAS;
*		+ New FJTAS (simple action server) is created:
*			- SimpleActionServer (ros::NodeHandle n, std::string name, ExecuteCallback execute_cb, bool auto_start), the callback is processed by JointTrajectoryActionControllerSimulator::processFollowTrajectory.
*	- sction_server_ (simple action server) is started
*	
*	- Se crea un thread para JointTrajectoryActionControllerSimulator::updateState
*/

void JointTrajectoryActionControllerSimulator::start()
{
    
    JointTrajectoryActionControllerSimulator::initialize("joint_trajectory_action_node/constraints/");

    command_sub_ = node_.subscribe("command", 50, &JointTrajectoryActionControllerSimulator::processCommand, this);
    joint_state_sub_ = node_.subscribe("/rescuer/joint_states", 100, &JointTrajectoryActionControllerSimulator::getJointStates, this);
    state_pub_ = node_.advertise<control_msgs::FollowJointTrajectoryFeedback>("state", 50);
    trajectory_pub_ = node_.advertise<trajectory_msgs::JointTrajectory>("/rescuer/arm_controller/command", 70);

    action_server_.reset(new FJTAS(node_, "follow_joint_trajectory",
                                   boost::bind(&JointTrajectoryActionControllerSimulator::processFollowTrajectory, this, _1),
                                   false));
    action_server_->start();
    feedback_thread_ = new boost::thread(boost::bind(&JointTrajectoryActionControllerSimulator::updateState, this));

    //JointTrajectoryActionControllerSimulator::initState();
}

/**
*	Stops the controller
*/

void JointTrajectoryActionControllerSimulator::stop()
{
    {
        boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
        terminate_ = true;
    }

    feedback_thread_->join();
    delete feedback_thread_;

    command_sub_.shutdown();
    state_pub_.shutdown();
    trajectory_pub_.shutdown();
    joint_state_sub_.shutdown();
    action_server_->shutdown();
}

/**
*	Process command request:
*		- Wait to server availability to process trajectory commands
*		- Call processTrajectory
*/

void JointTrajectoryActionControllerSimulator::processCommand(const trajectory_msgs::JointTrajectoryConstPtr& msg)
{
    if (action_server_->isActive())
    {
        action_server_->setPreempted();
    }

    while (action_server_->isActive())
    {
        ros::Duration(0.01).sleep();
    }

    setIsAction(false);
    processTrajectory(*msg);
}

//=============================================================================
// Process Follow Trajectory						      =
// This is what MoveIt is sending out                                         =
//=============================================================================


void JointTrajectoryActionControllerSimulator::processFollowTrajectory(const control_msgs::FollowJointTrajectoryGoalConstPtr& goal)
{
    setIsAction(true);
    processTrajectory(goal->trajectory);
}

/**
*	Init State - TODO: not used
*/

void JointTrajectoryActionControllerSimulator::initState()
{

	for (size_t j = 0; j < joint_names_.size(); ++j)
        {
            joint_states_.position[j] = 0;
            joint_states_.velocity[j] = 0;
        }

}

/**
*	Update state.
*		- Fills the feedback_msg_
*		- Publish the feedback_msg_
*/

void JointTrajectoryActionControllerSimulator::updateState()
{
    ros::Rate rate(getStateUpdateRate());

    while (node_.ok())
    {
        {
            boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
            if (terminate_) {
                break;
            }
        }


        feedback_msg_.header.stamp = ros::Time::now();
	
	if(sizeof(joint_states_.position) == 0)	
	{
		for (size_t j = 0; j < joint_names_.size(); ++j)
		{
		    feedback_msg_.desired.positions[j] = joint_states_.position[j];
		    feedback_msg_.desired.velocities[j] = std::abs(joint_states_.velocity[j]);
		    feedback_msg_.actual.positions[j] = joint_states_.position[j];
		    feedback_msg_.actual.velocities[j] = std::abs(joint_states_.velocity[j]);
		    feedback_msg_.error.positions[j] = feedback_msg_.actual.positions[j] - feedback_msg_.desired.positions[j];
		    feedback_msg_.error.velocities[j] = feedback_msg_.actual.velocities[j] - feedback_msg_.desired.velocities[j];
		}
	}

        state_pub_.publish(feedback_msg_);

        rate.sleep();
    }
}

/**
*	Check Controller Joints	
*		- Check that all the joints in the trajectory exist in this multiDOF controller
*/

bool JointTrajectoryActionControllerSimulator::checkControllerJoints(const trajectory_msgs::JointTrajectory& traj_msg, std::vector<int> &lookup)
{
    // Maps from an index in joint_names_ to an index in the JointTrajectory msg "traj_msg"

    for (size_t j = 0; j < num_joints_; ++j)
    {
        for (size_t k = 0; k < traj_msg.joint_names.size(); ++k)
        {
            if (traj_msg.joint_names[k] == joint_names_[j])
            {
                lookup[j] = k;
                break;
            }
        }

        if (lookup[j] == -1)
        {
	    generateErrorNotLocatedJoint(j);
            return false;
        }
    }

    return true;
}

/**
*	Generates a error with unable to locate joint
*/

void JointTrajectoryActionControllerSimulator::generateErrorNotLocatedJoint(const size_t index)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::INVALID_JOINTS;
	error_msg = "Unable to locate joint " + joint_names_[index] + " in the commanded trajectory";
	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg.c_str());
	}
}

/**
* 	Validate initial positions
*/

bool JointTrajectoryActionControllerSimulator::validateInitialPositions(const trajectory_msgs::JointTrajectory& traj_msg)
{

    // Check for initial position
    if (traj_msg.points[0].positions.empty())
    {
	generateErrorFirstPointNoPositions();
        return false;
    }

    return true;
}

/**
*	Generates a error: First point of trajectory has no positions
*/

void JointTrajectoryActionControllerSimulator::generateErrorFirstPointNoPositions()
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

        trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::INVALID_GOAL;
        error_msg = "First point of trajectory has no positions";
        ROS_ERROR("%s", error_msg.c_str());
        if (isAction())
        {
            action_server_->setAborted(trajectory_result, error_msg);
        }
}


/**
* 	Find out segments duration
*/

void JointTrajectoryActionControllerSimulator::fillSegmentsDuration(const trajectory_msgs::JointTrajectory& traj_msg, std::vector<double> &durations, double &trajectory_duration)
{
    int num_points = traj_msg.points.size();

    durations[0] = traj_msg.points[0].time_from_start.toSec();
    trajectory_duration = durations[0];

    for (int i = 1; i < num_points; ++i)
    {
        durations[i] = (traj_msg.points[i].time_from_start - traj_msg.points[i-1].time_from_start).toSec();
        trajectory_duration += durations[i];
        ROS_DEBUG("tpi: %f, tpi-1: %f", traj_msg.points[i].time_from_start.toSec(), traj_msg.points[i-1].time_from_start.toSec());
        ROS_DEBUG("i: %d, duration: %f, total: %f", i, durations[i], trajectory_duration);
    }
}

/*
*	Checks that the incoming segment has the right number of velocity elements
*/

bool JointTrajectoryActionControllerSimulator::checkSegmentVelocityElements(const trajectory_msgs::JointTrajectoryPoint point,const int position)
{


        if (!point.velocities.empty() && point.velocities.size() != num_joints_)
        {
		generateErrorUnexpectedVelocities(point.velocities.size(),position);
		return false;
        }

	return true;
}

/**
*	Generates a error: a command point has not the right number of velocities.
*/

void JointTrajectoryActionControllerSimulator::generateErrorUnexpectedVelocities(const size_t size, const int position)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::INVALID_GOAL;
	error_msg = 	"Command point " + boost::lexical_cast<std::string>(position) + " has " +
			boost::lexical_cast<std::string>(size) +
			" elements for the velocities, expecting " + boost::lexical_cast<std::string>(num_joints_);

	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}
}


/*
*	Checks that the point has a not empty number of velocities
*/

bool JointTrajectoryActionControllerSimulator::checkPointWithoutVelocity(const trajectory_msgs::JointTrajectoryPoint point)
{

        if(point.velocities.size() == 0)
        {
	     generateErrorNoVelocities();
             return false;
        }

	return true;
}

/**
*	Generates a error: No velocities included in the trajectory method.
*/

void JointTrajectoryActionControllerSimulator::generateErrorNoVelocities()
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	error_msg = "No velocities included in the trajectory method!";
	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}

}



/*
*	Checks that the incoming segment has the right number of position elements
*/

bool JointTrajectoryActionControllerSimulator::checkSegmentPositionElements(const trajectory_msgs::JointTrajectoryPoint point,const int position)
{

        if (!point.positions.empty() && point.positions.size() != num_joints_)
        {
		generateErrorUnexpectedPositions(point.positions.size(),position);
        	return false;
        }

	return true;
}

/**
*	Generates a error: a command point has not the right number of positions.
*/

void JointTrajectoryActionControllerSimulator::generateErrorUnexpectedPositions(const size_t size, const int position)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::INVALID_GOAL;
	error_msg =	"Command point " + boost::lexical_cast<std::string>(position) + " has " +
			boost::lexical_cast<std::string>(size) +
			" elements for the positions, expecting " + boost::lexical_cast<std::string>(num_joints_);

	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}
}


/*
*	Create Segments
*		- Decide segments start time
*/

bool JointTrajectoryActionControllerSimulator::createSegments(const trajectory_msgs::JointTrajectory& traj_msg, const ros::Time time, const std::vector<double> durations , const std::vector<int> lookup, std::vector<Segment> &trajectory)
{
    int num_points = traj_msg.points.size();

    for (int i = 0; i < num_points; ++i)
    {
        const trajectory_msgs::JointTrajectoryPoint point = traj_msg.points[i];
        Segment seg;

       	bool csve = checkSegmentVelocityElements(point,i);
	if(!csve){ return false;}

	bool cspe = checkSegmentPositionElements(point,i);
	if(!cspe){ return false;}

	bool cpwv = checkPointWithoutVelocity(point);
	if(!cpwv){ return false;}

	// Decide segments start time
        if (traj_msg.header.stamp == ros::Time(0.0)) {
            seg.start_time = (time + point.time_from_start).toSec() - durations[i];
        }
        else {
            seg.start_time = (traj_msg.header.stamp + point.time_from_start).toSec() - durations[i];
        }

        seg.duration = durations[i];
        seg.velocities.resize(num_joints_);
        seg.positions.resize(num_joints_);

        for (size_t j = 0; j < num_joints_; ++j)
        {
            seg.velocities[j] = point.velocities[lookup[j]];
            seg.positions[j] = point.positions[lookup[j]];
        }

        trajectory.push_back(seg);
    }

    return true;
}

/**
*	Process Trajectory
*	Error check the trajectory then send it to the controllers
*/

bool JointTrajectoryActionControllerSimulator::checkOnGoal(const std::vector<Segment> trajectory, const std::vector<int> lookup)
{

    // Check if this trajectory goal is already fullfilled by robot's current position
    bool outside_bounds = false; // flag for remembing if a different position was found
    const Segment* last_segment = &trajectory[trajectory.size()-1];

    for( std::size_t i = 0; i < last_segment->positions.size(); ++i)
    {

        std::string joint_name = joint_names_[i];
        int joint_index = lookup[i];

        ROS_DEBUG_STREAM("Checking for similarity on joint " << joint_name << " with real position " << joint_states_.position[joint_index]);
        ROS_DEBUG_STREAM("    Iterator id = " << i << " size " << last_segment->positions.size() << "    Goal position: " << last_segment->positions[i] );

        // Test if outside acceptable bounds, meaning we should continue trajectory as normal
        if( last_segment->positions[i] > (joint_states_.position[joint_index] + ACCEPTABLE_BOUND) ||
                last_segment->positions[i] < (joint_states_.position[joint_index] - ACCEPTABLE_BOUND) )
        {
            outside_bounds = true;
            break;
        }
    }
    // Check if all the states were inside the position bounds
    if( !outside_bounds )
    {
	generateSuccesfulTrajectorySkipped();
        return true;
    }

    return false;
}

/**
*	Generates a message: Trajectory execution skipped because goal is same as current state.
*/

void JointTrajectoryActionControllerSimulator::generateSuccesfulTrajectorySkipped()
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

        // We can exit trajectory
        trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::SUCCESSFUL;
        error_msg = "Trajectory execution skipped because goal is same as current state";
        ROS_INFO("%s", error_msg.c_str());
        action_server_->setSucceeded(trajectory_result, error_msg);
}

/**
*	Check motors are within their goal constraints
*/

bool JointTrajectoryActionControllerSimulator::checkGoalConstraints()
{

    for (size_t i = 0; i < num_joints_; ++i)
    {
        if (goal_constraints_[i] > 0 && std::abs(feedback_msg_.error.positions[i]) > goal_constraints_[i])
        {
	    generateErrorGoalConstraints(i);
            return false;
        }
    }

    return true;
}

/**
*	Generates a error: a command point has not the right number of positions.
*/

void JointTrajectoryActionControllerSimulator::generateErrorGoalConstraints(const size_t index)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;
	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::GOAL_TOLERANCE_VIOLATED;
				error_msg = "Aborting at end because " + joint_names_[index] +
				" joint wound up outside the goal constraints. The position error " +
				boost::lexical_cast<std::string>(fabs(feedback_msg_.error.positions[index])) +
				" is larger than the goal constraints " + boost::lexical_cast<std::string>(goal_constraints_[index]);
	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}
}


/**
*	Verify trajectory constraints
*/

bool JointTrajectoryActionControllerSimulator::validateTrajectoryConstraints(int segment_index)
{

    for (size_t j = 0; j < joint_names_.size(); ++j)
    {
        if (trajectory_constraints_[j] > 0.0 && feedback_msg_.error.positions[j] > trajectory_constraints_[j])
        {
		   generateErrorTrajectoryConstraints(segment_index, j);
                   return false;
        }
    }

    return true;
}

/**
*	Generates a error: a command point has not the right number of positions.
*/

void JointTrajectoryActionControllerSimulator::generateErrorTrajectoryConstraints(const int segment_index, const size_t index)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::PATH_TOLERANCE_VIOLATED;
				error_msg = "Unsatisfied position constraint for " + joint_names_[index] +
				" trajectory point " + boost::lexical_cast<std::string>(segment_index) +
				", " + boost::lexical_cast<std::string>(feedback_msg_.error.positions[index]) +
				" is larger than " + boost::lexical_cast<std::string>(trajectory_constraints_[index]);

	ROS_ERROR("%s", error_msg.c_str());

	if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}

}


/**
*	Get Joint States
*/

void JointTrajectoryActionControllerSimulator::getJointStates(const sensor_msgs::JointStateConstPtr &joint_state)
{
    joint_states_ = *joint_state;
    return;
}

/**
*	Process Trajectory
*	Error check the trajectory then send it to the controllers
*/

void JointTrajectoryActionControllerSimulator::processTrajectory(const trajectory_msgs::JointTrajectory& traj_msg)
{
    ROS_INFO_STREAM("\n inside process Trajectory with msg:\n" << traj_msg);

    int num_points = traj_msg.points.size();
    std::vector<int> lookup(num_joints_, -1);
    double trajectory_duration = 0.0;
    std::vector<double> durations;
    std::vector<Segment> trajectory;
    ros::Time time;
    ros::Rate rate(getTrajectoryUpdateRate());

    durations.resize(num_points);

    ROS_DEBUG("Received trajectory with %d points", num_points);

    if(!checkControllerJoints(traj_msg, lookup)) return;
    ROS_DEBUG("Checked controller joints");

    if(!validateInitialPositions(traj_msg)) return;
    ROS_DEBUG("Validated initial positions");

    fillSegmentsDuration(traj_msg, durations,trajectory_duration);
    ROS_DEBUG("Found out segments duration");

    time = ros::Time::now() + ros::Duration(0.01);
    if(!createSegments(traj_msg,time,durations,lookup,trajectory)) return;
    ROS_DEBUG("Segments created");

    if(checkOnGoal(trajectory, lookup)) return;
    ROS_DEBUG("Not on goal... going on...");

    ROS_INFO("Trajectory start requested at %.3lf, waiting...", traj_msg.header.stamp.toSec());
    ros::Time::sleepUntil(traj_msg.header.stamp);

    ros::Time end_time = traj_msg.header.stamp + ros::Duration(trajectory_duration);
    std::vector<ros::Time> seg_end_times(num_points, ros::Time(0.0));

    for (int i = 0; i < num_points; ++i)
    {
        seg_end_times[i] = ros::Time(trajectory[i].start_time + durations[i]);
    }

    ROS_INFO("Trajectory start time is %.3lf, end time is %.3lf, total duration is %.3lf", time.toSec(), end_time.toSec(), trajectory_duration);

    trajectory_ = trajectory;

    trajectory_msgs::JointTrajectory trajectorySimulated;

    trajectorySimulated.joint_names.resize(num_joints_);

    for(size_t i=0; i < num_joints_; i++)
    {
    	trajectorySimulated.joint_names[i] = joint_names_[i];
    }
 
    trajectorySimulated.points.resize(num_points);

    //------------------------------------------------------------------------------------------------
    // The main loop

    for (int segment_index = 0; segment_index < num_points; ++segment_index)
    {
        ROS_DEBUG("Processing segment %d -------------------------------------------------", segment_index);

        // first point in trajectories calculated by OMPL is current position with duration of 0 seconds, skip it
        //if (durations[segment_index] == 0.0)
        //{
        //    ROS_DEBUG("Skipping segment %d because duration is 0", segment_index);
        //    continue;
        //}

	trajectorySimulated.points[segment_index].positions.resize(num_joints_);
	trajectorySimulated.points[segment_index].velocities.resize(num_joints_);

	std::vector<double> desired_velocities(num_joints_,-1);
	std::vector<double> desired_positions(num_joints_,-1);

        // Loop through every joint 
	for(int joint_index = 0; joint_index < num_joints_ ; joint_index++)
        {

                // Get start position of this joint
                double start_position;

                if (segment_index != 0)
                {
                    start_position = trajectory[segment_index-1].positions[joint_index];
                }
                else
                {
                    start_position = joint_states_.position[lookup[joint_index]];
                }

                double desired_position = trajectory[segment_index].positions[joint_index];
		double desired_velocity = (desired_position - start_position) / durations[segment_index];

		desired_velocities[joint_index] = desired_velocity;
		desired_positions[joint_index] = desired_position;

                ROS_DEBUG("\tstart_position: %f, duration: %f", start_position, durations[segment_index]);

                if( std::abs(desired_velocity) > max_velocity_ )
                {
		    generateErrorMaxVelocityExceeded(segment_index,joint_index,desired_velocity);
                    return;
                }
        }

	trajectorySimulated.points[segment_index] = getTrajectoryPoint(desired_velocities,desired_positions,traj_msg.points[segment_index].time_from_start);

	/*	
        time = ros::Time::now(); //TODO maybe remove this

        while (time < seg_end_times[segment_index])
        {
            if (isAction() && action_server_->isPreemptRequested())
            {
                
		for(int joint_index = 0; joint_index < num_joints_ ; joint_index++)
                {

                        double desired_position = joint_states_.position[lookup[joint_index]];
                        double desired_velocity = joint_states_.velocity[lookup[joint_index]];

			desired_velocities[joint_index] = desired_velocity;
			desired_positions[joint_index] = desired_position;
                }

		publishVelocitiesMessage(getVelocitiesMessage(desired_velocities,desired_positions));
		generateSuccesfulTrajectoryPreemted();
	    }


	    publishVelocitiesMessage(getVelocitiesMessage(desired_velocities,desired_positions));
            rate.sleep(); //TODO maybe uncomment this
            time = ros::Time::now();
        }


        bool validTrajectoryConstraints = JointTrajectoryActionControllerSimulator::validateTrajectoryConstraints(segment_index);
        if (!validTrajectoryConstraints)
            return;

	*/

    } 

    publishTrajectoryMessage(trajectorySimulated);

    bool validGoalConstraints = JointTrajectoryActionControllerSimulator::checkGoalConstraints();
    if( !validGoalConstraints )
        return;

    // -----------------------------------------------------------------------------------------------
    // Finish up - Set result

    generateSuccesfulTrajectory();

}

/**
*	Generates a error: max velocity exceeded for a joint.
*/

void JointTrajectoryActionControllerSimulator::generateErrorMaxVelocityExceeded(const int segment_index, const int joint_index, const double desired_velocity)
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::PATH_TOLERANCE_VIOLATED;
	error_msg = "Invalid joint trajectory: max velocity exceeded for joint " + joint_names_[joint_index] +
                    " with a velocity of " + boost::lexical_cast<std::string>(desired_velocity) +
                    " when the max velocity is set to " + boost::lexical_cast<std::string>(max_velocity_) +
                    ". On trajectory step " + boost::lexical_cast<std::string>(segment_index);

	ROS_ERROR("%s", error_msg.c_str());

        if (isAction())
	{
		action_server_->setAborted(trajectory_result, error_msg);
	}
}

/**
*	Generates a succesful trajectory: Canceled trajectory (preemted).
*/

void JointTrajectoryActionControllerSimulator::generateSuccesfulTrajectoryPreemted()
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::SUCCESSFUL;
        error_msg = "Trajectory controller recieved preempt request from action server. Canceling trajectory.";

	action_server_->setPreempted(trajectory_result, error_msg);
        ROS_WARN("%s", error_msg.c_str());
}

/**
*	Generates a succesful trajectory.
*/

void JointTrajectoryActionControllerSimulator::generateSuccesfulTrajectory()
{
	control_msgs::FollowJointTrajectoryResult trajectory_result;
	std::string error_msg;

	trajectory_result.error_code = control_msgs::FollowJointTrajectoryResult::SUCCESSFUL;
	error_msg = "Trajectory execution successfully completed";
	ROS_INFO("%s", error_msg.c_str());

	action_server_->setSucceeded(trajectory_result, error_msg);
}

/**
*	Generates velocity command to arm.
*/
trajectory_msgs::JointTrajectoryPoint JointTrajectoryActionControllerSimulator::getTrajectoryPoint(const std::vector<double> velocities, const std::vector<double> positions, const ros::Duration duration)
{
	std::string error_msg;
	error_msg = "Building trajectory_msgs/JointTrajectoryPoint Message";
	ROS_INFO("%s", error_msg.c_str());

	trajectory_msgs::JointTrajectoryPoint jointTrajectoryPoint;

	jointTrajectoryPoint.positions.resize(num_joints_);
	jointTrajectoryPoint.velocities.resize(num_joints_);
	jointTrajectoryPoint.time_from_start = duration;

	for(size_t j=0; j < num_joints_; j++)
	{
		jointTrajectoryPoint.positions[j] = positions[j];	
		jointTrajectoryPoint.velocities[j] = velocities[j];	
	}

	return jointTrajectoryPoint;
}
trajectory_msgs::JointTrajectoryPoint JointTrajectoryActionControllerSimulator::getTrajectoryPointZero()
{
	std::string error_msg;
	error_msg = "Building trajectory_msgs/JointTrajectoryPoint Message";
	ROS_INFO("%s", error_msg.c_str());

	trajectory_msgs::JointTrajectoryPoint jointTrajectoryPoint;

	return jointTrajectoryPoint;
}

/**
*	Publish velocity command to arm.
*/

void JointTrajectoryActionControllerSimulator::publishTrajectoryMessage(const trajectory_msgs::JointTrajectory trajectorySimulated)
{
	std::string error_msg;
	error_msg = "Sending brics_actuator/JointVelocities Message";
	ROS_INFO("%s", error_msg.c_str());

        trajectory_pub_.publish(trajectorySimulated);
}

//=============================================================================
// MAIN                                                                       =
//=============================================================================

int main(int argc, char **argv)
{
    ros::init(argc, argv, "rescuer_joint_action_controller");
    //ros::NodeHandle n;

    //ros::ServiceServer service = n.advertiseService("Server", Server::Move);

    JointTrajectoryActionControllerSimulator jtac;    
    jtac.start();
    ROS_INFO("Ready to move rescuer arm.");
    ros::spin();

    return 0;
}
