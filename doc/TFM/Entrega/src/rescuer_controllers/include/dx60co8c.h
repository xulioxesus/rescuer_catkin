/** \file dx60co8c.h
 * \author Robotnik Automation S.L.L.
 * \author robotnik
 * \version 1.0
 * \date    2006-2007
 *
 * \brief Header for Servo Motor Driver dx60co8c  
 (C) 2007 Robotnik Automation, SLL
*/

#include "ntcan.h" 

#define TXBUFSIZE 				200			/*size of the can transmision buffer*/
#define RXBUFSIZE 				200			/*size of the can reader buffer*/
#define TXTOUT 					1000		/*timeout in ms for write accesses*/
#define RXTOUT 					5000		/*timeout in ms for read accesses*/
#define CAN_OPEN_MODE 			0

#define PERIODIC_CAN_MESAGES 	6

#define LEFT_ID 				2
#define RIGHT_ID 				3
#define NET_ID 					0	// CanBus id of the drivers network

#define MAX_SPEED_RPM			1250			// Maximal motor speed 
#define RPM2REF					6.66666667 		// Rpm to reference
#define REF2RPM 				1.0 / RPM2REF 	// Read speed to rpm
#define PERCENT2REF				MAX_SPEED_RPM * RPM2REF / 100.0 // rpm 2 speed reference

typedef unsigned char byte;

struct data_bytes{
	unsigned char byte_1;
	unsigned char byte_2;
	unsigned char byte_3;
	unsigned char byte_4;
};

typedef struct periodic_msg{
	short statusword_left;
	short statusword_right;
	double v_left_rpm;
	double v_right_rpm;
	int32_t v_left_ref;
	int32_t v_right_ref;
	unsigned char digital_outputs_right;
}periodic_msg;

int dx60co8c_Connect(HANDLE *handle, int NET, int baudrate);
int dx60co8c_SendMsg(HANDLE handle, CMSG *cmsg, int32_t *len);
int dx60co8c_ReadMsg(HANDLE handle, CMSG *cmsg, int32_t *len);
int dx60co8c_SendMsgWithConfirmation(HANDLE handle, CMSG cmsg, int32_t *len);
int dx60co8c_ProcessConfigFile(char *file, HANDLE handle, byte id);

void dx60co8c_CreateWriteMsg(CMSG* msg, unsigned char command, unsigned char MSB, unsigned char LSB, unsigned char subindex, struct data_bytes data, byte id);
void dx60co8c_CreateResetMsg(CMSG* msg, byte id);
void dx60co8c_CreateShutdownMsg(CMSG* msg, byte id);
void dx60co8c_CreateSwitchOnEnableMsg(CMSG* msg, byte id);
void dx60co8c_CreateVelocityRefMsg(CMSG* msg, byte id, double rpm );
void dx60co8c_CreateDisableVoltageMsg(CMSG* msg, byte id);
void dx60co8c_CreateAutoSwitchOnMsg(CMSG* msg, byte id);
void dx60co8c_CreateCanBusBitRateMsg(CMSG* msg, byte id);
void dx60co8c_CreateTorqueValueMsg(CMSG* msg, byte id);
void dx60co8c_CreateAccelerationValueMsg(CMSG* msg, byte id);
void dx60co8c_CreateVelocityValueMsg(CMSG* msg, byte id);
void dx60co8c_CreatePositionValueMsg(CMSG* msg, byte id);
void dx60co8c_CreateProgOverVoltageMsg(CMSG* msg, byte id);
void dx60co8c_CreateProgUnderVoltageMsg(CMSG* msg, byte id);
void dx60co8c_CreateApDcBusVoltageMsg(CMSG* msg, byte id);
void dx60co8c_CreateMotorDataTypeMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCPoleCountMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCBackEMFConstantMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCTorqueConstantMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCPtoPResistanceMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCPtoPInductanceMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCThermalTimeCMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCMaxCurrentCMsg(CMSG* msg, byte id);
void dx60co8c_CreateBLDCMaxSpeedCMsg(CMSG* msg, byte id);
void dx60co8c_CreateTPDOIncPosChangeCMsg(CMSG* msg, byte id);
void dx60co8c_CreateBufferPositionCMsg(CMSG* msg, byte id);
void dx60co8c_Create1stRecPDOComCMsg(CMSG* msg, byte id);
void dx60co8c_Create2stRecPDOComCMsg(CMSG* msg, byte id);

int dx60co8c_OpenCan(void);				// Open
int dx60co8c_CloseCan(void);				// Close
int dx60co8c_SendResetMsg(void);
int dx60co8c_SendSwitchOnMsg(void);
int dx60co8c_SendShutdownMsg(void);
int dx60co8c_SendEnableOperationMsg(void);
int dx60co8c_SendVelocityRefMsg(double rpm_left, double rpm_right);
int dx60co8c_SendDisableVoltageMsg(void);
int dx60co8c_SendStatusWordMsg(void);
int dx60co8c_QueryStatusWordMsg(void);
int dx60co8c_ReadStatusWordMsg(short *statusLeft, short *statusRight);
//void dx60co8c_SendReadVelocityMsg(byte id);
//int32_t dx60co8c_ReadVelocityMsg(byte id);
void dx60co8c_SendReadVelocityMsg(void);
int dx60co8c_ReadVelocityMsg(int32_t* v_left_rpm, int32_t* v_right_rpm);

void dx60co8c_SetSendVelocityRefMsg(CMSG* msg, double rpm);
void dx60co8c_CreateSendVelocityRefMsg(CMSG* msg, byte id);
void dx60co8c_CreateReadVelocityRefMsg(CMSG* msg, byte id);
void dx60co8c_CreateQueryStatusWordMsg(CMSG* msg, byte id);
int dx60co8c_CreatePeriodicMsgs();
int dx60co8c_SendPeriodicMsgs(periodic_msg data_to_send);
int dx60co8c_ReadPeriodicMsgs(periodic_msg *data_to_receive);
void dx60co8c_InitPeriodicMsg(periodic_msg *msg);
//ANALOG MESSAGES
void dx60co8c_CreateReadAnalogInputsParamaters(CMSG *msg, byte id, byte input);
void dx60co8c_CreateReadAnalogInputs(CMSG *msg, byte id, byte input);
int dx60co8c_ReadAnalogInputsParamaters(byte id, byte input, int *offset);
void dx60co8c_CreateReadDigitalOutputs(CMSG *msg, byte id, byte input);
void dx60co8c_CreateSetDigitalOutputs(CMSG *msg, byte id, byte input);
void dx60co8c_CreateReadDigitalInputs(CMSG *msg, byte id, byte input);
