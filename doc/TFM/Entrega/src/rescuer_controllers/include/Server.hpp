#include "ros/ros.h"
#include "PrimitiveDriver.h"
#include "rescuer_odom/Velocity.h"
#include "time.h"

#define DEFAULT_RESCUER_PORT            "/dev/can0"
#define MOTOR_DEF_MAX_SPEED		1		//Max Rover Linear Speed in m/s
#define MOTOR_DEF_MAX_TURNSPEED		60		//Max Rover Angular Speed in rad/s 
#define DEFAULT_LOG_FOLDER              "log"
#define	RTOD(r)   ((r) * 180 / M_PI)
#define	DTOR(d)   ((d) * M_PI / 180)

class Server
{
	
	public:
		static PrimitiveDriver driver;
		Server();
		~Server();
		static bool Move(rescuer_odom::Velocity::Request &req, rescuer_odom::Velocity::Response &res);
		static void Sleep(unsigned int mseconds);
};
