#ifndef __DEFINES_H
	#define __DEFINES_H
	
	#include <pthread.h>
	
	// DEBUG LEVEL IDs
	#define DEBUG_LEVEL_PD			3	//Primitive Driver
	#define DEBUG_LEVEL_CAN 		4	//CAN 	
	#define DEBUG_LEVEL_MOTORDRIVE  3	//MotorDrive
	
	// THREAD's PRIORITIES 0[min]-80[max]
	#define THREAD_PRIOR_PD			25
	#define THREAD_PRIOR_RESCUER	25
	#define THREAD_PRIOR_MOTORDRIVE 25

	
	// ERROR CODEs
	#define ERROR_NONE								1
	#define ERROR_COMM_STATUS_FAULT					10
	#define ERROR_MOTOR_STATUS_FAULT				11
	#define ERROR_INITILIZATION_FAULT				12
	#define ERROR_CAN_FATAL							13
	#define ERROR_CAN_CONNECTION					14
	#define ERROR_SENDING_COMMAND					15

	// RETURN VALUEs
	#define OK				0
	#define ERROR			-1
	
	///////////// POR VER... /////////////
	#define ROVER_PI 								3.14159265358979323846
	#define USEC_PER_SEC							1000000
	#define NSEC_PER_SEC							1000000000
	
	// Global functions
	// Use of real time nanosleep
	extern int clock_nanosleep(clockid_t __clock_id, int __flags, 
				__const struct timespec *__req, 
				struct timespec *__rem);
	// Common functions to all components
	// Normalize in sec.ns
	static inline void tsnorm(struct timespec *ts)
	{
		while (ts->tv_nsec >= NSEC_PER_SEC) {
			ts->tv_nsec -= NSEC_PER_SEC;
			ts->tv_sec++;
		}
	}
	
	// Normalize in rad
	static inline void radnorm(double* radians)
	{
		while (*radians >= (2.0 * ROVER_PI)) {
			*radians -= 2.0 * ROVER_PI;		
			}
		while (*radians <= (-2.0 * ROVER_PI)) {
			*radians += 2.0 * ROVER_PI;
			}
	}
	
	// Normalize in rad
	// Normalize between -PI and PI
	static inline void radnorm2(double* radians)
	{
		while (*radians >= (ROVER_PI)) {
			*radians -= 2.0 * ROVER_PI;	
			}
		while (*radians <= (-ROVER_PI)) {
			*radians += 2.0 * ROVER_PI;
			}
	}
	
	
	// Returns time difference in uS
	static inline long calcdiff(struct timespec t1, struct timespec t2)
	{
		long diff;
		diff = USEC_PER_SEC * ((int) t1.tv_sec - (int) t2.tv_sec);
		diff += ((int) t1.tv_nsec - (int) t2.tv_nsec) / 1000;
		return diff;
	}
	
	
	struct thread_param{
		int prio;			// Thread priority level 0[min]-80[max]
		int clock;  		// CLOCK_MONOTONIC or CLOCK_REALTIME	
	};
	
	
#endif
