/** \file Component.h
 * \author Robotnik Automation S.L.L.
 * \version 1.0
 * \date    
 * 
 * \brief RESCUER Components                                          
 * (C) 2007 Robotnik Automation, SLL  
 *  All rights reserved.
*/

#include <pthread.h>


#ifndef __COMPONENT_H
	#define __COMPONENT_H

	// Component Controller ID's
	#define NONE			30
	#define WAYPOINT_DRIVER 31
	
	typedef struct OdomType
	{							// Z points to ground
		float xAbsM;			// X - Absolute location [m] conv from Long-Lat
		float yAbsM;			// Y - Absolute location [m] conv from Long-Lat
		float xM;				// X - Location [m]
		float yM;				// Y - Location [m]
		float zM; 				// Z - Altitude [m]
		float yawRad;			// orientation in [rad]
		float angularSpeedRads;	// angular speed [rad/s]
		float linearSpeedMps;	// linear speed [m/s]
	} OdomType;
	
	
	typedef struct RoverState
	{
		float xM;							// X - Location [m]
		float yM;							// Y - Location [m]
		float yawRad;						// orientation rad ?
		float speedMps;						// Speed [m/s]
		float desiredSpeedMps;				// Speed reference [m/s]
		float phiEffort;					// Z Rotational effort 
		float desiredPhiEffort;				// Z Rotational effort reference	
		OdomType odom;						// Odometry location estimation
	} RoverState;
	
	
	
	/*! \class Component
	 *	\author Robotnik Automation S.L.L
	 *	\version 1.0
	 *  \brief Class for the platform component
	 */
	class Component {
	  // Attributes
	public:
		/** 
		   * a private variable.
		   * Contains the state of the component rover (INIT,STARTUP,READY,SHUTDOWN,EMERGENCY,FAILURE).
		*/
		int state;
	
		/** 
		   * a private variable.
		   * Controls the execution of the ControlThread
		*/
		bool run;
	
		/**
			* a private variable.
			* Component's pthread identifier
		*/
		pthread_t pthread_id;
	
		/**
			* a private variable.
			* Indicates if thread is running.
		*/
		bool pthread_running;
	
		/**
			* a private variable.
			* Contains the real frequency, calculated in time of execution.
		*/
		double pthread_hz;
		
		
		// Operations
	public:
		/**
			* Public constructor
		*/
		Component();	
	
		/**
			* Public destructor
		*/
		virtual ~Component();
	
		/**
			* function to get the state of the platform
			* @return Component's state
		*/
		virtual inline int GetState () = 0;
		
		/**
			* function to get current update rate of the thread
			* @return pthread_hz
		*/
		virtual inline double GetUpdateRate()=0;
						
		/**
			* This function allows the abstracted component functionality contained in this file to be started from an external source.
			* It must be called first before the component state machine and mailman interaction will begin
			* Each call to "StartUp" should be followed by one call to the "ShutDown" function
			* @return OK
			* @return ERROR
		*/
		virtual int StartUp () = 0;
	
		/**
			* This function allows the abstracted component functionality contained in this file to be stoped from an external source.
			* If the component is in the running state, this function will terminate all threads running 
			* This function will also close the connection to the mailman and check out the component from the mailman network
			* @return OK
			* @return ERROR
		*/
		virtual int ShutDown () = 0;
		
		/**
			* All core component functionality is contained in this thread.
			* All of the Rover component state machine code can be found here.
			* @param *threadData, priority of thread and type of clock
		*/
		virtual void ControlThread () = 0;
	
	private:
		/**
			* function called during startup state 	
		*/
		virtual void StartUpState () = 0;
	
		/**
			* function called during initialization state 	
		*/
		virtual void InitState () = 0;
	
		/**
			* function called during ready state 	
		*/
		virtual void ReadyState () = 0;
	
		/**
			* function called during initialization state 	
		*/
		virtual void EmergencyState () = 0;
	
		/**
			* function called during failure state 	
		*/
		virtual void FailureState ()= 0;
	
		/**
			* function called during shutdown state 	
		*/
		virtual void ShutDownState ()=0;
		
		/**
			* function called during all states 	
		*/
		virtual void AllState()=0;
	
		/**
			* function that switches the state of the component into the desired state
			* @param new_state as an integer, the new state of the component
		*/	
		virtual void SwitchToState (int new_state)=0;
		
	
	};

#endif
