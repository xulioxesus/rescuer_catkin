
#ifndef RESCUER_ARM_TO_ZERO_H
#define RESCUER_ARM_TO_ZERO_H

// Standard
#include <sstream>
#include <string>
#include <vector>
#include <XmlRpcValue.h>

// Messages
#include <sensor_msgs/JointState.h>
#include <brics_actuator/JointVelocities.h>

// ROS
#include <ros/ros.h>
#include <ros/package.h> // for getting package file path
#include <pluginlib/class_list_macros.h>


class ArmToZero
{
private:
    ros::NodeHandle node_;
    int update_rate_;
    bool joint_state_readed_;

    ros::Subscriber joint_state_sub_;
    ros::Publisher velocity_pub_;

    int num_joints_;
    std::vector<std::string> joint_names_;
    sensor_msgs::JointState joint_states_;
    


public:
    ArmToZero();
    virtual ~ArmToZero();

    void setUpdateRate(int value);
    int getUpdateRate();

    void setJointStateReaded(bool value);
    bool isJointStateReaded();

    bool initialize();
    void start();
    void stop();
    bool isOnGoal();
    bool isJointOnGoal(int joint);
    bool isJointPositiveValue(int joint);
    bool isJointNegativeValue(int joint);
    void getJointStates(const sensor_msgs::JointStateConstPtr& joint_state);
    brics_actuator::JointVelocities getVelocitiesMessage();
    brics_actuator::JointVelocities getVelocitiesMessageZero();
    void publishVelocitiesMessage(const brics_actuator::JointVelocities velocities);
};

#endif  // RESCUER_ARM_TO_ZERO_H
