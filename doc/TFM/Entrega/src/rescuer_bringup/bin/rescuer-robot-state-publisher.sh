#!/bin/bash
# file: rescuer-bringup-powercube
 
source /opt/ros/hydro/setup.bash
source /home/rescuer/development/ros/rescuer_catkin/devel/setup.bash
export ROS_IP=192.168.1.100
 
roslaunch rescuer_bringup rescuer_robot_state_publisher.launch
