#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <visualization_msgs/Marker.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/common/common.h>

typedef pcl::PointXYZ PointT;

tf::TransformListener *tf_listener;
ros::Publisher pub;
ros::Publisher cluster_marker;

void publishClusterMarker(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster, std::string ns ,int id, float r, float g, float b)
{
	// Tranform cloud to base_link
	pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	(*cloud_cluster).header.frame_id = "camera_depth_optical_frame";
	//  tf_listener->waitForTransform("openni_depth_optical_frame","base_link",ros::Time::now(),ros::Duration(3.0));
	pcl_ros::transformPointCloud(ns,*cloud_cluster,*transformed_cloud, *tf_listener);

	// transformed_cloud = cloud_cluster;

	Eigen::Vector4f centroid;
	Eigen::Vector4f min;
	Eigen::Vector4f max;

	pcl::compute3DCentroid (*transformed_cloud, centroid);
	pcl::getMinMax3D (*transformed_cloud, min, max);

	uint32_t shape = visualization_msgs::Marker::CYLINDER;
	visualization_msgs::Marker marker;
	marker.header.frame_id = "base_footprint";
	marker.header.stamp = ros::Time::now();

	marker.ns = ns;
	marker.id = id;
	marker.type = shape;
	marker.action = visualization_msgs::Marker::ADD;

	marker.pose.position.x = centroid[0];
	marker.pose.position.y = centroid[1];
	marker.pose.position.z = centroid[2];
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = (max[0]-min[0]);
	marker.scale.y = (max[1]-min[1]);
	marker.scale.z = (max[2]-min[2]);

	if (marker.scale.x ==0)
		marker.scale.x=0.1;

	if (marker.scale.y ==0)
		marker.scale.y=0.1;

	if (marker.scale.z ==0)
		marker.scale.z=0.1;

	marker.color.r = r;
	marker.color.g = g;
	marker.color.b = b;
	marker.color.a = 0.5;

	marker.lifetime = ros::Duration();
	//   marker.lifetime = ros::Duration(0.5);
	cluster_marker.publish(marker); 
	return;
}
void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{

	// All the objects needed
	pcl::PCDReader reader;
	pcl::PassThrough<PointT> pass;
	pcl::NormalEstimation<PointT, pcl::Normal> ne;
	pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
	pcl::PCDWriter writer;
	pcl::ExtractIndices<PointT> extract;
	pcl::ExtractIndices<pcl::Normal> extract_normals;
	pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

	// Datasets
	pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
	pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
	pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);

	// Convert to PCL data type
	pcl::PCLPointCloud2 pcl_pc2;
	pcl_conversions::toPCL(*cloud_msg,pcl_pc2);
	pcl::PointCloud<PointT>::Ptr temp_cloud(new pcl::PointCloud<PointT>);
	pcl::fromPCLPointCloud2(pcl_pc2,*temp_cloud);

	// Build a passthrough filter to remove spurious NaNs
	pass.setInputCloud (temp_cloud);
	pass.setFilterFieldName ("z");
	pass.setFilterLimits (0, 1.5);
	pass.filter (*cloud_filtered);
	std::cerr << "PointCloud after filtering has: " << cloud_filtered->points.size () << " data points." << std::endl;

	// Estimate point normals
	ne.setSearchMethod (tree);
	ne.setInputCloud (cloud_filtered);
	ne.setKSearch (50);
	ne.compute (*cloud_normals);

	// Create the segmentation object for the planar model and set all the parameters
	seg.setOptimizeCoefficients (true);
	seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
	seg.setNormalDistanceWeight (0.1);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (100);
	seg.setDistanceThreshold (0.03);
	seg.setInputCloud (cloud_filtered);
	seg.setInputNormals (cloud_normals);
	// Obtain the plane inliers and coefficients
	seg.segment (*inliers_plane, *coefficients_plane);
	std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

	// Extract the planar inliers from the input cloud
	extract.setInputCloud (cloud_filtered);
	extract.setIndices (inliers_plane);
	extract.setNegative (false);

	// Write the planar inliers to disk
	pcl::PointCloud<PointT>::Ptr cloud_plane (new pcl::PointCloud<PointT> ());
	extract.filter (*cloud_plane);
	std::cerr << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;
	writer.write ("table_scene_mug_stereo_textured_plane.pcd", *cloud_plane, false);

	// Remove the planar inliers, extract the rest
	extract.setNegative (true);
	extract.filter (*cloud_filtered2);
	extract_normals.setNegative (true);
	extract_normals.setInputCloud (cloud_normals);
	extract_normals.setIndices (inliers_plane);
	extract_normals.filter (*cloud_normals2);

	// Create the segmentation object for cylinder segmentation and set all the parameters
	seg.setOptimizeCoefficients (true);
	seg.setModelType (pcl::SACMODEL_CYLINDER);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setNormalDistanceWeight (0.1);
	seg.setMaxIterations (10000);
	seg.setDistanceThreshold (0.05);
	seg.setRadiusLimits (0, 0.1);
	seg.setInputCloud (cloud_filtered2);
	seg.setInputNormals (cloud_normals2);

	// Obtain the cylinder inliers and coefficients
	seg.segment (*inliers_cylinder, *coefficients_cylinder);
	std::cerr << "Cylinder coefficients: " << *coefficients_cylinder << std::endl;

	// Write the cylinder inliers to disk
	extract.setInputCloud (cloud_filtered2);
	extract.setIndices (inliers_cylinder);
	extract.setNegative (false);
	pcl::PointCloud<PointT>::Ptr cloud_cylinder (new pcl::PointCloud<PointT> ());
	extract.filter (*cloud_cylinder);
	if (cloud_cylinder->points.empty ()) 
		std::cerr << "Can't find the cylindrical component." << std::endl;
	else
	{
		std::cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << std::endl;
		writer.write ("table_scene_mug_stereo_textured_cylinder.pcd", *cloud_cylinder, false);
	}

	Eigen::Vector4f centroid;
	pcl::compute3DCentroid (*cloud_cylinder, centroid);
	std::cerr << "Centroid: " << centroid << std::endl;
	sensor_msgs::PointCloud2 output;

	float r = 1, g = 0, b = 0;
	std::string ns = "base_footprint";
	publishClusterMarker(cloud_cylinder,ns,1,r,g,b);


	// Convert to ROS data type
	//pcl_conversions::fromPCL(cloud_cylynder, output);

	// Publish the data.
	//pub.publish (output);
}

 

int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "my_pcl_tutorial");
	ros::NodeHandle nh;
	tf_listener = new tf::TransformListener();

	// Create a ROS subscriber for the input point cloud
	ros::Subscriber sub = nh.subscribe ("/rescuer/camera/depth_registered/points", 1, cloud_cb);

	// Create a ROS publisher for the output point cloud
	pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);

	cluster_marker = nh.advertise<visualization_msgs::Marker> ("base_footprint/cluster_marker", 1);

	// Spin
	ros::spin ();
}
