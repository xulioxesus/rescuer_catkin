#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Planificación.
 MoveIt!.
\begin_inset CommandInset label
LatexCommand label
name "chap:Planificación.-MoveIt!."

\end_inset


\end_layout

\begin_layout Standard
Se utiliza como guia 
\begin_inset Quotes eld
\end_inset

Integrating a new robot with MoveIt!
\begin_inset Quotes erd
\end_inset

 de 
\begin_inset CommandInset citation
LatexCommand cite
key "Mov15b"

\end_inset

.
\end_layout

\begin_layout Standard
El primer paso es instalar MoveIt!
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

sudo apt-get install ros-hydro-moveit-full
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Asistente MoveIt!
\end_layout

\begin_layout Standard
El asistente de 
\emph on
MoveIt!
\emph default
 se lanza mediante:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch moveit_setup_assistant setup_assistant.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Inicio
\end_layout

\begin_layout Standard
En la primera pantalla (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep1Start"

\end_inset

) del asistente se puede crear una configuración para un robot nuevo o editar
 una ya existente.
 En este caso veremos la configuración que se ha creado para 
\emph on
RESCUER
\emph default
 en este proyecto.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep1Start.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant Start
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep1Start"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Matriz de colisiones propias
\end_layout

\begin_layout Standard
En este paso (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep2SelfCollisionMatrix"

\end_inset

) se realiza una búsqueda de colisiones entre las distintas partes que conforman
 el robot.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep2SelfCollisionMatrix.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant Self Collision Matrix
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep2SelfCollisionMatrix"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Articulaciones virtuales
\end_layout

\begin_layout Standard
Es necesario definir una articulación virtual para conseguir relacionar
 odom_combined con base_footprint (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep3VirtualJoints"

\end_inset

).
 Esta relación se utilizará en la planificación y también en el simulador.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep3VirtualJoints.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant Virtual Joints
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep3VirtualJoints"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Grupos a planificar
\end_layout

\begin_layout Standard
En este paso (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep4PlanningGroups"

\end_inset

) se definen los grupos de los que posteriormente se podrán realizar tareas
 de planificación automática.
\end_layout

\begin_layout Standard
Para cada grupo que se añade hay que elegir que algoritmo cinemático se
 utilizará.
 En este caso se utiliza 
\begin_inset Quotes eld
\end_inset

kdl_kinematics_plugin/KDLKinematicsPlugin
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
Se define un grupo para el brazo, otro grupo para cada uno de los dedos
 de la mano y otro grupo para la mano formado por los subgrupos de los dedos.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep4PlanningGroups.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant Planning Groups
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep4PlanningGroups"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Poses
\end_layout

\begin_layout Standard
Se definen algunas poses interesantes para trabajar con el robot (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPoses"

\end_inset

):
\end_layout

\begin_layout Itemize
Brazo con todas las articulaciones a cero (arm_zero, ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesArmZero"

\end_inset

).
\end_layout

\begin_layout Itemize
Brazo en posición de trabajo (arm_home, ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesArmHome"

\end_inset

).
\end_layout

\begin_layout Itemize
Brazo en posición de navegación (arm_navigation, ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesArmNavigation"

\end_inset

).
\end_layout

\begin_layout Itemize
Mano abierta (hand_open, ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesHandOpen"

\end_inset

).
\end_layout

\begin_layout Itemize
Mano cerrada (hand_open, ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesHandClosed"

\end_inset

).
\end_layout

\begin_layout Standard
Se puede aprovechar este paso para comprobar los límites de las articulaciones
 (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep5RobotPosesCheckLimits"

\end_inset

).
\end_layout

\begin_layout Standard
Intentar mover todas las articulaciones para ver si os límites de las mismas
 están bien.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPoses.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPoses"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesArmZero.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Arm Zero
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesArmZero"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesArmHome.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Arm Home
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesArmHome"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesArmNavigation.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Arm Navigation
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesArmNavigation"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesHandOpen.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Hand Open
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesHandOpen"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesHandClosed.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Hand Closed
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesHandClosed"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep5RobotPosesCheckLimits.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Robot Poses - Comprobar límites de las articulaciones
 
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep5RobotPosesCheckLimits"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Manipulador
\end_layout

\begin_layout Standard
Se define con el nombre 
\emph on
hand_end_effector 
\emph default
un manipulador perteneciente al grupo hand (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep6EndEffectors"

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep6EndEffectors.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - End Effectors
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep6EndEffectors"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Articulaciones pasivas
\end_layout

\begin_layout Standard
No se define ninguna articulación pasiva para 
\emph on
RESCUER 
\emph default
(ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStep7PassiveJoints"

\end_inset

)
\emph on
.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep7PassiveJoints.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Passive Joints
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStep7PassiveJoints"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Generación de la configuración
\end_layout

\begin_layout Standard
En este último paso (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:MoveItSetupAssistantStepStep8ConfigurationFiles"

\end_inset

) se genera el paquete de configuración de MoveIt! para 
\emph on
RESCUER.
 
\emph default
En caso de ejecutar el asistente de MoveIt! sobre una configuración existente
 conviene hacer una copia del paquete antes de guardar los cambios.
 Posteriormente con una herramienta como 
\emph on
meld
\emph default
 se puede comprobar las diferencias entre la configuración nueva y la antigua.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/MoveItSetupAssistant/MoveItSetupAssistantStep8ConfigurationFiles.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
MoveIt! Setup Assistant - Configuration Files
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:MoveItSetupAssistantStepStep8ConfigurationFiles"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Descripción del paquete
\end_layout

\begin_layout Standard
Una vez generado el paquete con el asistente de MoveIt! se describe la configura
ción modificada manualmente y los lanzadores creados para RESCUER.
\end_layout

\begin_layout Subsection*
Lanzadores
\end_layout

\begin_layout Subsubsection*
rescuer.launch
\end_layout

\begin_layout Itemize
Carga 
\emph on
planning_context.launch
\emph default
 del mismo paquete, que a su vez:
\end_layout

\begin_deeper
\begin_layout Itemize
Carga la configuración del robot creada por el asistente en formato 
\emph on
srdf
\emph default
, el fichero cargado es 
\emph on
config/rescuer.srdf.
\end_layout

\begin_deeper
\begin_layout Itemize
Sobreescribe los límites de las articulaciones utilizando el fichero 
\emph on
config/joint_limits.yaml
\end_layout

\begin_layout Itemize
Carga la configuración por defecto para la resolución de problemas cinemáticos
 utilizando el fichero 
\emph on
config/kinematics.yaml.
\end_layout

\end_deeper
\end_deeper
\begin_layout Itemize
Pone en marcha el nodo 
\emph on
move_group, 
\emph default
mediante el lanzador 
\emph on
launch/move_group.launch
\end_layout

\begin_layout Itemize
Pone en marcha la interfaz gráfica 
\emph on
RVIZ
\end_layout

\begin_layout Standard
Llamada:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_moveit_config rescuer.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
rescuer_simulator.launch
\end_layout

\begin_layout Itemize
Realiza las mismas tareas que 
\emph on
rescuer.launch
\emph default
 pero en el espacio de nombres 
\emph on
rescuer
\emph default
, esto es necesario para la correcta interacción con el simulador Gazebo.
\end_layout

\begin_layout Itemize
Utiliza el lanzador 
\emph on
launch/move_group_simulator.launch.
\end_layout

\begin_layout Standard
Llamada:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_moveit_config rescuer_simulator.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
Otros lanzadores
\end_layout

\begin_layout Itemize
rescuer_moveit_controller_manager.launch.xml
\end_layout

\begin_deeper
\begin_layout Itemize
Carga la configuración existente en 
\emph on
config/controllers.yaml
\end_layout

\end_deeper
\begin_layout Itemize
rescuer_moveit_sensor_manager.launch.xml
\end_layout

\begin_deeper
\begin_layout Itemize
Carga la configuración respecto a Kinect.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

<launch>
\end_layout

\begin_layout Plain Layout

	<rosparam command="load" file="$(find rescuer_moveit_config)/config/sensors_kin
ect.yaml" />
\end_layout

\begin_layout Plain Layout

        <param name="octomap_frame" type="string" value="odom_combined"
 />
\end_layout

\begin_layout Plain Layout

	<param name="octomap_resolution" type="double" value="0.05" />
\end_layout

\begin_layout Plain Layout

	<param name="max_range" type="double" value="5.0" />
\end_layout

\begin_layout Plain Layout

</launch>
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Subsection*
Configuración
\end_layout

\begin_layout Standard
La configuración específica para 
\emph on
RESCUER, 
\emph default
que se utiliza en 
\emph on
MoveIt!
\emph default
, se define en los siguientes archivos:
\end_layout

\begin_layout Subsubsection*
controllers.yaml
\end_layout

\begin_layout Standard
Se definen los controladores de trayectoria para el brazo y para la mano.
 El controlador del brazo existe para el robot real, pero para la mano no.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

controller_list:
\end_layout

\begin_layout Plain Layout

 - name: arm_controller
\end_layout

\begin_layout Plain Layout

   action_ns: follow_joint_trajectory
\end_layout

\begin_layout Plain Layout

   type: FollowJointTrajectory
\end_layout

\begin_layout Plain Layout

   default: true
\end_layout

\begin_layout Plain Layout

   joints:
\end_layout

\begin_layout Plain Layout

     - arm_1_joint
\end_layout

\begin_layout Plain Layout

     - arm_2_joint
\end_layout

\begin_layout Plain Layout

     - arm_3_joint
\end_layout

\begin_layout Plain Layout

     - arm_4_joint
\end_layout

\begin_layout Plain Layout

     - arm_5_joint
\end_layout

\begin_layout Plain Layout

     - arm_6_joint
\end_layout

\begin_layout Plain Layout

 - name: hand_controller
\end_layout

\begin_layout Plain Layout

   action_ns: follow_joint_trajectory
\end_layout

\begin_layout Plain Layout

   type: FollowJointTrajectory
\end_layout

\begin_layout Plain Layout

   default: true
\end_layout

\begin_layout Plain Layout

   joints:
\end_layout

\begin_layout Plain Layout

     - hand_j11_joint
\end_layout

\begin_layout Plain Layout

     - hand_j12_joint
\end_layout

\begin_layout Plain Layout

     - hand_j13_joint
\end_layout

\begin_layout Plain Layout

     - hand_j21_joint
\end_layout

\begin_layout Plain Layout

     - hand_j22_joint
\end_layout

\begin_layout Plain Layout

     - hand_j23_joint
\end_layout

\begin_layout Plain Layout

     - hand_j23_joint
\end_layout

\begin_layout Plain Layout

     - hand_j32_joint
\end_layout

\begin_layout Plain Layout

     - hand_j33_joint
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
joint_limits.yaml
\end_layout

\begin_layout Standard
Se definen los límites de velocidades y aceleraciones de las articulaciones.
 Estos valores sobreescriben los descritos en la descripción 
\emph on
urdf
\emph default
 del robot.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

joint_limits:
\end_layout

\begin_layout Plain Layout

  arm_6_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.01
\end_layout

\begin_layout Plain Layout

  arm_4_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.1
\end_layout

\begin_layout Plain Layout

  arm_1_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.01
\end_layout

\begin_layout Plain Layout

  arm_2_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.01
\end_layout

\begin_layout Plain Layout

  arm_5_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.01
\end_layout

\begin_layout Plain Layout

  arm_3_joint:
\end_layout

\begin_layout Plain Layout

    has_velocity_limits: true
\end_layout

\begin_layout Plain Layout

    max_velocity: 0.49
\end_layout

\begin_layout Plain Layout

    has_acceleration_limits: true
\end_layout

\begin_layout Plain Layout

    max_acceleration: 0.01
\end_layout

\begin_layout Plain Layout

...
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
kinematics.yaml
\end_layout

\begin_layout Standard
La configuración cinématica para resolver problemas de planificación se
 mantiene tal y como la genera el asistente de 
\emph on
MoveIt!
\end_layout

\begin_layout Subsubsection*
sensors_kinect.yaml
\end_layout

\begin_layout Standard
La configuración de la Kinect.
 El tópico point_cloud_topic se tiene que modificar dependiendo del espacio
 de nombres en el que se lance 
\emph on
move_group.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

sensors:
\end_layout

\begin_layout Plain Layout

  - sensor_plugin: occupancy_map_monitor/PointCloudOctomapUpdater
\end_layout

\begin_layout Plain Layout

    point_cloud_topic: /rescuer/camera/depth_registered/points
\end_layout

\begin_layout Plain Layout

    max_range: 5.0
\end_layout

\begin_layout Plain Layout

    point_subsample: 1
\end_layout

\begin_layout Plain Layout

    padding_offset: 0.1
\end_layout

\begin_layout Plain Layout

    padding_scale: 1.0
\end_layout

\begin_layout Plain Layout

    filtered_cloud_topic: filtered_cloud
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Planificando con el robot real
\end_layout

\begin_layout Standard
Los componentes que se establecieron como necesarios para realizar tareas
 de planificación con MoveiIt! en el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Planificación"

\end_inset

 eran:
\end_layout

\begin_layout Itemize
☑ Controlador de la base
\end_layout

\begin_deeper
\begin_layout Itemize
Proporcionado por 
\emph on
rescuer_co
\emph default
ntrollers.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Controlador del brazo robótico.
\end_layout

\begin_deeper
\begin_layout Itemize
Proporcionado por 
\emph on
rescuer_co
\emph default
ntrollers.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Un controlador capaz de seguir trayectorias para el brazo.
\end_layout

\begin_deeper
\begin_layout Itemize
Proporcionado por 
\emph on
rescuer_co
\emph default
ntrollers.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Un sensor de puntos 3D.
\end_layout

\begin_deeper
\begin_layout Itemize
Instalada Kinect
\end_layout

\begin_layout Itemize
Controlador proporcionado por 
\emph on
rescuer_controllers.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Publicar el estado de las articulaciones del robot.
\end_layout

\begin_deeper
\begin_layout Itemize
Proporcionado por 
\emph on
rescuer_bringup
\end_layout

\end_deeper
\begin_layout Itemize
☑ Publicar el estado del robot y las transformaciones entre las distintas
 articulaciones del robot.
\end_layout

\begin_deeper
\begin_layout Itemize
Proporcionado por 
\emph on
rescuer_bringup
\end_layout

\end_deeper
\begin_layout Standard
Ahora que se cumplen todos los requisitos se pueden realizar tareas de planifica
ción.
\end_layout

\begin_layout Standard
Para hacer esto se tienen que lanzar los controladores y nodos necesarios
 en el robot real.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_bringup rescuer.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Se lanza MoveIt! con la configuración de 
\emph on
RESCUER.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_moveit_config rescuer.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\series bold
¡Ahora ya se pueden realizar tareas de planificación con el brazo!
\end_layout

\begin_layout Section
Un experimento
\end_layout

\begin_layout Standard
Para ejemplificar los posibles usos de 
\emph on
RESCUER, 
\emph default
se diseña el siguiente experimento.
\end_layout

\begin_layout Enumerate
Detectar automáticamente un cilindro situado frente al robot.
\end_layout

\begin_layout Enumerate
Calcular el centroide del cilindro.
\end_layout

\begin_layout Enumerate
Mediante 
\emph on
MoveIt! 
\emph default
colocar el brazo en la posición del cilindro.
\end_layout

\begin_layout Subsection*
rescuer_pcl
\end_layout

\begin_layout Standard
Se crea el paquete 
\emph on
rescuer_pcl
\emph default
.
\end_layout

\begin_layout Standard
Dentro de este paquete se crea un programa 
\emph on
src/detectCylinder.cpp 
\emph default
que procesa la nube de puntos obtenida de la Kinect y filtra un objeto cilindric
o.
\end_layout

\begin_layout Standard
El programa se lanza mediante el comando:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_pcl rescuer.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
rescuer_moveit_tutorials
\end_layout

\begin_layout Standard
Se crea el paquete 
\emph on
rescuer_moveit_tutorials.
\end_layout

\begin_layout Standard
Dentro del paquete se crea un programa 
\emph on
src/move_group_tutorial.cpp
\emph default
 que planifica de forma automática los movimientos necesarios del brazo
 para conseguir la pose adecuada para ir hasta el centroide obtenido por
 detectCylinder del apartado anterior.
\end_layout

\begin_layout Standard
El programa se lanza mediante el comando:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_moveit_tutorials planning_tutorial.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En el siguiente enlace puede verse un vídeo con la ejecución de este experimento.
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\small\ttfamily},breaklines=true"
inline false
status open

\begin_layout Plain Layout

https://drive.google.com/file/d/0B-5jNBSVP_EWNG5GaFdBZGZFd2c/view?usp=sharing
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
