#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "Gleichung"
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% put the link to figure floats to the beginning
% of the figure and not to its end
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}

\usepackage{wasysym}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 1
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Navegación
\begin_inset CommandInset label
LatexCommand label
name "chap:Navegación"

\end_inset


\end_layout

\begin_layout Section
Introducción
\end_layout

\begin_layout Standard
La pila de navegación de ROS utiliza la información odométrica y los datos
 obtenidos de los sensores para enviar comandos de velocidad a una base
 móvil.
\end_layout

\begin_layout Standard
Tal y como se refleja en la documentación de ROS, para poder usar la pila
 de navegación con un robot, RESCUER debe cumplir una serie de requisitos.
\end_layout

\begin_layout Standard
Los esfuerzos realizados para conseguir integrar la navegación con 
\emph on
RESCUER, 
\emph default
se han centrado en el robot simulado.
 La configuración confeccionada no se ha llegado a probar en el robot real.
 En el robot simulado ante la dificultad de poder mover la base mediante
 el controlador lanzado por 
\emph on
rescuer_control, 
\emph default
tampoco se ha conseguido probar la configuración.
\end_layout

\begin_layout Section
Requisitos
\end_layout

\begin_layout Standard
Para utilizar la pila de navegación, con el robot real y el simulado, se
 deben cumplir los prerrequisitos:
\end_layout

\begin_layout Itemize
☑ El robot tiene que estar utilizando ROS.
 Ver 
\begin_inset CommandInset citation
LatexCommand cite
key "Leo01"

\end_inset

.
\end_layout

\begin_layout Itemize
☑ Poseer un árbol de transformadas tf.
\end_layout

\begin_layout Itemize
☑ Publicar los datos de los sensores utilizando los tipos de mensajes de
 ROS.
\end_layout

\begin_layout Itemize
☒ Configurar la pila de navegación con la forma y la dinámica del robot
 a utilizar.
\end_layout

\begin_layout Standard
Se seguirá el procedimiento de la documentación de la pila de navegación,
 ver 
\begin_inset CommandInset citation
LatexCommand cite
key "Nav15"

\end_inset

, para cumplir con los prerrequisitos anteriores.
\end_layout

\begin_layout Standard
La pila de navegación a pesar de ser de propósito general tiene tres requisitos
 hardware que restringen su uso: 
\end_layout

\begin_layout Itemize
☑ El robot ha de tener una base diferencial u holonimica con ruedas.
 Se tienen que poder enviar comandos de velocidad con la forma, velocidad
 en x, y y theta.
\end_layout

\begin_deeper
\begin_layout Itemize
RESCUER es un robot con cadenas, la base móvil acepta comandos como si de
 un modelo diferencial se tratara.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Requiere un láser montado en la base que se utiliza para la construcción
 del mapa y para la localización.
 
\end_layout

\begin_deeper
\begin_layout Itemize
Usa kinect que realizará las funciones del laser.
\end_layout

\end_deeper
\begin_layout Itemize
☑ Se requiere que el robot sea cuadrado o circular.
\end_layout

\begin_deeper
\begin_layout Itemize
RESCUER es rectangular si se coloca el brazo plegado encima de la base.
\end_layout

\end_deeper
\begin_layout Standard
Según el esquema de la pila de navegación (ver 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:NavegacionPilaEsquema"

\end_inset

) los componentes en azul son los que debe proveer cada robot.
 El procedimiento para configurar la pila de navegación en 
\emph on
RESCUER 
\emph default
se puede consultar en 
\begin_inset CommandInset citation
LatexCommand cite
key "Nav15-2"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/RobotSetup.png
	width 100col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Esquema de la pila de navegación
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:NavegacionPilaEsquema"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Itemize
sensor sources:
\end_layout

\begin_deeper
\begin_layout Itemize
Se utiliza PointCloud provisto por Kinect.
\end_layout

\end_deeper
\begin_layout Itemize
base controller:
\end_layout

\begin_deeper
\begin_layout Itemize
base_controller proporciona el controlador para poder enviar los comandos
 de velocidad.
\end_layout

\end_deeper
\begin_layout Itemize
Información odómetrica y de transformadas necesaria
\end_layout

\begin_deeper
\begin_layout Itemize
Gazebo en el robot simulado.
\end_layout

\begin_layout Itemize
base_controller y robot_state_publisher en el robot real.
\end_layout

\end_deeper
\begin_layout Section
Adaptación a RESCUER
\end_layout

\begin_layout Subsection
rescuer_map
\end_layout

\begin_layout Standard
Se crea el paquete 
\emph on
rescuer_map.
\end_layout

\begin_layout Standard
En este paquete se guarda una imagen con un mapa para pruebas en 
\emph on
config/map.pgm.
\end_layout

\begin_layout Standard
Este mapa se corresponde con Willow Garage para poder utilizar en el entorno
 simulado un mapa conocido mientras que con el robot real no se ha llegado
 a construir ningún mapa.
\end_layout

\begin_layout Subsection
rescuer_navigation
\end_layout

\begin_layout Standard
Se crea el paquete 
\emph on
rescuer_navigation.
\end_layout

\begin_layout Subsubsection*
Mover la base
\end_layout

\begin_layout Standard
Mediante el lanzador 
\emph on
launch/move_base.launch.
\end_layout

\begin_layout Itemize
Carga un nodo que transforma los datos leidos por la Kinect al formato de
 lectura láser adecuado para la pila de navegación.
\end_layout

\begin_layout Itemize
Lanza un servidor de mapas con los datos de 
\emph on
rescuer_map.
\end_layout

\begin_layout Itemize
Lanza el nodo 
\emph on
amcl.
\end_layout

\begin_layout Itemize
Lana el nodo 
\emph on
move_base 
\emph default
cargando la configuración de 
\emph on
rescuer_navigation/config
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,showstringspaces=false,tabsize=4"
inline false
status open

\begin_layout Plain Layout

<launch>
\end_layout

\begin_layout Plain Layout

  <master auto="start"/>
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <!--include file="$(find rescuer_navigation)/launch/pointcloud_to_laserscan.lau
nch" /-->
\end_layout

\begin_layout Plain Layout

  <include file="$(find rescuer_navigation)/launch/depthimage_to_laserscan.launch
" />
\end_layout

\begin_layout Plain Layout

  <!-- Run the map server -->
\end_layout

\begin_layout Plain Layout

  <node name="map_server" pkg="map_server" type="map_server" args="$(find
 rescuer_map)/config/map.pgm 0.005"/>
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <!--- Run AMCL -->
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <include file="$(find rescuer_navigation)/launch/amcl_diff.launch" />
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <node pkg="move_base" type="move_base" respawn="false" name="move_base"
 output="screen">
\end_layout

\begin_layout Plain Layout

    <rosparam file="$(find rescuer_navigation)/config/costmap_common_params.yaml"
 command="load" ns="global_costmap" />
\end_layout

\begin_layout Plain Layout

    <rosparam file="$(find rescuer_navigation)/config/costmap_common_params.yaml"
 command="load" ns="local_costmap" />
\end_layout

\begin_layout Plain Layout

    <rosparam file="$(find rescuer_navigation)/config/local_costmap_params.yaml"
 command="load" />
\end_layout

\begin_layout Plain Layout

    <rosparam file="$(find rescuer_navigation)/config/global_costmap_params.yaml"
 command="load" />
\end_layout

\begin_layout Plain Layout

    <rosparam file="$(find rescuer_navigation)/config/base_local_planner_params.y
aml" command="load" />
\end_layout

\begin_layout Plain Layout

  </node>
\end_layout

\begin_layout Plain Layout

</launch>
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection*
Configuración
\end_layout

\begin_layout Standard
La configuración de la pila de navegación se encuentra en 
\emph on
rescuer_navigation/config
\emph default
 en los siguientes ficheros:
\end_layout

\begin_layout Paragraph*
base_local_planner_params.yaml
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

TrajectoryPlannerROS:
\end_layout

\begin_layout Plain Layout

  max_vel_x: 0.45
\end_layout

\begin_layout Plain Layout

  min_vel_x: 0.1
\end_layout

\begin_layout Plain Layout

  max_vel_theta: 1.0
\end_layout

\begin_layout Plain Layout

  min_in_place_vel_theta: 0.4
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  acc_lim_theta: 3.2
\end_layout

\begin_layout Plain Layout

  acc_lim_x: 2.5
\end_layout

\begin_layout Plain Layout

  acc_lim_y: 2.5
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  holonomic_robot: false
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Paragraph*
costmap_common_params.yaml
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

obstacle_range: 2.5
\end_layout

\begin_layout Plain Layout

raytrace_range: 3.0
\end_layout

\begin_layout Plain Layout

footprint: [[-0.4, -0.6], [-0.4, 0.6], [0.4,0.6], [0.4,-0.6]]
\end_layout

\begin_layout Plain Layout

#robot_radius: ir_of_robot
\end_layout

\begin_layout Plain Layout

inflation_radius: 0.55
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#observation_sources: laser_scan_sensor point_cloud_sensor
\end_layout

\begin_layout Plain Layout

observation_sources: point_cloud_sensor
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#laser_scan_sensor: {sensor_frame: frame_name, data_type: LaserScan, topic:
 topic_name, marking: true, clearing: true}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

point_cloud_sensor: {sensor_frame: rescuer/camera/camera_depth_optical_frame,
 data_type: PointCloud2, topic: /rescuer/camera/depth_registered/points,
 marking: true, clearing: true}
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Paragraph*
global_costmap_params.yaml
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

global_costmap:
\end_layout

\begin_layout Plain Layout

  global_frame: map
\end_layout

\begin_layout Plain Layout

  robot_base_frame: rescuer/base_footprint
\end_layout

\begin_layout Plain Layout

  update_frequency: 5.0
\end_layout

\begin_layout Plain Layout

  static_map: true
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Paragraph*
local_costmap_params.yaml
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status collapsed

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

local_costmap:
\end_layout

\begin_layout Plain Layout

  global_frame: rescuer/odom_combined
\end_layout

\begin_layout Plain Layout

  robot_base_frame: rescuer/base_footprint
\end_layout

\begin_layout Plain Layout

  update_frequency: 5.0
\end_layout

\begin_layout Plain Layout

  publish_frequency: 2.0
\end_layout

\begin_layout Plain Layout

  static_map: false
\end_layout

\begin_layout Plain Layout

  rolling_window: true
\end_layout

\begin_layout Plain Layout

  width: 6.0
\end_layout

\begin_layout Plain Layout

  height: 6.0
\end_layout

\begin_layout Plain Layout

  resolution: 0.05
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Conclusiones.
\end_layout

\begin_layout Standard
La configuración de la pila de navegación queda preparada hasta poder hacer
 funcionar la base en el simulador.
 Una vez probado en el simulador se podría pasar al robot real, empezando
 por la construcción de un mapa de laboratorio o de los pasillos circundantes.
\end_layout

\begin_layout Standard
La forma de lanzar la pila de navegación sería:
\end_layout

\begin_layout Standard
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

roslaunch rescuer_navigation move_base.launch
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status open

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
