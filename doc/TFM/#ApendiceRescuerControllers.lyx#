#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% Linkfläche für Querverweise vergrößern und automatisch benenne
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasngerman{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{Kap.\negthinspace}
 \renewcommand{\subsectionautorefname}{Kap.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{Kap.\negthinspace}
 \renewcommand{\figureautorefname}{Abb.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% für den Fall, dass jemand die Bezeichnung "Gleichung" haben will
%\renewcommand{\eqref}[1]{Gleichung~(\negthinspace\autoref{#1})}

% Setzt den Link für Sprünge zu Gleitabbildungen
% auf den Anfang des Gelitobjekts und nicht aufs Ende
\usepackage[figure]{hypcap}

% Die Seiten des Inhaltsverzeichnisses werden römisch numeriert,
% ein PDF-Lesezeichen für das Inhaltsverzeichnis wird hinzugefügt
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

%Vergrößert den Teil der Seite, in dem Gleitobjekte
% unten angeordnet werden dürfen
\renewcommand{\bottomfraction}{0.5}

% Vermeidet, dass Gleitobjekte vor ihrem Abschnitt gedruckt werden
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_amsmath 2
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
rescuer_controllers
\begin_inset CommandInset label
LatexCommand label
name "chap:ApendiceRescuerControllers"

\end_inset


\end_layout

\begin_layout Standard
La estructura del paquete 
\emph on
rescuer_controllers
\emph default
 puede verse en la 
\begin_inset CommandInset ref
LatexCommand ref
reference "img:ApendiceControllersEstructura"

\end_inset

.
 El fichero 
\emph on
CMakeLists.txt
\emph default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "img:ApendiceControllersCMakesLists"

\end_inset

) se usa para la construcción del paquete.
 En él se puede ver cómo se construyen las librerías y ejecutables a partir
 del código fuente existente en la antigua instalación de RESCUER.
 Por otra parte, en 
\emph on
package.xml
\emph default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "img:ApendiceControllersPackageXML"

\end_inset

) se señalan las dependencias del paquete con otros paquetes de ROS, tanto
 en tiempo de compilación como en tiempo de ejecución.
\end_layout

\begin_layout Standard
Los ficheros terminados en 
\begin_inset Quotes eld
\end_inset

.unset
\begin_inset Quotes erd
\end_inset

 se utilizan para desactivar temporalmente el paquete y no compilarlo con
 
\emph on
catkin_make
\emph default
.
 La técnica para desactivar consiste en borrar 
\emph on
CMakesLists.txt
\emph default
 y 
\emph on
package.xml,
\emph default
 de manera que el paquete 
\emph on
rescuer_controllers
\emph default
 sea 
\begin_inset Quotes eld
\end_inset

invisible
\begin_inset Quotes erd
\end_inset

 para 
\emph on
catkin_make
\emph default
.
 Los archivos con el sufijo 
\begin_inset Quotes eld
\end_inset

.unset
\begin_inset Quotes erd
\end_inset

 sirven de copia para restaurar el paquete.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/rescuerControllersFolder.png
	width 100col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Estructura del paquete rescuer_controllers
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:ApendiceControllersEstructura"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=make,tabsize=4"
inline false
status open

\begin_layout Plain Layout

cmake_minimum_required(VERSION 2.8.3)
\end_layout

\begin_layout Plain Layout

project(rescuer_controllers)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

find_package(catkin REQUIRED COMPONENTS roscpp rospy std_msgs genmsg)
\end_layout

\begin_layout Plain Layout

find_package(catkin REQUIRED COMPONENTS
\end_layout

\begin_layout Plain Layout

  roscpp
\end_layout

\begin_layout Plain Layout

  rospy
\end_layout

\begin_layout Plain Layout

  std_msgs
\end_layout

\begin_layout Plain Layout

  message_generation
\end_layout

\begin_layout Plain Layout

  tf
\end_layout

\begin_layout Plain Layout

  actionlib
\end_layout

\begin_layout Plain Layout

  actionlib_msgs
\end_layout

\begin_layout Plain Layout

)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

catkin_package()
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

include_directories(include ${catkin_INCLUDE_DIRS})
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

add_executable(base_controller src/base_controller.cpp)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

add_library(logLib src/logLib.cc)
\end_layout

\begin_layout Plain Layout

add_library(Component src/Component.cc)
\end_layout

\begin_layout Plain Layout

add_library(dx60co8c src/dx60co8c.c)
\end_layout

\begin_layout Plain Layout

add_library(dx60co8_esd src/dx60co8_esd.cc)
\end_layout

\begin_layout Plain Layout

add_library(MotorDrive src/MotorDrive.cc)
\end_layout

\begin_layout Plain Layout

add_library(PrimitiveDriver src/PrimitiveDriver.cc)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

target_link_libraries(dx60co8_esd ntcan)
\end_layout

\begin_layout Plain Layout

target_link_libraries(PrimitiveDriver logLib MotorDrive dx60co8_esd Component)
\end_layout

\begin_layout Plain Layout

target_link_libraries(base_controller ${catkin_LIBRARIES} PrimitiveDriver)
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
CMakesLists.txt de rescuer_controllers
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:ApendiceControllersCMakesLists"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Box Shadowbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "basicstyle={\footnotesize},breaklines=true,language=XML,tabsize=4"
inline false
status open

\begin_layout Plain Layout

<?xml version="1.0"?>
\end_layout

\begin_layout Plain Layout

<package>
\end_layout

\begin_layout Plain Layout

  <name>rescuer_controllers</name>
\end_layout

\begin_layout Plain Layout

  <version>0.0.0</version>
\end_layout

\begin_layout Plain Layout

  <description>The rescuer_controllers package</description>
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <maintainer email="xulioxesus@gmail.com">xulioxesus</maintainer>
\end_layout

\begin_layout Plain Layout

  <license>BSD</license>
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  <buildtool_depend>catkin</buildtool_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>roscpp</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>rospy</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>std_msgs</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>actionlib</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>actionlib_msgs</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>message_generation</build_depend>
\end_layout

\begin_layout Plain Layout

  <build_depend>tf</build_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>roscpp</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>rospy</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>std_msgs</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>actionlib</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>actionlib_msgs</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>message_runtime</run_depend>
\end_layout

\begin_layout Plain Layout

  <run_depend>tf</run_depend>
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

</package>
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
package.xml de rescuer_controllers
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "img:ApendiceControllersPackageXML"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
base_controller.cpp
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "biblio/Plasma"
options "biblio/alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
