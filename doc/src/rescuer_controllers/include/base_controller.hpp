#include "ros/ros.h"
#include "PrimitiveDriver.h"
#include "time.h"
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <iostream>

#define DEFAULT_RESCUER_PORT            "/dev/can0"
#define MOTOR_DEF_MAX_SPEED		1		//Max Rover Linear Speed in m/s
#define MOTOR_DEF_MAX_TURNSPEED		57.29577951	//Max Rover Angular Speed in grados/s: one radian
#define LINEAR_SPEED_TO_EFFORT_FACTOR   3.900039
#define ANGULAR_SPEED_TO_EFFORT_FACTOR  4.816027774 
#define DEFAULT_LOG_FOLDER              "log"
#define	RTOD(r)   ((r) * 180 / M_PI)
#define	DTOR(d)   ((d) * M_PI / 180)

#define TICKS_PER_METER	100.0 
#define WIDTH_ROBOT 0.1

class base_controller
{

	private:
		static double x;
		static double y;
		static double theta;
		static double vx;
		static double vth;
		static geometry_msgs::Quaternion odom_quat;
		static ros::Time last_time;
		
	public:
		static geometry_msgs::TransformStamped odom_trans;
		static nav_msgs::Odometry odom;
		static PrimitiveDriver driver;
		base_controller();
		~base_controller();
		static void StartUp();
		static void SetX(double pos);
		static void SetY(double pos);
		static void SetTheta(double pos);
		static void SetXVelocity(double vel);
		static void SetThetaVelocity(double vel);
		static double GetX();
		static double GetY();
		static double GetTheta();
		static double GetXVelocity();
		static double GetThetaVelocity();
		static void CalculateOdometry();
		static void cmd_velCallback(const geometry_msgs::Twist &twist);
//		static bool Move(rescuer_odom::Velocity::Request &req, rescuer_odom::Velocity::Response &res);
		static void Sleep(unsigned int mseconds);
};
