/** \file Component.cpp
 * \author Robotnik Automation S.L.L.
 * \version 1.0
 * \date    
 * 
 * \brief Standard Component
 *                                         
 * (C) 2007 Robotnik Automation, SLL  
 *  All rights reserved.
*/

#include "Component.h"

Component::Component(){
	run = false;
	pthread_running= false;
	pthread_hz  = 0;
}


Component::~Component(){

}
