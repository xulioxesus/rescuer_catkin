#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x4fa08db3, "module_layout" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0x1fedf0f4, "__request_region" },
	{ 0xc71c08b, "pci_bus_read_config_byte" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x5dce6b75, "complete_and_exit" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0xc996d097, "del_timer" },
	{ 0xd0d8621b, "strlen" },
	{ 0x19574cf7, "dev_set_drvdata" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x20000329, "simple_strtoul" },
	{ 0x2222e10f, "remove_proc_entry" },
	{ 0x6729d3df, "__get_user_4" },
	{ 0x1b57f41, "__register_chrdev" },
	{ 0x2888decc, "x86_dma_fallback_dev" },
	{ 0xfb0e29f, "init_timer_key" },
	{ 0xf259bf5d, "mutex_unlock" },
	{ 0x999e8297, "vfree" },
	{ 0x2f34091e, "pci_bus_write_config_word" },
	{ 0x27574f0a, "kthread_create_on_node" },
	{ 0x7d11c268, "jiffies" },
	{ 0x48eb0c0d, "__init_waitqueue_head" },
	{ 0x3fa58ef8, "wait_for_completion" },
	{ 0x4cb3a411, "kern_path" },
	{ 0x2bc95bd4, "memset" },
	{ 0xff7559e4, "ioport_resource" },
	{ 0x635b0c5f, "proc_mkdir" },
	{ 0xf97456ea, "_raw_spin_unlock_irqrestore" },
	{ 0xeae4c432, "current_task" },
	{ 0xb5175bb, "__mutex_init" },
	{ 0x50eedeb8, "printk" },
	{ 0x5152e605, "memcmp" },
	{ 0x2f287f0d, "copy_to_user" },
	{ 0xb4390f9a, "mcount" },
	{ 0x98503a08, "_raw_spin_unlock_irq" },
	{ 0xe9decdc2, "pci_bus_write_config_dword" },
	{ 0xbf4e0f, "mutex_lock" },
	{ 0x8834396c, "mod_timer" },
	{ 0xd50e6e0e, "dma_release_from_coherent" },
	{ 0x2072ee9b, "request_threaded_irq" },
	{ 0x2233e895, "dma_alloc_from_coherent" },
	{ 0xb2fd5ceb, "__put_user_4" },
	{ 0x42c8de35, "ioremap_nocache" },
	{ 0x13d0bec5, "pci_bus_read_config_word" },
	{ 0xf575d287, "pci_bus_read_config_dword" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0x4292364c, "schedule" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0xf1faac3a, "_raw_spin_lock_irq" },
	{ 0x1664f032, "create_proc_entry" },
	{ 0x7bbae442, "wake_up_process" },
	{ 0x7c61340c, "__release_region" },
	{ 0x1249fe4, "pci_unregister_driver" },
	{ 0x21fb443e, "_raw_spin_lock_irqsave" },
	{ 0x7afa89fc, "vsnprintf" },
	{ 0xe45f60d8, "__wake_up" },
	{ 0x1d2e87c6, "do_gettimeofday" },
	{ 0x1c3bf1ca, "pci_bus_write_config_byte" },
	{ 0x37a0cba, "kfree" },
	{ 0x2e60bace, "memcpy" },
	{ 0x622fa02a, "prepare_to_wait" },
	{ 0xedc03953, "iounmap" },
	{ 0x3078bc95, "__pci_register_driver" },
	{ 0x75bb675a, "finish_wait" },
	{ 0xb81960ca, "snprintf" },
	{ 0xc85ce150, "pci_enable_device" },
	{ 0x362ef408, "_copy_from_user" },
	{ 0xc3fe87c8, "param_ops_uint" },
	{ 0xea10a17a, "dev_get_drvdata" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0xe768dc61, "dma_ops" },
	{ 0xdc43a9c8, "daemonize" },
	{ 0xf20dabd8, "free_irq" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v000010B5d00009050sv000012FEsd00000001bc*sc*i*");
MODULE_ALIAS("pci:v000010B5d00009030sv000012FEsd0000000Cbc*sc*i*");
