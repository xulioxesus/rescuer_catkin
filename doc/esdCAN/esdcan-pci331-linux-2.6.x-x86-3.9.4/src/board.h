/* -*- linux-c -*-
 * FILE NAME board.h
 *           copyright 2002 by esd electronic system design gmbh
 *
 * BRIEF MODULE DESCRIPTION
 *           board layer API
 *
 *
 * Author:   Matthias Fuchs
 *           matthias.fuchs@esd-electronics.com
 *
 * history:
 *
 *  27.05.02 - first version                                       mf
 *  11.05.04 - changed OSIF_EXTERN into extern and OSIF_CALLTYPE   ab
 *
 */
/************************************************************************
 *
 *  Copyright (c) 1996 - 2012 by electronic system design gmbh
 *
 *  This software is copyrighted by and is the sole property of
 *  esd gmbh.  All rights, title, ownership, or other interests
 *  in the software remain the property of esd gmbh. This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of esd gmbh.
 *
 *  esd gmbh, reserves the right to modify this software without notice.
 *
 *  electronic system design gmbh          Tel. +49-511-37298-0
 *  Vahrenwalder Str 207                   Fax. +49-511-37298-68
 *  30165 Hannover                         http://www.esd-electronics.com
 *  Germany                                sales@esd-electronics.com
 *
 *************************************************************************/
/*! \file board.h
    \brief Card interface API

    This file contains the API for accessing the CAN driver's card layer.
    The functions are called by the nucleus modules.
*/

#ifndef __BOARD_H__
#define __BOARD_H__

#include <esdcan.h>
#include <osif.h>
#include <cm.h>
#include <canio.h>

#ifndef CIF_TS2MS
#define CIF_TS2MS(ts, tsf)  ts *= 1000; OSIF_DIV64_64(ts, tsf)
#endif

extern INT32 cif_open( CAN_OCB *ocb, CAN_NODE *node, INT32 flags );
extern INT32 cif_close( CAN_OCB *ocb );
extern INT32 cif_node_attach( CAN_NODE *node );
extern INT32 cif_node_detach( CAN_NODE *node );
extern INT32 cif_id_filter( CAN_NODE *node, UINT32 cmd, UINT32 id, UINT32 *count );
extern INT32 OSIF_CALLTYPE cif_tx( CAN_NODE *node );
extern INT32 cif_tx_obj( CAN_NODE *node, UINT32 cmd, VOID *arg );
extern INT32 cif_tx_abort( CAN_NODE *node, CM *cm, INT32 status );
extern INT32 cif_baudrate_set( CAN_NODE *node, UINT32 baud );

/* can_node MUST be provided, ocb and waitContext might be NULL (should fail if ocb or waitContext needed then) */
extern INT32 cif_ioctl( CAN_NODE *can_node, CAN_OCB *ocb, VOID *waitContext, UINT32 cmd,
                        VOID *buf_in, UINT32 len_in, VOID *buf_out, UINT32 *len_out );

extern INT32 cif_timestamp( CAN_NODE *node, CAN_TS *timestamp );

/* 1) implementation for soft timestamps is in board/<board>/board.c
   2) implementation for hard timestamps is in board/<board>/boardrc.c
      (this is very hardware specific !)
*/
extern INT32 cif_softts_get( VOID *dummy, CAN_TS *ts );
extern INT32 cif_softts_freq_get( VOID *dummy, CAN_TS_FREQ *ts_freq );


INT32 OSIF_CALLTYPE can_board_attach( CAN_CARD *crd );
INT32 OSIF_CALLTYPE can_board_attach_final( CAN_CARD *crd );
INT32 OSIF_CALLTYPE can_board_detach( CAN_CARD *crd );
/* CAN_BOARD_DETACH_FINAL may be defined in boardrc.h */
#ifndef CAN_BOARD_DETACH_FINAL
#define CAN_BOARD_DETACH_FINAL(pCrd)
#else
INT32 OSIF_CALLTYPE can_board_detach_final( CAN_CARD *crd );
#endif

#endif /* __BOARD_H__ */
