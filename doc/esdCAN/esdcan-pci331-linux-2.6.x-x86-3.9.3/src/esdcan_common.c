/* -*- linux-c -*-
 * FILE NAME esdcan_common.c
 *           copyright 2002-2012 by esd electronic system design gmbh
 *
 * BRIEF MODULE DESCRIPTION
 *           This file contains the common entries
 *           for Linux/IRIX
 *           for the esd CAN driver
 *
 *
 * Author:   Matthias Fuchs
 *           matthias.fuchs@esd-electronics.com
 *
 * history:
 *
 *  $Log: esdcan_common.c,v $
 *  Revision 1.101  2012/02/17 18:52:51  andreas
 *  Fixed OSIF_CALLTYPE position
 *
 *  Revision 1.100  2011/11/04 14:46:32  andreas
 *  Removed single line comments
 *  Tiny cleanup
 *
 *  Revision 1.99  2011/11/01 15:29:20  andreas
 *  Merged with preempt_rt branch
 *  With OSIF_USE_PREEMPT_RT_IMPLEMENTATION the new implementation is used for
 *    all kernels > 2.6.20
 *  Cleanup and whitespace changes
 *  Updated copyright notice
 *
 *  Revision 1.98  2011/08/31 12:52:03  manuel
 *  Added EEI stuff
 *
 *  Revision 1.97  2011/05/18 15:59:51  andreas
 *  Changed to make use of CHAR8
 *  Changed CAN_ACTION_CHECK macro slightly
 *  Another fix in esdcan_read_common (removed sema_rx_abort)
 *  Some cleanup
 *
 *  Revision 1.96  2010/12/10 16:44:55  andreas
 *  Added IOCTL_ESDCAN_RESET_CAN_ERROR_CNT
 *
 *  Revision 1.95  2010/11/26 15:49:58  andreas
 *  Removed redundant parameter on esdcan_ioctl_internal()
 *
 *  Revision 1.94  2010/08/30 10:15:19  andreas
 *  Fixed 29-Bit filter (AMR wrongly initialized)
 *
 *  Revision 1.93  2010/06/16 15:18:52  manuel
 *  Made compileable for linux again
 *
 *  Revision 1.92  2010/06/16 14:50:31  michael
 *  Smart id filter nucleus support.
 *  New filter not yet connected to user api.
 *  2nd trial.
 *
 *  Revision 1.91  2010/06/16 12:49:13  manuel
 *  Use CANIO_ID_20B_BASE instead of CANIO_20B_BASE
 *
 *  Revision 1.90  2010/04/16 16:24:25  andreas
 *  "Fixed" boardstatus in CAN_DEBUG ioctl (actually removed wrong code, only
 *    and made room for a fix in phase two ;)
 *  Correctly added ctrl_type to CAN_DEBUG ioctl
 *
 *  Revision 1.89  2010/03/16 10:31:20  andreas
 *  Added esdcan_rx_notify() and esdcan_poll() for select implementation
 *
 *  Revision 1.88  2010/03/11 12:35:22  manuel
 *  Added nuc_check_links (used only if NUC_CHECK_LINKS is set)
 *
 *  Revision 1.87  2009/07/31 14:17:05  andreas
 *  FUNCTIONAL CHANGE (!!!): Timeout is no longer multiplied with number of
 *    frames on canWrite()
 *  Add IOCTL_ESDCAN_SER_REG_READ and IOCTL_ESDCAN_SER_REG_WRITE
 *
 *  Revision 1.86  2009/07/29 05:01:38  tkoerper
 *  - Fixed copying bus statistics
 *
 *  Revision 1.85  2009/06/16 12:54:02  andreas
 *  Fixed canRead(), length wasn't returned correctly anymore since last checkin
 *
 *  Revision 1.84  2009/04/22 14:32:03  andreas
 *  Another fix for signal handling in canRead().
 *  Forgot to reset rx_cm_count.
 *
 *  Revision 1.83  2009/02/25 16:45:00  andreas
 *  Added IOCTL_ESDCAN_GET_BUS_STATISTIC, IOCTL_ESDCAN_RESET_BUS_STATISTIC,
 *        IOCTL_ESDCAN_GET_ERROR_COUNTER, IOCTL_ESDCAN_GET_BITRATE_DETAILS
 *  Fixed IOCTL_ESDCAN_SET_BUSLOAD_INTERVAL, IOCTL_ESDCAN_GET_BUSLOAD_INTERVAL
 *  Fixed canRead() and canWrite(), when interrupted by signal
 *    (several bugs), SIGSTOP and SIGCONT should work as expected, now
 *  Replaced usage of OSIF_SEMA_- with direct usage of down(), up(), etc.
 *  Removed old RTAI-paths
 *
 *  Revision 1.82  2008/08/27 13:48:11  andreas
 *  Fixed zone of several debug outputs to OSIF_ZONE_IRQ
 *
 *  Revision 1.81  2008/06/06 14:12:48  andreas
 *  Fixed IOCTLS without parameter (DESTROY, FLUSH_RX_FIFO, PURGE_TX_FIFO,
 *    SCHEDULE_START, SCHEDULE_STOP, SET_ALT_RTR_ID) for 32-Bit applications
 *    on x86_64 Linux with kernels > 2.6.10
 *
 *  Revision 1.80  2008/06/03 19:15:20  manuel
 *  Removed IOCTL_ESDCAN_DEBUG support when ocb==0
 *  (when no IOCTL_ESDCAN_CREATE was done) for now.
 *  This breaks the support for very old firmware-updaters.
 *
 *  Revision 1.79  2008/05/23 13:49:08  andreas
 *  Removed timeout on canSend() (finally!!!)
 *
 *  Revision 1.78  2008/03/20 11:27:46  andreas
 *  Adapted new way of I20 firmware update
 *
 *  Revision 1.77  2007/11/05 14:58:07  andreas
 *  Added IOCTL_ESDCAN_SET_BUSLOAD_INTERVAL and IOCTL_ESDCAN_GET_BUSLOAD_INTERVAL
 *
 *  Revision 1.76  2006/11/16 12:39:44  andreas
 *  Fixed rx object (flat) mode
 *
 *  Revision 1.75  2006/10/12 09:53:26  andreas
 *  Replaced some forgotten CANIO_-error codes with the OSIF_ counterparts
 *  Replaces some errnos with their OSIF_counterparts
 *  Cleaned up return of error codes to user space,
 *    please use positive error codes, only!!!
 *
 *  Revision 1.74  2006/10/11 10:13:31  andreas
 *  Fixed warnings about ignoring return value from copy_to_user
 *
 *  Revision 1.73  2006/08/17 13:26:59  michael
 *  Rebirth of OSIF_ errorcodes
 *
 *  Revision 1.72  2006/07/04 12:40:35  andreas
 *  Added some missing (although deprecated) IOCTLs.
 *  Sorted OSIF_NOT_IMPLEMENTED and OSIF_NOT_SUPPORTED errors.
 *
 *  Revision 1.71  2006/06/27 13:13:12  andreas
 *  Exchanged OSIF_errors with CANIO_errors
 *
 *  Revision 1.70  2006/06/27 09:55:19  andreas
 *  Added CAN controller type to canStatus info
 *  Use CANIO defines for baudrates, where possible
 *
 *  Revision 1.69  2006/04/28 12:31:59  andreas
 *  Added compat_ioctl and unlocked_ioctl
 *
 *  Revision 1.68  2006/03/07 19:20:25  manuel
 *  canWrite will now work after a canSend on the same handle which has not completely been sent. Timeout applies to all frames from old canSend and canWrite. Len will count only frames from the canWrite. Won\'t work with deferred_tx. If deferred_tx is in the queue, canWrite will return with PENDING_WRITE.
 *
 *  Revision 1.67  2005/12/06 13:25:16  andreas
 *  No functional changes. Removed some old "#if 0" paths and small cleanup.
 *
 *  Revision 1.66  2005/11/02 14:12:46  andreas
 *  Added missing OSIF_CALLTYPE to esdcan_ioctl_done()
 *
 *  Revision 1.65  2005/11/02 07:11:04  andreas
 *  Change for PCI405: sleep_on() is deprecated in kernels > 2.6.x
 *  Corrected canStatus (status field is zero now and not driver mode,
 *    features are masked with FEATURE_MASK_DOCUMENTED)
 *  Added IOCTL_ESDCAN_SET_RX_TIMEOUT
 *  Added IOCTL_ESDCAN_SET_TX_TIMEOUT
 *  Added IOCTL_ESDCAN_GET_SERIAL
 *  Added IOCTL_ESDCAN_GET_FEATURES
 *
 *  Revision 1.64  2005/10/06 07:37:54  matthias
 *  -added esdcan_ioctl handling
 *
 *  Revision 1.63  2005/09/14 15:59:11  michael
 *  filter 20b added
 *
 *  Revision 1.62  2005/08/23 14:45:54  andreas
 *  Fixed broken canIdDelete
 *
 *  Revision 1.61  2005/07/29 08:19:11  andreas
 *  crd-structure stores pointer (pCardIdent) into cardFlavours structure instead of index (flavour), now
 *
 *  Revision 1.60  2005/07/28 07:21:53  andreas
 *  Added calltype to esdcan_tx_obj_sched_get
 *  Removed BOARD define, board name is retrieved from "cardFlavours" array (defined in boardrc.c)
 *
 *  Revision 1.59  2005/06/21 10:48:12  michael
 *  TX_OBJ_SCHEDULING added
 *
 *  Revision 1.58  2005/06/08 09:20:24  andreas
 *  Removed timeout for canTake (nuc_rx call with timeout zero)
 *
 *  Revision 1.57  2005/04/28 15:27:00  andreas
 *  Changed board names (match old candev-driver, now)
 *
 *  Revision 1.56  2005/04/20 13:42:54  andreas
 *  Changed for 64-Bit Linux (ioctl-wrapper, C-types,...)
 *  Cleanup
 *
 *  Revision 1.55  2005/03/22 09:53:10  matthias
 *  added debug output to see timestamp just before they reac hthe userspace
 *
 *  Revision 1.54  2005/03/10 17:01:09  michael
 *  CAN_ACTION_CHECK Macro for Hardware/Firmware validation
 *
 *  Revision 1.53  2005/02/21 15:24:39  matthias
 *  -because CM-size increased we have to copy CMSG and CMSG_T struct from/to userspace
 *
 *  Revision 1.52  2004/12/02 15:53:48  stefan
 *  Please don't use "//" comments!!!
 *
 *  Revision 1.51  2004/10/29 14:59:51  matthias
 *  -removed TICKS code for DEFERRED TX (now use real timestamps)
 *  -removed unused code
 *
 *  Revision 1.50  2004/10/11 09:16:38  andreas
 *  Changed debug output to use zones
 *  Fixed deadlock for USB331
 *
 *  11.05.04 - Changed OSIF_EXTERN into extern and OSIF_CALLTYPE    ab
 *  03.03.04 - TX_OBJ-ioctl-case changed
 *             TX_OBJ_ENABLE-ioctl added                            ab
 *  26.02.04 - New IOCTL Interface (=> No use of cm->reserved)
 *             canRead and canWrite as IOCTL for linux too
 *             New IOCTLS: ...READ_T...TAKE_T...WRITE_T             mt
 *  04.02.04 - Corrected read+write to return ERESTARTSYS after
 *             they've been interrupted by a signal                 ab
 *  13.11.03 - USB331-Support added (THIS IS STILL BETA!!!)
 *             USB-specific stuff is implemented in esdcan.c.
 *             There are NO effects for non-USB-cards. Nevertheless
 *             some cleanup is still needed.                        ab
 *  29.09.03 - canTake doesn't return EIO anymore (also library
 *             changed)
 *           - read/write common: linux also has node-mutex already
 *             (same as IRIX IOCTRL)                                sr
 *  15.01.03 - esdcan_common started for Linux/RTAI/IRIX            sr
 *  21.11.02 - start of irix version based on linux version         sr
 *  18.11.02 - added callback interface for nucleus                 mf
 *  07.11.02 - Version 0.1.1                                        mf
 *  21.05.02 - added hardware timestamping (Version 0.1.0)          mf
 *  17.05.02 - first release (Version 0.1.0)                        mf
 *  02.05.02 - first version                                        mf
 *
 */
/************************************************************************
 *
 *  Copyright (c) 1996 - 2012 by electronic system design gmbh
 *
 *  This software is copyrighted by and is the sole property of
 *  esd gmbh.  All rights, title, ownership, or other interests
 *  in the software remain the property of esd gmbh. This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of esd gmbh.
 *
 *  esd gmbh, reserves the right to modify this software without notice.
 *
 *  electronic system design gmbh          Tel. +49-511-37298-0
 *  Vahrenwalder Str 207                   Fax. +49-511-37298-68
 *  30165 Hannover                         http://www.esd-electronics.com
 *  Germany                                sales@esd-electronics.com
 *
 *************************************************************************/
/*! \file esdcan_common.c
    \brief common driver entries

    This file contains the common entries for the es CAN driver.
*/

/* block attempts to access cards with wrong firmware or hardware version */
#define CAN_ACTION_CHECK(ocb, result, action)                                                     \
       if ( (ocb->node->status) & (CANIO_STATUS_WRONG_HARDWARE | CANIO_STATUS_WRONG_FIRMWARE) ) { \
               if ( (ocb->node->status) & CANIO_STATUS_WRONG_HARDWARE ) {                         \
                       result = OSIF_INVALID_HARDWARE;                                            \
               } else {                                                                           \
                       result = OSIF_INVALID_FIRMWARE;                                            \
               }                                                                                  \
               action;                                                                            \
       }

INT32 OSIF_CALLTYPE esdcan_close_done( CAN_OCB *ocb )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter (ocb = %p)\n", OSIF_FUNCTION, ocb));
#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
        ocb->close_state |= 0x01;
        wake_up(&ocb->wqCloseDelay);
#else
        up(&ocb->sema_close);
#endif
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

INT32 OSIF_CALLTYPE esdcan_tx_done( CAN_OCB *ocb, INT32 result, UINT32 cm_count )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter, result=%d, cm_count=%d\n", OSIF_FUNCTION, result, cm_count));
        ocb->tx_result = result;
        ocb->tx_cm_count = cm_count;
#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
        if (ocb->tx_state & (TX_STATE_PENDING_SEND | TX_STATE_PENDING_TXOBJ)) {
                /* asynchronous TX done, nobody's waiting, but yet needed as canSend() left without resetting state */
                ocb->tx_state = 0;
        } else if (ocb->tx_state & RX_STATE_SIGNAL_INTERRUPT) {
                ocb->tx_state &= ~RX_STATE_SIGNAL_INTERRUPT;
                wake_up(&ocb->wqTx);
        } else {
                /* synchronous TX finished normally */
                if (cm_count == 0) {
                        ocb->tx_state |= TX_STATE_ABORT;
                }
                wake_up_interruptible(&ocb->wqTx);
        }
#else
        if ( ocb->tx_state & (TX_STATE_PENDING_SEND | TX_STATE_PENDING_TXOBJ) ) {
                ocb->tx_state = 0;
        } else if (ocb->tx_state & TX_STATE_ABORTING) {
                up( &ocb->sema_tx_abort );
        } else {
                up( &ocb->sema_tx );
        }
#endif
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

INT32 OSIF_CALLTYPE esdcan_tx_get( CAN_OCB *ocb, CM *cm ) {
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        CAN_DBG((ESDCAN_ZONE, "%s: copy from tx_user_buf tx_obj_size=%d\n", OSIF_FUNCTION, ocb->tx_cm_size));
        /* copy the first CAN_MSG/CM from user space to cm */
        if (copy_from_user(cm, ocb->tx_user_buf, ocb->tx_cm_size)) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: copy_from_user failed (ocb=%x)\n", OSIF_FUNCTION, ocb));
                return OSIF_EFAULT;
        }
        if (ocb->tx_cm_size == sizeof(CAN_MSG)) {
                cm->timestamp.tick = 0LL;
        }
        ocb->tx_user_buf += ocb->tx_cm_size;
        CAN_DBG((ESDCAN_ZONE, "%s: id= %x len=%x\n", OSIF_FUNCTION, cm->id, cm->len));
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

INT32 OSIF_CALLTYPE esdcan_tx_obj_get( CAN_OCB *ocb, CM *cm )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        /* copy the first 16 bytes (CAN_MSG) from user space to cm */
        if (copy_from_user(cm, ocb->tx_user_buf, sizeof(CAN_MSG))) {
                return OSIF_EFAULT;
        }
        ocb->tx_user_buf += sizeof(CAN_MSG);
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}


INT32 OSIF_CALLTYPE esdcan_tx_obj_sched_get( CAN_OCB *ocb, CMSCHED *sched)
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        /* copy the first 16 bytes (CAN_MSG) from user space to cm */
        if (copy_from_user(sched, ocb->tx_user_buf, sizeof(CMSCHED))) {
                return OSIF_EFAULT;
        }
        ocb->tx_user_buf += sizeof(CMSCHED);
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}


INT32 OSIF_CALLTYPE esdcan_rx_done( CAN_OCB *ocb, INT32 result, UINT32 cm_count )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter, result=%d, cm_count=%d\n", OSIF_FUNCTION, result, cm_count));
        ocb->rx_result = result;
        ocb->rx_cm_count = cm_count;

#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
        if (ocb->rx_state & RX_STATE_PENDING_TAKE) {
                ocb->rx_state = 0;  /* actually redundant */
        } else if (ocb->rx_state & RX_STATE_SIGNAL_INTERRUPT) {
                /* This was triggered by "interrupted by signal" branch in esdcan_read_common */
                ocb->rx_state &= ~RX_STATE_SIGNAL_INTERRUPT;
                wake_up(&ocb->wqRx);
        } else {
                /* Wake pending canRead() */
                if (ocb->rx_cm_count == 0) {
                        /* timeout occurred */
                        ocb->rx_state |= RX_STATE_ABORT; /* set only to wake esdcan_read_common, with no frames received */
                }
                wake_up_interruptible(&ocb->wqRx);
        }
#else
        if ( ocb->rx_state & RX_STATE_PENDING_TAKE ) {
                ocb->rx_state = 0;
        } else {
                /* Wake pending canRead() (normal branch, if RX works without interruption) */
                up( &ocb->sema_rx );
        }
#endif
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

/*! \fn esdcan_rx_put( CAN_OCB *ocb, CM *cm );
 *  \brief copy received CM into ocb's rx buffer
 *  \param ocb open control block for this driver instance
 *  \cm pointer to a CM structure that contains the recieved CAN message.
 *  Only the first part containing a CAN_MSG is copied to the \a rx_cmbuf of the ocb structure.
 *  \return OSIF_SUCCESS
 */
INT32 OSIF_CALLTYPE esdcan_rx_put( CAN_OCB *ocb, CM *cm )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        /* Attention: we only copy the CAN_MSG_T part (16 bytes) of the CM into the receive buffer */
        OSIF_MEMCPY(ocb->rx_cmbuf_in++, cm, sizeof(CAN_MSG_T));
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

INT32 OSIF_CALLTYPE esdcan_rx_obj_put( CAN_OCB *ocb, CM *cm, UINT32 *next_id )
{
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        OSIF_MEMCPY(ocb->rx_cmbuf_in++, cm, sizeof(CAN_MSG_T));
        ocb->rx_user_buf += ocb->rx_cm_size;
        if (next_id) {
                UINT32 id = CANIO_ID_INVALID;

                if (get_user(id, &((CAN_MSG*)ocb->rx_user_buf)->id)) {
                        return OSIF_EFAULT;
                }
                *next_id = id;
        }
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}

INT32 OSIF_CALLTYPE esdcan_rx_notify( CAN_OCB *ocb )
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
        wake_up_interruptible(&ocb->node->wqRxNotify); /* wake select */
#endif
        return OSIF_SUCCESS;
}

typedef struct {
        INT32 status;
        wait_queue_head_t wq;
} IOCTL_WAIT_CONTEXT;

INT32 OSIF_CALLTYPE esdcan_ioctl_done( CAN_OCB *ocb, VOID *waitContext, UINT32 cmd, INT32 status, UINT32 len_out )
{
        IOCTL_WAIT_CONTEXT *wait = (IOCTL_WAIT_CONTEXT *)waitContext;

        CAN_DBG((OSIF_ZONE_IRQ, "%s: enter (waitContext = %p)\n", OSIF_FUNCTION, waitContext));
        wait->status = status;
        wake_up(&wait->wq);
        CAN_DBG((OSIF_ZONE_IRQ, "%s: leave\n", OSIF_FUNCTION));
        return OSIF_SUCCESS;
}


NUC_CALLBACKS esdcan_callbacks = {
        esdcan_close_done,
        esdcan_tx_done,
        esdcan_tx_get,
        esdcan_tx_obj_get,
        esdcan_tx_obj_sched_get,
        esdcan_rx_done,
        esdcan_rx_put,
        esdcan_rx_obj_put,
        esdcan_ioctl_done,
        esdcan_rx_notify
};


static INT32 id_filter( CAN_OCB *ocb, CAN_NODE *node, UINT32 cmd, UINT32 id_start, UINT32 *count )
{
        INT32  result = OSIF_SUCCESS;
        UINT32 count_to_do;
        UINT32 count_done;

        if (id_start & CANIO_ID_20B_BASE) {
                count_to_do = CANIO_IDS_20B;
                result = nuc_id_filter(ocb, cmd, CANIO_ID_20B_BASE, &count_to_do);
                if (result == OSIF_SUCCESS) {
                        nuc_id_filter_mask(ocb, id_start, ocb->filter20b.amr, CANIO_IDS_REGION_20B);
                        ocb->filter20b.acr = id_start;
                } else {
                        *count = 0;
                }
        } else {
                count_to_do = *count;
                count_done = 0;
                do {
                        result = nuc_id_filter(ocb, cmd, id_start + count_done, &count_to_do);
                        CAN_DBG((ESDCAN_ZONE, "%s: nuc_id_filter returns %x\n", OSIF_FUNCTION, result));
                        count_done += count_to_do;
                        count_to_do = *count - count_done;
                        if (OSIF_EAGAIN == result) {
                                OSIF_MUTEX_UNLOCK(&node->lock);
                                OSIF_SLEEP(1);
                                OSIF_MUTEX_LOCK(&node->lock);
                        }
                }
                while ( (result == OSIF_EAGAIN) && (count_done < *count) );
                *count = count_done;
        }
        return result;
}


/* only allocate ocb... */
static INT32 ocb_create( INT32 net_no, CAN_OCB **newocb )
{
        INT32    result = OSIF_SUCCESS;
        CAN_OCB *ocb;
        VOID    *vptr;

        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_create: enter\n", OSIF_FUNCTION));
        /* check if node is available */
        if ( (net_no >= (MAX_CARDS * NODES_PER_CARD)) || (NULL == nodes[net_no]) ) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: node %d not available\n", OSIF_FUNCTION,net_no));
                return OSIF_NET_NOT_FOUND;
        }
#if defined (BOARD_USB)
        if (USB_OK != ((USB_MODULE*)((CAN_CARD*)nodes[net_no]->crd)->p_mod)->usb_status) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: node %d not available (usb not ready)\n", OSIF_FUNCTION, net_no));
                return OSIF_NET_NOT_FOUND;
        }
#endif
        /* allocate an OCB */
        CAN_DBG((ESDCAN_ZONE, "%s: allocate OCB\n", OSIF_FUNCTION));
        result = OSIF_MALLOC(sizeof(CAN_OCB),&vptr);
        if (result != OSIF_SUCCESS) {
                return result;
        }
        ocb = (CAN_OCB*)vptr;
        OSIF_MEMSET(ocb, 0, sizeof(CAN_OCB));
#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
        init_waitqueue_head(&ocb->wqRx);
        init_waitqueue_head(&ocb->wqTx);
        init_waitqueue_head(&ocb->wqCloseDelay);
#else
        sema_init(&ocb->sema_rx, 0);
        sema_init(&ocb->sema_tx, 0);
        sema_init(&ocb->sema_tx_abort, 0);
        sema_init(&ocb->sema_close, 0);
#endif
        ocb->minor = net_no; /* TODO rename minor -> net_no */
        ocb->node = nodes[ocb->minor];
        *newocb = ocb;
        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_create: leave, ocb=%p\n", OSIF_FUNCTION, ocb));
        return result;
}


/* now do the main action... */
static INT32 ocb_create2( CAN_OCB *ocb, INT32 net_no, CAN_INIT_ARG *can_init )
{
        INT32   result = OSIF_SUCCESS;
        UINT32  queue_size_rx, queue_size_tx;
        UINT32  cmd;

        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_create: enter\n", OSIF_FUNCTION));
        /* check if node is available */
        if ( (net_no >= (MAX_CARDS * NODES_PER_CARD)) || (NULL == nodes[net_no]) ) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: node %d not available\n", OSIF_FUNCTION, net_no));
                return OSIF_NET_NOT_FOUND;
        }
#if defined (BOARD_USB)
        if (USB_OK != ((USB_MODULE*)((CAN_CARD*)nodes[net_no]->crd)->p_mod)->usb_status) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: node %d not available (usb not ready)\n", OSIF_FUNCTION,net_no));
                return OSIF_NET_NOT_FOUND;
        }
#endif
        queue_size_rx = can_init->queue_size_rx;
        queue_size_tx = can_init->queue_size_tx;
        /* patch queue sizes to at least 1 / check on -1 for CANopen */
        if ( (queue_size_tx == 0) || (queue_size_tx == ~0x0) ) {
                queue_size_tx = 1;
        }
        if ( (queue_size_rx == 0) || (queue_size_rx == ~0x0) ) {
                queue_size_rx = 1;
        }
        /* range check for queue sizes */
        if ( (queue_size_tx > CANIO_MAX_TX_QUEUESIZE) ||
             (queue_size_rx > CANIO_MAX_RX_QUEUESIZE) ) {
                return OSIF_INVALID_PARAMETER;
        }
        ocb->mode = can_init->mode;
        /* TODO: use open_count !!!! */
        if (can_init->mode & FEATURE_LOM) {  /* LOM can never be reset except unloading the driver */
                if (ocb->node->features & FEATURE_LOM) {
                        ocb->node->mode |= FEATURE_LOM;
                } else {
                        CAN_DBG((ESDCAN_ZONE, "%s: LOM not support by this node\n", OSIF_FUNCTION));
                        OSIF_FREE(ocb);
                        return OSIF_INVALID_PARAMETER;
                }
        }
        /* Calculate command for ID-filter */
        if (ocb->mode & CANIO_MODE_OBJECT) {
                cmd = FILTER_OBJ;
        } else {
                cmd = (ocb->mode & CANIO_MODE_NO_RTR) ? 0 : FILTER_RTR;
                cmd |= (ocb->mode & CANIO_MODE_NO_DATA) ? 0 : FILTER_DATA;
        }
        if (ocb->node->features & FEATURE_FULL_CAN) {
                cmd &= ~FILTER_RTR;
        }
        /*
         * BL: RTR needs to be enabled on full CAN-controller, when the
         * handle is opened with the NO_DATA-flag (compatibility with candev-driver)
         */
        if ( (ocb->node->features & FEATURE_FULL_CAN) &&
             (ocb->mode & CANIO_MODE_NO_DATA) &&
             !(ocb->mode & CANIO_MODE_AUTO_ANSWER) ) {
                cmd |= FILTER_RTR;
        }
        ocb->filter_cmd = cmd;
        ocb->filter20b.amr = 0xFFFFFFFF;
        /* rx buffer */
        CAN_DBG((ESDCAN_ZONE, "%s: requesting rx buffer\n", OSIF_FUNCTION));
        result = OSIF_MALLOC((UINT32)(sizeof(CAN_MSG_T) * queue_size_rx), &ocb->rx_cmbuf);
        if (result != OSIF_SUCCESS) {
                OSIF_FREE(ocb);
                return result;
        }
        CAN_DBG((ESDCAN_ZONE, "%s: calling nuc_open@%p\n", OSIF_FUNCTION, nuc_open));
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        result = nuc_open(ocb,
                          ocb->node,
                          &esdcan_callbacks,
                          ocb->mode,
                          queue_size_tx,
                          queue_size_rx);
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        if (result != OSIF_SUCCESS) {
                OSIF_FREE(ocb->rx_cmbuf);
                OSIF_FREE(ocb);
                return result;
        }
        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_create: leave, ocb=%p\n", OSIF_FUNCTION, ocb));
        return result;
}


INT32 ocb_destroy( CAN_OCB *ocb )
{
        INT32 result;

        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_destroy: enter\n", OSIF_FUNCTION));
#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
        /*
         * We need to wait for esdcan_close_done to be called by nucleus.
         * Additionally while closing the ocb, the nucleus might have aborted
         * a RX and/or a TX job. Since we have no control, when these jobs
         * will exit the driver, we mark this ocb as CLOSING and keep delaying
         * destruction of ocb, until the jobs have left the driver.
         */
        if (ocb->rx_state) {
                ocb->rx_state |= RX_STATE_CLOSING;
        }
        if (ocb->tx_state) {
                ocb->tx_state |= TX_STATE_CLOSING;
        }
        CAN_DBG((ESDCAN_ZONE, "%s: calling nuc_close\n", OSIF_FUNCTION));
        result = nuc_close(ocb);
        CAN_DBG((ESDCAN_ZONE, "%s: nuc_close returned %d\n", OSIF_FUNCTION, result));
        CAN_DBG((ESDCAN_ZONE, "%s: waiting for sema_close...\n", OSIF_FUNCTION));
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        wait_event(ocb->wqCloseDelay, (ocb->close_state & 0x01) &&
                                      (!(ocb->tx_state & TX_STATE_CLOSING)) &&
                                      (!(ocb->rx_state & RX_STATE_CLOSING)));
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        CAN_DBG((ESDCAN_ZONE, "%s: close conditions fulfilled, close continues\n", OSIF_FUNCTION));
        OSIF_FREE(ocb->rx_cmbuf);
        OSIF_FREE(ocb);
#else
        CAN_DBG((ESDCAN_ZONE, "%s: calling nuc_close\n", OSIF_FUNCTION));
        result = nuc_close(ocb);
        CAN_DBG((ESDCAN_ZONE, "%s: nuc_close returned %d\n", OSIF_FUNCTION,result));
        /* waiting for aborts to finish */
        CAN_DBG((ESDCAN_ZONE, "%s: waiting for sema_close...\n", OSIF_FUNCTION));
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        down(&ocb->sema_close);
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        CAN_DBG((ESDCAN_ZONE, "%s: sema_close signaled\n", OSIF_FUNCTION));
        /* While closing the ocb, nucleus might have aborted a RX job
         * since we have no control, when this reader will exit the driver,
         * we mark this OCB as ABORTING andf let the reader do the cleanup */
        if ( ocb->rx_state ) {
                /* do not destroy when there is a reader */
                ocb->rx_state |= RX_STATE_CLOSING;
        } else {
                OSIF_FREE(ocb->rx_cmbuf);
                OSIF_FREE(ocb);
        }
#endif
        CAN_DBG((ESDCAN_ZONE_FU, "%s:ocb_destroy: leave\n", OSIF_FUNCTION));
        return result;
}

#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
/* returns error codes negative and received messages positive */
int esdcan_read_common( CAN_OCB *ocb, void *buf, UINT32 cm_count, UINT32 objSize )
{
        INT32 result;
        INT32 ret;
        INT32 flagInterruptedBySignal = 0;
        INT32 flagOcbClosing = 0;

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter, ocb=%p, cm_count=%d\n", OSIF_FUNCTION, ocb, cm_count));
        CAN_ACTION_CHECK(ocb, result, return -result);
        /* return if any read or take is pending */
        if (ocb->rx_state) {
                return -OSIF_PENDING_READ;
        }
        /* no other process is allowed to enter read from now on */
        ocb->rx_state = RX_STATE_PENDING_READ;
        ocb->rx_result = 0;
        /* reset rx buffer pointer */
        ocb->rx_user_buf = buf;
        CAN_DBG((ESDCAN_ZONE, "%s: rx_user_buf=%p\n", OSIF_FUNCTION, buf));
        ocb->rx_cm_size = objSize;
        ocb->rx_cmbuf_in = ocb->rx_cmbuf;
        /* trigger rx for cm_count cm */
        ret = nuc_rx(ocb, ocb->rx_tout, &cm_count);
        if (ret != OSIF_SUCCESS) {
                ocb->rx_state = 0;
                return -ret;
        }
        /* wait for rx finished (interruptible) */
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        if ( wait_event_interruptible( ocb->wqRx,
                                       ( (ocb->rx_cm_count > 0) ||                  /* Successfull end of canRead() */
                                         (ocb->rx_state & RX_STATE_ABORT) ||        /* Set by esdcan_rx_done, e.g. on timeout */
                                         (ocb->rx_state & RX_STATE_CLOSING) ) ) ) {
                /* Interrupted by signal, we might abort here */
                OSIF_MUTEX_LOCK(&ocb->node->lock);
                if ( (ocb->rx_cm_count == 0) &&
                     (!(ocb->rx_state & RX_STATE_ABORT)) &&
                     (!(ocb->rx_state & RX_STATE_CLOSING)) ) {
                        flagInterruptedBySignal = 1;
                        ocb->rx_state |= RX_STATE_SIGNAL_INTERRUPT;  /* Set only in this branch, nuc_rx_abort guarantees one more call of esdcan_rx_done */
                        nuc_rx_abort(ocb, OSIF_OPERATION_ABORTED); /* call of esdcan_rx_done() is guaranteed */
                        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                        wait_event(ocb->wqRx, (!(ocb->rx_state & RX_STATE_SIGNAL_INTERRUPT)));
                } else {
                        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                }
        }
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        ret = ocb->rx_cm_count;
        ocb->rx_cm_count = 0;
        if (ocb->rx_state & RX_STATE_CLOSING) {
                flagOcbClosing = 1;
        }
        ocb->rx_state = 0;
        if (0 == ret) {
                /* return if rx failed */
                CAN_DBG((ESDCAN_ZONE, "%s: rx failed (%d)\n", OSIF_FUNCTION, -(INT32)ocb->rx_result));
                if (flagOcbClosing) {
                        /* Delayed close needs to be woken */
                        /* The ocb is in process of closing and is (amongst others) waiting
                         * for this job to leave the driver */
                        wake_up(&ocb->wqCloseDelay);
                        CAN_DBG((ESDCAN_ZONE, "%s: RX delayed cleanup\n", OSIF_FUNCTION));
                        return -EINTR;
                }
                if (flagInterruptedBySignal) {
                        /* Interrupted by signal, system will reenter this read */
                        return -ERESTARTSYS;
                } else {
                        /* No messages, return rx_result stored by "interruptor",
                         * in most cases RX timeout  */
                        return -(INT32)ocb->rx_result;
                }
        }
        if (flagOcbClosing) {
                wake_up(&ocb->wqCloseDelay);
        }
        /* If we are here, canRead finishes successfully with RX messages */
        {
                UINT8     *dest = buf;
                CAN_MSG_T *src = ocb->rx_cmbuf;
                INT32      i;

                for (i = 0; i < ret; i++) {
                        if (copy_to_user(dest, src, ocb->rx_cm_size)) {
                                return -OSIF_EFAULT;
                        }
                        CAN_DBG((ESDCAN_ZONE, "%s: ts=%lx.%lx\n", OSIF_FUNCTION,
                                 src->timestamp.h.HighPart, src->timestamp.h.LowPart));
                        src++;
                        dest += ocb->rx_cm_size;
                }
        }
        CAN_DBG((ESDCAN_ZONE_FU, "%s:IOCTL_read: leave, copied %d CM(SG)s\n", OSIF_FUNCTION, ret));
        return (int)ret; /* return count and not count*size */
}
#else
/* returns error codes negative and received messages positive */
int esdcan_read_common( CAN_OCB *ocb, void *buf, UINT32 cm_count, UINT32 objSize )
{
        INT32 result;
        INT32 ret;
        INT32 flagAbort = 0;

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter, ocb=%p, cm_count=%d\n", OSIF_FUNCTION, ocb, cm_count));
        CAN_ACTION_CHECK(ocb, result, return -result);
        /* return if any read or take is pending */
        if (ocb->rx_state) {
                return -OSIF_PENDING_READ;
        }
        /* no other process is allowed to enter read from now on */
        ocb->rx_state = RX_STATE_PENDING_READ;
        ocb->rx_result = 0;
        /* reset rx buffer pointer */
        ocb->rx_user_buf = buf;
        CAN_DBG((ESDCAN_ZONE, "%s: rx_user_buf=%p\n", OSIF_FUNCTION, buf));
        ocb->rx_cm_size = objSize;
        ocb->rx_cmbuf_in = ocb->rx_cmbuf;
        sema_init(&ocb->sema_rx, 0);
        /* trigger rx for cm_count cm */
        ret = nuc_rx(ocb, ocb->rx_tout, &cm_count);
        if (ret != OSIF_SUCCESS) {
                ocb->rx_state = 0;
                return -ret;
        }
        /* wait for rx finished (interruptible) */
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        ret = down_interruptible(&ocb->sema_rx);
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        if (ret != OSIF_SUCCESS) {
                if (ocb->rx_state & RX_STATE_CLOSING) {
                        /* The ocb got closed already, but since we are here,
                         * we will have to do the final cleanup */
                        CAN_DBG((ESDCAN_ZONE, "%s: RX delayed cleanup\n", OSIF_FUNCTION));
                        OSIF_FREE(ocb->rx_cmbuf);
                        OSIF_FREE(ocb);
                        return -EINTR; /* system error code, never reaches user, thus no OSIF_ */
                } else if (ocb->rx_cm_count == 0) {
                        /* Interrupted by signal, we'll abort here */
                        ocb->rx_state |= RX_STATE_ABORTING;
                        flagAbort = 1;
                        nuc_rx_abort(ocb, OSIF_OPERATION_ABORTED); /* call of esdcan_rx_done() is guaranteed */
                        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                        down(&ocb->sema_rx);
                        OSIF_MUTEX_LOCK(&ocb->node->lock);
                }
        }
        /* return when rx failed */
        ret = ocb->rx_cm_count;
        if (ret == 0) {
                CAN_DBG((ESDCAN_ZONE, "%s: rx failed (%d)\n", OSIF_FUNCTION, -(INT32)ocb->rx_result));
                ocb->rx_state = 0;
                if (flagAbort) {
                        return -ERESTARTSYS;
                } else {
                        return -(INT32)ocb->rx_result;
                }
        }
        {      /* read with(out) timestamps */
                UINT8     *dest = buf;
                CAN_MSG_T *src  = ocb->rx_cmbuf;
                INT32      i;

                for (i = 0; i < ret; i++) {
                        if (copy_to_user(dest, src, ocb->rx_cm_size)) {
                                return -OSIF_EFAULT;
                        }
                        CAN_DBG((ESDCAN_ZONE, "%s: ts=%lx.%lx\n", OSIF_FUNCTION,
                                 src->timestamp.h.HighPart, src->timestamp.h.LowPart));
                        src++;
                        dest += ocb->rx_cm_size;
                }
                ocb->rx_cm_count = 0;
        }
        ocb->rx_state = 0;
        CAN_DBG((ESDCAN_ZONE_FU, "%s:IOCTL_read: leave, copied %d CM(SG)s\n", OSIF_FUNCTION, ocb->rx_cm_count ));
        return (int)ret; /* return count and not count*size */
}
#endif

#ifdef OSIF_USE_PREEMPT_RT_IMPLEMENTATION
/* Returns error codes negative and transmitted messages positive */
int esdcan_write_common( CAN_OCB *ocb, void *buf, UINT32 cm_count, UINT32 objSize )
{
        INT32   result;
        UINT32  old_job_tx_pending = 0;
        INT32   tx_len;
        INT32   flagInterruptedBySignal = 0;
        INT32   flagOcbClosing = 0;

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter cm_count=%d\n", OSIF_FUNCTION, cm_count));
        if (0 == cm_count) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (OSIF_INVALID_PARAMETER)\n", OSIF_FUNCTION));
                return -OSIF_INVALID_PARAMETER; /* BL TODO: change error-code??? */
        }
        /* block attempts to access cards with wrong firmware */
        CAN_ACTION_CHECK(ocb, result, return -result);
        if (ocb->tx_state & (TX_STATE_PENDING_WRITE | TX_STATE_PENDING_TXOBJ | TX_STATE_ABORT)) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (PENDING_WRITE)\n", OSIF_FUNCTION));
                return -OSIF_PENDING_WRITE;
        } else if (ocb->tx_state & TX_STATE_PENDING_SEND) {
                UINT32 dtx;

                if (nuc_deferred_tx_messages(ocb, &dtx) != OSIF_SUCCESS) {
                        /* nuc_deferred_tx_messages should never fail, but anyway, you may try again */
                        return -ERESTARTSYS; /* system error code, never reaches user, thus no OSIF_ */
                }
                if (dtx) {
                        return -OSIF_PENDING_WRITE;
                }
                if (nuc_tx_messages(ocb, &old_job_tx_pending) != OSIF_SUCCESS) {
                        /* nuc_tx_messages should never fail, but anyway, you may try again */
                        return -ERESTARTSYS; /* system error code, never reaches user, thus no OSIF_ */
                }
        }
        ocb->tx_state = TX_STATE_PENDING_WRITE;
        ocb->tx_user_buf = buf;
        ocb->tx_cm_size = objSize;
        /* trigger tx job, copy_from_user is done in our context from nuc_tx() */
        result = nuc_tx(ocb, ocb->tx_tout, &cm_count);
        if ( (result != OSIF_SUCCESS) && (cm_count == 0) ) {
                ocb->tx_state = 0;
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (nuc_tx, err = %d, cm_count = %d)\n",
                         OSIF_FUNCTION, result, cm_count));
                return -result;
        }
        /* wait until job is finished or error occured */
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        if (wait_event_interruptible(ocb->wqTx,
                                     ( (0 < (ocb->tx_cm_count - old_job_tx_pending)) ||
                                       (ocb->tx_state & TX_STATE_ABORT) ||
                                       (ocb->tx_state & TX_STATE_CLOSING) ) ) ) {
                OSIF_MUTEX_LOCK(&ocb->node->lock);
                if ( (0 == (ocb->tx_cm_count - old_job_tx_pending)) &&
                     (!(ocb->tx_state & TX_STATE_ABORT)) &&
                     (!(ocb->tx_state & TX_STATE_CLOSING)) ) {
                        flagInterruptedBySignal = 1;
                        ocb->tx_state |= TX_STATE_SIGNAL_INTERRUPT;
                        nuc_tx_abort(ocb, OSIF_OPERATION_ABORTED, 0);
                        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                        wait_event(ocb->wqTx, (!(ocb->tx_state & TX_STATE_SIGNAL_INTERRUPT))); /* wait for esdcan_tx_done */
                } else {
                        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                }
        }
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        tx_len = ocb->tx_cm_count - old_job_tx_pending;
        ocb->tx_cm_count = 0;
        if (ocb->tx_state & TX_STATE_CLOSING) {
                flagOcbClosing = 1;
        }
        ocb->tx_state = 0;
        /* check errors */
        if (0 >= tx_len) {
                if (flagOcbClosing) {
                        /* Delayed close needs to be woken */
                        /* The ocb is in process of closing and is (amongst others) waiting
                         * for this job to leave the driver */
                        wake_up(&ocb->wqCloseDelay);
                        CAN_DBG((ESDCAN_ZONE, "%s: TX delayed cleanup\n", OSIF_FUNCTION));
                        return -EINTR;
                }
                if (flagInterruptedBySignal) {
                        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (ERESTARTSYS, tx_cm_count = %d, old_job_tx_pending = %d)\n",
                                 OSIF_FUNCTION, (int)ocb->tx_cm_count, (int)old_job_tx_pending));
                        return -ERESTARTSYS;
                } else {
                        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (result: %ld, tx_cm_count = %d, old_job_tx_pending = %d)\n",
                                 OSIF_FUNCTION, -(ocb->tx_result), (int)ocb->tx_cm_count, (int)old_job_tx_pending));
                        return -(ocb->tx_result);
                }
        }
        if (flagOcbClosing) {
                wake_up(&ocb->wqCloseDelay);
        }
        CAN_DBG((ESDCAN_ZONE, "%s: copied %d bytes\n",
                 OSIF_FUNCTION, ocb->tx_cm_count * sizeof(CAN_MSG)));
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (success, tx_cm_count = %d, tx_len = %d)\n",
                 OSIF_FUNCTION, (int)ocb->tx_cm_count, (int)tx_len));
        return (int)tx_len; /* return count and not count*size */
}
#else
/* Returns error codes negative and transmitted messages positive */
int esdcan_write_common( CAN_OCB *ocb, void *buf, UINT32 cm_count, UINT32 objSize )
{
        INT32   result, ret;
        UINT32  old_job_tx_pending = 0;
        INT32   tx_len;
        INT32   flagAbort = 0;

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter cm_count=%d\n", OSIF_FUNCTION, cm_count));

        if ( cm_count == 0 ) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (OSIF_INVALID_PARAMETER)\n", OSIF_FUNCTION));
                return -OSIF_INVALID_PARAMETER; /* AB TODO: change error-code??? */
        }

        /* block attempts to access cards with wrong firmware */
        CAN_ACTION_CHECK(ocb, result, return -result);

        if ( ocb->tx_state & (TX_STATE_PENDING_WRITE | TX_STATE_PENDING_TXOBJ | TX_STATE_ABORTING) ) {
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (PENDING_WRITE)\n", OSIF_FUNCTION));
                return -OSIF_PENDING_WRITE;
        } else if ( ocb->tx_state & TX_STATE_PENDING_SEND ) {
                UINT32 dtx;
                if (nuc_deferred_tx_messages(ocb, &dtx) != OSIF_SUCCESS) {
                        /* nuc_deferred_tx_messages should never fail, but anyway, you may try again */
                        return -ERESTARTSYS; /* system error code, never reaches user, thus no OSIF_ */
                }
                if (dtx) {
                        return -OSIF_PENDING_WRITE;
                }
                if (nuc_tx_messages(ocb, &old_job_tx_pending) != OSIF_SUCCESS) {
                        /* nuc_tx_messages should never fail, but anyway, you may try again */
                        return -ERESTARTSYS; /* system error code, never reaches user, thus no OSIF_ */
                }
        }
        ocb->tx_state = TX_STATE_PENDING_WRITE;

        ocb->tx_user_buf = buf;
        ocb->tx_cm_size = objSize;
        sema_init(&ocb->sema_tx, 0);

        /* trigger tx job, copy_from_user is done in our context from nuc_tx() */
        result = nuc_tx(ocb, ocb->tx_tout, &cm_count);
        if ( (result != OSIF_SUCCESS) && (cm_count == 0) ) {
                ocb->tx_state = 0;
                CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (nuc_tx, err = %d, cm_count = %d)\n",
                         OSIF_FUNCTION, result, cm_count));
                return -result;
        }

        /* wait until job is finished or error occured */
        OSIF_MUTEX_UNLOCK(&ocb->node->lock);
        ret = down_interruptible(&ocb->sema_tx);
        OSIF_MUTEX_LOCK(&ocb->node->lock);
        /* Check, if the job was finished between wakeup and mutex lock */
        tx_len = ocb->tx_cm_count;
        tx_len -= old_job_tx_pending;
        if ( (ret != OSIF_SUCCESS) &&
             (tx_len <= 0) ) {
                ocb->tx_state |= TX_STATE_ABORTING;
                flagAbort = 1;
                nuc_tx_abort(ocb, OSIF_OPERATION_ABORTED, 0);
                OSIF_MUTEX_UNLOCK(&ocb->node->lock);
                down(&ocb->sema_tx_abort); /* wait for esdcan_tx_done */
                OSIF_MUTEX_LOCK(&ocb->node->lock);
        }
        tx_len = ocb->tx_cm_count;
        tx_len -= old_job_tx_pending;
        ocb->tx_cm_count = 0;
        ocb->tx_state = 0;
        /* check errors */
        if (tx_len <= 0) {
                if (flagAbort) {
                        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (ERESTARTSYS, tx_cm_count = %d, old_job_tx_pending = %d)\n",
                                 OSIF_FUNCTION, (int)ocb->tx_cm_count, (int)old_job_tx_pending));
                        return -ERESTARTSYS;
                } else {
                        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (result: %ld, tx_cm_count = %d, old_job_tx_pending = %d)\n",
                                 OSIF_FUNCTION, -(ocb->tx_result), (int)ocb->tx_cm_count, (int)old_job_tx_pending));
                        return -(ocb->tx_result);
                }
        }
        CAN_DBG((ESDCAN_ZONE, "%s: copied %d bytes\n",
                 OSIF_FUNCTION, ocb->tx_cm_count * sizeof(CAN_MSG)));
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (success, tx_cm_count = %d, tx_len = %d)\n",
                 OSIF_FUNCTION, (int)ocb->tx_cm_count, (int)tx_len));
        return (int)tx_len; /* return count and not count*size */
}
#endif

#ifdef OSIF_OS_LINUX /* AB: This won't work on IRIX */
static int my_nuc_ioctl( CAN_OCB *ocb, UINT32 cmd,
                         VOID *buf_in, UINT32 len_in, VOID *buf_out, UINT32 *len_out )
{
        INT32              status;
        IOCTL_WAIT_CONTEXT wait;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
        DEFINE_WAIT(waitQ);
#endif
        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter cmd=%08lx\n", OSIF_FUNCTION, cmd));
        init_waitqueue_head(&wait.wq);
        status = nuc_ioctl(ocb, &wait, cmd, buf_in, len_in, buf_out, len_out);
        if (OSIF_EBUSY == status) {
                CAN_NODE *node = ocb->node;

                /* Must wait here */
                OSIF_MUTEX_UNLOCK(&node->lock);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
                /* BL: sleep_on is deprecated since 2.6.10 */
                prepare_to_wait(&wait.wq, &waitQ, TASK_UNINTERRUPTIBLE);
                schedule();
                finish_wait(&wait.wq, &waitQ);
#else
                sleep_on(&wait.wq);
#endif
                OSIF_MUTEX_LOCK(&node->lock);
                status = wait.status;
        }
        CAN_DBG((ESDCAN_ZONE_FU, "%s: leave (status=%d)\n", OSIF_FUNCTION, status));
        return status;
}
#endif

static int esdcan_ioctl_internal(struct inode *inode, struct file *file,
                                 unsigned int cmd, IOCTL_ARG *ioArg,
                                 CAN_OCB *ocb )
{
        INT32      result = OSIF_SUCCESS;
        CAN_NODE  *node;
        UINT32     objSize;

        /* if private_data == 0 -> only allow CAN_CREATE */
        if (!ocb) {
                CAN_DBG((ESDCAN_ZONE_FU,
                         "%s: create path: cmd: 0x%X, IOCTL_ESDCAN_CREATE: 0x%0X\n",
                         OSIF_FUNCTION, cmd, IOCTL_ESDCAN_CREATE));
                if (cmd == IOCTL_ESDCAN_CREATE) {
                        CAN_INIT_ARG can_init;
#ifdef OSIF_OS_LINUX
                        INT32 net_no = INODE2CANNODE(inode);
#endif /* OSIF_OS_LINUX */
#ifdef OSIF_OS_IRIX
                        INT32 net_no = file->node->net_no;
                        ocb = file->ocb; /* set ocb (on irix ocb_create already called) */
#endif /* OSIF_OS_IRIX */

                        CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_CREATE, net_no=%d\n", OSIF_FUNCTION, net_no));
                        if (copy_from_user(&can_init, (VOID*)ioArg->buffer, sizeof(CAN_INIT_ARG))) {
                                HOTPLUG_GLOBAL_UNLOCK;
                                return OSIF_EFAULT;
                        }
#ifdef OSIF_OS_LINUX
                        result = ocb_create(net_no, &ocb);
                        if (result != OSIF_SUCCESS) {
                                HOTPLUG_GLOBAL_UNLOCK;
                                return (int)result;
                        }
#endif /* OSIF_OS_LINUX */
                        result = ocb_create2(ocb, net_no, &can_init);
                        if (result != OSIF_SUCCESS) {
                                HOTPLUG_GLOBAL_UNLOCK;
                                return (int)result;
                        }

#ifdef OSIF_OS_LINUX
                        file->private_data = ocb;
                        /*
                         *  AB: storing pointer to file-struct in ocb, in order
                         *      to be able to clear file->private_data on
                         *      USB-surprise-removal
                         */
                        ocb->file = file;
#endif /* OSIF_OS_LINUX */
#ifdef OSIF_OS_IRIX
                        file->private_data = ocb; /* mark as ready */
#endif /* ifdef (OSIF_OS_IRIX) */
                        nuc_check_links(ocb);
                        HOTPLUG_GLOBAL_UNLOCK;
                        return OSIF_SUCCESS;
                } else {
                        CAN_DBG((ESDCAN_ZONE_FU,
                                 "%s: determine lib path: cmd: 0x%X, IOCTL_CAN_SET_QUEUESIZE: 0x%0X\n",
                                 OSIF_FUNCTION, cmd, IOCTL_CAN_SET_QUEUESIZE));

                        HOTPLUG_GLOBAL_UNLOCK;
                        /*
                         *  Normally we've discovered, that an old "candev"-ntcan-library (major 1)
                         *  is trying to access the new driver. Unfortunately this case does happen
                         *  with "surprise"-removed USB-modules, too (although ntcan-library has a
                         *  correct version). Since there're no modules out there, which use the old
                         *  candev-driver and thus all USB-modules should be delivered with a new
                         *  library, we deliver another error-code depending on the IOCTL-command
                         *  in case of the USB-module.
                         */
#if defined (BOARD_USB)
                        if (cmd == IOCTL_CAN_SET_QUEUESIZE) {
                                return OSIF_INVALID_DRIVER;
                        } else {
                                return OSIF_WRONG_DEVICE_STATE;
                        }
#else
                        return OSIF_INVALID_DRIVER;
#endif
                }
        }
        HOTPLUG_GLOBAL_UNLOCK;
        node = ocb->node;
        if (-1 == HOTPLUG_BOLT_USER_ENTRY(((CAN_CARD*)node->crd)->p_mod)) {
                CAN_DBG((ESDCAN_ZONE, "%s: bolt_user_entry returned -1\n", OSIF_FUNCTION));
                return OSIF_EIO;
        }
        OSIF_MUTEX_LOCK(&node->lock);
        if (0 == file->private_data) {
                ioArg->size = 0;
                OSIF_MUTEX_UNLOCK(&node->lock);
                return OSIF_INVALID_HANDLE;
        }
        switch(cmd) {
        case IOCTL_ESDCAN_DESTROY:
        case IOCTL_ESDCAN_DESTROY_DEPRECATED:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_DESTROY, minor=%d\n", OSIF_FUNCTION,ocb->minor));
#if defined (OSIF_OS_LINUX) || defined (OSIF_OS_IRIX)
                file->private_data = 0; /* mark ocb as no more valid! */
#endif /* defined (OSIF_OS_LINUX) || defined (OSIF_OS_IRIX) */
                result = ocb_destroy(ocb);
                break;
        }
        case IOCTL_ESDCAN_SEND_T:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_SEND_T: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG_T);
                goto IOCTL_ESDCAN_SEND_00;
        case IOCTL_ESDCAN_SEND:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_SEND: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG);
        IOCTL_ESDCAN_SEND_00:
        {
                UINT32 cm_count;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                /* entering not allowed when a write is pending */
                if (ocb->tx_state & TX_STATE_PENDING_WRITE) {
                        result = OSIF_PENDING_WRITE;
                        break;
                }
                /* this prevents us for processes that want to write - send is still allowed */
                ocb->tx_state = TX_STATE_PENDING_SEND;
                ocb->tx_user_buf = (VOID*)ioArg->buffer;
                ocb->tx_cm_size = objSize;
                cm_count = ioArg->size;
                CAN_DBG((ESDCAN_ZONE, "%s: cm_count=%d\n", OSIF_FUNCTION, cm_count));
                /* trigger tx job, copy_from_user is done in our context from nuc_tx() */
                result = nuc_tx(ocb, 0, &cm_count);
                /* check errors */
                if ( (result != OSIF_SUCCESS) && (cm_count == 0) ) {
                        ocb->tx_state = 0;
                        break;
                }
                CAN_DBG((ESDCAN_ZONE, "%s: copied %d bytes\n", OSIF_FUNCTION, cm_count * sizeof(CAN_MSG)));
                result = OSIF_SUCCESS;
                ioArg->size = cm_count; /* return count and not count*size */
                break;
        }
        case IOCTL_ESDCAN_TAKE_T:
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_TAKE_T\n", OSIF_FUNCTION));
                objSize = sizeof(CAN_MSG_T);
                goto    IOCTL_ESDCAN_TAKE_00;
        case IOCTL_ESDCAN_TAKE:
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_TAKE\n", OSIF_FUNCTION));
                objSize = sizeof(CAN_MSG);
        IOCTL_ESDCAN_TAKE_00:
        {
                INT32 cm_count;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                /* only one process may take/read at a time */
                if (ocb->rx_state) {
                        result = OSIF_PENDING_READ;
                        break;
                }
                ocb->rx_state = RX_STATE_PENDING_TAKE;
                /* reset rx buffer pointer */
                ocb->rx_user_buf = (VOID*)ioArg->buffer;
                ocb->rx_cm_size = objSize;
                ocb->rx_cmbuf_in = ocb->rx_cmbuf;
                ocb->rx_cm_count = 0;
                cm_count = (UINT32)(ioArg->size & (CANIO_MAX_RX_QUEUESIZE-1));
                ioArg->size = 0;
                CAN_DBG((ESDCAN_ZONE, "%s: cm_count=%d\n", OSIF_FUNCTION,cm_count));
                if (ocb->mode & CANIO_MODE_OBJECT) {
                        UINT32 id;

                        if (get_user(id, &((CAN_MSG*)ocb->rx_user_buf)->id)) {
                                ocb->rx_state = 0;
                                result = OSIF_EFAULT;
                                break;
                        }
                        /* TODO: Michael: RX object mode also needs a ## !!! */
                        result = nuc_rx_obj(ocb, (UINT32*)&cm_count, id);
                        if (result != OSIF_SUCCESS) {
                                ocb->rx_state = 0;
                                break;
                        }
                        ocb->rx_cm_count = cm_count; /* non object mode does this in rx_done */
                } else {
                        /* trigger rx for cm_count cm */
                        result = nuc_rx(ocb, 0, (UINT32*)&cm_count);
                        if (result != OSIF_SUCCESS) {
                                ocb->rx_state = 0;
                                break;
                        }
                }
                /* return if rx failed */
                if (0 == ocb->rx_cm_count) {
                        nuc_rx_abort(ocb, OSIF_SUCCESS); /* return OSIF_SUCCESS and _not_ OSIF_EIO! */
                        if (0 == ocb->rx_cm_count) {
                                ocb->rx_state = 0;
                                result = (int)ocb->rx_result;
                                break;
                        }
                }
                {       /* read with(out) timestamps */
                        UINT8     *dest = (VOID*)ioArg->buffer;
                        CAN_MSG_T *src = ocb->rx_cmbuf;
                        INT32      i;

                        for (i = 0; i < ocb->rx_cm_count; i++) {
                                if (copy_to_user(dest, src, ocb->rx_cm_size)) {
                                        result = OSIF_EFAULT;
                                        break;
                                }
                                src++;
                                dest += ocb->rx_cm_size;
                        }
                }
                ocb->rx_state = 0;
                CAN_DBG((ESDCAN_ZONE, "%s: leave, copied %d CM(SG)s\n", OSIF_FUNCTION, ocb->rx_cm_count));
                ioArg->size = ocb->rx_cm_count; /* return count and not count*size */
                break;
        }
        case IOCTL_ESDCAN_WRITE_T:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_WRITE_T: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG_T);
                goto IOCTL_ESDCAN_WRITE_00;
        case IOCTL_ESDCAN_WRITE:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_WRITE: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG);
        IOCTL_ESDCAN_WRITE_00:
                result = esdcan_write_common(ocb, (VOID*)ioArg->buffer, ioArg->size, objSize);
                if (0 > result) {
                        result = -result;
                } else {
                        ioArg->size = result;
                        result = OSIF_SUCCESS;
                }
                break;

        case IOCTL_ESDCAN_READ_T:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_READ_T: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG_T);
                goto IOCTL_ESDCAN_READ_00;
        case IOCTL_ESDCAN_READ:
                CAN_DBG((ESDCAN_ZONE, "%s:IOCTL_ESDCAN_READ: cm_count=%d\n", OSIF_FUNCTION, ioArg->size));
                objSize = sizeof(CAN_MSG);
        IOCTL_ESDCAN_READ_00:
                result = esdcan_read_common(ocb, (VOID*)ioArg->buffer, ioArg->size, objSize);
                if (0 > result) {
                        result = -result;
                } else {
                        ioArg->size = result;
                        result = OSIF_SUCCESS;
                }
                break;
        case IOCTL_ESDCAN_PURGE_TX_FIFO:
        case IOCTL_ESDCAN_PURGE_TX_FIFO_DEPRECATED:
        case IOCTL_ESDCAN_TX_ABORT:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_TX_ABORT\n", OSIF_FUNCTION));
                result = nuc_tx_abort(ocb, OSIF_OPERATION_ABORTED,0);
                break;
        }
        case IOCTL_ESDCAN_FLUSH_RX_FIFO:
        case IOCTL_ESDCAN_FLUSH_RX_FIFO_DEPRECATED:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_PURGE_RX_FIFO\n", OSIF_FUNCTION));
                result = nuc_rx_purge( ocb );
                break;
        }
        case IOCTL_ESDCAN_RX_ABORT:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_RX_ABORT\n", OSIF_FUNCTION));
                result = nuc_rx_abort(ocb, OSIF_OPERATION_ABORTED );
                break;
        }
        case IOCTL_ESDCAN_RX_OBJ:
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_RX_OBJ: TODO !!!!\n", OSIF_FUNCTION));
                /* TODO */
                result = OSIF_NOT_IMPLEMENTED;
                break;

        case IOCTL_ESDCAN_TX_OBJ_CREATE:
                cmd = NUC_TX_OBJ_CREATE;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_UPDATE:
                cmd = NUC_TX_OBJ_UPDATE;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_AUTOANSWER_ON:
                cmd = NUC_TX_OBJ_AUTOANSWER_ON;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_AUTOANSWER_OFF:
                cmd = NUC_TX_OBJ_AUTOANSWER_OFF;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_DESTROY:
                cmd = NUC_TX_OBJ_DESTROY;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_SCHEDULE:
                cmd = NUC_TX_OBJ_SCHEDULE;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_SCHEDULE_START:
                cmd = NUC_TX_OBJ_SCHEDULE_START;
                goto IOCTL_ESDCAN_TX_OBJ;
        case IOCTL_ESDCAN_TX_OBJ_SCHEDULE_STOP:
                cmd = NUC_TX_OBJ_SCHEDULE_STOP;
        IOCTL_ESDCAN_TX_OBJ:
        {
                UINT32 cm_count;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_TX_OBJ_xxx\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                /* entering not allowed when a write is pending */
                if (ocb->tx_state & TX_STATE_PENDING_WRITE){
                        result = OSIF_PENDING_WRITE;
                        break;
                }
                /* this prevents us for processes that want to write - send is still allowed */
                ocb->tx_state = TX_STATE_PENDING_TXOBJ;
                ocb->tx_user_buf = (VOID*)ioArg->buffer;
                ocb->tx_cm_size = sizeof(CAN_MSG);
                cm_count = 1;
                CAN_DBG((ESDCAN_ZONE, "%s: cm_count=%d\n", OSIF_FUNCTION, cm_count));
                result = nuc_tx_obj(ocb, cmd, &cm_count);
                ocb->tx_state = 0;
                break;
        }
        case IOCTL_ESDCAN_ID_RANGE_ADD: /* TODO: put in lib, add ioctl CAN_ID_FILTER */
        {
                CAN_ID_RANGE_ARG id_range;
                UINT32           count;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_ID_RANGE_ADD\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if ( copy_from_user(&id_range, (VOID*)ioArg->buffer, sizeof(CAN_ID_RANGE_ARG)) ) {
                        result = OSIF_EFAULT;
                        break;
                }
                count = id_range.id_stop - id_range.id_start + 1;
                result = id_filter(ocb, node, FILTER_ON | ocb->filter_cmd, id_range.id_start, &count);
                if (result != OSIF_SUCCESS) {
                        id_filter(ocb, node,  FILTER_OFF | ocb->filter_cmd, id_range.id_start, &count);
                }
                break;
        }
        case IOCTL_ESDCAN_ID_RANGE_DEL: /* TODO: put in lib, add ioctl CAN_ID_FILTER */
        {
                CAN_ID_RANGE_ARG id_range;
                UINT32           count;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_ID_RANGE_DEL\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (copy_from_user(&id_range, (VOID*)ioArg->buffer, sizeof(CAN_ID_RANGE_ARG))) {
                        result = OSIF_EFAULT;
                        break;
                }
                count = id_range.id_stop - id_range.id_start + 1;
                result = id_filter(ocb, node, FILTER_OFF | ocb->filter_cmd, id_range.id_start, &count);
                break;
        }
        case IOCTL_ESDCAN_SET_BAUD:
        {
                UINT32 baud;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_SET_BAUD\n", OSIF_FUNCTION ));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(baud, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BAUDRATE_SET,
                                      &baud, sizeof(UINT32), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_GET_BAUD:
        {
                UINT32 baud;
                INT32  outbuflen = sizeof(UINT32);

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_BAUD\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BAUDRATE_GET,
                                      NULL, 0, &baud, &outbuflen);
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_BAUD: nuc_baudrate_get returned %x (node=%x baud=%d)\n",
                         OSIF_FUNCTION, result, ocb->node, baud));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (put_user(baud, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_BAUD (baud=%08x)\n", OSIF_FUNCTION, baud));
                break;
        }
        case IOCTL_ESDCAN_SET_TIMEOUT:
        {
                CAN_TIMEOUT_ARG timeout;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_SET_TIMEOUT\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (copy_from_user(&timeout, (VOID*)ioArg->buffer, sizeof(CAN_TIMEOUT_ARG))) {
                        result = OSIF_EFAULT;
                        break;
                }
                if ( (timeout.rxtout > CANIO_MAX_RX_TIMEOUT) || (timeout.txtout > CANIO_MAX_TX_TIMEOUT) ) {
                        result = OSIF_INVALID_PARAMETER;
                        break;
                }
                ocb->rx_tout = timeout.rxtout;
                ocb->tx_tout = timeout.txtout;
                break;
        }
        case IOCTL_ESDCAN_GET_RX_MESSAGES:
        {
                UINT32 msgs = 0;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_RX_MESSAGES\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                CAN_DBG((ESDCAN_ZONE, "%s: calling nuc_rx_messages\n", OSIF_FUNCTION));
                result = nuc_rx_messages( ocb, &msgs );
                if (put_user(msgs, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_RX_MESSAGES (rx_msgs = %d)\n", OSIF_FUNCTION, msgs));
                break;
        }
        case IOCTL_ESDCAN_GET_RX_TIMEOUT:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_RX_TIMEOUT\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (put_user(ocb->rx_tout, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_RX_TIMEOUT (rx_tout = %d)\n", OSIF_FUNCTION, ocb->rx_tout));
                break;
        }
        case IOCTL_ESDCAN_GET_TX_TIMEOUT:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_TX_TIMEOUT\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (put_user(ocb->tx_tout, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_TX_TIMEOUT (tx_tout = %d)\n", OSIF_FUNCTION, ocb->tx_tout));
                break;
        }
        case IOCTL_ESDCAN_GET_TIMESTAMP_FREQ:
        case IOCTL_ESDCAN_GET_TIMESTAMP:
        {
                CAN_TS ts;
                INT32  outbuflen = sizeof(CAN_TS);

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_TIMESTAMP_...\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result =  my_nuc_ioctl(ocb, (cmd == IOCTL_ESDCAN_GET_TIMESTAMP_FREQ) ? ESDCAN_CTL_TIMEST_FREQ_GET : ESDCAN_CTL_TIMESTAMP_GET,
                                       NULL, 0, &ts, &outbuflen);
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_TIMESTAMP_... result=%d, ts=0x%llX\n",
                         OSIF_FUNCTION, result, ts.tick));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (copy_to_user((VOID*)ioArg->buffer, &ts, sizeof(CAN_TS))) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_DEBUG:
        {
                CAN_DEBUG_ARG can_debug;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_DEBUG\n", OSIF_FUNCTION));
                if ( copy_from_user( &can_debug, (VOID*)ioArg->buffer, sizeof(can_debug) ) ) {
                        result = OSIF_EFAULT;
                        break;
                }
                switch (can_debug.cmd) {
                case CAN_DEBUG_VER:
                {
                        CAN_CARD *crd = (CAN_CARD*)node->crd;
                        can_debug.p.ver.drv_ver = MAKE_VERSION(LEVEL, REVI, BUILD);
                        can_debug.p.ver.firm_ver = MAKE_VERSION(crd->version_firmware.level,
                                                                crd->version_firmware.revision,
                                                                crd->version_firmware.build);
                        can_debug.p.ver.hard_ver = MAKE_VERSION(crd->version_hardware.level,
                                                                crd->version_hardware.revision,
                                                                crd->version_hardware.build);
                        can_debug.p.ver.board_stat = 0;
                        OSIF_SNPRINTF(((CHAR8*)can_debug.p.ver.firm_info,
                                       sizeof(can_debug.p.ver.firm_info),
                                       "%s", crd->pCardIdent->all.name));
                        can_debug.p.ver.features = (UINT16)(ocb->node->features & FEATURE_MASK_DOCUMENTED);
                        can_debug.p.ver.ctrl_type = node->ctrl_type;
                        OSIF_MEMSET(can_debug.p.ver.reserved, 0, sizeof(can_debug.p.ver.reserved));
                        break;
                }
                default:
                {
                        INT32 outbuflen = sizeof(can_debug);
                        result = my_nuc_ioctl(ocb, ESDCAN_CTL_DEBUG,
                                              &can_debug, sizeof(can_debug), &can_debug, &outbuflen);
                }
                }
                if (copy_to_user((VOID*)ioArg->buffer, &can_debug, sizeof(can_debug))) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_SET_20B_HND_FILTER:
        {
                UINT32 amr;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_CAN_SET_20B_FILTER\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(amr, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = nuc_id_filter_mask(ocb, ocb->filter20b.acr, amr, CANIO_IDS_REGION_20B);
                if (OSIF_SUCCESS == result) {
                        ocb->filter20b.amr = amr;
                }
                break;
        }
        case IOCTL_ESDCAN_SET_RX_TIMEOUT:
        case IOCTL_ESDCAN_SET_TX_TIMEOUT:
        {
                UINT32 tout;

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_SET_R/TX_TIMEOUT\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(tout, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                if (IOCTL_ESDCAN_SET_TX_TIMEOUT == cmd) {
                        ocb->tx_tout = tout;
                } else {
                        ocb->rx_tout = tout;
                }
                break;
        }
        case IOCTL_ESDCAN_GET_SERIAL:
        {
                CAN_CARD *crd = (CAN_CARD*)node->crd;
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_SERIAL\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (put_user(crd->serial, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_GET_FEATURES:
        {
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_FEATURES\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (put_user(ocb->node->features, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_UPDATE:         /* candev legacy ioctl, not needed anymore, replaced by IOCTL_ESDCAN_TX_OBJ_UPDATE */
        case IOCTL_ESDCAN_SET_MODE:       /* candev legacy ioctl, not needed for esdcan */
        case IOCTL_ESDCAN_GET_TICKS_FREQ: /* legacy ioctl, not needed anymore */
        case IOCTL_ESDCAN_GET_TICKS:      /* legacy ioctl, not needed anymore */
        case IOCTL_ESDCAN_SET_ALT_RTR_ID: /* deprecated customer specific extension */
        {
                result = OSIF_NOT_IMPLEMENTED; /* ...and never will be */
                break;
        }
        case IOCTL_ESDCAN_SET_BUSLOAD_INTERVAL:
        {
                UINT32 interval;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(interval, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BUSLOAD_INTERVAL_SET,
                                      &interval, sizeof(UINT32), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_GET_BUSLOAD_INTERVAL:
        {
                UINT32 interval;
                UINT32 outbuflen = sizeof(UINT32);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BUSLOAD_INTERVAL_GET,
                                      NULL, 0, &interval, &outbuflen);
                if ( put_user(interval, (UINT32*)ioArg->buffer) ) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_GET_BUS_STATISTIC:
        {
                CAN_STAT stat;
                UINT32   outbuflen = sizeof(CAN_STAT);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BUS_STATISTIC_GET,
                                      NULL, 0, &stat, &outbuflen);
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_GET_BUS_STATISTIC result=%d\n",
                         OSIF_FUNCTION, result));
                if (result != OSIF_SUCCESS) {
                        break;
                }

                if (copy_to_user((VOID*)ioArg->buffer, &stat, (INTPTR)&stat.msg_count_std - (INTPTR)&stat.timestamp )) { /*  copy public part only */
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_RESET_BUS_STATISTIC:
        {
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BUS_STATISTIC_RESET,
                                      NULL, 0, NULL, NULL);
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_RESET_BUS_STATISTIC result=%d\n",
                         OSIF_FUNCTION, result));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                break;
        }
        case IOCTL_ESDCAN_GET_ERROR_COUNTER:
        {
                UINT32  errCnt;
                UINT32  outbuflen = sizeof(errCnt);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_ERROR_COUNTER_GET,
                                      NULL, 0, &errCnt, &outbuflen);
                CAN_DBG((ESDCAN_ZONE, "%s: ESDCAN_CTL_ERROR_COUNTER_GET result=%d, errCnt=0x%08X\n",
                         OSIF_FUNCTION, result));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (put_user(errCnt, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_GET_BITRATE_DETAILS:
        {
                CAN_BITRATE  btrInfo;
                UINT32       outbuflen = sizeof(btrInfo);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_BITRATE_DETAILS_GET,
                                      NULL, 0, &btrInfo, &outbuflen);
                CAN_DBG((ESDCAN_ZONE, "%s: ESDCAN_CTL_BITRATE_DETAILS_GET result=%d, errCnt=0x%08X\n",
                         OSIF_FUNCTION, result));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (copy_to_user((VOID*)ioArg->buffer, &btrInfo, sizeof(btrInfo))) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_SER_REG_READ:
                cmd = ESDCAN_CTL_SER_REG_READ;
                goto IOCTL_ESDCAN_SER_REG_COMMON;
        case IOCTL_ESDCAN_SER_REG_WRITE:
                cmd = ESDCAN_CTL_SER_REG_WRITE;
IOCTL_ESDCAN_SER_REG_COMMON:
        {
                SERIAL_ARG   uartTuple;
                UINT32       buflen = sizeof(uartTuple);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (copy_from_user(&uartTuple, (VOID*)ioArg->buffer, buflen)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, cmd,
                                      &uartTuple, buflen, &uartTuple, &buflen);
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_SER_REG_COMMON cmd=%d result=%d\n",
                         OSIF_FUNCTION, cmd, result));
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (copy_to_user((VOID*)ioArg->buffer, &uartTuple, buflen)) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_RESET_CAN_ERROR_CNT:
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                cmd = ESDCAN_CTL_RESET_CAN_ERROR_CNT;
                result = my_nuc_ioctl(ocb, cmd,
                                      NULL, 0, NULL, NULL);
                break;

        case IOCTL_ESDCAN_EEI_CREATE:
        {
                UINT32  eeihandle;
                INT32   outbuflen = sizeof(UINT32);

                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL_ESDCAN_EEI_CREATE...\n", OSIF_FUNCTION));
                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                result =  my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_CREATE, NULL, 0, &eeihandle, &outbuflen);
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (copy_to_user((VOID*)ioArg->buffer, &eeihandle, sizeof(UINT32))) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_EEI_DESTROY:
        {
                UINT32 eeihandle;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(eeihandle, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_DESTROY, &eeihandle, sizeof(UINT32), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_EEI_STATUS:
        {
                EEI_STATUS eeistatus;
                INT32 outbuflen = sizeof(EEI_STATUS);

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (copy_from_user(&eeistatus, (VOID*)ioArg->buffer, sizeof(EEI_STATUS))) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_STATUS, &eeistatus, sizeof(EEI_STATUS), &eeistatus, &outbuflen);
                if (result != OSIF_SUCCESS) {
                        break;
                }
                if (copy_to_user((VOID*)ioArg->buffer, &eeistatus, sizeof(EEI_STATUS))) {
                        result = OSIF_EFAULT;
                }
                break;
        }
        case IOCTL_ESDCAN_EEI_CONFIGURE:
        {
                EEI_UNIT eeiunit;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (copy_from_user(&eeiunit, (VOID*)ioArg->buffer, sizeof(EEI_UNIT))) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_CONFIGURE, &eeiunit, sizeof(EEI_UNIT), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_EEI_START:
        {
                UINT32 eeihandle;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(eeihandle, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_START, &eeihandle, sizeof(UINT32), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_EEI_STOP:
        {
                UINT32 eeihandle;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(eeihandle, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_STOP, &eeihandle, sizeof(UINT32), NULL, NULL);
                break;
        }
        case IOCTL_ESDCAN_EEI_TRIGGER_NOW:
        {
                UINT32 eeihandle;

                /* block attempts to access cards with wrong firmware */
                CAN_ACTION_CHECK(ocb, result, break);
                if (get_user(eeihandle, (UINT32*)ioArg->buffer)) {
                        result = OSIF_EFAULT;
                        break;
                }
                result = my_nuc_ioctl(ocb, ESDCAN_CTL_EEI_TRIGGER_NOW, &eeihandle, sizeof(UINT32), NULL, NULL);
                break;
        }
        default:
                CAN_DBG((ESDCAN_ZONE, "%s: IOCTL(%08x) not supported\n", OSIF_FUNCTION,cmd));
                result = OSIF_NOT_SUPPORTED;
        } /* switch */
        nuc_check_links(ocb);
        OSIF_MUTEX_UNLOCK(&node->lock);
        HOTPLUG_BOLT_USER_EXIT(((CAN_CARD*)node->crd)->p_mod);
        return result;
}

#if PLATFORM_64BIT
# if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,10)
static int esdcan_ioctl_32(unsigned int fd, unsigned int cmd, unsigned long arg, struct file *file)
{
        struct inode *inode;
        INT32         result = OSIF_SUCCESS;
        IOCTL_ARG     ioArg;
        IOCTL_ARG_32  ioArg32;
        CAN_OCB      *ocb;

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        /* Patch ioctl-cmd to ioctl-command in 64-Bit driver
         * This function is called for IOCTLs with parameters, only. */
        cmd = ((cmd & (~0x00040000)) | 0x00080000);
        inode = file->f_dentry->d_inode;
        HOTPLUG_GLOBAL_LOCK;
        ocb = (CAN_OCB*)file->private_data;
        CAN_DBG((ESDCAN_ZONE, "%s: params: ioctl=%x, ocb=%p, arg=%p\n", OSIF_FUNCTION, cmd, ocb, arg));
        if (NULL != (IOCTL_ARG_32*)arg) {
                if (copy_from_user(&ioArg32, (IOCTL_ARG_32*)arg, sizeof(ioArg32))) {
                        HOTPLUG_GLOBAL_UNLOCK;
                        CAN_DBG((ESDCAN_ZONE, "%s: leave (cfu failed)\n", OSIF_FUNCTION));
                        RETURN_TO_USER(OSIF_EFAULT);
                }
                ioArg.buffer = (void*)((unsigned long)ioArg32.buffer);
                ioArg.size = ioArg32.size;
        }
        result = esdcan_ioctl_internal(inode, file, cmd, &ioArg, ocb);
        if ( ( IOCTL_ESDCAN_CREATE != cmd ) &&
             ( NULL != ocb ) ) {
                if (result == OSIF_SUCCESS) {
                        ioArg32.buffer = (uint32_t)((unsigned long)ioArg.buffer);
                        ioArg32.size = ioArg.size;
                        if (NULL != (IOCTL_ARG_32*)arg) {
                                /* result of esdcan_ioctl_internal is aliased! */
                                if (copy_to_user((IOCTL_ARG_32*)arg, &ioArg32,  sizeof(ioArg32))) {
                                        result = OSIF_EFAULT;
                                }
                        }
                }
        }
        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: leave (result=%d)\n", OSIF_FUNCTION, result));
        RETURN_TO_USER(result);
}
# else
static long esdcan_compat_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
        struct inode *inode;
        INT32         result = OSIF_SUCCESS;
        IOCTL_ARG     ioArg;
        IOCTL_ARG_32  ioArg32;
        CAN_OCB      *ocb;

        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: enter (cmd=0x%X, arg=%p)\n",
                 OSIF_FUNCTION, cmd, arg));
        inode = file->f_dentry->d_inode;
        HOTPLUG_GLOBAL_LOCK;
        /* Patch ioctl-cmd to ioctl-command in 64-Bit driver */
        if (IOCTL_CAN_SET_QUEUESIZE != cmd) { /* Don't change IOCTL from old library (needed for differentiation) */
                if ( (cmd & IOCSIZE_MASK) == 0x00040000 ) { /* Don't change IOCTL's without parameter */
                        cmd = ((cmd & (~0x00040000)) | 0x00080000);
                }
        }
        ocb = (CAN_OCB*)file->private_data;
        CAN_DBG((ESDCAN_ZONE, "%s: params (ioctl=%x, ocb=%p, arg=%p)\n", OSIF_FUNCTION, cmd, ocb, arg));
        if (NULL != (IOCTL_ARG_32*)arg) {
                if (copy_from_user(&ioArg32, (IOCTL_ARG_32*)arg, sizeof(ioArg32))) {
                        HOTPLUG_GLOBAL_UNLOCK;
                        CAN_DBG((ESDCAN_ZONE, "%s: leave (cfu failed)\n", OSIF_FUNCTION));
                        RETURN_TO_USER(OSIF_EFAULT);
                }
                ioArg.buffer = (void*)((unsigned long)ioArg32.buffer);
                ioArg.size = ioArg32.size;
        }
        result = esdcan_ioctl_internal(inode, file, cmd, &ioArg, ocb);
        if ( ( IOCTL_ESDCAN_CREATE != cmd ) &&
             ( NULL != ocb ) ) {
                if (result == OSIF_SUCCESS) {
                        ioArg32.buffer = (uint32_t)((unsigned long)ioArg.buffer);
                        ioArg32.size = ioArg.size;
                        if (NULL != (IOCTL_ARG_32*)arg) {
                                /* result of esdcan_ioctl_internal is aliased! */
                                if (copy_to_user((IOCTL_ARG_32*)arg, &ioArg32,  sizeof(ioArg32))) {
                                        result = OSIF_EFAULT;
                                }
                        }
                }
        }
        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: leave (result=%d)\n", OSIF_FUNCTION, result));
        RETURN_TO_USER(result);
}
# endif
#endif

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,10)
static long esdcan_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
        struct inode *inode;
        long          result = OSIF_SUCCESS;

        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: enter (cmd=0x%X, arg=%p)\n",
                 OSIF_FUNCTION, cmd, arg));
        inode = file->f_dentry->d_inode;
        result = esdcan_ioctl(inode, file, cmd, arg);
        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: leave (result=%d)\n", OSIF_FUNCTION, result));
        return result; /* returns to user, but result is already inverted in esdcan_ioctl() */
}
#endif


/*! \fn static int esdcan_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
 *  \brief ioctl entry for driver
 *  \param inode pointer to inode structure
 *  \param file pointer to file structure (contains private_data pointer)
 *  \param cmd ioctl command (see esdcanio.h for IOCTL_x defines)
 *  \param ioctl argument (this is usually a user space pointer)
 *  \return Linux or CAN error code
 */
ESDCAN_IOCTL_PROTO
{
        IOCTL_ARG        ioArg;
        INT32            result = OSIF_SUCCESS;
        CAN_OCB         *ocb;
#ifdef OSIF_OS_IRIX
        vertex_hdl_t     vhdl_dev, vhdl;
        void            *drvarg;
        CAN_HANDLE_INFO *file; /* call this variable "file" because of linux */
#endif /* OSIF_OS_IRIX */

        CAN_DBG((ESDCAN_ZONE_FU, "%s: enter\n", OSIF_FUNCTION));
        HOTPLUG_GLOBAL_LOCK;
#ifdef OSIF_OS_LINUX
        ocb = (CAN_OCB*)file->private_data;
#endif /* OSIF_OS_LINUX */
#ifdef OSIF_OS_IRIX
        /*  Get the vertex handle and pointer to dev-structure */
        vhdl_dev = dev_to_vhdl(devp);
        if (vhdl_dev == NULL) {
                CAN_PRINT((":CAN: dev_to_vhdl returns NULL"));
                HOTPLUG_GLOBAL_UNLOCK;
                RETURN_TO_USER(OSIF_EIO);
        }
        CAN_DBG((ESDCAN_ZONE, "%s:CAN: vhdl_dev=%x", OSIF_FUNCTION, vhdl_dev));
        file = (CAN_HANDLE_INFO*)device_info_get(vhdl_dev);
        ocb = file->private_data;
#endif /* OSIF_OS_IRIX */
        CAN_DBG((ESDCAN_ZONE, "%s: enter (ioctl=%x, ocb=%p, arg=%p)\n", OSIF_FUNCTION, cmd, ocb, arg));
        if (NULL != (IOCTL_ARG*)arg) {
                if (copy_from_user(&ioArg, (IOCTL_ARG*)arg, sizeof(ioArg))) {
                        HOTPLUG_GLOBAL_UNLOCK;
                        CAN_DBG((ESDCAN_ZONE, "%s: leave (cfu failed)\n", OSIF_FUNCTION));
                        RETURN_TO_USER(OSIF_EFAULT);
                }
        }
        result = esdcan_ioctl_internal(inode, file, cmd, &ioArg, ocb);
        if ( ( IOCTL_ESDCAN_CREATE != cmd ) &&
             ( NULL != ocb ) ) {
                if (result == OSIF_SUCCESS) {
#ifdef OSIF_OS_IRIX
                        *rvalp = 0;
#endif
                        if (NULL != (IOCTL_ARG*)arg) {
                                /* result of esdcan_ioctl_internal is aliased! */
                                if (copy_to_user((IOCTL_ARG*)arg, &ioArg, sizeof(ioArg))) {
                                        result = OSIF_EFAULT;
                                }
                        }
                }
        }
        CAN_DBG((ESDCAN_ZONE_FU,
                 "%s: leave (result=%d)\n", OSIF_FUNCTION, result));
        RETURN_TO_USER(result);
}


static unsigned int esdcan_poll(struct file *pFile, struct poll_table_struct *pPollTable)
{
        CAN_OCB        *ocb;
        CAN_NODE       *node;
        unsigned int    mask = 0;
        UINT32          num;

        ocb = (CAN_OCB*)pFile->private_data;
        node = ocb->node;
        OSIF_MUTEX_LOCK(&node->lock);
        poll_wait(pFile, &node->wqRxNotify, pPollTable);
        nuc_rx_messages(ocb, &num);
        if (num) {
                mask |= POLLIN | POLLRDNORM;
        }
        OSIF_MUTEX_UNLOCK(&node->lock);
        return mask;
}
