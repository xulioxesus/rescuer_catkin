/* -*- linux-c -*-
 * FILE NAME boardrc.h
 *           copyright 2002 by esd electronic system design GmbH
 *
 * BRIEF MODULE DESCRIPTION
 *           board resources for PCI331 boards
 *
 *
 * Author:   Andreas Block
 *           andreas.block@esd-electronics.com
 *
 * history:
 *
 *  $Log: boardrc.h,v $
 *  Revision 1.22  2008/07/22 13:21:11  michael
 *  MAX_CARDS now 16
 *
 *  Revision 1.21  2008/02/28 15:41:13  michael
 *  Firmware flashing is now made via blocking nuc_ioctl framework.
 *
 *  Revision 1.20  2007/01/04 09:26:04  andreas
 *  Added PCI-IDs for PMC331-3,3V
 *
 *  Revision 1.19  2006/06/29 12:45:52  andreas
 *  Added controller type and clockrate (needed for numerical setting of baudrate)
 *
 *  Revision 1.18  2005/09/28 06:25:30  andreas
 *  Made tx_count in crd-struct volatile, since it's changed from backend and
 *  user context.
 *  Removed tabs.
 *
 *  Revision 1.17  2005/07/28 09:32:10  andreas
 *  PCI defines chagned slightly, when adding cardFlavours array and
 *  implemented hotplugable PCI driver.
 *  BOARD_STRING removed, ESDCAN_DRIVER_NAME added.
 *  Board type is defined here, now (BOARD_PCI).
 *
 *  Revision 1.16  2005/06/22 06:57:00  oliver
 *  New define IDX_BOARD.
 *  Added member pciDev to CIF_CARD to prevent HW dependency in esdcan.
 *
 *  Revision 1.15  2004/10/11 08:40:17  andreas
 *  Added cvs-log tag
 *
 *  11.05.04 - added _ID_ suffix to PCI-IDs (for kernel 2.6)
 *             changed OSIF_EXTERN to extern (s. OSIF_CALLTYPE)     ab
 *  13.11.03 - Inherited stuff from pci331.h
 *             Fix for boards with less than NODES_PER_CARD nets    ab
 *  27.10.03 - Added statistics
 *             Added defines for statistics                         ab
 *  13.08.02 - version for pci331                                   ab
 *  15.07.02 - first version                                        mf
 *
 */
/************************************************************************
 *
 *  Copyright (c) 1996 - 2002 by electronic system design gmbh
 *
 *  This software is copyrighted by and is the sole property of
 *  esd gmbh.  All rights, title, ownership, or other interests
 *  in the software remain the property of esd gmbh. This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of esd gmbh.
 *
 *  esd gmbh, reserves the right to modify this software without notice.
 *
 *  electronic system design gmbh          Tel. +49-511-37298-0
 *  Vahrenwalder Str 207                   Fax. +49-511-37298-68
 *  30165 Hannover                         http://www.esd-electronics.com
 *  Germany                                sales@esd-electronics.com
 *
 *************************************************************************/
/*! \file boardrc.h
    \brief Board specific board API

    This file contains board specific constants for a particular board implementation.
    This file may be empty for some boards.
*/

#ifndef __BOARDRC_H__
#define __BOARDRC_H__

/* Following defines, give the possibility to compile
   a somewhat more communicative driver */
/* #define ISR_TIMING     */
/* #define ISR_STATISTIC  */
/* #define DOUBLE_TX_DONE */

/*
 *  ALL I20-cards need this define!
 *  With this define, the nucleus doesn't lock against interrupt-level.
 */
#define NUC_IRQ_NOSYNC

#include <cm.h>
#include <osif.h>

#define ESDCAN_DRIVER_NAME "CAN_PCI331"

#define BOARD_PCI

#define SJA1000_CLOCK          16000000

#define PCI_VENDOR_PLX         0x10B5
#define PCI_DEVICE_PLX_9030    0x9030
#define PCI_DEVICE_PLX_9050    0x9050
#define PCI_SUB_VENDOR_ESD     0x12FE
#define PCI_SUB_SYSTEM_PCI331  0x0001
#define PCI_SUB_SYSTEM_PMC331  0x000C /* 3,3V PMC331, only */

#define ESDCAN_IDS_PCI331 { \
        PCI_VENDOR_PLX, PCI_DEVICE_PLX_9050, \
        PCI_SUB_VENDOR_ESD, PCI_SUB_SYSTEM_PCI331, \
        0, 0, 0 }

#define ESDCAN_IDS_PMC331_3_3V { \
        PCI_VENDOR_PLX, PCI_DEVICE_PLX_9030, \
        PCI_SUB_VENDOR_ESD, PCI_SUB_SYSTEM_PMC331, \
        0, 0, 0 }

#define ESDCAN_IDS_PCI  ESDCAN_IDS_PCI331, ESDCAN_IDS_PMC331_3_3V

#define USED_ADDRESS_SPACES    0x0D

extern CARD_IDENT cardFlavours[];

#define CARD_FEATURES          (FEATURE_TS)

#define NODES_PER_CARD         2
#define MAX_CARDS              16

#define IDX_BOARD              "C331"

#define HARDWARE_VERSION_1ST   0x0001
#define HARDWARE_VERSION_LST   0x1FFF
#define FIRMWARE_VERSION_1ST   0x0900
#define FIRMWARE_VERSION_LST   0x0CFF

#define FIFO_SIZE           1024
#define FIFO_PARTITION_TX   128 /* AB TODO: check impact of a reduction to 16 on send performance */
#define FIFO_PARTITION_CMD  512
#define HND_CNT_MAX         255

#define SHARED_RAM_16BIT
#define CAN_LINK_BASE       0x00000846L

/*
 * This is the board-specific extension of the card-structure.
 * Actual declaration of CAN_CARD in esdcan.h
 */
#define CIF_CARD \
        VOID           *can_cfg;          \
        VOID           *can_sram;         \
        VOID           *flash_ocb;        \
        VOID           *flash_wait;       \
        VOID           *flash_buf_out;    \
        UINT16          passwd_try;       \
        volatile INT32  tx_count;         \
        INT32           irq_functional;   \
        OSIF_PCIDEV     pciDev;           \
        OSIF_MUTEX      lock;             \
        OSIF_DPC        backend;          \
        INT32           baud_max;            \
        UINT32          net_needs_trigger;   \
        INT32           nodes_working;       \
        INT32           nodes_provided;      \
        UINT32          lf_cnt;              \
        UINT32          lm_cnt;              \
        UINT32          e_cnt;               \
        UINT32          w_cnt;               \
        UINT32          ob_cnt;              \
        UINT32          o_cnt;               \
        UINT32          ncm_cnt;

#pragma pack(1)
typedef struct _CAN_CFG CAN_CFG;
struct _CAN_CFG {
        UINT8  unused[0x4C];
        UINT32 ics;           /* interrupt-controll / status */
        UINT32 misc_ctrl;     /* user-io/pci-target-response/eeprom/init-control */
};
#pragma pack()

/*
 *  Wrap function-calls to the names used in i20.c and board.c.
 *  This can be used to prevent useless function calls.
 */
#define board_interrupt_source_reset(crd)                pci331_interrupt_source_reset(crd)
#define board_interrupt_disable(crd)                     pci331_interrupt_disable(crd)
#define board_interrupt_enable(crd)                      pci331_interrupt_enable(crd)
#define board_reset(crd)                                 pci331_reset(crd)
#define board_trigger_write(crd)                         /* not needed */

/*
 *  Following stuff is for debugging-purposes, only and is NOT
 *  useable with a released archive! Will be moved to a less exposed
 *  place, later.
 */
#ifdef ISR_TIMING
UINT32 isrTimeCnt;
UINT64 isrTime[100];
UINT64 maxIsrTime;
/* ISA331-stuff:
UINT64 retrCnt;
UINT64 retrFifoHalfFull;
UINT32 retrTimeCnt;
UINT64 retrTime[100];
UINT64 maxRetrTime;
UINT32 writeTimeCnt;
UINT64 writeTime[100];
UINT64 maxWriteTime;
*/
#endif
#ifdef ISR_STATISTIC
UINT32 sent_cnt;
UINT32 err_skip_cnt;
UINT32 err_recv_cnt;
UINT32 done_skip_cnt;
UINT32 done_recv_cnt;
#endif

#endif /* __BOARDRC_H__ */
