Extraction of esd CAN driver          Version > 3.0.0               21.05.2008
------------------------------------------------------------------------------

This document is just an excerpt from the README contained within the
driver archive. This is meant to show you the extraction of the driver
components from their archive.
Please have a look into the README contained within the archive for further
instructions on installation and usage.

  Unpack the archive:

        A1) Unzip the archive:

        "unzip esdcan-crd-os-arch-ver-ext.zip"
        with    crd  = card-id (e.g.: crd=pci200 or crd=cpci405 ...)
                os   = host-operation-system (e.g.: os=linux-2.4.x)
                arch = host-architecture (e.g.: arch=x86 or arch=x86_64)
                ver  = driver version (e.g.: ver=3.7.2)
                ext  = extension (applicable to certain cards only, e.g.: ext=gcc2)

        You'll be prompted for a password.
        Password: esdCAN2007

        Resulting file:
        "esdcan-crd-os-arch-ver-ext.tar"

        A2) Untar the driver directory:

        "tar -xvf esdcan-crd-os-arch-ver-ext.tar"
        with    crd  = card-id (e.g.: crd=pci200 or crd=cpci405 ...)
                os   = host-operation-system (e.g.: os=linux-2.4.x)
                arch = host-architecture (e.g.: arch=x86 or arch=x86_64)
                ver  = driver version (e.g.: ver=3.7.2)
                ext  = extension (applicable to certain cards only, e.g.: ext=gcc2)

        You'll end up with a directory named as the archive.

  Alternatively the above can be accomplished in a single step:

        B) Unzip and untar in a single step:

        "unzip -p esdcan-crd-os-arch-ver-ext.zip | tar -xv"

        You'll be prompted for a password.
        Password: esdCAN2007
