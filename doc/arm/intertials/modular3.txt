===============================================================================
Primera computación de la matriz de inercia desde meshlab:
===============================================================================

LOG: 2 Mesh Bounding Box Size 0.110949 0.141000 0.143001
LOG: 2 Mesh Bounding Box Diag 0.229434 
LOG: 2 Mesh Volume  is 0.001382
LOG: 2 Mesh Surface is 0.115053
LOG: 2 Thin shell barycenter   0.000000   0.000733  -0.021030
LOG: 2 Center of Mass  is 0.000000 0.013305 -0.006775
LOG: 2 Inertia Tensor is :
LOG: 2     |  0.000003   0.000000   0.000000 |
LOG: 2     |  0.000000   0.000003  -0.000000 |
LOG: 2     |  0.000000  -0.000000   0.000003 |
LOG: 2 Principal axes are :
LOG: 2     |  1.000000  -0.000010   0.000010 |
LOG: 2     |  0.000013   0.923658  -0.383219 |
LOG: 2     | -0.000006   0.383219   0.923658 |
LOG: 2 axis momenta are :
LOG: 2     |  0.000003   0.000002   0.000003 |

===============================================================================
Factor de escala: 1/Mesh Volume
===============================================================================

	s^3 = 1/0.001382 = 723.59
	s = 8.9777
	s Meshlab = 8.9777

===============================================================================
Segunda computación de la matriz de inercia desde meshlab:
===============================================================================

LOG: 2 Mesh Bounding Box Size 0.996069 1.265856 1.283820
LOG: 2 Mesh Bounding Box Diag 2.059791 
LOG: 2 Mesh Volume  is 1.000350
LOG: 2 Mesh Surface is 9.273326
LOG: 2 Thin shell barycenter   0.000000  -0.077184  -0.061167
LOG: 2 Center of Mass  is 0.000001 0.035681 0.066816
LOG: 2 Inertia Tensor is :
LOG: 2     |  0.204017   0.000001   0.000000 |
LOG: 2     |  0.000001   0.151848  -0.014833 |
LOG: 2     |  0.000000  -0.014833   0.181446 |
LOG: 2 Principal axes are :
LOG: 2     |  1.000000  -0.000010   0.000010 |
LOG: 2     |  0.000013   0.923657  -0.383219 |
LOG: 2     | -0.000006   0.383219   0.923657 |
LOG: 2 axis momenta are :
LOG: 2     |  0.204017   0.145694   0.187601 |

===============================================================================
Multiplicando Inertia Tensor por 1/s^2 en Octave
===============================================================================

octave:12> A = [0.204017 ,  0.000001  , 0.000000; 0.000001 ,  0.151848,  -0.014833; 0.000000 , -0.014833 ,  0.181446]
A =

   0.20402   0.00000   0.00000
   0.00000   0.15185  -0.01483
   0.00000  -0.01483   0.18145

octave:13> A*1/(8.9777^2)
ans =

   0.0025313   0.0000000   0.0000000
   0.0000000   0.0018840  -0.0001840
   0.0000000  -0.0001840   0.0022512

===============================================================================
URDF tags
===============================================================================
<xacro:property name="inertia_3_link">
                <inertia        ixx="0.0025313" ixy="0" ixz="0"
                                         iyy="0.0018840"  iyz="-0.0001840"
                                                 izz="0.0022512"/>
        </xacro:property>
