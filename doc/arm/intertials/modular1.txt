LOG: 0 Opened mesh modular1.stl in 283 msec
LOG: 0 All files opened in 1880 msec
LOG: 0 Started Mode Measuring Tool
LOG: 2 Mesh Bounding Box Size 0.130000 0.148000 0.307999
LOG: 2 Mesh Bounding Box Diag 0.365605 
LOG: 2 Mesh Volume  is 0.002147
LOG: 2 Mesh Surface is 0.176083
LOG: 2 Thin shell barycenter   0.000001  -0.020811   0.100715
LOG: 2 Center of Mass  is 0.000003 -0.009599 0.141766
LOG: 2 Inertia Tensor is :
LOG: 2     |  0.000012  -0.000000  -0.000000 |
LOG: 2     | -0.000000   0.000012  -0.000002 |
LOG: 2     | -0.000000  -0.000002   0.000005 |
LOG: 2 Principal axes are :
LOG: 2     |  0.999994  -0.003515   0.000028 |
LOG: 2     |  0.003336   0.951636   0.307208 |
LOG: 2     | -0.001107  -0.307206   0.951642 |
LOG: 2 axis momenta are :
LOG: 2     |  0.000012   0.000013   0.000005 |
LOG: 0 Applied filter Compute Geometric Measures in 40 msec

SCALE FACTOR: 1/Mesh Volume
	s^3 = 1/0.002147 = 465.76618537494176
	s = 7.751563668531336
	s Meshlab = 7.751563

Getting info again:

LOG: 2 Mesh Bounding Box Size 1.007703 1.147231 2.387471
LOG: 2 Mesh Bounding Box Diag 2.834012 
LOG: 2 Mesh Volume  is 0.999817
LOG: 2 Mesh Surface is 10.579684
LOG: 2 Thin shell barycenter   0.000002  -0.080299   0.179806
LOG: 2 Center of Mass  is 0.000017 0.006611 0.498014
LOG: 2 Inertia Tensor is :
LOG: 2     |  0.346973  -0.000016  -0.000001 |
LOG: 2     | -0.000016   0.330281  -0.064760 |
LOG: 2     | -0.000001  -0.064760   0.150580 |
LOG: 2 Principal axes are :
LOG: 2     |  0.999994  -0.003515   0.000028 |
LOG: 2     |  0.003336   0.951637   0.307208 |
LOG: 2     | -0.001107  -0.307206   0.951642 |
LOG: 2 axis momenta are :
LOG: 2     |  0.346973   0.351187   0.129674 |
LOG: 0 Applied filter Compute Geometric Measures in 41 msec

Multiplicando Inertia Tensor por 1/s^2

octave:1> A = [0.346973, -0.000016,  -0.000001; -0.000016   ,0.330281  ,-0.064760; -0.000001,  -0.064760,   0.150580]
A =

   3.4697e-01  -1.6000e-05  -1.0000e-06
  -1.6000e-05   3.3028e-01  -6.4760e-02
  -1.0000e-06  -6.4760e-02   1.5058e-01

octave:2> A*1/(7.751563^2)
ans =

   5.7745e-03  -2.6628e-07  -1.6643e-08
  -2.6628e-07   5.4967e-03  -1.0778e-03
  -1.6643e-08  -1.0778e-03   2.5060e-03


<xacro:property name="inertia_1_link">
                <inertia        ixx="5.7745e-03" ixy="-2.6628e-07" ixz="-1.6643e-08"
                                                 iyy="5.4967e-03"  iyz="-1.0778e-03"
                                                                   izz="2.5060e-03"/>
